package cz.muni.irtis.datacollector.lite.testutils.jsonmodels;

import com.google.gson.annotations.SerializedName;

public class BatteryStateJson extends DateTimeJson {

    @SerializedName("state_percent")
    private int statePercent;

    public BatteryStateJson() {
    }

    public BatteryStateJson(String datetime, int statePercent) {
        this.statePercent = statePercent;
        this.datetime = datetime;
    }

    public int getStatePercent() {
        return statePercent;
    }

    public void setStatePercent(int statePercent) {
        this.statePercent = statePercent;
    }

}
