package cz.muni.irtis.datacollector.lite.questionnaires;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import cz.muni.irtis.datacollector.lite.Config;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Identity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.metrics.database.MetricsDatabase;

@RunWith(AndroidJUnit4.class)
public class DownloadQuestionnairesTest {
    protected Context context;
    protected MetricsDatabase database;

    private QuestionnairesModel model;

    @Before
    public void create() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        model = new QuestionnairesModel(context);
        Identity.getInstance().signIn(Config.IDENTITY_ID, Config.IDENTITY_PASS);
    }

    @After
    public void destroy() {
    }


    @Test
    public void downloadItems() {
        List<Questionnaire> items = model.download();
        Assert.assertNotNull(items);
//        if(items!=null && !items.isEmpty()) {
//            Iterator<Questionnaire> iItems = items.iterator();
//            while (iItems.hasNext()) {
//                Questionnaire item = iItems.next();
//                // @TODO additional check
//            }
//        }
    }


    @Test
    public void downloadItem() {
        Questionnaire item = model.download(3095L);
        Assert.assertNotNull(item);
    }
}
