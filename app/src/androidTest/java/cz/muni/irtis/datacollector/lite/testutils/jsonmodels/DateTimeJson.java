package cz.muni.irtis.datacollector.lite.testutils.jsonmodels;

public abstract class DateTimeJson {
    protected String datetime;

    public DateTimeJson() {
    }

    public DateTimeJson(String datetime) {
        this.datetime = datetime;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
