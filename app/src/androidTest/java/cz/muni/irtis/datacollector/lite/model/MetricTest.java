package cz.muni.irtis.datacollector.lite.model;

import android.provider.CallLog;

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.ActivityEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.BatteryEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.HeadphonesEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.LocationEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.NotificationEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.PlaybackEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.StepEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.phone.PhoneCallEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.phone.PhoneSmsEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenTapEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenshotMetadataEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi.WifiEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.WifiAvailableRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.WifiRepository;
import cz.muni.irtis.datacollector.lite.metrics.model.BatteryState;
import cz.muni.irtis.datacollector.lite.metrics.model.Headphones;
import cz.muni.irtis.datacollector.lite.metrics.model.Location;
import cz.muni.irtis.datacollector.lite.metrics.model.Notifications;
import cz.muni.irtis.datacollector.lite.metrics.model.PhysicalActivity;
import cz.muni.irtis.datacollector.lite.metrics.model.Playback;
import cz.muni.irtis.datacollector.lite.metrics.model.Steps;
import cz.muni.irtis.datacollector.lite.metrics.model.applications.BackgroundApplication;
import cz.muni.irtis.datacollector.lite.metrics.model.applications.ForegroundApplication;
import cz.muni.irtis.datacollector.lite.metrics.model.phone.CallHistory;
import cz.muni.irtis.datacollector.lite.metrics.model.phone.SmsConversation;
import cz.muni.irtis.datacollector.lite.metrics.model.screen.Screen;
import cz.muni.irtis.datacollector.lite.metrics.model.screen.ScreenTap;
import cz.muni.irtis.datacollector.lite.metrics.model.screen.Screenshot;
import cz.muni.irtis.datacollector.lite.metrics.model.util.callhistory.CallRecord;
import cz.muni.irtis.datacollector.lite.metrics.model.util.physicalactivity.RecognizedActivity;
import cz.muni.irtis.datacollector.lite.metrics.model.util.sms.SmsRecord;
import cz.muni.irtis.datacollector.lite.metrics.model.wifi.AvailableWifi;
import cz.muni.irtis.datacollector.lite.metrics.model.wifi.ConnectedWifi;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;

@RunWith(AndroidJUnit4ClassRunner.class)
public class MetricTest {

    @Before
    public void setUp() {
        Playback playback = new Playback(getApplicationContext(), 5);
        playback.getDatabase().clearAllTables();
    }

    @Test
    public void playback_test() {
        Playback playback = new Playback(getApplicationContext(), 5);

        Long timestamp = playback.save(true, 8);

        PlaybackEntity playbackEntity = playback.getDatabase().getPlaybackRepository().getByTime(timestamp);

        Assert.assertTrue(playbackEntity.getActive());
    }

    @Test
    public void steps_test() {
        Steps steps = new Steps(getApplicationContext(), 5);

        Integer expected = 25;
        Long timestamp = steps.save(expected);

        StepEntity stepEntity = steps.getDatabase().getStepsRepository().getByTime(timestamp);

        Assert.assertEquals(expected, stepEntity.getSteps());
    }

    @Test
    public void physicalActivity_test() {
        PhysicalActivity physicalActivity = new PhysicalActivity(getApplicationContext(), 5);

        Integer expectedType = 25;
        Integer expectedConfidence = 26;
        Long timestamp = physicalActivity.save(expectedType, expectedConfidence);

        ActivityEntity activityEntity = physicalActivity.getDatabase().getPhysicalActivityRepository().getByTime(timestamp);

        Assert.assertEquals(expectedConfidence, activityEntity.getConfidence());
        Assert.assertEquals(RecognizedActivity.toString(expectedType), activityEntity.getActivity());
    }

    @Test
    public void notifications_test() {
        Notifications notifications = new Notifications(getApplicationContext(), 5);

        String expectedIdentifier = UUID.randomUUID().toString();
        Long expectedPosted = Time.getTime();
        Long expectedRemoved = Time.getTime();
        String expectedTitle = "Random notification";
        String expectedText = "Random notification text";


        Long timestamp = notifications.save(expectedIdentifier, expectedPosted, expectedRemoved, expectedTitle, expectedText);

        NotificationEntity expected = new NotificationEntity(timestamp, expectedIdentifier, expectedPosted, expectedRemoved, expectedTitle, expectedText);

        NotificationEntity actual = notifications.getDatabase().getNotificationsRepository().getByTime(timestamp);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void location_test() {
        Location location = new Location(getApplicationContext(), 5);

        Double expectedLatitude = Double.MAX_VALUE;
        Double expectedLongitude = Double.MAX_VALUE;
        Long timestamp = location.save(expectedLatitude, expectedLongitude);

        LocationEntity locationEntity = location.getDatabase().getLocationRepository().getByTime(timestamp);

        Assert.assertEquals(expectedLatitude, locationEntity.getLatitude());
        Assert.assertEquals(expectedLongitude, locationEntity.getLongitude());
    }

    @Test
    public void headphones_test() {
        Headphones headphones = new Headphones(getApplicationContext(), 5);


        Long timestamp = headphones.save(true);

        HeadphonesEntity headphonesEntity = headphones.getDatabase().getHeadphonesRepository().getByTime(timestamp);

        Assert.assertTrue(headphonesEntity.getConnected());
    }

    @Test
    public void batteryState_test() {
        BatteryState batteryState = new BatteryState(getApplicationContext(), 5);

        int expected = Integer.MAX_VALUE;
        Long timestamp = batteryState.save(expected);

        BatteryEntity batteryEntity = batteryState.getDatabase().getBatteryStateRepository().getByTime(timestamp);

        Assert.assertEquals(expected, batteryEntity.getStatePercent());
    }

    @Test
    public void availableWifi_test() {
        AvailableWifi availableWifi = new AvailableWifi(getApplicationContext(), 5);
        Map<String, Long> pkMap = fillWifiData(availableWifi.getDatabase().getWifiRepository());

        WifiAvailableRepository wifiAvailableRepository = availableWifi.getDatabase().getAvailableWifiRepository();

        availableWifi.save(Arrays.asList("Public", "Private", "Square"), null);

        WifiEntity actualPublic = wifiAvailableRepository.getByTime(pkMap.get("Public"));
        Assert.assertEquals("Public", actualPublic.getSsid());

        WifiEntity actualSquare = wifiAvailableRepository.getByTime(pkMap.get("Square"));
        Assert.assertEquals("Square", actualSquare.getSsid());
    }

    private Map<String, Long> fillWifiData(WifiRepository wifiRepository) {
        Long publicExpectedTime = Time.getTime();
        Long privateExpectedTime = Time.getTime() + Time.MINUTE;
        Long squareExpectedTime = Time.getTime() + Time.HOUR;

        List<WifiEntity> wifiEntities = new ArrayList<>();
        wifiEntities.add(new WifiEntity(publicExpectedTime, "Public"));
        wifiEntities.add(new WifiEntity(privateExpectedTime, "Private"));
        wifiEntities.add(new WifiEntity(squareExpectedTime, "Square"));

        for (WifiEntity wifiEntity : wifiEntities) {
            wifiRepository.insert(wifiEntity);
        }
        Map<String, Long> pkMap = new HashMap<>();

        pkMap.put("Public", publicExpectedTime);
        pkMap.put("Private", privateExpectedTime);
        pkMap.put("Square", squareExpectedTime);

        return pkMap;
    }

    @Test
    public void connectedWifi_test() {
        ConnectedWifi connectedWifi = new ConnectedWifi(getApplicationContext(), 5);

        Map<String, Long> pkMap = fillWifiData(connectedWifi.getDatabase().getWifiRepository());

        String expectedSsid = "Public";

        connectedWifi.save(null, expectedSsid);
        WifiEntity actual = connectedWifi.getDatabase().getConnectedWifiRepository().getByTime(pkMap.get("Public"));

        Assert.assertEquals(expectedSsid, actual.getSsid());
    }

    @Test
    public void screen_test() {
        Screen screen = new Screen(getApplicationContext(), 5);

        Long timestamp = screen.save(true);
        ScreenEntity screenEntity = screen.getDatabase().getScreenRepository().getByTime(timestamp);

        Assert.assertTrue(screenEntity.getState());
    }

    @Test
    public void screenTap_test() {
        ScreenTap screenTap = new ScreenTap(getApplicationContext(), 5);

        Integer expectedX = 10;
        Integer expectedY = 12;
        Long timestamp = screenTap.save(expectedX, expectedY);
        ScreenTapEntity screenTapEntity = screenTap.getDatabase().getScreenTapsRepository().getByTime(timestamp);

        Assert.assertEquals(expectedX, screenTapEntity.getX());
        Assert.assertEquals(expectedY, screenTapEntity.getY());
    }

    @Test
    public void screenshot_test() {
        Screenshot screenshot = new Screenshot(getApplicationContext(), 5);

        String expectedUrl = "/data/user/0/cz.muni.irtis.datacollector.lite/cache/screenshots/test.png";
        String expectedMetadata = "0:26 / 4:14 Mix - AC/DC - Back In Black (Official Video)";
        Long timestamp = screenshot.save(expectedUrl, expectedMetadata);
        ScreenshotMetadataEntity screenshotMetadataEntity = screenshot.getDatabase().getScreenshotsMetadataRepository().getByTime(timestamp);

        Assert.assertEquals("test.png", screenshotMetadataEntity.getFilename());
        Assert.assertEquals(expectedMetadata, screenshotMetadataEntity.getMetadata());
    }

    @Test
    public void smsConversation_test() {
        SmsConversation smsConversation = new SmsConversation(getApplicationContext(), 5);

        SmsRecord expected = new SmsRecord();
        expected.setPhoneNumber("252623541");
        expected.setContent("Hi, this is test");
        expected.setType(SmsRecord.INCOMING);
        expected.setMessageDate(Time.getTime());

        Long timestamp = smsConversation.save(expected);

        PhoneSmsEntity actual = smsConversation.getDatabase().getSmsConversationRepository().getByTime(timestamp);

        Assert.assertEquals(expected.getPhoneNumber(), actual.getPhoneNumber());
        Assert.assertEquals(expected.getMessageDate(), actual.getMessageDate().longValue());
        Assert.assertEquals(expected.getContent(), actual.getContent());
        Assert.assertEquals(expected.getType(), actual.getType());
    }

    @Test
    public void callHistory_test() {
        CallHistory callHistory = new CallHistory(getApplicationContext(), 5);

        CallRecord expected = new CallRecord();
        expected.setPhoneNumber("252623541");
        expected.setName("Test");
        expected.setType(CallLog.Calls.INCOMING_TYPE);
        expected.setCallDate(Time.getTime());
        expected.setDuration(Time.MINUTE);

        Long timestamp = callHistory.save(expected);

        PhoneCallEntity actual = callHistory.getDatabase().getCallHistoryRepository().getByTime(timestamp);

        Assert.assertEquals(expected.getPhoneNumber(), actual.getPhoneNumber());
        Assert.assertEquals(expected.getName(), actual.getName());
        Assert.assertEquals(expected.getType(), actual.getType());
        Assert.assertEquals(expected.getCallDate(), actual.getCallDate().longValue());
        Assert.assertEquals(expected.getDuration(), actual.getDuration().longValue());
    }

    @Test
    public void backgroundApplication_test() {
        BackgroundApplication backgroundApplication = new BackgroundApplication(getApplicationContext(), 5);

        String expectedName = "cz.muni.irtis.datacollector.lite";
        Long timestamp = backgroundApplication.save(expectedName);

        ApplicationEntity actual = backgroundApplication.getDatabase().getBackgroundApplicationRepository().getByTime(timestamp);

        Assert.assertEquals(expectedName, actual.getName());
    }

    @Test
    public void foregroundApplication_test() {
        ForegroundApplication foregroundApplication = new ForegroundApplication(getApplicationContext(), 5);

        String expectedName = "com.google.android.youtube";
        Long timestamp = foregroundApplication.save(expectedName);

        ApplicationEntity actual = foregroundApplication.getDatabase().getForegroundApplicationRepository().getByTime(timestamp);

        Assert.assertEquals(expectedName, actual.getName());
    }

}
