package cz.muni.irtis.datacollector.lite.persistence;

import android.content.Context;


import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;

import cz.muni.irtis.datacollector.lite.Application;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import cz.muni.irtis.datacollector.lite.application.utils.Files;
import okhttp3.Response;

import static cz.muni.irtis.datacollector.lite.Config.IDENTITY_TOKEN;

@RunWith(AndroidJUnit4.class)
public class DebugTest {
    protected Context context;

    @Before
    public void create() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    @After
    public void destroy() {

    }


    @Test
    public void upload() {
        File directory = new File(Application.getInstance().getApplicationContext()
                .getCacheDir().getAbsolutePath() + "/exceptions");
        if (directory.exists()) {
            File[] exceptions = directory.listFiles();
            for (int i = 0; i < exceptions.length; i++) {
                final File file = exceptions[i];
                Connection.build(context).postFile(
                        UrlComposer.compose("debug/exceptions/upload",
                                IDENTITY_TOKEN),
                        file,
                        new Connection.OnConnectionListener() {
                            @Override
                            public void onStart() {}

                            @Override
                            public void onError(Exception e) {}

                            @Override
                            public void onDone(Response response, Object content) {
                                file.delete();
                            }
                        }, false
                );
            }
        }


        File[] logs = new File[] {
            new File(Application.getInstance().getApplicationContext()
                    .getCacheDir().getAbsolutePath() + "/debug.log"),
            new File(Application.getInstance().getApplicationContext()
                    .getCacheDir().getAbsolutePath() + "/errors.log"),
            new File(Application.getInstance().getApplicationContext()
                    .getCacheDir().getAbsolutePath() + "/warnings.log")
        };
        for (int i = 0; i < logs.length; i++) {
            final File original = logs[i];
            if(original.exists()) {
                final File transfer = new File(Application.getInstance().getApplicationContext()
                        .getCacheDir().getAbsolutePath() + "/" +
                        original.getName().replace(".log", "") + "-transfer.log");
                try {
                    Files.copy(original, transfer);
                } catch (IOException e) {}
                Connection.build(context).postFile(
                    UrlComposer.compose("debug/"+transfer.getName()
                            .replace(".log", "")+"/upload",
                            IDENTITY_TOKEN),
                        transfer,
                    new Connection.OnConnectionListener() {
                        @Override
                        public void onStart() {}

                        @Override
                        public void onError(Exception e) {}

                        @Override
                        public void onDone(Response response, Object content) {
                            original.delete();
                            transfer.delete();
                        }
                    }, false
                );
            }
        }
    }


}
