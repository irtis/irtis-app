package cz.muni.irtis.datacollector.lite.questionnaires;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Iterator;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.metrics.database.MetricsDatabase;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnaireFromTemplateBuilder;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;

@RunWith(AndroidJUnit4.class)
public class CreateFromTemplateTest {
    protected Context context;
    protected MetricsDatabase database;

    @Before
    public void create() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    @After
    public void destroy() {

    }


    @Test
    public void storeFromTemplate() {
        QuestionnairesModel model = new QuestionnairesModel(context);

        Questionnaire originalItem = QuestionnaireFromTemplateBuilder.build(
                context, QuestionnaireFromTemplateBuilder.TYPE_WELLBEING);
        Assert.assertNotNull(originalItem);
        model.store(originalItem);

        Questionnaire storedItem = model.getItem(originalItem.getId());
        Assert.assertNotNull(storedItem);
    }

    @Test
    public void storeAnswersFromTemplate() {
        QuestionnairesModel model = new QuestionnairesModel(context);

        Questionnaire originalItem = QuestionnaireFromTemplateBuilder.build(
                context, QuestionnaireFromTemplateBuilder.TYPE_WELLBEING);
        Assert.assertNotNull(originalItem);
        model.store(originalItem);

        originalItem = model.getItem(originalItem.getId());
        Assert.assertNotNull(originalItem);

        Iterator<Question> iQuestions = originalItem.getQuestions().iterator();
        while(iQuestions.hasNext()) {
            Question question = iQuestions.next();
            Iterator<Answer> iAnswers = question.getAnswers().iterator();
            while(iAnswers.hasNext()) {
                Answer answer = iAnswers.next();
                answer.setSelected();
                answer.setValue("1");
            }
        }
        originalItem.setCompleted();
        model.store(originalItem);


        Questionnaire storedItem = model.getItem(originalItem.getId());
        Assert.assertNotNull(storedItem);

        Debug.getInstance().log("itemExpect", originalItem.toString());
        Debug.getInstance().log("itemActual", storedItem.toString());

        Assert.assertEquals(originalItem, storedItem);
    }
}
