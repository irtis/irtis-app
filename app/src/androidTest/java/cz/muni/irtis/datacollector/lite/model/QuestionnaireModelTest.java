package cz.muni.irtis.datacollector.lite.model;


import android.content.Context;

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.QuestionnairesDatabase;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.QuestionnairesNotificationsRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.testutils.PrintUtils;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;

@RunWith(AndroidJUnit4ClassRunner.class)
public class QuestionnaireModelTest {

    private QuestionnairesModel questionnairesModel;

    private QuestionnairesNotificationsRepository notificationRepository;

    private QuestionnairesDatabase database;

    @Before
    public void setUp() {
        Context context = getApplicationContext();
        questionnairesModel = new QuestionnairesModel(context);
        database = QuestionnairesDatabase.getDatabase(context);
        notificationRepository = database.getQuestionnairesNotificationsRepository();
        createQuestionnaresTestData();
    }

    @After
    public void tearDown() {
        database.clearAllTables();
    }

    @Test
    public void storeAndGetItem_test() {

        questionnairesModel.store(multiSlider);

        Questionnaire data = questionnairesModel.getItem(multiSlider.getId());
        Assert.assertNotNull(data);
        Assert.assertEquals(data, multiSlider);
    }

    @Test
    public void getItems_test() {
        questionnairesModel.store(multiSlider);
        questionnairesModel.store(morning);
        questionnairesModel.store(filters);

        // The result is sorted by created descending
        List<Questionnaire> expected_4 = new ArrayList<>();
        expected_4.add(filters);
        expected_4.add(morning);
        expected_4.add(multiSlider);
        expected_4.sort(Comparator.comparing(Questionnaire::getCreated).reversed());

        List<Questionnaire> data_4 = questionnairesModel.getItems(0, 4);

        Assert.assertEquals(expected_4, data_4);

        List<Questionnaire> expected_2 = new ArrayList<>();
        expected_2.add(filters);

        List<Questionnaire> data_2 = questionnairesModel.getItems(2, 2);
        Assert.assertEquals(expected_2, data_2);

    }

    @Test
    public void getItemsPrepared_test() {
        questionnairesModel.store(multiSlider);
        morning.setStarting(Time.getTime() + Time.DAY);
        morning.setEnding(Time.getTime() + Time.DAY + Time.HOUR);
        questionnairesModel.store(morning);

        List<Questionnaire> expected = new ArrayList<>();
        expected.add(multiSlider);

        List<Questionnaire> data = questionnairesModel.getItemsToInvoke();
        Assert.assertEquals(expected, data);
    }

    @Test
    public void getItemsAvailable_test() {
        questionnairesModel.store(multiSlider);
        morning.setCompleted();
        questionnairesModel.store(morning);

        List<Questionnaire> expected = new ArrayList<>();
        expected.add(multiSlider);

        List<Questionnaire> data = questionnairesModel.getItemsAvailable();
        Assert.assertNotNull(data);
        Assert.assertEquals(expected, data);
    }

    @Test
    public void getItemAvailableNewest_test() {
        multiSlider.setClosing(Time.getTime() + 30 * 60000);
        questionnairesModel.store(multiSlider);
        morning.setClosing(Time.getTime() + 30 * 60000);
        questionnairesModel.store(morning);
        filters.setClosing(Time.getTime() + 45 * 60000);
        questionnairesModel.store(filters);

        Questionnaire data = questionnairesModel.getItemAvailableNewest();
        Assert.assertNotNull(data);
        Assert.assertEquals(filters, data);
    }

    @Test
    public void getItemsNotUploaded_test() {
        morning.setCompleted();
        multiSlider.setCompleted();
        questionnairesModel.store(multiSlider);
        questionnairesModel.store(morning);
        questionnairesModel.store(filters);

        List<Questionnaire> expected = new ArrayList<>();
        expected.add(multiSlider);
        expected.add(morning);
        expected.sort(Comparator.comparing(Questionnaire::getCreated).reversed());


        List<Questionnaire> data = questionnairesModel.getItemsNotUploaded();
        try {
            Assert.assertNotNull(data);
            Assert.assertEquals(data, expected);
        } catch (AssertionError assertionError) {
            if (data != null) {
                PrintUtils.printCompareData(expected, data);
            }
            throw new AssertionError(assertionError);
        }
    }

    @Test
    public void getItemByPersonQuestionnaireId_test() {
        multiSlider.setIdPeopleQuestionnaire(5L);
        questionnairesModel.store(multiSlider);
        questionnairesModel.store(filters);
        Questionnaire data = questionnairesModel.getItemByPersonQuestionnaireId(5L);

        Assert.assertNotNull(data);
        Assert.assertEquals(data, multiSlider);
    }


    @Test
    public void set_test() {
        questionnairesModel.store(multiSlider);
        // Do it once
        set_subTest(multiSlider);
        // Do it secondTime just to be sure it rewrites
        set_subTest(multiSlider);
    }

    @Test
    public void upload_test() {
        questionnairesModel.store(multiSlider);

        multiSlider.setCompleted();

        Assert.assertTrue(multiSlider.isClosed());

        questionnairesModel.upload(multiSlider);

        Questionnaire actual = questionnairesModel.getItem(multiSlider.getId());

        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getUploaded());
    }


    // ---------------------- Subtests and other helpfull methods ------------- /

    public void set_subTest(Questionnaire itemToUpgrade) {
        setNotified_subTest(itemToUpgrade);
        setOpened_subTest(itemToUpgrade);
        setPostponed_subTest(itemToUpgrade);
        setSilenced_subTest(itemToUpgrade);
        setUploaded_subTest(itemToUpgrade);
    }


    public void setNotified_subTest(Questionnaire itemToUpgrade) {
        Long expectedTime = Time.getTime();
        questionnairesModel.setNotified(itemToUpgrade, expectedTime);
        Questionnaire data = questionnairesModel.getItem(itemToUpgrade.getId());
        Assert.assertNotNull(data);
        Assert.assertEquals(data, itemToUpgrade);
    }


    public void setOpened_subTest(Questionnaire itemToUpgrade) {
        Long expectedTime = Time.getTime();
        questionnairesModel.setOpened(itemToUpgrade, expectedTime);
        Questionnaire data = questionnairesModel.getItem(itemToUpgrade.getId());
        itemToUpgrade.setOpened(expectedTime);
        Assert.assertNotNull(data);
        Assert.assertEquals(data, itemToUpgrade);
    }

    public void setPostponed_subTest(Questionnaire itemToUpgrade) {
        questionnairesModel.setPostponed(itemToUpgrade, 1);
        Questionnaire data = questionnairesModel.getItem(itemToUpgrade.getId());
        Assert.assertNotNull(data);
        Assert.assertEquals(data, itemToUpgrade);
    }

    public void setSilenced_subTest(Questionnaire itemToUpgrade) {
        Long expectedTime = Time.getTime();
        questionnairesModel.setSilenced(itemToUpgrade, expectedTime);
        Questionnaire data = questionnairesModel.getItem(itemToUpgrade.getId());
        itemToUpgrade.setSilenced(expectedTime);
        Assert.assertNotNull(data);
        Assert.assertEquals(data, itemToUpgrade);

    }

    public void setUploaded_subTest(Questionnaire itemToUpgrade) {
        Long expectedTime = Time.getTime();
        questionnairesModel.setUploaded(itemToUpgrade, expectedTime);
        itemToUpgrade.setUploaded(expectedTime);
        Questionnaire data = questionnairesModel.getItem(itemToUpgrade.getId());
        Assert.assertNotNull(data);
        Assert.assertEquals(data, itemToUpgrade);
    }

    private Questionnaire multiSlider;

    private Questionnaire morning;

    private Questionnaire filters;


    private void createQuestionnaresTestData() {
        filters = Questionnaire.buildFromAssets(getApplicationContext(), Questionnaire.getTemplatePathByIdentifier("debug-filters"));

        multiSlider = Questionnaire.buildFromAssets(getApplicationContext(), Questionnaire.getTemplatePathByIdentifier("debug-multislider"));

        morning = Questionnaire.buildFromAssets(getApplicationContext(), Questionnaire.getTemplatePathByIdentifier("debug-ranni-dotaznik-pilot-5"));
    }

}
