package cz.muni.irtis.datacollector.lite.questionnaires;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.MetricsDatabase;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.notifications.QuestionnairesNotification;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnaireFromTemplateBuilder;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;

@RunWith(AndroidJUnit4.class)
public class MorningQuestionnaireTest {
    protected Context context;
    protected MetricsDatabase database;

    private QuestionnairesModel model;

    @Before
    public void create() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        model = new QuestionnairesModel(context);
    }

    @After
    public void destroy() {

    }


    @Test
    public void morning() {
        DateTime now = DateTime.now();
        DateTime morningFrom = new DateTime(now.year().get(),now.monthOfYear().get(),now.dayOfMonth().get(),6,0);
        DateTime morningTo = new DateTime(now.year().get(),now.monthOfYear().get(),now.dayOfMonth().get(),7,0);
        if(Time.getTime()>morningFrom.getMillis() && Time.getTime()<morningTo.getMillis()) {
            Questionnaire item = QuestionnaireFromTemplateBuilder.build(
                    context, QuestionnaireFromTemplateBuilder.TYPE_MORNING);
            model.store(item);
            if (!QuestionnairesNotification.isBuilt(item)) {
                QuestionnairesNotification.build(context, item).show();
            }
        }
    }

}
