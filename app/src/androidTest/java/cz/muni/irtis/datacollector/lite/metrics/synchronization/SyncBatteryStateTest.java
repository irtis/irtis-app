package cz.muni.irtis.datacollector.lite.metrics.synchronization;

import android.content.Context;
import android.content.res.Resources;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;


import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import cz.muni.irtis.datacollector.R;


@RunWith(AndroidJUnit4.class)
public class SyncBatteryStateTest {
    private final String TAG = this.getClass().getSimpleName();
    private static final String SYNC_URL_BASE = "sync_server_url";
    private Resources resources;
    private Context context;
    private Properties properties;

    @BeforeClass
    private void loadProperties() throws IOException {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        resources = context.getResources();
        InputStream rawResource = resources.openRawResource(R.raw.unit_test_local);
        properties = new Properties();
        properties.load(rawResource);
    }

    private String getPropertyValue(String key) {
        return properties.getProperty(key);
    }


}
