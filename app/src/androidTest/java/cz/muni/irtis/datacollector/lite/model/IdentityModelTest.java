package cz.muni.irtis.datacollector.lite.model;

import android.app.Instrumentation;
import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import com.google.gson.GsonBuilder;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Identity;
import cz.muni.irtis.datacollector.lite.gui.identity.model.response.ConfigResponseEntity;
import okhttp3.Response;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;

@RunWith(AndroidJUnit4ClassRunner.class)
public class IdentityModelTest {

    private final String LOGIN = "1004";

    private final String PASSWORD = "trixtech";

    @Inject
    private Identity identityModel;

    @Before
    public void setUp() {
        Context context = getApplicationContext();
        identityModel = new Identity(context);
    }

    @After
    public void tearDown() {
        // Send signout request to server
        Connection.build(getApplicationContext()).getJson(
                UrlComposer.compose("identity/signout"));

    }

    @Test
    public void signIn_test() {
        boolean isLogged = identityModel.signIn(LOGIN, PASSWORD);
        Assert.assertTrue(isLogged);
        // isLogged should have same value as next assert but just to be sure it takes the token again
        Assert.assertNotNull(identityModel.getToken());

        Assert.assertTrue(identityModel.isSignedIn());
    }

    @Test
    public void isSingedIn_test() {

        Assert.assertFalse("User wasn't signed but signedIn returned true", identityModel.isSignedIn());
        boolean isLogged = identityModel.signIn(LOGIN, PASSWORD);

        Assert.assertTrue(isLogged);

        Assert.assertTrue("Expected user to be signed, isSigned returned false", identityModel.isSignedIn());
    }

    static ConfigResponseEntity expected;


    @Test
    public void downloadConfig_test() {
        String content = Connection.build(getApplicationContext()).getJson(UrlComposer.compose("identity/config/download", identityModel.getToken()));
        if (content != null && !content.isEmpty()) {
            expected = new GsonBuilder().create().fromJson(content, ConfigResponseEntity.class);
        }
        identityModel.downloadConfig(new Repository.RepositoryListener<ConfigResponseEntity>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onDone(ConfigResponseEntity data) {
                Assert.assertNotNull(data);
                Assert.assertNotNull(data.getBursts());
                Assert.assertEquals(expected.getBursts(), data.getBursts());
            }
        });
    }

    @Test
    public void signOut_test() {
        Assert.assertFalse("Expected: User isSignOut (false) Got: User isSignedIn(true)", identityModel.isSignedIn());

        boolean isLogged = identityModel.signIn(LOGIN, PASSWORD);

        Assert.assertTrue(isLogged);

        boolean isLoggedIn = identityModel.signOut();

        Assert.assertFalse(isLoggedIn);

        Assert.assertNull("Expected: getToken() = null", identityModel.getToken());

        Assert.assertFalse("Expected: User isSignOut (false) Got: User isSignedIn(true)", identityModel.isSignedIn());
    }

}
