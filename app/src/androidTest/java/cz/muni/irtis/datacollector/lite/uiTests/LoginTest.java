package cz.muni.irtis.datacollector.lite.uiTests;

import androidx.test.core.app.ActivityScenario;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.rule.ActivityTestRule;

import com.google.android.gms.common.api.Api;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.gui.LauncherActivity;
import cz.muni.irtis.datacollector.lite.gui.MainActivity;
import cz.muni.irtis.datacollector.lite.gui.identity.CredentialsActivity;
import cz.muni.irtis.datacollector.lite.gui.identity.SignInActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.*;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;


/**
 * @author Patrik Procházka
 */

@RunWith(AndroidJUnit4ClassRunner.class)
public class LoginTest {

    @Rule
    public ActivityTestRule<SignInActivity> launcherActivityTestRule =
            new ActivityTestRule<>(SignInActivity.class);

    @Test
    public void loginTest() {
        onView(withId(R.id.login))
                .perform(typeText("1004"));
        onView(withId(R.id.pass))
                .perform(typeText("trixtech"))
                .perform(closeSoftKeyboard());
        onView(withId(R.id.submit))
                .check(matches(isDisplayed()))
                .perform(click());
        onView(withId(R.id.list))
                .check(matches(isDisplayed()));
    }


}
