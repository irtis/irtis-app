package cz.muni.irtis.datacollector.lite.metrics.database;

import org.junit.Assert;
import org.junit.Test;

import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi.WifiAvailableEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi.WifiEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.WifiAvailableRepository;

public class AvailableWifiTest extends SimpleTablesTest {

    @Test
    public void addNewAvailableWifi() {
        WifiAvailableRepository repository = database.getAvailableWifiRepository();
        String expectedSSID = "WifiStar";
        Integer expectedConfidence = 90;
        Long expectedTime = Time.getTime();

        WifiEntity toSaveWifi = new WifiEntity(expectedTime, expectedSSID);
        long insertWifi = database.getWifiRepository().insert(toSaveWifi);
        Assert.assertEquals(expectedTime, Long.valueOf(insertWifi));

        WifiAvailableEntity toSaveAvailable = new WifiAvailableEntity(expectedTime, toSaveWifi.getId());
        long insertConnected = repository.insert(toSaveAvailable);
        Assert.assertEquals(expectedTime, Long.valueOf(insertConnected));

        WifiEntity actual = repository.getByTime(expectedTime);
        Assert.assertNotNull(actual);
//        Assert.assertEquals(expectedSSID, actual.get());
        Assert.assertEquals(expectedTime, actual.getDatetime());
    }

    @Test
    public void getByTime() {
        // TODO: implement
    }

    @Test
    public void getPrevious() {
        // TODO: implement
    }

    @Test
    public void deletePrevious() {
        // TODO: implement
    }

}
