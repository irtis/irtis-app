package cz.muni.irtis.datacollector.lite.metrics.database;

import org.junit.Assert;
import org.junit.Test;

import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationForegroundEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ApplicationsForegroundRepository;

public class ForegroundApplicationTest extends SimpleTablesTest {

    @Test
    public void foregroundApplication() {
        ApplicationsForegroundRepository repository = database.getForegroundApplicationRepository();
        String expectedName = "DataCollector";
        Integer expectedConfidence = 90;
        Long expectedTime = Time.getTime();

        ApplicationEntity toSaveApplication = new ApplicationEntity(expectedTime, expectedName);
        long insertApplication = database.getApplicationRepository().insert(toSaveApplication);
        Assert.assertEquals(expectedTime, Long.valueOf(insertApplication));

        ApplicationForegroundEntity toSaveForeground = new ApplicationForegroundEntity(expectedTime, toSaveApplication.getId());
        long insertForeground = repository.insert(toSaveForeground);
        Assert.assertEquals(expectedTime, Long.valueOf(insertForeground));

        ApplicationEntity actual = repository.getByTime(expectedTime);
        Assert.assertNotNull(actual);
        Assert.assertEquals(expectedName, actual.getName());
        Assert.assertEquals(expectedTime, actual.getDatetime());
    }

    @Test
    public void getByTime() {
        // TODO: implement
    }

    @Test
    public void getPrevious() {
        // TODO: implement
    }

    @Test
    public void deletePrevious() {
        // TODO: implement
    }
}
