package cz.muni.irtis.datacollector.lite.questionnaires;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import cz.muni.irtis.datacollector.lite.Config;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Identity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.QuestionnairesDatabase;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionnaireEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.QuestionnairesRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;

@RunWith(AndroidJUnit4.class)
public class MissingJsonFileTest {
    protected Context context;
    protected QuestionnairesRepository repository;

    private QuestionnairesModel model;

    @Before
    public void create() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        repository = QuestionnairesDatabase.getDatabase(context).getQuestionnairesRepository();
        model = new QuestionnairesModel(context);
        Identity.getInstance().signIn(Config.IDENTITY_ID, Config.IDENTITY_PASS);
    }

    @After
    public void destroy() {
    }

    @Test
    public void downloadWhenJsonIsMissing() {
        Questionnaire item = model.download(38789L);
        Assert.assertNotNull(item);

        if(item.getSource().exists()) {
            item.getSource().delete();
            QuestionnaireEntity entity = repository.getItem(item.getId());
            if (entity.getSource()!=null && !entity.getSource().isEmpty()) {
                try {
                    FileInputStream fin = new FileInputStream(entity.getSource());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    reader.close();
                    item = Questionnaire.buildFromString(sb.toString());
                    Assert.assertFalse("The source exists",true);
                } catch (FileNotFoundException e) {
                    Debug.getInstance().exception(e, this,
                            "Missing JSON cache file for ID: " + entity.getIdentifier() + " (" + entity.getId() + ")");
                    try {
                        Debug.getInstance().log(this.getClass().getSimpleName(), "Download missing JSON file: started");
                        item = model.download(entity.getIdPeopleQuestionnaire());
                        Debug.getInstance().log(this.getClass().getSimpleName(),"Download missing JSON file: finished");
                        Assert.assertNotNull(item);
                        if (item != null) {
                            Assert.assertNotNull(item.getSource());
                            Assert.assertTrue(item.getSource().exists());
                        }
                    } catch (Exception exception) {
                        Debug.getInstance().exception(exception, this, "Redownloading missing JSON cache file failed (" + entity.getIdentifier() + " (" + entity.getId() + "))");
                        Debug.getInstance().log(this.getClass().getSimpleName(),"Download missing JSON file: failed");
                    }
                } catch (IOException e) {
                    Debug.getInstance().exception(e, this, entity.getSource());
                }
                Assert.assertNotNull(item);
            } else {
                Assert.assertFalse("The source exists",true);
            }
        } else {
            Assert.assertFalse("The source does not exists",true);
        }
    }
}
