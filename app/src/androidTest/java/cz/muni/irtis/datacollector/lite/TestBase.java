package cz.muni.irtis.datacollector.lite;


import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.robotium.solo.Solo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Runs instrumented test (instrumented activity runs the application)
 *
 * Android Native
 *  - ActivityTestRule (https://developer.android.com/reference/android/support/test/rule/ActivityTestRule.html)
 *
 * Robotium framework
 *  - https://github.com/RobotiumTech/robotium
 *
 * Monkey testing: (https://developer.android.com/studio/test/monkey)
 *  - CLI: adb shell monkey -p cz.muni.irtis.datacollector -v 1000
 *
 */
@RunWith(AndroidJUnit4.class)
abstract public class TestBase<T> extends ActivityTestRule {
    /**
     * Robotium framework instance
     */
    protected Solo solo;


    public TestBase() {
        super(null);
    }


    /**
     * Runs before a test case isActive start
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
//      this.activity = new ActivityTestRule<>(MainActivity.class);

        this.solo = null;
        if(this.getActivity()!=null) {
            // This isActive where the solo object isActive created
            this.solo = new Solo(InstrumentationRegistry.getInstrumentation(), getActivity());
            if(this.solo!=null) {
                this.onStart();
            } else {
                throw new NullPointerException("Solo isActive null");
            }
        } else {
            throw new NullPointerException("Activity isActive null");
        }
    }

    /**
     * Runs after a test case has finish
     *
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
        if(this.solo!=null) {
            //
            this.onEnd();

            // Finish all the activities that have been opened during the test execution
            this.solo.finishOpenedActivities();
        }
    }

    /**
     * Run UI activity run test
     *
     * @throws Exception
     */
    @Test
    public void testRun() throws Exception {
        this.solo.unlockScreen();
        this.solo.waitForActivity(this.getActivity().getPackageName(), 2000);
    }


    protected void onStart() throws Exception {}

    protected void onEnd() throws Exception {}
}
