package cz.muni.irtis.datacollector.lite.metrics.database;

import org.junit.Assert;
import org.junit.Test;

import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi.WifiConnectedEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi.WifiEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.WifiConnectedRepository;

public class ConnectedWifiRepository extends SimpleTablesTest {
    @Test
    public void connectedWifi() {
        WifiConnectedRepository repository = database.getConnectedWifiRepository();
        String expectedSSID = "WifiStar";
        Integer expectedConfidence = 90;
        Long expectedTime = Time.getTime();

        WifiEntity toSaveWifi = new WifiEntity(expectedTime, expectedSSID);
        long insertWifi = database.getWifiRepository().insert(toSaveWifi);
        Assert.assertEquals(expectedTime, Long.valueOf(insertWifi));

        WifiConnectedEntity toSaveConnected = new WifiConnectedEntity(expectedTime, toSaveWifi.getId());
        long insertConnected = repository.insert(toSaveConnected);
        Assert.assertEquals(expectedTime, Long.valueOf(insertConnected));

        WifiEntity actual = repository.getByTime(expectedTime);
        Assert.assertNotNull(actual);
//        Assert.assertEquals(expectedSSID, actual.get());
        Assert.assertEquals(expectedTime, actual.getDatetime());
    }

    @Test
    public void getByTime() {
        // TODO: implement
    }

    @Test
    public void getPrevious() {
        // TODO: implement
    }

    @Test
    public void deletePrevious() {
        // TODO: implement
    }
}
