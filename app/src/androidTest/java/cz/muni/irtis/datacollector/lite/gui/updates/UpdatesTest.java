package cz.muni.irtis.datacollector.lite.gui.updates;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;

import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import okhttp3.Response;

@RunWith(AndroidJUnit4.class)
public class UpdatesTest {
    protected Context context;

    @Before
    public void create() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    @After
    public void destroy() {

    }


    @Test
    public void downloadUpdate() {
        final Updates updates = new Updates(context);

        Connection.build(context).getFile(
            UrlComposer.compose("/update/check", "l1EcUuTwErlgr9WyMScshjgUU9WYb6ElWC5eECbNTRdLFGxCrtlhMIGWZG3D"),
            new Connection.OnConnectionFileListener() {
                @Override
                public void onStart() { }
                @Override
                public void onError(Exception e) { }
                @Override
                public File onCreateFile(String name) {
                    return updates.create(name);
                }
                @Override
                public void onDone(Response response, File file) {
                    Assert.assertNotNull(file);
                    Assert.assertNotEquals(file.length(), 0);
                }
            }, false
        );
    }


}
