package cz.muni.irtis.datacollector.lite.uiTests;


import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObjectNotFoundException;
import androidx.test.uiautomator.UiSelector;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.utils.Time;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static cz.muni.irtis.datacollector.lite.uiTests.PermissionActivityTest.getTextFromAppContextById;
import static cz.muni.irtis.datacollector.lite.uiTests.PermissionActivityTest.permissionSetTest;

@RunWith(AndroidJUnit4ClassRunner.class)
public class YoutubeTest {

    public static final String START_IRTIS_APP = "monkey -p cz.muni.irtis.datacollector.lite 10";

    public static final String START_YOUTUBE = "monkey -p com.google.android.youtube 10";

    public static final DateTimeFormatter LOG_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");

    public static final String LOG_FILENAME = "/data/user/0/cz.muni.irtis.datacollector.lite/cache/logs/debug.log";

    public static final String VIDEO_TITLE = "AC/DC - Back In Black (Official Video)";

    @After
    public void tearDown() {
        try {
            PrintWriter writer = new PrintWriter(LOG_FILENAME);
            writer.print("");
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException("Failed opening file", e);
        }
    }

    @Test
    public void basicTest() throws InterruptedException, UiObjectNotFoundException, FileNotFoundException {
        UiDevice mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        // If you want to set up app automatically
        // setUpIrtisApp(mDevice);
        executeAdbCommand(START_YOUTUBE);
        Thread.sleep(1000);
        UiObject searchButton = mDevice.findObject(new UiSelector().resourceId("com.google.android.youtube:id/menu_item_0"));
        searchButton.click();
        mDevice.findObject(new UiSelector().resourceId("com.google.android.youtube:id/search_edit_text")).setText(VIDEO_TITLE);
        Thread.sleep(1000);
        mDevice.pressEnter();
        Thread.sleep(1000);
        long clickTime = Time.getTime();
        UiObject video = mDevice.findObject(new UiSelector().descriptionContains(VIDEO_TITLE));
        video.click();
        Thread.sleep(10000);
        LocalDateTime clickBeginTime = Instant.ofEpochMilli(clickTime).atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime clickEndTime = Instant.ofEpochMilli(clickTime + 2 * Time.SECOND).atZone(ZoneId.systemDefault()).toLocalDateTime();

        BufferedReader reader = null;
        try {
            boolean containsMetadata = false;
            String line;
            reader = new BufferedReader(new FileReader(LOG_FILENAME));
            while ((line = reader.readLine()) != null) {
                LocalDateTime lineTime = LocalDateTime.parse(line.substring(0, 19), LOG_TIME_FORMAT);
                if (lineTime.isAfter(clickBeginTime) && lineTime.isBefore(clickEndTime)) {
                    if (line.contains(VIDEO_TITLE)) {
                        containsMetadata = true;
                    }
                }
                System.out.println(line);

            }
            Assert.assertTrue(containsMetadata);
        } catch (IOException e) {
            throw new IllegalStateException("Failed opening file", e);
        }
    }

    public void setUpIrtisApp(UiDevice mDevice) throws InterruptedException, UiObjectNotFoundException {
        executeAdbCommand(START_IRTIS_APP);
        Thread.sleep(10000);

        if (mDevice.hasObject(By.text("SIGN IN"))) {
            onView(withId(R.id.login))
                    .perform(typeText("1004"));
            onView(withId(R.id.pass))
                    .perform(typeText("trixtech"))
                    .perform(closeSoftKeyboard());
            onView(withId(R.id.submit))
                    .check(matches(isDisplayed()))
                    .perform(click());
        }
        // TODO for @prochazkapa fix the missing references
        if (mDevice.hasObject(By.text(getTextFromAppContextById(R.string.permissions_requirement)))) {
            permissionSetTest();
            Thread.sleep(10000);
        }

        // TODO for @prochazkapa fix the missing reference
        UiObject metricsToggle = mDevice.findObject(new UiSelector().text(getTextFromAppContextById(R.string.main_capture_metrics)));
        if (metricsToggle != null) {
            metricsToggle.click();
        }
        // TODO for @prochazkapa fix the missing reference
        UiObject screensToggle = mDevice.findObject(new UiSelector().text(getTextFromAppContextById(R.string.main_capture_metrics_screenshots)));
        if (screensToggle != null) {
            screensToggle.click();
        }
        if (mDevice.hasObject(By.text("Start now"))) {
            UiObject startNowButton = mDevice.findObject(new UiSelector().text("Start now"));
            if (startNowButton != null) {
                startNowButton.click();
            }
        }

    }

    private void executeAdbCommand(String command) {
        InstrumentationRegistry.getInstrumentation().getUiAutomation().executeShellCommand(command);
    }
}
