package cz.muni.irtis.datacollector.lite.metrics.database;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.BatteryEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.phone.PhoneCallEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.LocationEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.ActivityEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenshotEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.phone.PhoneSmsEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ActivitiesRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.BatteryRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.LocationsRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.PhoneCallsRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ScreenshotsRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.PhoneSmsesRepository;

@RunWith(AndroidJUnit4.class)
public class SimpleTablesTest {
    protected Context context;
    protected MetricsDatabase database;

    @Before
    public void create() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();

        database = MetricsDatabase.getDatabase(context);
        Assert.assertNotNull(database);
    }

    @After
    public void destroy() {
        database.clearAllTables();
    }


    @Test
    public void addNewScreenshot() {
        ScreenshotsRepository repository = database.getScreenshotRepository();
        String expectedDeviceUrl = "/storage/emulated/0/Android/data/cz.muni.irtis.datacollector/files/20190619092220489.png";
        Long expectedTime = Time.getTime();

        ScreenshotEntity toSave = new ScreenshotEntity(expectedTime, expectedDeviceUrl);
        long insert = repository.insert(toSave);
        Assert.assertEquals(expectedTime, Long.valueOf(insert));

        ScreenshotEntity actual = repository.getByTime(expectedTime);
        Assert.assertNotNull(actual);
        Assert.assertEquals(expectedDeviceUrl, actual.getDeviceUrl());
        Assert.assertEquals(expectedTime, actual.getDatetime());
    }

    @Test
    public void addNewBatteryState() {
        BatteryRepository repository = database.getBatteryStateRepository();
        int expectedLevel = 50;
        Long expectedTime = Time.getTime();

        BatteryEntity toSave = new BatteryEntity(expectedTime, expectedLevel);
        long insert = repository.insert(toSave);
        Assert.assertEquals(expectedTime, Long.valueOf(insert));

        BatteryEntity actual = repository.getByTime(expectedTime);
        Assert.assertNotNull(actual);
        Assert.assertEquals(expectedLevel, actual.getStatePercent());
        Assert.assertEquals(expectedTime, actual.getDatetime());
    }

    @Test
    public void addNewLocation() {
        LocationsRepository repository = database.getLocationRepository();
        Double expectedLatitude = 50.5156456;
        Double expectedLongitude = 50.5156456;
        Long expectedTime = Time.getTime();

        LocationEntity toSave = new LocationEntity(expectedTime, expectedLatitude, expectedLongitude);
        long insert = repository.insert(toSave);
        Assert.assertEquals(expectedTime, Long.valueOf(insert));

        LocationEntity actual = repository.getByTime(expectedTime);
        Assert.assertNotNull(actual);
        Assert.assertEquals(expectedLatitude, actual.getLatitude());
        Assert.assertEquals(expectedLongitude, actual.getLongitude());
        Assert.assertEquals(expectedTime, actual.getDatetime());
    }

    @Test
    public void addNewPhysicalActivity() {
        ActivitiesRepository repository = database.getPhysicalActivityRepository();
        String expectedActivity = "walking";
        Integer expectedConfidence = 90;
        Long expectedTime = Time.getTime();

        ActivityEntity toSave = new ActivityEntity(expectedTime, expectedActivity, expectedConfidence);
        long insert = repository.insert(toSave);
        Assert.assertEquals(expectedTime, Long.valueOf(insert));

        ActivityEntity actual = repository.getByTime(expectedTime);
        Assert.assertNotNull(actual);
        Assert.assertEquals(expectedActivity, actual.getActivity());
        Assert.assertEquals(expectedConfidence, actual.getConfidence());
        Assert.assertEquals(expectedTime, actual.getDatetime());
    }

    @Test
    public void addNewCallHistory() {
        PhoneCallsRepository repository = database.getCallHistoryRepository();
        String expectedName = "Johnny Ranger";
        String expectedPhoneNumber = "+420123456789";
        String expectedType = "incoming";
        Long expectedDuration = 123568L;
        Long expectedCallDate = Time.getTime();
        Long expectedTime = Time.getTime();

        PhoneCallEntity toSave = new PhoneCallEntity(expectedTime, expectedName, expectedPhoneNumber, expectedType, expectedDuration, expectedCallDate);
        long insert = repository.insert(toSave);
        Assert.assertEquals(expectedTime, Long.valueOf(insert));

        PhoneCallEntity actual = repository.getByTime(expectedTime);
        Assert.assertNotNull(actual);
        Assert.assertEquals(expectedName, actual.getName());
        Assert.assertEquals(expectedPhoneNumber, actual.getPhoneNumber());
        Assert.assertEquals(expectedType, actual.getType());
        Assert.assertEquals(expectedDuration, actual.getDuration());
        Assert.assertEquals(expectedCallDate, actual.getCallDate());
        Assert.assertEquals(expectedTime, actual.getDatetime());
    }

    @Test
    public void addNewSmsConversation() {
        PhoneSmsesRepository repository = database.getSmsConversationRepository();
//        String expectedName = "Johnny Ranger";
        String expectedPhoneNumber = "+420123456789";
        String expectedType = "incoming";
        String expectedContent = "Hello, I'll be late. Go get some coffee for me, please :-)";
        Long expectedMessageDate = Time.getTime();
        Long expectedTime = Time.getTime();

        PhoneSmsEntity toSave = new PhoneSmsEntity(expectedTime, /*expectedName,*/ expectedPhoneNumber, expectedType, expectedContent, expectedMessageDate);
        long insert = repository.insert(toSave);
        Assert.assertEquals(expectedTime, Long.valueOf(insert));

        PhoneSmsEntity actual = repository.getByTime(expectedTime);
        Assert.assertNotNull(actual);
//        Assert.assertEquals(expectedName, actual.getName());
        Assert.assertEquals(expectedPhoneNumber, actual.getPhoneNumber());
        Assert.assertEquals(expectedType, actual.getType());
        Assert.assertEquals(expectedContent, actual.getContent());
        Assert.assertEquals(expectedTime, actual.getMessageDate());
        Assert.assertEquals(expectedTime, actual.getDatetime());
    }

}
