package cz.muni.irtis.datacollector.lite.metrics.synchronization;

import android.content.Context;
import android.content.res.AssetManager;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;

/**
 * SyncManagerServerTest
 *
 * Syncing a particular metric with the server test
 */
@RunWith(AndroidJUnit4.class)
public class SyncManagerServerTest {
    private final String TAG = this.getClass().getSimpleName();
    private static final String SYNC_URL_BASE = "https://irtis.fi.muni.cz:8080";       // irtis-server

    Context context;

    @Before
    public void createSyncManager() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();

    }

//
//    public void testMetric(final Metric metric, final SyncResource resource) {
//        ArrayList<File> files = resource.getFiles();
//        for(int i=0; i<files.size(); i++) {
//            if(files.get(i).exists()) {
//                String url = SYNC_URL_BASE + "/" + metric.getSyncUrl() + "/" + Identity.getInstance().getToken().toString();
//                Connection.build(context).put(url, files.get(i), new Connection.OnConnectionListener<String>() {
//                    @Override
//                    public void onStart() {
//                        Debug.getInstance().log(TAG, "Task started: "+metric.getSyncUrl());
//                    }
//
//                    @Override
//                    public void onError(Exception e) {
//                        Debug.getInstance().error(TAG, metric.getSyncUrl()+": failed ("+e.getText()+")");
//                        assertFalse(false);
//                    }
//
//                    @Override
//                    public void onDone(Response response, String content) {
//                        if (response != null) {
//                            switch (response.code()) {
//                                default:
//                                    try {
//                                        JSONObject json = new JSONObject(content);
//                                        Debug.getInstance().error(TAG, response.code() + ": " + json.getString("message"));
//                                    } catch (JSONException e) {
//                                        Debug.getInstance().exception(e, this);
//                                    }
//                                    break;
//                                case 200:
//                                    Debug.getInstance().log(TAG, "Task finished: "+metric.getSyncUrl()+" (result="+response.code()+")");
//                                    break;
//                            }
//                        }
//
//                        assertTrue(response != null && response.code()==200);
//                    }
//                }, false);
//            }
//        }
//    }

//    @Test
//    public void testScreenshot() {
//        SyncResource resource = new SyncResource(context);
//        resource.put(getFileFromAssets("synchronization/screenshot.png"));
//        this.testMetric(new Screenshot(context), resource);
//    }
//
//    @Test
//    public void testBatteryState() {
//        SyncResource resource = new SyncResource(context);
//        resource.put(getFileFromAssets("synchronization/batterystate.json"));
//        this.testMetric(new BatteryState(context), resource);
//    }


    private File getFileFromAssets(String path) {
        AssetManager assets = context.getAssets();
        try {
            InputStream is = assets.open(path);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);

            File file = new File(context.getCacheDir(), "screenshot.png");
            OutputStream os = new FileOutputStream(file);
            os.write(buffer);

            return file;
        } catch (Exception e) {
            Debug.getInstance().exception(e, this);
        }
        return null;
    }
}