package cz.muni.irtis.datacollector.lite.bursts;

import static org.junit.Assert.assertTrue;

import android.content.Context;
import android.util.Pair;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

import cz.muni.irtis.datacollector.lite.Config;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import cz.muni.irtis.datacollector.lite.gui.bursts.game.BurstCoin;
import cz.muni.irtis.datacollector.lite.gui.bursts.game.BurstCoinDeserializer;
import cz.muni.irtis.datacollector.lite.gui.bursts.game.BurstCoinFactory;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.Burst;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.BurstsModel;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Identity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionnaireEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;

@RunWith(AndroidJUnit4.class)
public class BurstFromServerTest {
    protected Context context;
    private BurstsModel model;

    @Before
    public void create() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        model = new BurstsModel(context);
//        Identity.getInstance().signIn(Config.IDENTITY_ID, Config.IDENTITY_PASS);
        Identity.getInstance().setToken("B6CZpJMBxLgMmEhgfKGka0LgBeRtg2WHaGqdRXphjFsh9L3hzPs8zg423Yc0");
    }

    @After
    public void destroy() {
    }

    @Test
    public void download() {
        Burst item = Burst.build(499L); //model.getItemFromServer(Burst.build(499L));
        Assert.assertNotNull(item);

        String content = Connection.build(context).getJson(
            UrlComposer.compose("identity/bursts/detail",
                Arrays.asList(
                    new Pair<String, String>("burst", item.getId().toString())
                )));

        if (content != null && !content.isEmpty()) {
            item = new GsonBuilder()
                .registerTypeAdapter(
                    BurstCoin.class,
                    new BurstCoinDeserializer()
                )
                .create()
                .fromJson(content, new TypeToken<Burst>() {}.getType());

            assertTrue(item!=null && item.getPurse()!=null && item.getPurse().isCoins());
        }
    }
}
