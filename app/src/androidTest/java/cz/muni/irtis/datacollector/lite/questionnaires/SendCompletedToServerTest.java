package cz.muni.irtis.datacollector.lite.questionnaires;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import com.google.gson.GsonBuilder;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Iterator;

import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import cz.muni.irtis.datacollector.lite.metrics.database.MetricsDatabase;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnaireFromTemplateBuilder;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.ResultResponse;
import okhttp3.Response;

@RunWith(AndroidJUnit4.class)
public class SendCompletedToServerTest {
    protected Context context;
    protected MetricsDatabase database;

    @Before
    public void create() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    @After
    public void destroy() {

    }


    @Test
    public void send() {
        final QuestionnairesModel model = new QuestionnairesModel(context);

        // Create item from template
        Questionnaire originalItem = QuestionnaireFromTemplateBuilder.build(
                context, QuestionnaireFromTemplateBuilder.TYPE_MORNING);
        Assert.assertNotNull(originalItem);
        originalItem.setCompleted();

        Iterator<Question> iQuestions = originalItem.getQuestions().iterator();
        while(iQuestions.hasNext()) {
            Question question = iQuestions.next();

            Iterator<Answer> iAnswers = question.getAnswers().iterator();
            while(iAnswers.hasNext()) {
                Answer answer = iAnswers.next();
                answer.setValue("1");
            }
        }

        model.store(originalItem);

        // Get stored item
        final Questionnaire storedItem = model.getItem(originalItem.getId());
        Assert.assertNotNull(storedItem);

        // Sent item to the server
        Connection.build(context).postJson(
            UrlComposer.compose("/questionnaires/person/upload", "ISFMypPWaCtragXDuaL4IpcT0lDxBXyaeBqOLNQ4Mxjn3ey55jMmmoczXHjI"),
            storedItem.toRequest().toString(),
            new Connection.OnConnectionListener<String>() {
                @Override
                public void onStart() {
                }

                @Override
                public void onError(Exception e) {
                    Assert.assertFalse(true);
                }

                @Override
                public void onDone(Response response, String content) {
                    if(content!=null) {
                        ResultResponse result = new GsonBuilder().create().fromJson(content, ResultResponse.class);
                        if(result!=null && result.isSuccessful()) {
                            storedItem.setUploaded();
                            model.store(storedItem);
                        }
                        Assert.assertTrue(result.isSuccessful());
                    } else {
                        Assert.assertFalse(true);
                    }
                }
            }, false
        );

    }
}
