package cz.muni.irtis.datacollector.lite.testutils;

import android.database.Cursor;

import cz.muni.irtis.datacollector.lite.testutils.jsonmodels.BatteryStateJson;

public class MockCursors {


    /**
     * Get Cursor with single row
     * @param json BatteryStateJson
     * @return cursor loaded with data from json
     */
    public static Cursor getBatteryStateMockCursor_singleRow(BatteryStateJson json) {
        final int size = 2;
        String[] names = new String[size];
        names[0] = "datetime";
        names[1] = "state_percent";

        String[] values = new String[size];
        values[0] = json.getDatetime();
        values[1] = Integer.toString(json.getStatePercent());

        int[] types = new int[size];
        types[0] = Cursor.FIELD_TYPE_INTEGER;
        types[1] = Cursor.FIELD_TYPE_INTEGER;

        return new TestCursor(names, values, types);
    }
}
