package cz.muni.irtis.datacollector.lite.application.network;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import okhttp3.Response;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * ConnectionTest
 *
 */
@RunWith(AndroidJUnit4.class)
public class ConnectionTest {
    Context context;

    @Before
    public void createSyncManager() {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();

    }

    @Test
    public void testGoogleServer() {
        Connection.build(context).getJson("https://google.com", new Connection.OnConnectionListener() {
            @Override
            public void onStart() {}
            @Override
            public void onError(Exception e) {
                assertFalse(false);
            }
            @Override
            public void onDone(Response response, Object content) {
                assertTrue(content != null);
            }
        }, false);
    }

    @Test
    public void testIRTISServer() {
        Connection.build(context).getJson("https://irtis.fi.muni.cz:8080", new Connection.OnConnectionListener() {
            @Override
            public void onStart() {}
            @Override
            public void onError(Exception e) {
                assertFalse(false);
            }
            @Override
            public void onDone(Response response, Object content) {
                assertTrue(content != null);
            }
        }, false);


    }
}
