package cz.muni.irtis.datacollector.lite.testutils;

import java.util.List;

public class PrintUtils {

    public static <T> void printList(List<T> listForPrint, String premise) {
        StringBuilder sb = new StringBuilder();
        sb.append(premise).append(": ").append("\n");
        for (T print : listForPrint) {
            sb.append(print.toString()).append("\n");
        }
        System.out.println(sb.toString());
    }

    public static <T> void printCompareData(List<T> expected, List<T> actual) {
        printList(expected, "Expected");
        printList(actual, "Got");
    }

    public static <T> String printDetail(T expected, T actual) {
        return "Expected: " + expected.toString() + ", Got:" + actual.toString();
    }
}
