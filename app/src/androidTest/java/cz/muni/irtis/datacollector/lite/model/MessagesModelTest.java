package cz.muni.irtis.datacollector.lite.model;

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Identity;
import cz.muni.irtis.datacollector.lite.gui.messages.database.MessageEntity;
import cz.muni.irtis.datacollector.lite.gui.messages.database.MessagesRepository;
import cz.muni.irtis.datacollector.lite.gui.messages.model.Message;
import cz.muni.irtis.datacollector.lite.gui.messages.model.MessagesModel;
import cz.muni.irtis.datacollector.lite.testutils.PrintUtils;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;

@RunWith(AndroidJUnit4ClassRunner.class)
public class MessagesModelTest {

    private MessagesModel messagesModel;

    private MessagesRepository repository;

    private Identity identityModel;

    @Before
    public void setUp() {
        // identityModel = new Identity(getApplicationContext());
        // identityModel.signIn("1004", "trixtech");
        messagesModel = new MessagesModel(getApplicationContext());
        repository = messagesModel.getDatabase().getMessagesRepository();
        createTestMessages();
        messagesModel.getDatabase().clearAllTables();
    }

    @Test
    public void storeAndGetItem_test() throws InterruptedException {
        messagesModel.store(basic, new Repository.RepositoryListener<Boolean>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onDone(Boolean data) {
                Assert.assertTrue(data);
            }
        });
        Thread.sleep(500);
        getItem_subTest(basic);
    }


    private void getItem_subTest(Message expected) throws InterruptedException {
        messagesModel.getItem(expected, new Repository.RepositoryListener<Message>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onDone(Message data) {
                try {
                    Assert.assertEquals(expected, data);
                } catch (AssertionError assertionError) {
                    System.out.println("Expected = " + expected.toString());
                    System.out.println("Got = " + data.toString());
                    throw new AssertionError(assertionError);
                }
            }
        });
        Thread.sleep(500);
    }

    @Test
    public void getItems_test() {
        repository.store(MessageEntity.build(basic));
        repository.store(MessageEntity.build(opened));
        repository.store(MessageEntity.build(uploaded));

        List<Message> expected = new ArrayList<>();
        expected.add(basic);
        expected.add(opened);
        expected.add(uploaded);

        messagesModel.getItems(new Repository.RepositoryListener<List<Message>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onDone(List<Message> data) {
                try {
                    Assert.assertNotNull(data);
                    Assert.assertEquals(data, expected);
                } catch (AssertionError assertionError) {
                    if (data != null) {
                        PrintUtils.printCompareData(expected, data);
                    }
                    throw new AssertionError(assertionError);
                }

            }
        });
    }

    @Test
    public void set_test() throws InterruptedException {
        repository.store(MessageEntity.build(basic));
        // Do it once
        set_subTest(basic);
        // Do it secondTime just to be sure it rewrites
        set_subTest(basic);
    }

    @Test
    public void getItemsNotUploaded_test() {
        repository.store(MessageEntity.build(basic));
        repository.store(MessageEntity.build(opened));
        repository.store(MessageEntity.build(uploaded));

        List<Message> expected = new ArrayList<>();
        expected.add(basic);
        expected.add(opened);

        List<Message> data = messagesModel.getItemsNotUploaded();
        try {
            Assert.assertNotNull(data);
            Assert.assertEquals(data, expected);
        } catch (AssertionError assertionError) {
            if (data != null) {
                PrintUtils.printCompareData(expected, data);
            }
            throw new AssertionError(assertionError);
        }
    }

    @Test
    public void getItemsNewest_test() {
        repository.store(MessageEntity.build(basic));
        repository.store(MessageEntity.build(opened));
        repository.store(MessageEntity.build(opened_2));
        repository.store(MessageEntity.build(opened_3));
        repository.store(MessageEntity.build(opened_4));

        List<Message> expected = new ArrayList<>();
        expected.add(basic);
        expected.add(opened);
        expected.add(opened_2);

        messagesModel.getItemsNewest(new Repository.RepositoryListener<List<Message>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onDone(List<Message> data) {
                try {
                    Assert.assertNotNull(data);
                    Assert.assertEquals(data, expected);
                } catch (AssertionError assertionError) {
                    if (data != null) {
                        PrintUtils.printCompareData(expected, data);
                    }
                    throw new AssertionError(assertionError);
                }
            }
        });

    }

    @Test
    public void download_test() {

        List<Message> downloadedData = messagesModel.download();
        Assert.assertNotNull(downloadedData);

        for (Message downloaded : downloadedData) {
            Assert.assertNotNull("Expected: Downloaded was set", downloaded.getDownloaded());
            Assert.assertNull("Expected: Uploaded wasn't set", downloaded.getUploaded());
        }

        List<Message> expected = messagesModel.getItemsNotUploaded();

        try {
            Assert.assertEquals(downloadedData, expected);
        } catch (AssertionError assertionError) {
            if (downloadedData != null) {
                PrintUtils.printCompareData(expected, downloadedData);
            }
            throw new AssertionError(assertionError);
        }

    }

    @Test
    public void upload_test() throws InterruptedException {
        repository.store(MessageEntity.build(basic));
        repository.store(MessageEntity.build(opened));
        messagesModel.upload(basic);
        messagesModel.upload(opened);

        messagesModel.getItem(basic, new Repository.RepositoryListener<Message>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onDone(Message data) {
                Assert.assertNotNull(data);
                Assert.assertNotNull(data.getUploaded());
            }
        });
        Thread.sleep(500);
        messagesModel.getItem(opened, new Repository.RepositoryListener<Message>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onDone(Message data) {
                Assert.assertNotNull(data);
                Assert.assertNotNull(data.getUploaded());
            }
        });


    }

    // ---------------------- Subtests and other helpfull methods ------------- /

    private void set_subTest(Message itemToUpgrade) throws InterruptedException {
        setNotified_subtest(itemToUpgrade);
        setOpened_subtest(itemToUpgrade);
        setRead_subtest(itemToUpgrade);
        setUploaded_subtest(itemToUpgrade);
        setResponse_subtest(itemToUpgrade);
    }

    private void setNotified_subtest(Message itemToUpgrade) throws InterruptedException {
        Long expectedTime = Time.getTime();
        messagesModel.setNotified(itemToUpgrade, expectedTime);
        itemToUpgrade.setNotified(expectedTime);
        getItem_subTest(itemToUpgrade);
    }

    private void setOpened_subtest(Message itemToUpgrade) throws InterruptedException {
        Long expectedTime = Time.getTime();
        messagesModel.setOpened(itemToUpgrade, expectedTime);
        itemToUpgrade.setOpened(expectedTime);
        getItem_subTest(itemToUpgrade);
    }

    private void setRead_subtest(Message itemToUpgrade) throws InterruptedException {
        Long expectedTime = Time.getTime();
        messagesModel.setRead(itemToUpgrade, expectedTime);
        itemToUpgrade.setRead(expectedTime);
        getItem_subTest(itemToUpgrade);
    }

    private void setUploaded_subtest(Message itemToUpgrade) throws InterruptedException {
        Long expectedTime = Time.getTime();
        messagesModel.setUploaded(itemToUpgrade, expectedTime);
        itemToUpgrade.setUploaded(expectedTime);
        getItem_subTest(itemToUpgrade);
    }

    private void setResponse_subtest(Message itemToUpgrade) throws InterruptedException {
        String expectedResponse = UUID.randomUUID().toString();
        messagesModel.setResponse(itemToUpgrade, expectedResponse);
        itemToUpgrade.setResponse(expectedResponse);
        getItem_subTest(itemToUpgrade);
    }

    // Test objects

    private Message basic;

    private Message opened;

    private Message uploaded;

    private Message opened_2;

    private Message opened_3;

    private Message opened_4;

    private void createTestMessages() {
        basic = Message.build(Long.valueOf(1));
        basic.setResponse("Basic response");

        opened = Message.build(Long.valueOf(2));
        opened.setResponse("opened_1 response");
        opened.setNotified(Time.getTime());
        opened.setDownloaded(Time.getTime());
        opened.setOpened(Time.getTime());

        uploaded = Message.build(Long.valueOf(3));
        uploaded.setResponse("Uploaded response");
        uploaded.setDownloaded(Time.getTime());
        uploaded.setNotified(Time.getTime());
        uploaded.setRead(Time.getTime());
        uploaded.setUploaded(Time.getTime());

        opened_2 = Message.build(Long.valueOf(4));
        opened_2.setResponse("opened_2 response");
        opened_2.setNotified(Time.getTime());
        opened_2.setDownloaded(Time.getTime());
        opened_2.setOpened(Time.getTime());

        opened_3 = Message.build(Long.valueOf(5));
        opened_3.setResponse("opened_3 response");
        opened_3.setNotified(Time.getTime());
        opened_3.setDownloaded(Time.getTime());
        opened_3.setOpened(Time.getTime());

        opened_4 = Message.build(Long.valueOf(6));
        opened_4.setResponse("opened_4 response");
        opened_4.setNotified(Time.getTime());
        opened_4.setDownloaded(Time.getTime());
        opened_4.setOpened(Time.getTime());

    }


}
