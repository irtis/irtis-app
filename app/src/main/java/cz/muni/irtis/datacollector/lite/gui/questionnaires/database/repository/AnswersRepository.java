package cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.AnswerEntity;

@Dao
public interface AnswersRepository {
    public static final String TABLE_NAME = "questionnaires_answers";

    @Insert
    long insert(AnswerEntity item);

    @Query("DELETE FROM "+TABLE_NAME+" WHERE questionnaire_id = :idQuestionnaire AND question_id = :idQuestion")
    void delete(Long idQuestionnaire, Long idQuestion);

    @Query("DELETE FROM "+TABLE_NAME+" WHERE questionnaire_id = :idQuestionnaire")
    void delete(Long idQuestionnaire);

    @Update
    void update(AnswerEntity item);

    @Query("SELECT 1 FROM " + TABLE_NAME + " WHERE questionnaire_id = :idQuestionnaire AND question_id = :idQuestion AND answer_id = :idAnswer")
    Integer isItem(Long idQuestionnaire, Long idQuestion, Long idAnswer);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE questionnaire_id = :idQuestionnaire AND question_id = :idQuestion AND answer_id = :idAnswer")
    AnswerEntity getItem(Long idQuestionnaire, Long idQuestion, Long idAnswer);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE questionnaire_id = :idQuestionnaire AND question_id = :idQuestion")
    List<AnswerEntity> getItems(Long idQuestionnaire, Integer idQuestion);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE questionnaire_id = :idQuestionnaire AND question_id = :idQuestion LIMIT :offset, :count")
    List<AnswerEntity> getAll(Long idQuestionnaire, Long idQuestion, int offset, int count);
}
