package cz.muni.irtis.datacollector.lite.gui.questionnaires.components;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.NumberPicker;

import androidx.appcompat.app.AlertDialog;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;

public class QuestionnairesConfigurationDialog extends Dialog {
    private AlertDialog dialog;
    private Questionnaire item;
    private OnSubmitListener listener;
    private QuestionnairesModel model;

    NumberPicker pickerHours;
    NumberPicker pickerMinutes;

    public interface OnSubmitListener {
        void onSubmit(Questionnaire item, Integer hours, Integer minutes);
        void onCancel();
        void onReset(Questionnaire item);
    }


    public QuestionnairesConfigurationDialog(Context context, Questionnaire item, OnSubmitListener listener) {
        super(context);
        model = new QuestionnairesModel(getContext());

        this.item = item;
        this.listener = listener;
        this.dialog = onBuilderCreate(null);
    }


    protected AlertDialog onBuilderCreate(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View vContent = getLayoutInflater().inflate(R.layout.component_questionnaires_configuration, null);

        pickerHours = iniHours((NumberPicker) vContent.findViewById(R.id.hours));
        pickerMinutes = iniMinutes((NumberPicker) vContent.findViewById(R.id.minutes));

        Integer hours = Time.getHour(item.getStarting());
        Integer minutes = Time.getMinute(item.getStarting());
        String invocations = model.getInvocations(item);
        if(invocations!=null) {
            String[] parts = invocations.split(":");
            hours = Integer.valueOf(parts[0]);
            minutes = Integer.valueOf(parts[1]);
        }

        pickerHours.setValue(hours);
        pickerMinutes.setValue(minutes);

        builder
                .setNeutralButton(getContext().getText(R.string.questionnaires_configuration_dialog_default_time_button), new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onReset(item);
                        close();
                    }
                })
            .setView(vContent)
            .setTitle(getContext().getString(R.string.questionnaires_configuration_dialog_title))
            .setPositiveButton("Ok", new OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    listener.onSubmit(item, pickerHours.getValue(), pickerMinutes.getValue());
                    close();
                }
            })
            .setNegativeButton("Cancel", new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    listener.onCancel();
                    close();
                }
            })
            .setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        listener.onCancel();
                        close();
                    }
                    return true;
                }
            }).setOnCancelListener(new OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    listener.onCancel();
                    close();
                }
            });

        return builder.create();
    }

    private NumberPicker iniHours(NumberPicker view) {
        view.setWrapSelectorWheel(true);

        view.setMinValue(Time.getHour(item.getStarting())-QuestionnairesModel.VALIDFRAME_HOUR_OFFSET);
        view.setMaxValue(Time.getHour(item.getEnding())+(QuestionnairesModel.VALIDFRAME_HOUR_OFFSET-1));

        view.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
            }
        });
        return view;
    }

    private NumberPicker iniMinutes(NumberPicker view) {
        view.setWrapSelectorWheel(true);
        view.setMinValue(0);
        view.setMaxValue(59);
        view.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
            }
        });
        return view;
    }

    public void show() { this.dialog.show(); }

    public void hide() { this.dialog.hide(); }

    public void close() { this.dialog.hide(); }

    public AlertDialog getDialog() { return this.dialog; }
}
