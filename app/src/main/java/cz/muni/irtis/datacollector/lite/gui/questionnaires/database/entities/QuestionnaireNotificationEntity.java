package cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.QuestionnairesNotificationsRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.QuestionnairesRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;

@Entity(tableName = QuestionnairesNotificationsRepository.TABLE_NAME)
public class QuestionnaireNotificationEntity {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private Long id;
    @ColumnInfo(name = "idLocalQuestionnaire")
    private Long idLocalQuestionnaire;
    @ColumnInfo(name = "questionnaire_id")
    private Long idQuestionnaire;
    @ColumnInfo(name = "people_questionnaire_id")
    private Long idPeopleQuestionnaire;
    @ColumnInfo(name = "action")
    private Integer action;
    @ColumnInfo(name = "time")
    private Long time;
    @ColumnInfo(name = "uploaded")
    private Long uploaded;

    public QuestionnaireNotificationEntity() {
    }

    public QuestionnaireNotificationEntity(Questionnaire item, Integer action, Long time) {
        this.idLocalQuestionnaire = item.getId();
        this.idQuestionnaire = item.getIdQuestionnaire();
        this.idPeopleQuestionnaire = item.getIdPeopleQuestionnaire();
        this.action = action;
        this.time = time;
    }

    @NonNull
    public Long getId() {
        return id;
    }

    public void setId(@NonNull Long id) {
        this.id = id;
    }

    public Long getIdLocalQuestionnaire() {
        return idLocalQuestionnaire;
    }

    public void setIdLocalQuestionnaire(Long idLocalQuestionnaire) {
        this.idLocalQuestionnaire = idLocalQuestionnaire;
    }

    public Long getIdQuestionnaire() {
        return idQuestionnaire;
    }

    public void setIdQuestionnaire(Long idQuestionnaire) {
        this.idQuestionnaire = idQuestionnaire;
    }

    public Long getIdPeopleQuestionnaire() {
        return idPeopleQuestionnaire;
    }

    public void setIdPeopleQuestionnaire(Long idPeopleQuestionnaire) {
        this.idPeopleQuestionnaire = idPeopleQuestionnaire;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Long getUploaded() {
        return uploaded;
    }

    public void setUploaded(Long uploaded) {
        this.uploaded = uploaded;
    }
}
