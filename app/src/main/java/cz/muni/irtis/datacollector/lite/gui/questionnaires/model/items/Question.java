package cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.json.Exclude;

public class Question {
    public static final int TYPE_TEXT = 1;
    public static final int TYPE_CHECK = 2;
    public static final int TYPE_SCALE = 3;
    public static final int TYPE_SLIDER = 4;
    public static final int TYPE_MULTIPLE_SLIDER = 9;
    public static final int TYPE_NUMBER = 5;
    public static final int TYPE_SPEECH = 6;
    public static final int TYPE_TIME_SPINNER = 7;
    public static final int TYPE_TIME_CLOCK = 8;
    public static final int TYPE_TIME_DATE = 10;
    public static final int TYPE_TIME_BIRTHDATE = 11;
    public static final int TYPE_TIME_HOURSMINUTES = 12;



    @Exclude
    private Questionnaire questionnaire;

    private Long id;
    private Integer type;
    private Integer density;
    private Boolean shuffle;
    private Long link;
    private List<QuestionFilter> filters;
    private String text;
    private List<Answer> answers;
    private Answer answer;
    private Integer position;
    private Integer elapsed;
    private Boolean skipped;


    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public Integer getType() {
        return type;
    }

    public Integer getDensity() {
        return density;
    }

    public Boolean getShuffle() {
        return shuffle;
    }

    public Integer getPosition() {
        return position;
    }

    public boolean isLink() {
        return link !=null && link >0;
    }

    public Long getLink() {
        return link;
    }

    public List<QuestionFilter> getFilters() {
        return filters;
    }

    public void setFilters(List<QuestionFilter> filters) {
        this.filters = filters;
    }

    public Boolean getSkipped() {
        return skipped;
    }

    public Boolean isSkipped() {
        return (skipped!=null && skipped) || skipped==null;
    }

    public List<Answer> getAnswers() {
        if(answers==null) {
            answers = new ArrayList<Answer>();
            answers.add(new Answer(this, 1L, getText()));
        } else if(answers.isEmpty()) {
            answers.add(new Answer(this, 1L, getText()));
        }
        return answers;
    }

    public boolean isShuffle() {
        return shuffle!=null ? shuffle : false;
    }


    /**
     * shuffle()
     *
     * Sort the answers randomly
     *
     * @return
     */
    public List<Answer> shuffle() {
        if(isShuffle() && answers!=null && answers.size()>1) {
            Answer last = answers.remove(answers.size()-1);
            Collections.shuffle(answers);
            answers.add(last);
        }
        return answers;
    }

    public Answer getAnswer() {
        return getAnswers().get(0);
    }

    public Answer getAnswerById(Long id) {
        Iterator<Answer> iAnswers = getAnswers().iterator();
        while(iAnswers.hasNext()) {
            Answer answer = iAnswers.next();
            if(answer.getId().equals(id)) {
                return answer;
            }
        }
        return null;
    }


    public List<Answer> getAnswersSelected() {
        List<Answer> selected = new ArrayList<Answer>();
        Iterator<Answer> iAnswers = getAnswers().iterator();
        while(iAnswers.hasNext()) {
            Answer answer = iAnswers.next();
            if(answer.isSelected()) {
                selected.add(answer);
            }
        }
        return selected;
    }


    public boolean isFiltered() {
        if(getFilters()!=null && !getFilters().isEmpty()) {
            boolean isFiltered = false;
            Iterator<QuestionFilter> iFilters = getFilters().iterator();
            while(iFilters.hasNext()) {
                QuestionFilter filter = iFilters.next();
                Question question = getQuestionnaire().getQuestionById(filter.getIdQuestion());
                if(question!=null) {
                    Answer answer = question.getAnswerById(filter.getIdAnswer());
                    if(answer!=null && !question.isSkipped()) {
                        switch(filter.getType().intValue()) {
                            default:
                                return true;

                            case QuestionFilter.TYPE_SELECTION:
                                isFiltered = !answer.isSelected();
                                break;
                        }

                        // @todo this was fix for postburst questionnaire, however, disrupting the functionality of other questionnaires
                        // was reverted back (therefore commented)
//                        if(filter.getOperation()==QuestionFilter.OPERATION_OR) {
//                            if(!isFiltered) {
//                                // End the cycle and do not skip the question
//                                break;
//                            }
//                        }
                    } else {
                        // This deals with skipped questions cases
                        isFiltered = true;
                        break;
                    }
                }
            }
            return isFiltered;
        }
        return false;
    }


    public boolean isCompleted() {
        Boolean isCompleted = false;
        Iterator<Answer> iAnswers = getAnswers().iterator();

        if(getType()!=null) {
            switch (getType()) {
                default:
                    while (iAnswers.hasNext()) {
                        Answer answer = iAnswers.next();
                        if (answer.isValue()) {
                            isCompleted = true;
                            break;
                        }
                    }
                    break;

                case Question.TYPE_MULTIPLE_SLIDER:
                    Integer check = getAnswers().size();
                    while (iAnswers.hasNext()) {
                        Answer answer = iAnswers.next();
                        if (answer.isValue()) {
                            check--;
                        }
                    }
                    isCompleted = check == 0;
                    break;
            }
        }

        return isCompleted;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getElapsed() {
        if(elapsed!=null) {
            return elapsed;
        } else {
            return 0;
        }
    }

    public void setElapsed(Integer elapsed) {
        this.elapsed = elapsed;
    }

    public void setSkipped(Boolean skipped) {
        this.skipped = skipped;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        Question object = (Question) obj;
        if(!this.id.equals(object.getId())) {
            return false;
        }

        if(!this.type.equals(object.getType())) {
            return false;
        }

        if(this.link !=null) {
            if (!this.link.equals(object.getLink())) {
                return false;
            }
        } else {
            if(this.link ==null && this.link !=null) {
                return false;
            }
        }

        if(!this.text.equals(object.getText())) {
            return false;
        }

        if(this.position!=null) {
            if (!this.position.equals(object.getPosition())) {
                return false;
            }
        } else {
            if(this.position==null && this.position!=null) {
                return false;
            }
        }

        Integer matches = 0;
        Iterator<Answer> iAnswers = getAnswers().iterator();
        while(iAnswers.hasNext()) {
            Answer answer = iAnswers.next();
            Iterator<Answer> iObjectAnswers = object.getAnswers().iterator();
            while(iObjectAnswers.hasNext()) {
                Answer objectAnswer = iObjectAnswers.next();
                if(answer.equals(objectAnswer)) {
                    matches++;
                    break;
                }
            }
        }
        if(!matches.equals(getAnswers().size())) {
            return false;
        }

        return true;
    }

}
