package cz.muni.irtis.datacollector.lite.metrics.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import cz.muni.irtis.datacollector.lite.metrics.database.repository.HeadphonesRepository;

@Entity(tableName = HeadphonesRepository.TABLE_NAME)
public class HeadphonesEntity extends EntityBase {
    @ColumnInfo(name = "connected")
    private Boolean connected;

    public HeadphonesEntity(Long datetime, Boolean connected) {
        super(datetime);
        this.connected = connected;
    }

    public Boolean getConnected() {
        return connected;
    }
}
