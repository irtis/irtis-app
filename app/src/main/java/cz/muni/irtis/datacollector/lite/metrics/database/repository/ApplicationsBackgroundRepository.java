package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationBackgroundEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationEntity;

@Dao
public interface ApplicationsBackgroundRepository extends IRepository<ApplicationEntity> {
    public static final String TABLE_NAME = "applications_background";

    /**
     * Add new entity to database.
     * @param item entity
     */
    @Insert
    long insert(ApplicationBackgroundEntity item);

    /**
     * Get entity by it's PK datetime
      * @param time java style timestamp (1561544094*1000)
     * @return entity
     */
    @Query("SELECT "+TABLE_NAME+".datetime, "+ ApplicationsRepository.TABLE_NAME+".name " +
            "FROM "+TABLE_NAME+" JOIN " + ApplicationsRepository.TABLE_NAME + " ON " + ApplicationsRepository.TABLE_NAME + ".datetime = " + TABLE_NAME + ".apps_name_id" + " " +
            "WHERE "+TABLE_NAME+".datetime = :time")
    ApplicationEntity getByTime(Long time);

    /**
     * {@link IRepository#getPrevious(Long time)}
     */
    @Query("SELECT "+TABLE_NAME+".datetime, "+ ApplicationsRepository.TABLE_NAME+".name " +
            "FROM "+TABLE_NAME+" JOIN " + ApplicationsRepository.TABLE_NAME + " ON " + ApplicationsRepository.TABLE_NAME + ".datetime = " + TABLE_NAME + ".apps_name_id" + " " +
            "WHERE "+TABLE_NAME+".datetime < :time")
    List<ApplicationEntity> getPrevious(Long time);

    /**
     * {@link IRepository#getPrevious(Long time, int offset, int count)}
     */
    @Query("SELECT "+TABLE_NAME+".datetime, "+ ApplicationsRepository.TABLE_NAME+".name " +
            "FROM "+TABLE_NAME+" JOIN " + ApplicationsRepository.TABLE_NAME + " ON " + ApplicationsRepository.TABLE_NAME + ".datetime = " + TABLE_NAME + ".apps_name_id" + " " +
            "WHERE "+TABLE_NAME+".datetime < :time" + " " +
            "LIMIT :offset, :count")
    List<ApplicationEntity> getPrevious(Long time, int offset, int count);

    /**
     * {@link IRepository#deleteById(Long)}
     */
    @Query("DELETE FROM " + TABLE_NAME + " WHERE datetime = :id")
    int deleteById(Long id);

//    @Query("DELETE FROM " + TABLE_NAME + " " +
//            "WHERE " + TABLE_NAME + ".datetime IN (" +
//            "SELECT " + TABLE_NAME + ".datetime FROM " + TABLE_NAME + " " +
//            "WHERE datetime < :time" + " " +
//            "LIMIT :offset, :count)")
//    void deletePrevious(Long time, int offset, int count);


}
