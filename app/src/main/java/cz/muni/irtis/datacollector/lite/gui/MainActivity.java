package cz.muni.irtis.datacollector.lite.gui;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;


import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.Applications;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionAccessibility;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.Repeat;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.application.widgets.Box;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.Burst;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.BurstsModel;
import cz.muni.irtis.datacollector.lite.gui.identity.SignInActivity;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Identity;
import cz.muni.irtis.datacollector.lite.gui.messages.MessagesFragment;
import cz.muni.irtis.datacollector.lite.gui.messages.model.MessagesModel;
import cz.muni.irtis.datacollector.lite.gui.permissions.AccessibilityDialog;
import cz.muni.irtis.datacollector.lite.gui.permissions.PermissionsActivity;

import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesFragment;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.metrics.mediaprojection.MediaProjectionProvider;

import cz.muni.irtis.datacollector.lite.metrics.MetricsService;
import cz.muni.irtis.datacollector.lite.metrics.model.util.Storage;
import cz.muni.irtis.datacollector.lite.metrics.synchronization.MetricsSynchronizationService;
import cz.muni.irtis.datacollector.lite.persistence.PersistentService;

public class MainActivity extends ActivityBase {
    public static final String PARAM_SELECTED_FRAGMENT = "selected_fragment";

    public static final String PARAM_START_SYNC = "start_sync";
    public static final String PARAM_START_MEDIAPROJECTION = "start_mediaprojection";
    public static final String PARAM_START_ACCESSIBILITY = "start_metadata";

    private MessagesModel messages;
    private QuestionnairesModel questionnaires;
    private BurstsModel burstsModel;

    private Repeat hBadgesRefreshRepeat;

    /**
     * onCreate()
     *
     * We were granted the permissions, lets go ahead...
     * Run the scheduler service only if it's not already running.
     *
     * @param bundle
     */
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(null);
        setContentView(R.layout.activity_layout);
        setSupportActionBar(getToolbarView());

        // Set application's first start as carried
        Applications.setFirstStart();

        // Start the PersistentService if it isActive not already started
        if(!PersistentService.isRunning(this)) {
            PersistentService.start(this);
        }

        if(Network.isConnection()) {
            Connection.isAvailable(new Connection.IsAvailableListener() {
                @Override
                public void onSuccess() {
                    Identity.getInstance().isSignedIn(new Identity.OnIdentitySignInListener() {
                        @Override
                        public void onStart() {
                        }

                        @Override
                        public void onDone(boolean isLogged) {
                            if(!isLogged) {
                                startActivity(SignInActivity.class);
                                finish();
                            }
                        }
                    });
                }

                @Override
                public void onFailure() {

                }
            });
        }

        messages = new MessagesModel(this);
        questionnaires = new QuestionnairesModel(this);
        burstsModel = new BurstsModel(this);

        AppBarConfiguration configuration = new AppBarConfiguration.Builder(
                R.id.home,
                R.id.questionnaires,
                R.id.messages,
                R.id.bursts
        ).build();

        NavigationUI.setupActionBarWithNavController(this, getNavigationController(), configuration);
        NavigationUI.setupWithNavController(getToolbarBottomView(), getNavigationController());

        // Select a specific fragment
        if (getIntent().getStringExtra(PARAM_SELECTED_FRAGMENT) != null) {
            // MessagesFragment selection
            if (getIntent().getStringExtra(PARAM_SELECTED_FRAGMENT).equals(MessagesFragment.class.getSimpleName())) {
                navigate(R.id.messages);
            }

            // QuestionnairesFragment selection
            if (getIntent().getStringExtra(PARAM_SELECTED_FRAGMENT).equals(QuestionnairesFragment.class.getSimpleName())) {
                navigate(R.id.questionnaires);
                List<Burst> bursts = burstsModel.getItemsActive();
                if (bursts != null) {
                    for (Burst burst : bursts) {
                        Settings.getSettings()
                            .edit()
                            .putBoolean(Settings.QUESTIONNAIRES_INVOCATIONS_SETTING_REMINDER + "_" + burst.getIdentifier(), true)
                            .commit();
                        Debug.getInstance().log(Settings.QUESTIONNAIRES_INVOCATIONS_SETTING_REMINDER+": notification successfully consumed for burst with id: " + burst.getIdentifier());
                    }
                } else {
                    Debug.getInstance().log(Settings.QUESTIONNAIRES_INVOCATIONS_SETTING_REMINDER+": Intent called from questionnaire configuration notification, but no active bursts were found");
                }
            }
        }

        if(getIntent().getBooleanExtra(PARAM_START_SYNC, false)) {
            MetricsSynchronizationService.start(this);
        } else if(getIntent().getBooleanExtra(PARAM_START_MEDIAPROJECTION, false)) {
            startCaptureScreenshots();
        } else if(getIntent().getBooleanExtra(PARAM_START_ACCESSIBILITY, false)) {
            startPermissionsMetadata();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(hBadgesRefreshRepeat!=null) {
            hBadgesRefreshRepeat.cancel();
        }
        hBadgesRefreshRepeat = Repeat.build(Time.SECOND*10, new Runnable() {
            @Override
            public void run() {
                questionnaires.getItemAvailableNewest(new Repository.RepositoryListener<Questionnaire>() {
                    @Override
                    public void onStart() {
                    }
                    @Override
                    public void onDone(Questionnaire item) {
                        setBadge(R.id.questionnaires, item!=null && !item.isClosed() ? 1 : 0);
                    }
                });
                messages.getItemsNewestCount(new Repository.RepositoryListener<Integer>() {
                    @Override
                    public void onStart() {
                    }
                    @Override
                    public void onDone(Integer count) {
                        setBadge(R.id.messages, count);
                    }
                });
            }
        }, true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(hBadgesRefreshRepeat!=null) {
            hBadgesRefreshRepeat.cancel();
            hBadgesRefreshRepeat = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        messages.cancel();
    }


    public void startCapture() {
        if(isGrantedPermissions()) {
            if (!MetricsService.isRunning(PersistentService.getInstance())) {
                if (Storage.isFreeSpace()) {
                    MetricsService.setEnabled(true);
                    MetricsService.start(PersistentService.getInstance());
                } else {
                    Debug.getInstance().warning(MetricsService.TAG,
                            "Cannot start capturing | Reason: not enough free space in the storage");
                    Box.ok(this, getString(
                        R.string.main_capture_outofspace_title), getString(R.string.main_capture_outofspace_message));
                }
            }
        } else {
            Debug.getInstance().warning(MetricsService.TAG,
                "Cannot start capturing | Reason: required permissions are not granted");
            Intent intent = new Intent(this, PermissionsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    public void stopCapture() {
        if (MetricsService.isRunning(PersistentService.getInstance())) {
            MetricsService.setEnabled(false);
            MetricsService.stop(PersistentService.getInstance());
        }
    }

    public void startCaptureScreenshots() {
        if(MetricsService.isRunning(PersistentService.getInstance())) {
            if (!MetricsService.isMetricRunningScreenshots(PersistentService.getInstance())) {
                MediaProjectionProvider.getInstance().request(this);
            } else {
                Debug.getInstance().warning(MetricsService.TAG,
                "Cannot start metric: Screenshots | Reason: the metric is already running");
            }
        } else {
            Debug.getInstance().warning(MetricsService.TAG,
            "Cannot start metric: Screenshots | Reason: the capturing is not running");
        }
    }

    public void stopCaptureScreenshots() {
        if (MetricsService.isMetricRunningScreenshots(PersistentService.getInstance())) {
            MetricsService.stopMetricScreenshots(PersistentService.getInstance());
        }
    }

    public void startPermissionsMetadata() {
        new AccessibilityDialog(this,
                new PermissionAccessibility(this, getString(R.string.permissions_accessibility), getString(R.string.permissions_accessibility_description)))
            .show();
    }


    public NavController getNavigationController() {
        return Navigation.findNavController(this, R.id.host);
    }

    public BottomNavigationView getToolbarBottomView() {
        return findViewById(R.id.menu);
    }

    public Toolbar getToolbarView() {
        return (Toolbar) this.findViewById(R.id.toolbar);
    }

    public void setBadge(int id, Integer count) {
        if(count!=null && count>0) {
            getToolbarBottomView().getOrCreateBadge(id).setNumber(count);
        } else {
            getToolbarBottomView().removeBadge(id);
        }
    }

    public void navigate(int resId, Bundle bundle) {
        if(getNavigationController()!=null) {
            getNavigationController().navigate(resId, bundle);
        }
    }

    public void navigate(int resId, String action, String value) {
        Bundle bundle = new Bundle();
        bundle.putString(action, value);
        navigate(resId, bundle);
    }

    public void navigate(int resId, String action, Integer value) {
        Bundle bundle = new Bundle();
        bundle.putInt(action, value);
        navigate(resId, bundle);
    }

    public void navigate(int resId, String action, Long value) {
        Bundle bundle = new Bundle();
        bundle.putLong(action, value);
        navigate(resId, bundle);
    }

    public void navigate(int resId) {
        Bundle bundle = new Bundle();
        navigate(resId, bundle);
    }


    /**
     * Start SchedulerService if user has given permission, then return to main layout
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(MediaProjectionProvider.getInstance().onActivityResult(requestCode, resultCode, data)) {
            MetricsService.startMetricScreenshots(PersistentService.getInstance());
        }
    }
}
