package cz.muni.irtis.datacollector.lite.application.components;

import android.os.Bundle;
import android.view.View;

import androidx.navigation.NavController;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.widgets.LoadingView;
import cz.muni.irtis.datacollector.lite.gui.MainActivity;

public abstract class NavigationFragmentBase extends FragmentBase {


    public NavigationFragmentBase(int layout) {
        super(layout);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
//        View view = super.onCreateView(inflater, container, bundle);
//
//        return view;
//    }

    @Override
    public void onResume() {
        super.onResume();
    }


    public NavController getNavigationController() {
        if(getActivity() instanceof MainActivity) {
            return ((MainActivity) getActivity()).getNavigationController();
        } else {
            return null;
        }
    }


    public void navigate(int resId, Bundle bundle) {
        if(getNavigationController()!=null) {
            getNavigationController().navigate(resId, bundle);
        }
    }

    public void navigate(int resId, String action, String value) {
        Bundle bundle = new Bundle();
        bundle.putString(action, value);
        navigate(resId, bundle);
    }

    public void navigate(int resId, String action, Integer value) {
        Bundle bundle = new Bundle();
        bundle.putInt(action, value);
        navigate(resId, bundle);
    }

    public void navigate(int resId, String action, Long value) {
        Bundle bundle = new Bundle();
        bundle.putLong(action, value);
        navigate(resId, bundle);
    }

    public void navigate(int resId) {
        Bundle bundle = new Bundle();
        navigate(resId, bundle);
    }

    public MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    public LoadingView getLoadingView(int resId) {
        return ((LoadingView) findViewById(resId));
    }
    public LoadingView getLoadingView() {
        return getLoadingView(R.id.loading);
    }

    public SwipeRefreshLayout getRefreshView(int resId) {
        return ((SwipeRefreshLayout) findViewById(resId));
    }
    public SwipeRefreshLayout getRefreshView() {
        return getRefreshView(R.id.refresh);
    }

    public View getEmptyView() {
        return findViewById(R.id.empty);
    }
}
