package cz.muni.irtis.datacollector.lite.gui.questionnaires.components.widgets;

import android.content.Context;
import android.util.AttributeSet;

import cz.muni.irtis.datacollector.lite.application.widgets.ExtendedProgressBar;

public class QuestionnairesProgressBar extends ExtendedProgressBar {
    public QuestionnairesProgressBar(Context context) {
        super(context);
    }

    public QuestionnairesProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public QuestionnairesProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public QuestionnairesProgressBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public synchronized void setMax(int max) {
        super.setMax(max-1);
    }
}
