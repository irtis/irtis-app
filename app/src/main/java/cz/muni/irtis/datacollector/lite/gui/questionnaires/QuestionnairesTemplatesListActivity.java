package cz.muni.irtis.datacollector.lite.gui.questionnaires;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListView;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewAdapter;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewBuilder;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;


public class QuestionnairesTemplatesListActivity extends AppCompatActivity {
    private RecyclerListViewAdapter list;
    private QuestionnairesModel model;

    static public class ViewHolder extends RecyclerListViewAdapter.ItemsListViewHolderBase {
        public ImageView vCheck;
        public TextView vTime;
        public TextView vTitle;

        public ViewHolder(RecyclerListViewAdapter adapter, View view) {
            super(adapter, view);
            vCheck = view.findViewById(R.id.check);
            vTime = view.findViewById(R.id.time);
            vTitle = view.findViewById(R.id.title);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaires);

        model = new QuestionnairesModel(this);

        // Build ListView with listeners (getting adapter as reference)
        list = RecyclerListViewBuilder.build(this)
            .setView(R.id.list)
            .setAdapter(new RecyclerListViewAdapter<Questionnaire, ViewHolder>(R.layout.activity_questionnaires_list_row))
            .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<Questionnaire, ViewHolder>() {
                @Override
                public ViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                    return new ViewHolder(adapter, view);
                }

                @Override
                public void onBind(ViewHolder holder, Questionnaire item) {
                    holder.vCheck.setVisibility(View.GONE);
                    holder.vTime.setVisibility(View.GONE);
                    holder.vTitle.setText(item.getTitle());
                }
            })
            .setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<Questionnaire>() {
                @Override
                public void onClick(View view, final Questionnaire item) {
                    item.setStarting(Time.getTime());
                    item.setInvoking(Time.getTime());
                    item.setEnding(Time.getTime());
                    item.setClosing(Time.getTime()+Time.SECOND);
                    model.store(item, new Repository.RepositoryListener<Boolean>() {
                        @Override
                        public void onStart() {
                        }

                        @Override
                        public void onDone(Boolean data) {
                            QuestionnairesFormActivity.start(QuestionnairesTemplatesListActivity.this, item);
                            finish();
                        }
                    });

                }
            })
            .create()
            .getAdapter();

        // Set the first contents of the ListView
        get();
    }


    public void get() {
        model.getItemsTemplates(new Repository.RepositoryListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
            }

            @Override
            public void onDone(List<Questionnaire> items) {
                if(items!=null && !items.isEmpty()) {
                    list.setItems(items, true);
                }
            }
        });
    }
}
