package cz.muni.irtis.datacollector.lite.application.utils;

/**
 * BooleanCounted
 * <p>
 * Author: Michal Schejbal<br>
 * Created: 27.12.2019
 */
public class BooleanSet {
    private Integer count;
    private Integer falses;
    private Integer truths;

    private BooleanSet(Integer count) {
        this.count = count;
        falses = 0;
        truths = 0;
    }

    static public BooleanSet build(Integer count) {
        return new BooleanSet(count);
    }

    public void setFalse() {
        falses++;
    }
    public void setTrue() {
        truths++;
    }

    public boolean isTrue() {
        return truths.equals(count);
    }

    public boolean isFalse() {
        return falses.equals(count);
    }
}
