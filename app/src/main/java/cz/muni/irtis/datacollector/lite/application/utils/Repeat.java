package cz.muni.irtis.datacollector.lite.application.utils;

import android.os.Handler;

public class Repeat {
    private Handler handler;
    private Runnable repeat;

    /**
     * build()
     *
     * @param time Period of code run
     * @param closure Code to be run
     * @param run   Run now
     * @return
     */
    static public Repeat build(final long time, final Runnable closure, boolean run) {
        if(run) {
            closure.run();
        }
        return new Repeat(time, closure);
    }

    /**
     *  build()
     *
     * @param time Period of code run
     * @param closure Code to be run
     * @return
     */
    static public Repeat build(final long time, final Runnable closure) {
        return build(time, closure, false);
    }

    private Repeat(final long time, final Runnable closure) {
        this.handler = new Handler();
        this.repeat = new Runnable() {
            @Override
            public void run() {
               handler.postDelayed(repeat, time);
               closure.run();
            }
        };

        handler.postDelayed(repeat, time);
    }

    public void cancel() {
        this.handler.removeCallbacks(this.repeat);
    }
}
