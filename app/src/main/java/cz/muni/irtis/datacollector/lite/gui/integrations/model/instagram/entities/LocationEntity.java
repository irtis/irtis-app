package cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities;

import com.google.gson.annotations.SerializedName;

public class LocationEntity {
    private Long id;
    private Double latitude;
    private Double longitude;
    private String name;
    @SerializedName("street_address")
    private String street;

    public Long getId() {
        return id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }
}
