package cz.muni.irtis.datacollector.lite.metrics.mediaprojection;


import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.Applications;
import cz.muni.irtis.datacollector.lite.application.components.NotificationBase;
import cz.muni.irtis.datacollector.lite.gui.MainActivity;
import cz.muni.irtis.datacollector.lite.metrics.hooking.AccessibilityNotification;

/**
 * MediaProjectionNotification
 *
 * This notification is shown when the screenshots metric is enabled but
 * the mediaprojection permission is missing and needs to be granted.
 */
public class MediaProjectionReminderNotification extends NotificationBase {
    public static final int NOTIFICATION_IDENTIFIER = 39859;

    private NotificationCompat.Builder builder;

    private MediaProjectionReminderNotification(Context context) {
        super(context);
    }

    static public MediaProjectionReminderNotification build(Context context) {
        return new MediaProjectionReminderNotification(context);
    }


    @Override
    public Notification create() {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(MainActivity.PARAM_START_MEDIAPROJECTION, true);
        if(!Applications.isActivityRunning(context, MainActivity.class)) {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        } else {
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        PendingIntent pending = PendingIntent.getActivity(
                context, NOTIFICATION_IDENTIFIER, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder = new NotificationCompat.Builder(context, CHANNEL_HIGH)
            .setSmallIcon(R.drawable.ic_cast_grey_24dp)
            .setSubText(context.getString(R.string.permissions_mediaprojection))
            .setContentTitle(context.getString(R.string.permissions_mediaprojection_reminder_notification))
            .setContentText(context.getString(R.string.permissions_mediaprojection_reminder_notification_description))
            .setStyle(new NotificationCompat.BigTextStyle()
                    .bigText(context.getString(R.string.permissions_mediaprojection_description)))
            .setCategory(NotificationCompat.CATEGORY_ERROR)
            .setContentIntent(pending)
            .setAutoCancel(true)
            .setOnlyAlertOnce(true);

        return notification = builder.build();
    }

    public void show() {
        manager.notify(NOTIFICATION_IDENTIFIER, create());
        manager.cancel(MediaProjectionNotification.NOTIFICATION_IDENTIFIER);
        manager.cancel(AccessibilityNotification.NOTIFICATION_IDENTIFIER);
    }

    public void cancel() {
        if(notification!=null){
            manager.cancel(NOTIFICATION_IDENTIFIER);
        }
    }
}