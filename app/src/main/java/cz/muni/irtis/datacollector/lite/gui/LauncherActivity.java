package cz.muni.irtis.datacollector.lite.gui;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.Applications;
import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;
import cz.muni.irtis.datacollector.lite.application.utils.Delay;
import cz.muni.irtis.datacollector.lite.gui.identity.SignInActivity;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Identity;
import cz.muni.irtis.datacollector.lite.gui.integrations.IntegrationsActivity;
import cz.muni.irtis.datacollector.lite.gui.permissions.PermissionsActivity;

public class LauncherActivity extends ActivityBase {
    protected void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_launcher);

        Delay.build(500, new Runnable() {
            @Override
            public void run() {
                if(!Identity.getInstance().isLogged()) {
                    Intent intent = new Intent(LauncherActivity.this, SignInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    start(LauncherActivity.this);
                }
            }
        });
    }

    static public void start(ActivityBase activity) {
        Intent intent = null;
        if(Applications.isFirstStart()) {
            intent = new Intent(activity, PermissionsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            activity.startActivity(intent);

            if(Config.REQUEST_INTEGRATIONS) {
                intent = new Intent(activity, IntegrationsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            }
        } else {
            if (activity.isGrantedPermissions()) {
                intent = new Intent(activity, MainActivity.class);
            } else {
                intent = new Intent(activity, PermissionsActivity.class);
            }

            if(intent!=null) {
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(intent);
            }
        }
    }
}
