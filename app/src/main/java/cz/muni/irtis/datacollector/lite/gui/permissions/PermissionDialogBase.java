package cz.muni.irtis.datacollector.lite.gui.permissions;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;

import cz.muni.irtis.datacollector.lite.application.permissions.Permission;

abstract public class PermissionDialogBase extends Dialog {
    private AlertDialog dialog;
    private Permission permission;
    private String identifier;

    public interface PermissionDialogResult {
        public void onDone(boolean isSuccess);
    }
    private PermissionDialogResult listener;

    public PermissionDialogBase(Context context, Permission permission, String identifier) {
        super(context);
        this.identifier = identifier;
        this.permission = permission;
        this.dialog = onBuilderCreate(null);
    }

    public PermissionDialogBase(Context context, Permission permission, String identifier, PermissionDialogResult listener) {
        super(context);
        this.identifier = identifier;
        this.permission = permission;
        this.dialog = onBuilderCreate(null);
        this.listener = listener;
    }

    abstract protected AlertDialog onBuilderCreate(Bundle bundle);

    public void show() { this.dialog.show(); }

    public void hide() { this.dialog.hide(); }

    public void close() { this.dialog.hide(); }

    public String getIdentifier() { return this.identifier; }

    public AlertDialog getDialog() { return this.dialog; }

    public Permission getPermission() { return this.permission; }

    public PermissionDialogResult getListener() {
        return listener;
    }
}
