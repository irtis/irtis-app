package cz.muni.irtis.datacollector.lite.gui.integrations.model.twitter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.Applications;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.IntegrationBase;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;

public class TwitterIntegration extends IntegrationBase {
    private TwitterService twitter;

    public TwitterIntegration(Context context, final ServiceBase.OnServiceListener onSignInListener, final ServiceBase.OnServiceListener onSignOutListener) {
        super(context);
        twitter = TwitterServiceBuilder.buildTwitter(context, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
                onSignInListener.onStart();
            }

            @Override
            public void onError(int code) {
                onSignInListener.onError(code);
            }

            @Override
            public void onFinished(boolean isLogged) {

                onSignInListener.onFinished(isLogged);
            }
        }, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
                onSignOutListener.onStart();
            }

            @Override
            public void onError(int code) {
                onSignOutListener.onError(code);
            }

            @Override
            public void onFinished(boolean isLogged) {
                onSignOutListener.onFinished(false);
            }
        });
    }
    public TwitterIntegration(Context context) {
        super(context);
        twitter = TwitterServiceBuilder.buildTwitter(context);
    }

    static public TwitterIntegration build(Context context) {
        return new TwitterIntegration(context);
    }

    @Override
    public boolean isConnected() {
        return twitter.isConnected();
    }

    /**
     * isApplication()
     *
     * Check if youtube app isActive installed on the device
     *
     * @return
     */
    @Override
    public boolean isApplication() {
        return Applications.isApplicationInstalled(getContext(), "com.twitter.android") ||
                Applications.isApplicationInstalled(getContext(), "com.twitter.android.lite");
    }

    @Override
    public String getName() {
        return getContext().getString(R.string.twitter_integration_title);
    }

    @Override
    public Drawable getImage() {
        return getContext().getDrawable(R.drawable.integration_twitter_icon);
    }

    public void connect(Activity activity) {
        twitter.signIn(activity);
    }

    public void disconnect() {
        twitter.signOut();
    }

    public TwitterService getService() {
        return twitter;
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        twitter.onActivityResult(requestCode, resultCode, data);
    }
}
