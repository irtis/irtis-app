package cz.muni.irtis.datacollector.lite.gui.bursts.game;

public class BurstCoinSilver implements BurstCoin {

    static public BurstCoinSilver build() {
        return new BurstCoinSilver();
    }

    @Override
    public Integer getValue() {
        return 5;
    }
}
