package cz.muni.irtis.datacollector.lite.metrics.model;

import android.content.Context;
import android.media.AudioManager;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.Metric;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.PlaybackEntity;

/**
 * Playback
 *
 */
public class Playback extends Metric {
    private static final String TAG = Playback.class.getSimpleName();
    public static final int IDENTIFIER = 259;

    private AudioManager manager;

    public Playback(Context context, Integer delay) {
        super(context, delay);
        manager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "Playback";
    }

    @Override
    public void run() {
        setRunning(true);
        if(manager.isMusicActive()) {
            save(manager.isMusicActive(), manager.getStreamVolume(AudioManager.STREAM_MUSIC));
        }
    }

    @Override
    public void stop() {
        setRunning(false);
    }

    @Override
    public long save(Object... params) {
        Boolean isActive = (Boolean) params[0];
        Integer volume = (Integer) params[1];

        try {
            Debug.getInstance().log(TAG, isActive.toString() + " | volume: "+volume.toString());
            return getDatabase().getPlaybackRepository().insert(new PlaybackEntity(
                Time.getTime(),
                isActive
            ));
        } catch (Exception e) {
            Debug.getInstance().exception(e);
            return -1;
        }
    }


}
