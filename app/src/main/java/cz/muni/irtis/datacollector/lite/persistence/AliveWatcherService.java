package cz.muni.irtis.datacollector.lite.persistence;

import android.content.Context;

import cz.muni.irtis.datacollector.lite.application.components.TaskServiceBase;
import cz.muni.irtis.datacollector.lite.persistence.model.AliveWatcher;

public class AliveWatcherService extends TaskServiceBase {
    public AliveWatcherService(Context context) {
        super(context, AliveWatcherService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        AliveWatcher.build(getContext())
            .send();

        AliveWatcherJob.schedule(getContext());
        stop(getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * start()
     *
     * Start the service
     *
     * @param context
     */
    public static void start(Context context) {
        TaskServiceBase.startService(context, AliveWatcherService.class);
    }

    /**
     * stop()
     *
     * Stop the service
     *
     * @param context
     */
    public static void stop(Context context) {
        TaskServiceBase.stopService(context, AliveWatcherService.class);
    }
}
