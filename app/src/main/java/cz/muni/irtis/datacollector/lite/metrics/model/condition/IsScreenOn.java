package cz.muni.irtis.datacollector.lite.metrics.model.condition;

import android.content.Context;

import cz.muni.irtis.datacollector.lite.metrics.model.util.ScreenState;

public class IsScreenOn implements Condition {
    @Override
    public boolean check(Context context) {
        return ScreenState.isOn(context);
    }
}
