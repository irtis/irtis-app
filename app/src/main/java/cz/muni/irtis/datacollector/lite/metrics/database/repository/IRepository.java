package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Delete;

import java.util.List;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.EntityBase;

public interface IRepository<T extends EntityBase> {
    /**
     * Get entities with LOWER datetime than given.
     * @param time java style timestamp (1561544094*1000)
     * @return list of entities with LOWER datetime than given
     */
    List<T> getPrevious(Long time);
    List<T> getPrevious(Long time, int offset, int count);



    @Delete
    int delete(T item);

    @Delete
    int delete(List<T> items);

    @Delete
    int deleteById(Long id);

}
