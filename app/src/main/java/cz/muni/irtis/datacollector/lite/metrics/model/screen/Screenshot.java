package cz.muni.irtis.datacollector.lite.metrics.model.screen;

import android.content.Context;
import android.graphics.Point;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.projection.MediaProjection;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.Display;
import android.view.WindowManager;

import com.google.gson.GsonBuilder;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cz.muni.irtis.datacollector.lite.Application;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenshotMetadataEntity;
import cz.muni.irtis.datacollector.lite.metrics.hooking.BusProvider;
import cz.muni.irtis.datacollector.lite.metrics.hooking.TextEvent;
import cz.muni.irtis.datacollector.lite.metrics.mediaprojection.MediaProjectionProvider;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.MetricsDatabase;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenshotEntity;
import cz.muni.irtis.datacollector.lite.metrics.model.condition.IsScreenOn;
import cz.muni.irtis.datacollector.lite.metrics.model.util.screenshot.ImageTransmogrifier;
import cz.muni.irtis.datacollector.lite.metrics.Metric;
import cz.muni.irtis.datacollector.lite.metrics.optimizer.Optimizer;
import cz.muni.irtis.datacollector.lite.persistence.runtime.RuntimeCaptureScreenshots;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Capture screenshots
 */
public class Screenshot extends Metric {
    private static final String TAG = Screenshot.class.getSimpleName();
    public static final int IDENTIFIER = 728;

    private final int VIRT_DISPLAY_FLAGS =
            DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY |
                    DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC;

    private final HandlerThread handlerThread =
            new HandlerThread(getClass().getSimpleName(),
                    android.os.Process.THREAD_PRIORITY_BACKGROUND);

    private Handler handler;
    private WindowManager windowManager;
    private MediaProjection projection;
    private MediaProjection.Callback callback;
    private VirtualDisplay virtualDisplay;
    private ImageTransmogrifier imageTransmogrifier;
    private int width;
    private int height;
    private List<String> metadata;

    /**
     * Constructor.
     * Init system services, handler thread, get permission from params.
     * @param context context
     * @param delay
     */
    public Screenshot(Context context, int delay) {
        super(context, delay, Optimizer.buildExponential(context, delay, Time.MINUTE*10));

        windowManager = (WindowManager) getContext().getSystemService(WINDOW_SERVICE);

        addPrerequisity(new IsScreenOn());

        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
        metadata = new ArrayList<>();
        initScreenSize();
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }

    /**
     * Run the media projection.
     * Register callback with the image saver (ImageTransmogrifier).
     */
    @Override
    public void run() {
        Debug.getInstance().log(TAG, "run()");

        try {
            if (projection == null) {
                projection = MediaProjectionProvider.getInstance().getProjection();
            }
        } catch(IllegalStateException e) {
            Debug.getInstance().warning(TAG, "Cannot start already started MediaProjection");
            return;
        }

        imageTransmogrifier = new ImageTransmogrifier(this);
        callback = new MediaProjection.Callback() {
            @Override
            public void onStop() {
                virtualDisplay.release();
            }
        };
        if (projection != null && imageTransmogrifier != null) {
            virtualDisplay = projection.createVirtualDisplay(
                getClass().getSimpleName(),
                imageTransmogrifier.getWidth(),
                imageTransmogrifier.getHeight(),
                getContext().getResources().getDisplayMetrics().densityDpi,
                VIRT_DISPLAY_FLAGS,
                imageTransmogrifier.getSurface(),
                null,
                handler);
            projection.registerCallback(callback, handler);
        }

        BusProvider.build().register(this);

        RuntimeCaptureScreenshots.build(getContext()).start();
        setRunning(true);
    }

    @Override
    public void stop() {
        if (projection != null) {
            projection.stop();
            projection.unregisterCallback(callback);
            if (virtualDisplay != null) {
                virtualDisplay.release();
            }
            projection = null;
        }
        if (imageTransmogrifier != null) {
            imageTransmogrifier.close();
        }

        BusProvider.build().unregister(this);

        RuntimeCaptureScreenshots.build(getContext()).stop();
        setRunning(false);
    }

    @Override
    public long save(Object... params) {
        String url = (String) params[0];
        String metadata = (String) params[1];
        try {
            Long time = Time.getTime();
            Debug.getInstance().log(TAG, url + " | " + metadata);
            long id = getDatabase().getScreenshotRepository().insert(
                new ScreenshotEntity(time, url));
            if(metadata!=null && !metadata.isEmpty()) {
                getDatabase().getScreenshotsMetadataRepository().insert(
                        new ScreenshotMetadataEntity(time, new File(url).getName(), metadata));
            }
            return id;
        } catch (Exception e) {
            Debug.getInstance().exception(e);
            return -1;
        }
    }

//    @Override
//    protected SyncResource onSyncResourceProcess(Cursor cursor, SyncResource resource) {
//        String uri = cursor.getString(cursor.getColumnIndex("device_url"));
//
//        File file = new File(getContext().getExternalFilesDir(null), new File(uri).getName());
//        resource.put(file);
//
//        return resource;
//    }


    /**
     * Save the image URL & clean virtual display resources.
     * @param imagePath absolute path to image
     */
    public void finishCapture(String imagePath) {
        if (imagePath != null && !"".equals(imagePath)) {
            if (isPrerequisitiesSatisfied()) {
                save(imagePath, !metadata.isEmpty() ? new GsonBuilder().create().toJson(metadata) : null);
            }
            metadata.clear();
        }
    }

    @Subscribe
    public void onMetadataEvent(TextEvent event) {
        if(event!=null && event.getContent()!=null && !event.getContent().isEmpty()) {
            if(!metadata.contains(event.getContent())) {
                metadata.add(event.getContent());
            }
        }
    }

    public Handler getHandler() {
        return handler;
    }

    public WindowManager getWindowManager() {
        return windowManager;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }


    private void initScreenSize() {
        if (width > 0 && height > 0) {
            return;
        }

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();

        display.getSize(size);
        width = size.x;
        height = size.y;

        while (width * height > (2 << 19)) {
            width = width >> 1;
            height = height >> 1;
        }
    }

    static public void deleteAllFiles(Context context) {
        MetricsDatabase.getDatabase(context).getScreenshotRepository().deleteAll();

        File directory = Application.getInstance().getExternalFilesDir(null);
        if (directory.isDirectory()) {
            String[] children = directory.list();
            for (int i = 0; i < children.length; i++) {
                new File(directory, children[i]).delete();
            }
        }
    }
}
