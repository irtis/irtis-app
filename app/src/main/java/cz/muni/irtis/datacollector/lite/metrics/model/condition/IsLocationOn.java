package cz.muni.irtis.datacollector.lite.metrics.model.condition;

import android.content.Context;
import android.location.LocationManager;

public class IsLocationOn implements Condition {
    @Override
    public boolean check(Context context) {
        LocationManager manager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        boolean isGpsEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        return isGpsEnabled || isNetworkEnabled;
    }
}
