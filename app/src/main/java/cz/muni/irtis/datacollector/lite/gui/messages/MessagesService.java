package cz.muni.irtis.datacollector.lite.gui.messages;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import cz.muni.irtis.datacollector.lite.application.components.TaskServiceBase;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.application.widgets.Box;
import cz.muni.irtis.datacollector.lite.gui.messages.components.MessagesNotification;
import cz.muni.irtis.datacollector.lite.gui.messages.model.Message;
import cz.muni.irtis.datacollector.lite.gui.messages.model.MessagesModel;
import cz.muni.irtis.datacollector.lite.persistence.model.Events;

public class MessagesService extends TaskServiceBase {
    public static final String TAG = MessagesService.class.getSimpleName();
    private MessagesModel model;

    public MessagesService(Context context) {
        super(context, MessagesService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if(Network.isConnection()) {
            model = new MessagesModel(getContext());
            download();
            upload();
        }

        stop(getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(model!=null) {
            model.cancel();
        }
    }

    public static void start(Context context) {
        TaskServiceBase.startService(context, MessagesService.class);
    }

    public static void stop(Context context) {
        TaskServiceBase.stopService(context, MessagesService.class);
    }


    private void download() {
        model.download(new Repository.RepositoryListener<List<Message>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onDone(List<Message> items) {
                if (items!=null && !items.isEmpty()) {
                    Message item = items.get(items.size()-1);
                    MessagesNotification.build(getContext(), item).show();
                }
                Events.build(getContext()).log(Events.EVENT_MESSAGES_DOWNLOADED);
            }
        });
    }

    private void upload() {
        model.upload(new Repository.RepositoryListener<Boolean>() {
            @Override
            public void onStart() {
            }

            @Override
            public void onDone(Boolean result) {
                Events.build(getContext()).log(Events.EVENT_MESSAGES_UPLOADED);
            }
        });
    }
}
