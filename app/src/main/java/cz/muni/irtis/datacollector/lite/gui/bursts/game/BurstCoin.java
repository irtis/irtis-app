package cz.muni.irtis.datacollector.lite.gui.bursts.game;

public interface BurstCoin {
    Integer getValue();
}
