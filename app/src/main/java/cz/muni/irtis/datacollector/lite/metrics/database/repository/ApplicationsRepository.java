package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationEntity;

@Dao
public interface ApplicationsRepository {
    public static final String TABLE_NAME = "applications";

    /**
     * Add new entity to database.
     * @param item entity
     */
    @Insert
    long insert(ApplicationEntity item);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE name = :name")
    ApplicationEntity getIdByName(String name);
}
