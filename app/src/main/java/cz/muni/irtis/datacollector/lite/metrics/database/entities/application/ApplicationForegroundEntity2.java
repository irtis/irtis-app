package cz.muni.irtis.datacollector.lite.metrics.database.entities.application;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.EntityBase;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ApplicationsForegroundRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ApplicationsForegroundRepository2;

@Entity(tableName = ApplicationsForegroundRepository2.TABLE_NAME)
public class ApplicationForegroundEntity2 extends EntityBase {
    @ColumnInfo(name = "apps_name_id")
    @ForeignKey(entity = ApplicationEntity.class, childColumns = "appsNameId", parentColumns = "datetime")
    private Long appsNameId;
    @Ignore
    private String name;


    public ApplicationForegroundEntity2(Long datetime, Long appsNameId) {
        super(datetime);
        this.appsNameId = appsNameId;
    }

    public Long getAppsNameId() {
        return appsNameId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
