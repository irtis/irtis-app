package cz.muni.irtis.datacollector.lite.application.widgets.lists;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * See:
 *  - http://stackoverflow.com/questions/18608209/listview-without-scroll-on-android
 */
public class StaticListView extends LinearLayout {
	protected ExtendedListViewAdapter adapter;
	protected Observer observer = new Observer(this);

	protected ExtendedListView.OnItemClickListener onItemClickListener;


	public StaticListView(Context context)
	{
		super(context);
	}

	public StaticListView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public void setAdapter(ExtendedListViewAdapter adapter) {
		if(this.adapter != null) {
			this.adapter.unregisterDataSetObserver(observer);
		}
		this.adapter = adapter;
		adapter.registerDataSetObserver(observer);
		observer.onChanged();
	}

	private class Observer extends DataSetObserver {
		StaticListView context;

		public Observer(StaticListView context)
		{
			this.context = context;
		}

		@Override
		public void onChanged() {
			List<View> oldViews = new ArrayList<View>(this.context.getChildCount());
			for (int i = 0; i < this.context.getChildCount(); i++) {
				oldViews.add(this.context.getChildAt(i));
			}

			Iterator<View> iterator = oldViews.iterator();
			context.removeAllViews();
			for (int i = 0; i < context.getAdapter().getCount(); i++) {
				View convertView = iterator.hasNext() ? iterator.next() : null;
				View view = context.getAdapter().getView(i, convertView, context);
				if(view!=null) {
					context.addView(view);
					view.setId(i);
					view.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							if (onItemClickListener != null) {
								onItemClickListener.onClick(getAdapter(), getAdapter().getItem(v.getId()));
							}
						}
					});
				}
			}

			super.onChanged();
		}

		@Override
		public void onInvalidated() {
			context.removeAllViews();
			super.onInvalidated();
		}
	}

	public void setOnItemClickListener(ExtendedListView.OnItemClickListener listener) {
		this.onItemClickListener = listener;
	}

	public ExtendedListViewAdapter getAdapter() {
	    return this.adapter;
    }
}
