package cz.muni.irtis.datacollector.lite.metrics.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import cz.muni.irtis.datacollector.lite.metrics.database.repository.StepsRepository;

@Entity(tableName = StepsRepository.TABLE_NAME)
public class StepEntity extends EntityBase {
    @ColumnInfo(name = "steps")
    private Integer steps;

    public StepEntity(Long datetime, Integer steps) {
        super(datetime);
        this.steps = steps;
    }

    public Integer getSteps() {
        return steps;
    }
}
