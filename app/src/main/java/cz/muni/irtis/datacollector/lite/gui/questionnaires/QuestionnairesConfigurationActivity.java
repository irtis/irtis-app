package cz.muni.irtis.datacollector.lite.gui.questionnaires;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListView;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewAdapter;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewBuilder;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.QuestionnairesConfigurationDialog;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;


public class QuestionnairesConfigurationActivity extends AppCompatActivity {
    private RecyclerListViewAdapter list;
    private QuestionnairesModel model;

    static public class ViewHolder extends RecyclerListViewAdapter.ItemsListViewHolderBase {
        public ImageView vCheck;
        public TextView vTime;
        public TextView vTitle;

        public ViewHolder(RecyclerListViewAdapter adapter, View view) {
            super(adapter, view);
            vCheck = view.findViewById(R.id.check);
            vTime = view.findViewById(R.id.time);
            vTitle = view.findViewById(R.id.title);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaires_configuration);

        model = new QuestionnairesModel(this);

        // Build ListView with listeners (getting adapter as reference)
        list = RecyclerListViewBuilder.build(this)
            .setView(R.id.list)
            .setAdapter(new RecyclerListViewAdapter<Questionnaire, ViewHolder>(R.layout.activity_questionnaires_list_row))
            .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<Questionnaire, ViewHolder>() {
                @Override
                public ViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                    return new ViewHolder(adapter, view);
                }

                @Override
                public void onBind(ViewHolder holder, Questionnaire item) {
                    holder.vCheck.setVisibility(item.isCompleted() ? View.VISIBLE : View.INVISIBLE);
                    holder.vTitle.setText(item.getTitle());

                    String invocationTime = Settings.getSettings().getString("questionnaires_invocation_" + item.getIdentifier(), null);
                    if(invocationTime!=null) {
                        holder.vTime.setText(item.getIntervalHoursFormatted() + " | " + invocationTime);
                    } else {
                        holder.vTime.setText(item.getIntervalHoursFormatted() + " | " + getString(R.string.questionnaires_configuration_dialog_default_time_button));
                    }
                }
            })
            .setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<Questionnaire>() {
                @Override
                public void onClick(View view, Questionnaire item) {
                    new QuestionnairesConfigurationDialog(QuestionnairesConfigurationActivity.this, item, new QuestionnairesConfigurationDialog.OnSubmitListener() {
                        @Override
                        public void onSubmit(Questionnaire item, Integer hours, Integer minutes) {
                            model.setInvocations(item, hours, minutes, new Repository.RepositoryListener<Void>() {
                                @Override
                                public void onStart() {}

                                @Override
                                public void onDone(Void data) {
                                    get();
                                }
                            });
                        }

                        @Override
                        public void onCancel() {
                        }

                        @Override
                        public void onReset(Questionnaire item) {
                            model.resetInvocations(item, new Repository.RepositoryListener<Void>() {
                                @Override
                                public void onStart() {
                                }

                                @Override
                                public void onDone(Void data) {
                                    get();
                                }
                            });
                        }
                    }).show();
                }
            })
            .setOnScrollListener(new RecyclerListView.OnScrollListener() {
                @Override
                public void onScroll(Integer count) {
                    get(count);
                }
            })
            .create()
            .getAdapter();

        // Set the first contents of the ListView
        get();
    }


    public void get(int offset) {
        model.getItemsFuture(offset, 15, new Repository.RepositoryListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
            }

            @Override
            public void onDone(List<Questionnaire> items) {
                if(items!=null && !items.isEmpty()) {
                    list.addItems(items, true);
                }
            }
        });
    }
    public void get() {
        list.clear();
        list.refresh();
        get(0);
    }

}
