package cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;


import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.connection.AccessToken;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class InstagramService extends ServiceBase {
    private static final int RC_SIGN_IN = 837;


    public InstagramService(Context context, OnServiceListener onSignInListener, OnServiceListener onSignOutListener) {
        super(context, onSignInListener, onSignOutListener);

    }
    public InstagramService(Context context, OnServiceListener onSignInListener) {
        this(context, onSignInListener, null);
    }
    public InstagramService(Context context) {
        this(context, null, null);
    }


    @Override
    public boolean isConnected() {
        return AccessToken.getInstance().isValid();
    }

    public void signIn(Activity activity) {
        onSignInListener.onStart();
        startSignInActivity(activity, RC_SIGN_IN);
    }


    public void signOut() {
        onSignOutListener.onStart();
        AccessToken.getInstance().delete();
        onSignOutListener.onFinished(isConnected());
    }


    public AccessToken getAccessToken() {
        return AccessToken.getInstance();
    }


    static public void startSignInActivity(Activity activity, int code) {
        Intent intent = new Intent(activity, InstagramServiceSignInActivity.class);
        activity.startActivityForResult(intent, code);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RC_SIGN_IN:
                if(resultCode == RESULT_OK) {
                    onSignInListener.onFinished(true);
                } else if(resultCode == RESULT_CANCELED) {
                    Integer errorCode = -1;
                    if(data!=null) {
                        errorCode = data.getIntExtra("result_error", errorCode);
                        if (errorCode != -1) {
                            Exception exception = new AccessToken.AccessTokenErrorException(
                                    errorCode, data.getStringExtra("result_message"));
                        }
                    }
                    onSignInListener.onError(errorCode);
                }
                break;

        }
    }
}
