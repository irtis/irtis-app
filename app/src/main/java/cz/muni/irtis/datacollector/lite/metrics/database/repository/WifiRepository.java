package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi.WifiEntity;

@Dao
public interface WifiRepository {
    public static final String TABLE_NAME = "wifis";

    /**
     * Add new entity to database.
     * @param item entity
     */
    @Insert
    long insert(WifiEntity item);

    @Query("SELECT ssid FROM " + TABLE_NAME)
    List<String> getAllNames();

    @Query("SELECT datetime FROM " + TABLE_NAME + " WHERE ssid = :ssid")
    Long getIdBySSID(String ssid);
}
