package cz.muni.irtis.datacollector.lite.application.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

import cz.muni.irtis.datacollector.R;

/**
 * Created by Michal Schejbal on 20.07.2017.
 */
public class HtmlViewer extends WebView {
	private Integer fontsize = 18;
	private int textcolor = 0xFF222222;
	private String textalign = "justify";


	public HtmlViewer(Context context) {
		super(context);
		this.ini();
	}

	public HtmlViewer(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.ini();
	}

	public HtmlViewer(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.ini();
	}

	public HtmlViewer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		this.ini();
	}

	private void ini() {
		setBackgroundColor(getResources().getColor(android.R.color.transparent));
	}


	public void setContent(String content, String html) {
		if(content!=null && !content.isEmpty()) {
			setHtml(injectContent(content, html));
		}
	}

	public void setContent(String content) {
		if(content!=null && !content.isEmpty()) {
			this.setContent(content, getContext().getResources().getString(R.string.html));
		}
	}

	public String injectContent(String content, String html) {
		if(content!=null && !content.isEmpty()) {
			// Text
			html = html.replace("{textcolor}", String.format("#%06X", 0xFFFFFF & textcolor));
			html = html.replace("{textalign}", textalign);
			html = html.replace("{fontsize}", fontsize.toString());

			// Content
			html = html.replace("{content}", content);
		}
		return html;
	}

	public void setHtml(String html) {
		if(html!=null && !html.isEmpty()) {
			loadDataWithBaseURL("file:///android_asset/", html, "text/html", "UTF-8", null);
		}
	}

	public void setTextColor(int textcolor)	{
		this.textcolor = textcolor;
	}
	public void setFontSize(Integer size) {
		this.fontsize = size;
	}
	public void setTextAlign(String aligment) {
		this.textalign = aligment;
	}
}
