package cz.muni.irtis.datacollector.lite.gui.questionnaires.model;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;

public class NotificationsDays {

    static public NotificationsDays build() {
        return new NotificationsDays();
    }

    public List<NotificationsDay> getItems() {
        List<NotificationsDay> items = new ArrayList<>();
        for(int day=1; day<=7; day++) {
            items.add(getItem(day));
        }
        return items;
    }

    public NotificationsDay getItem(int day) {
        long starting = !NotificationsDay.isWeekend(day) ?
                Settings.getSettings().getLong(Settings.QUESTIONNAIRES_MORNING+"_"+day, Config.QUESTIONNAIRES_WEEK_MORNING_DEFAULT) :
                Settings.getSettings().getLong(Settings.QUESTIONNAIRES_MORNING+"_"+day, Config.QUESTIONNAIRES_WEEKEND_MORNING_DEFAULT);
        long ending = !NotificationsDay.isWeekend(day) ?
                Settings.getSettings().getLong(Settings.QUESTIONNAIRES_NIGHT+"_"+day, Config.QUESTIONNAIRES_WEEK_NIGHT_DEFAULT) :
                Settings.getSettings().getLong(Settings.QUESTIONNAIRES_NIGHT+"_"+day, Config.QUESTIONNAIRES_WEEKEND_NIGHT_DEFAULT);
        return NotificationsDay.build(day, starting, ending);
    }

    public NotificationsDay getItemByToday() {
        int today = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)-1;
        if(today==0) {
            today = 7;
        }
        return getItem(today);
    }
}
