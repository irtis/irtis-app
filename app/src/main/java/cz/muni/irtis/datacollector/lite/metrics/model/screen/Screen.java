package cz.muni.irtis.datacollector.lite.metrics.model.screen;

import android.content.Context;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.Metric;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenEntity;
import cz.muni.irtis.datacollector.lite.metrics.model.util.ScreenState;

/**
 * ActiveScreen
 *
 * This metrics takes care of screen on/off state
 */
public class Screen extends Metric {
    private static final String TAG = Screen.class.getSimpleName();
    public static final int IDENTIFIER = 247;

    public Screen(Context context, Integer delay) {
        super(context, delay);
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }

    @Override
    public void run() {
        setRunning(true);
        Boolean state = ScreenState.isOn(getContext());
        save(state);
    }

    @Override
    public void stop() {
        setRunning(false);
    }

    @Override
    public long save(Object... params) {
        Boolean state = (Boolean) params[0];
        try {
            Debug.getInstance().log(TAG, state.toString());
            return getDatabase().getScreenRepository().insert(new ScreenEntity(
                Time.getTime(),
                state
            ));
        } catch (Exception e) {
            Debug.getInstance().exception(e);
            return -1;
        }
    }


}
