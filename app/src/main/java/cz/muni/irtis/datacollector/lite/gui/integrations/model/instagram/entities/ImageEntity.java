package cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities;

import com.google.gson.annotations.SerializedName;

public class ImageEntity {
    @SerializedName("thumbnail")
    private FileEntity thumb;
    @SerializedName("low_resolution")
    private FileEntity low;
    @SerializedName("standard_resolution")
    private FileEntity standard;

    public FileEntity getThumb() {
        return thumb;
    }

    public FileEntity getLow() {
        return low;
    }

    public FileEntity getStandard() {
        return standard;
    }
}
