package cz.muni.irtis.datacollector.lite.metrics.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;

import cz.muni.irtis.datacollector.lite.metrics.database.repository.RuntimeRepository;

@Entity(tableName = RuntimeRepository.TABLE_NAME)
public class RuntimeEntity extends EntityBase {
    @ColumnInfo(name = "started")
    private Long started;
    @ColumnInfo(name = "stopped")
    private Long stopped;
    @ColumnInfo(name = "type")
    private Integer type;
    @ColumnInfo(name = "cause")
    private Integer cause;
    @ColumnInfo(name = "uploaded")
    private Long uploaded;

    @Ignore
    public RuntimeEntity(Long datetime, Long started, Integer type) {
        super(datetime);
        this.started = started;
        this.type = type;
        this.uploaded = null;
    }

    @Ignore
    public RuntimeEntity(Long datetime, Long started, Long stopped, Integer cause, Integer type) {
        super(datetime);
        this.started = started;
        this.stopped = stopped;
        this.cause = cause;
        this.type = type;
        this.uploaded = null;
    }

    public RuntimeEntity(Long datetime, Long started, Long stopped, Integer type, Long uploaded) {
        super(datetime);
        this.started = started;
        this.stopped = stopped;
        this.type = type;
        this.uploaded = uploaded;
    }

    public Long getStarted() {
        return started;
    }

    public void setStarted(Long started) {
        this.started = started;
    }

    public Long getStopped() {
        return stopped;
    }

    public void setStopped(Long stopped) {
        this.stopped = stopped;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCause() {
        return cause;
    }

    public void setCause(Integer cause) {
        this.cause = cause;
    }

    public Long getUploaded() {
        return uploaded;
    }

    public void setUploaded(Long uploaded) {
        this.uploaded = uploaded;
    }
}
