package cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionnaireEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionnaireNotificationEntity;

@Dao
public interface QuestionnairesNotificationsRepository {
    public static final String TABLE_NAME = "questionnaires_notifications";

    @Insert
    long insert(QuestionnaireNotificationEntity item);
    @Update
    void update(QuestionnaireNotificationEntity item);


    @Query("SELECT * FROM " + TABLE_NAME + " WHERE idLocalQuestionnaire = :idLocal AND uploaded IS NULL")
    List<QuestionnaireNotificationEntity> getNotUploaded(Long idLocal);
}
