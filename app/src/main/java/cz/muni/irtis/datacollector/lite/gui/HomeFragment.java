package cz.muni.irtis.datacollector.lite.gui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.components.NavigationFragmentBase;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.debug.DebugLogs;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.Repeat;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.application.widgets.Box;
import cz.muni.irtis.datacollector.lite.gui.integrations.IntegrationsActivity;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.facebook.FacebookIntegration;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.google.YoutubeIntegration;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.InstagramIntegration;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.twitter.TwitterIntegration;
import cz.muni.irtis.datacollector.lite.gui.messages.MessagesService;
import cz.muni.irtis.datacollector.lite.gui.permissions.PermissionsActivity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesFormActivity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesService;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.notifications.QuestionnairesNotification;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.gui.settings.SettingsActivity;
import cz.muni.irtis.datacollector.lite.metrics.Metric;
import cz.muni.irtis.datacollector.lite.metrics.database.MetricsDatabase;
import cz.muni.irtis.datacollector.lite.metrics.MetricsService;
import cz.muni.irtis.datacollector.lite.metrics.model.util.Storage;
import cz.muni.irtis.datacollector.lite.metrics.model.util.screenshot.ScreenshotStorageSize;
import cz.muni.irtis.datacollector.lite.metrics.synchronization.MetricsSynchronizationService;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.Burst;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.BurstsModel;
import cz.muni.irtis.datacollector.lite.persistence.model.Events;
import cz.muni.irtis.datacollector.lite.persistence.runtime.RuntimeBase;
import cz.muni.irtis.datacollector.lite.persistence.runtime.RuntimeCapture;

public class HomeFragment extends NavigationFragmentBase {
    private Switch swCapture;
    private Switch swCaptureScreenshots;
    private TextView tvStorage;
    private TextView tvLastSync;

    private Repeat hRefresh;

    private BurstsModel burstsModel;


    public HomeFragment() {
        super(R.layout.fragment_home);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        burstsModel = new BurstsModel(getContext());

        iniCapture();
        iniStatistics();
        iniPermissions();
        iniMetrics();
        iniIntegrations();

        findViewById(R.id.debug).setVisibility(Debug.isDebug() ? View.VISIBLE : View.GONE);
        if(Debug.isDebug()) {
            findViewById(R.id.debugLogs).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DebugLogs.build(getContext()).upload();
                }
            });
            findViewById(R.id.debugEvents).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Events.build(getContext()).upload();
                }
            });

            findViewById(R.id.debugCreateQuestionnaire).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    QuestionnairesModel model = new QuestionnairesModel(getContext());
//                    Questionnaire item = Questionnaire.buildFromAssets(getContext(), Questionnaire.getTemplatePathByIdentifier("debug-ranni-dotaznik-2021"));
//                    Questionnaire item = Questionnaire.buildFromAssets(getContext(), Questionnaire.getTemplatePathByIdentifier("debug-shrnujici-dotaznik-poslednich-14-dnu"));
//                    Questionnaire item = Questionnaire.buildFromAssets(getContext(), Questionnaire.getTemplatePathByIdentifier("debug-postburst"));
                    Questionnaire item = Questionnaire.buildFromAssets(getContext(), Questionnaire.getTemplatePathByIdentifier("debug-denni-dotaznik-ii-2021"));
                    item.setStarting(Time.getTime());   // Time frame start
                    item.setEnding(Time.getTime() + Time.HOUR); // Time frame end
                    item.setInvoking(Time.getTime() + Time.SECOND*5);   // Exact time when the questionnaire should be invoked
//                     item.setClosing() - do not set here, if you want to make the QuestionnairesService to invoke the questionnaire
//                    item.setClosing(Time.getTime() + Time.HOUR);
                    model.store(item);
                    QuestionnairesFormActivity.start(getContext(), item); // Open the questionnaire form activity to start filling
                }
            });


            findViewById(R.id.debugQuestionnairesService).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Settings.getSettings().edit().putLong(QuestionnairesService.SETTING_SYNC_LASTTIME, 0).commit();
                    QuestionnairesService.start(getContext());
                }
            });

            findViewById(R.id.debugMessagesService).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MessagesService.start(getContext());
                }
            });
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        refresh();

        // Start periodical refresh for the statistics
        if (hRefresh != null) {
            hRefresh.cancel();
        }
        hRefresh = Repeat.build(Time.SECOND, new Runnable() {
            @Override
            public void run() {
                refresh();
            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();

        // Stop periodical refresh
        if (hRefresh != null) {
            hRefresh.cancel();
            hRefresh = null;
        }

        burstsModel.cancel();
    }

    private void iniCapture() {
        // Metrics
        swCapture = findViewById(R.id.capture_switch);
        swCapture.setChecked(MetricsService.isRunning(getContext()));
        swCapture.setClickable(false);
        findViewById(R.id.capture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!Config.METRICS_BURSTS_ENABLED) {
                    toggleCapture();
                }
            }
        });

        // Screenshots
        swCaptureScreenshots = findViewById(R.id.capture_screenshots_switch);
        swCaptureScreenshots.setChecked(MetricsService.isMetricRunningScreenshots(getContext()));
        swCaptureScreenshots.setClickable(false);
        findViewById(R.id.capture_screenshots).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MetricsService.isRunning(getContext())) {
                    toggleCaptureScreenshots();
                }
            }
        });
    }

    private void iniStatistics() {
        iniStatisticsRunningTime();
        iniStatisticsStorage();
        iniStatisticsSync();
    }

    private void iniStatisticsRunningTime() {
        findViewById(R.id.runtime).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void iniStatisticsStorage() {
        findViewById(R.id.storage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        tvStorage = findViewById(R.id.storage_value);
    }

    private void iniStatisticsSync() {
        findViewById(R.id.sync).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Debug.isDebug()) {
                    Intent intent = new Intent(getContext(), SettingsActivity.class);
                    intent.putExtra(SettingsActivity.SELECTED_SCREEN, SettingsActivity.SCREEN_SYNC);
                    startActivity(intent);
                } else {
                    Box.yesno(getContext(), getString(R.string.sync_now), getString(R.string.sync_now_text), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(Network.isWIFI()) {
                                MetricsSynchronizationService.start(getContext());
                            } else {
                                Box.ok(getContext(), getString(R.string.sync_now_no_wifi), getString(R.string.sync_now_no_wifi_text));
                            }
                        }
                    });
                }
            }
        });

        tvLastSync = findViewById(R.id.sync_value);
    }


    private void iniPermissions() {
        findViewById(R.id.permissions).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), PermissionsActivity.class);
                intent.putExtra(PermissionsActivity.DISPLAY, PermissionsActivity.DISPLAY_OVERVIEW);
                startActivity(intent);
            }
        });
    }

    private void iniMetrics() {
        if(Debug.isDebug()) {
            findViewById(R.id.metrics).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), SettingsActivity.class);
                    intent.putExtra(SettingsActivity.SELECTED_SCREEN, SettingsActivity.SCREEN_METRICS);
                    startActivity(intent);
                }
            });
        } else {
            findViewById(R.id.metrics).setVisibility(View.GONE);
        }
    }

    private void iniIntegrations() {
        findViewById(R.id.integrations).setVisibility(View.GONE);
        findViewById(R.id.integrations).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), IntegrationsActivity.class);
                intent.putExtra(IntegrationsActivity.DISPLAY, IntegrationsActivity.DISPLAY_OVERVIEW);
                startActivity(intent);
            }
        });
    }


    public void toggleCapture() {
        swCapture.toggle();
        if(swCapture.isChecked()) {
            getMainActivity().startCapture();
        } else {
            getMainActivity().stopCapture();
        }
    }

    public void toggleCaptureScreenshots() {
        swCaptureScreenshots.toggle();
        if(swCaptureScreenshots.isChecked()) {
            if(Storage.isFreeSpace()) {
                getMainActivity().startCaptureScreenshots();
            } else {
                Box.ok(getContext(), getString(
                        R.string.main_capture_outofspace_title), getString(R.string.main_capture_outofspace_message));
            }
        } else {
            Settings.getSettings().edit()
                .putLong(Settings.MEDIAPROJECTION_PERMISSION_DISABLED_TIME, Time.getTime())
                .commit();
            getMainActivity().stopCaptureScreenshots();
        }
    }


    /**
     * refresh()
     *
     * Refresh all of the desired indicators of the state of the app
     */
    private void refresh() {
        SharedPreferences settings = Settings.getSettings();

        // Check if capturing isActive running
        swCapture.setChecked(MetricsService.isRunning(getContext()));
        // Check if screenshots capturing isActive running
        swCaptureScreenshots.setChecked(MetricsService.isMetricRunningScreenshots(getContext()));

        // Update capture schedule
        if(Config.METRICS_BURSTS_ENABLED) {
            findViewById(R.id.capture_schedule).setVisibility(View.VISIBLE);
            burstsModel.getItemClosest(new Repository.RepositoryListener<Burst>() {
                @Override
                public void onStart() {
                }
                @Override
                public void onDone(Burst item) {
                    TextView scheduled = findViewById(R.id.capture_scheduled);
                    if (item != null) {
                        if (item.isActive()) {
                            scheduled.setText(getString(R.string.main_capture_active)
                                    .replace("%s", item.getIdentifier())
                                    .replace("%t", item.getTimeFormatted()));
                        } else {
                            scheduled.setText(getString(R.string.main_capture_scheduled)
                                    .replace("%s", item.getIdentifier())
                                    .replace("%t", item.getTimeFormatted()));
                        }
                    } else {
                        scheduled.setText(getString(R.string.main_capture_inactive));
                    }
                }
            });
        } else {
            findViewById(R.id.capture_schedule).setVisibility(View.GONE);
        }

        // Runtime
        if(MetricsService.isRunning(getContext())) {
            RuntimeCapture.build(getContext()).getRuntimeElapsed(new RuntimeBase.GetRuntimeElapsedListener() {
                @Override
                public void onDone(String elapsed) {
                    ((TextView) findViewById(R.id.runtime_value))
                            .setText(elapsed);
                }
            });
        } else {
            ((TextView) findViewById(R.id.runtime_value))
                    .setText(RuntimeCapture.calculateElapsedTime(Time.getTime()));
        }

        // Local storage
        ScreenshotStorageSize.build(getContext()).getSizeAsync(new ScreenshotStorageSize.GetSizeAsyncListener() {
            @Override
            public void onDone(long storage) {
                long database = MetricsDatabase.getDatabase(getContext()).getSize();
                tvStorage.setText(
                        Debug.isDebug() && false ?
                                (String.valueOf(ScreenshotStorageSize.toMB(storage)) + "MB/" + String.valueOf(ScreenshotStorageSize.toMB(database)) + "MB") :
                                (String.valueOf(ScreenshotStorageSize.toMB(storage) + ScreenshotStorageSize.toMB(database)) + "MB")
                );
            }
        });


        // Last sync
        if(MetricsSynchronizationService.isSyncing(getContext())) {
            tvLastSync.setText(getString(R.string.main_statistics_sync_running));
        } else {
            String last = settings.getLong(Settings.SYNCHRONIZATION_LAST, 0)!=0 ?
                    Time.getTimeStamp(settings.getLong(Settings.SYNCHRONIZATION_LAST, 0), "dd.MM.YYYY HH:mm:ss") : getString(R.string.main_never);
            tvLastSync.setText(last);
        }


        // Permissions state
        TextView vPermissionsValue = findViewById(R.id.permissions_value);
        vPermissionsValue.setText(getString(getMainActivity().isGrantedPermissions() ?
                R.string.main_permissions_value_granted : R.string.main_permissions_value_denied));
        vPermissionsValue.setTextColor(ContextCompat.getColor(getContext(), getMainActivity().isGrantedPermissions() ?
                R.color.colorPositive : R.color.colorNegative));

        // Metrics state running
        if(Debug.isDebug()) {
            TextView metricsValue = findViewById(R.id.metrics_value);
            List<Metric> metricsRunning = MetricsService.getMetricsRunning();
            if(metricsRunning!=null) {
                Iterator<Metric> iMetricsRunning = metricsRunning.iterator();
                StringBuilder metricsRunningBuilder = new StringBuilder();
                while (iMetricsRunning.hasNext()) {
                    Metric running = iMetricsRunning.next();
                    metricsRunningBuilder.append((metricsRunningBuilder.length() > 0 ? ", " : "") + running.getName());
                }
                metricsValue.setText(metricsRunningBuilder.toString());
            } else {
                metricsValue.setText(getString(R.string.main_none));
            }
        }

        // Integrations
        TextView vIntegrationsValue = findViewById(R.id.integrations_value);
        String integtrationsValues = "";
        if(YoutubeIntegration.build(getContext()).isConnected()) {
            integtrationsValues += integtrationsValues.isEmpty() ? getString(R.string.youtube_integration_title) : ", " + getString(R.string.youtube_integration_title);
        }
        if(FacebookIntegration.build(getContext()).isConnected()) {
            integtrationsValues += integtrationsValues.isEmpty() ? getString(R.string.facebook_integration_title) : ", " + getString(R.string.facebook_integration_title);
        }
        if(InstagramIntegration.build(getContext()).isConnected()) {
            integtrationsValues += integtrationsValues.isEmpty() ? getString(R.string.instagram_integration_title) : ", " + getString(R.string.instagram_integration_title);
        }
        if(TwitterIntegration.build(getContext()).isConnected()) {
            integtrationsValues += integtrationsValues.isEmpty() ? getString(R.string.twitter_integration_title) : ", " + getString(R.string.twitter_integration_title);
        }
        vIntegrationsValue.setText(integtrationsValues.isEmpty() ? getString(R.string.main_none) : integtrationsValues);
        vIntegrationsValue.setTextColor(ContextCompat.getColor(getContext(), integtrationsValues.isEmpty() ?
                R.color.colorAccent : R.color.colorPositive));

    }

}
