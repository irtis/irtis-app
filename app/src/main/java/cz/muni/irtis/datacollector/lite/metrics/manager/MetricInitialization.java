package cz.muni.irtis.datacollector.lite.metrics.manager;

import cz.muni.irtis.datacollector.lite.metrics.Metric;

/**
 * MetricsInitialization
 *
 * A simple factory for metric initialization
 */
public class MetricInitialization {
    private Metric metric;
    private Integer interval;

    private MetricInitialization(Metric metric, Integer interval) {
        this.metric = metric;
        this.interval = interval;
    }

    static public MetricInitialization build(Metric metric, Integer interval) {
        return new MetricInitialization(metric, metric.getDelay());
    }

    static public MetricInitialization build(Metric metric) {
        return build(metric, metric.getDelay());
    }

    public Metric getMetric() {
        return metric;
    }

    public Integer getInterval() {
        return interval;
    }
}
