package cz.muni.irtis.datacollector.lite.persistence.runtime;

import android.content.Context;

/**
 * RuntimeCapture
 *
 * Invocation of this metric isActive guided by localbroadcast
 *
 * Caution: This metric isActive not supposed to be run from TaskScheduler
 */
public class RuntimeCaptureScreenshots extends RuntimeBase {
    private RuntimeCaptureScreenshots(Context context) {
        super(context);
    }

    static public RuntimeCaptureScreenshots build(Context context) {
        return new RuntimeCaptureScreenshots(context);
    }

    @Override
    public void start() {
        started(TYPE_CAPTURE_SCREEN);
    }

    @Override
    public void stop() {
        stopped(TYPE_CAPTURE_SCREEN);
    }
}
