package cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.AnswersRepository;

@Entity(
    tableName = AnswersRepository.TABLE_NAME,
    primaryKeys = {
        "questionnaire_id", "question_id", "answer_id"
    }
)
public class AnswerEntity {
    @NonNull
    @ColumnInfo(name = "questionnaire_id")
    private Long idQuestionnaire;
    @NonNull
    @ColumnInfo(name = "question_id")
    private Long idQuestion;
    @NonNull
    @ColumnInfo(name = "answer_id")
    private Long idAnswer;
    @ColumnInfo(name = "value")
    private String value;


    public AnswerEntity(@NonNull Long idQuestionnaire, @NonNull Long idQuestion, @NonNull Long idAnswer, String value) {
        this.idQuestionnaire = idQuestionnaire;
        this.idQuestion = idQuestion;
        this.idAnswer = idAnswer;
        this.value = value;
    }

    public Long getIdQuestionnaire() {
        return idQuestionnaire;
    }

    public Long getIdQuestion() {
        return idQuestion;
    }

    public Long getIdAnswer() {
        return idAnswer;
    }

    public String getValue() {
        return value;
    }

}
