package cz.muni.irtis.datacollector.lite.application.components;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;

import cz.muni.irtis.datacollector.lite.application.Applications;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;

abstract public class ServiceBase extends Service {
    protected String TAG;

    public ServiceBase(String tag) {
        this.TAG = tag;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Debug.getInstance().task(TAG, "onCreate()");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Debug.getInstance().task(TAG, "onCommand()");
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Debug.getInstance().task(TAG, "onTaskRemoved()");
    }

    @Override
    public void onDestroy() {
        Debug.getInstance().task(TAG, "onDestroy()");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Debug.getInstance().task(TAG, "onBind()");
        throw new IllegalStateException("Non-bindable service");
    }



    /**
     * setServiceEnabled()
     *
     * Save service running state. This can help determine whether was the service naturally terminated.
     * If it was terminated by the system, it will be setItem to true. If it isActive setItem to false, it was destroyed
     * at it end of life-cycle
     *
     * @param enabled
     */
    static public void setServiceEnabled(boolean enabled, Class<?> type) {
        SharedPreferences.Editor editor = Settings.getSettings().edit();
        editor.putBoolean(type.getSimpleName().toLowerCase()+"_enabled", enabled);
        editor.commit();
    }

    /**
     * isServiceEnabled()
     *
     * Check the saved running state if the service was running before its life was ended.
     *
     * @param type
     * @return
     */
    static public boolean isServiceEnabled(Class<?> type) {
        return Settings.getSettings().getBoolean(
                type.getSimpleName().toLowerCase()+"_enabled", false);
    }

    /**
     * isRunning()
     *
     * Check if the service isActive running
     *
     * @param context
     * @return
     */
    static public boolean isServiceRunning(Context context, Class<?> type) {
        return Applications.isServiceRunning(context, type);
    }

    /**
     * start()
     *
     * Start the service
     *
     * @param context App context
     */
    public static void startService(Context context, Class<?> type, boolean foreground) {
        if (!isServiceRunning(context, type)) {
            Intent i = new Intent(context, type);
            if (foreground && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(i);
            } else {
                context.startService(i);
            }
            Debug.getInstance().task(type.getSimpleName(), "Service started");
        } else {
            Debug.getInstance().task(type.getSimpleName(), "Service isActive already running");
        }
    }
    public static void startService(Context context, Class<?> type) {
        startService(context, type, false);
    }

    /**
     * stop()
     *
     * Stop the service
     *
     * @param context
     * @param service
     */
    public static void stopService(Context context, Intent service, Class<?> type) {
        if (isServiceRunning(context, type)) {
            context.stopService(service);
            Debug.getInstance().task(type.getSimpleName(), "Service stopped");
        } else {
            Debug.getInstance().task(type.getSimpleName(), "Service can not be stopped since it isActive not running");
        }
    }
}
