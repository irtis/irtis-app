package cz.muni.irtis.datacollector.lite.gui.questionnaires;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.NavigationFragmentBase;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Repeat;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewAdapter;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewBuilder;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.QuestionnairesInvocationsDialog;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.QuestionnairesNotificationDayDialog;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.QuestionnairesPostponeDialog;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.QuestionnairesViewHolder;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.NotificationsDayViewHolder;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.notifications.QuestionnairesNotification;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.NotificationsDays;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.NotificationsDay;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;

public class QuestionnairesFragment extends NavigationFragmentBase {
    private RecyclerListViewAdapter activeList;
    private RecyclerListViewAdapter invocationsLabels;
    private RecyclerListViewAdapter invocationsStarting;
    private RecyclerListViewAdapter invocationsEnding;
    private RecyclerListViewAdapter invocationsMornings;
    private RecyclerListViewAdapter invocationsNights;
    private QuestionnairesModel model;

    private Repeat hRefresh;


    public QuestionnairesFragment() {
        super(R.layout.fragment_questionnaires);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        model = new QuestionnairesModel(getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        model.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(hRefresh!=null) {
            hRefresh.cancel();
        }
        hRefresh = Repeat.build(Time.SECOND, new Runnable() {
            @Override
            public void run() {
                get();
            }
        }, true);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Stop periodical refresh
        if (hRefresh != null) {
            hRefresh.cancel();
            hRefresh = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        ini();
        get();
        getInvocationsDays();
        getInvocationsMornings();
        getInvocationsNights();

        return view;
    }

    private void ini() {
        activeList = RecyclerListViewBuilder.build(getActivity())
            .setView(findViewById(R.id.active_list))
            .setAdapter(new RecyclerListViewAdapter<Questionnaire, QuestionnairesViewHolder>())
            .setOnLayoutListener(new RecyclerListViewAdapter.OnLayoutListener() {
                @Override
                public View onCreate(ViewGroup parent) {
                    return LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.component_questionnaires_timeout, parent, false);
                }
            })
            .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<Questionnaire, QuestionnairesViewHolder>() {
                @Override
                public QuestionnairesViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                    return new QuestionnairesViewHolder(adapter, view);
                }
                @Override
                public void onBind(QuestionnairesViewHolder holder, final Questionnaire item) {
                    holder.vTime.setText(item.getCreatedFormatted());
                    holder.vTitle.setText(item.getTitle());
                    holder.vRemainingProgress.setProgress(item.getClosedRemainingPercents());
                    holder.vRemainingValue.setText(item.getClosedRemainingFormatted());
                    holder.vPostpone.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new QuestionnairesPostponeDialog(getContext(), new QuestionnairesPostponeDialog.OnSubmitListener() {
                                @Override
                                public void onSubmit(int number) {
                                    model.setPostponed(item, number);
                                }
                                @Override
                                public void onCancel() {}
                            }).show();
                        }
                    });
                    holder.vPostpone.setVisibility(View.GONE);
                }
            })
            .setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<Questionnaire>() {
                @Override
                public void onClick(View view, Questionnaire item) {
                    QuestionnairesFormActivity.start(getContext(), item);
                }
            })
            .create()
            .getAdapter();
        activeList.getListView().setNestedScrollingEnabled(false);


        invocationsLabels = RecyclerListViewBuilder.build(getActivity())
                .setView(findViewById(R.id.questionnaires_invocations_labels))
                .setAdapter(new RecyclerListViewAdapter<NotificationsDay, NotificationsDayViewHolder>())
                .setOnLayoutListener(new RecyclerListViewAdapter.OnLayoutListener() {
                    @Override
                    public View onCreate(ViewGroup parent) {
                        return LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.component_questionnaires_invocations_day, parent, false);
                    }
                })
                .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<NotificationsDay, NotificationsDayViewHolder>() {
                    @Override
                    public NotificationsDayViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                        return new NotificationsDayViewHolder(adapter, view);
                    }
                    @Override
                    public void onBind(NotificationsDayViewHolder holder, final NotificationsDay item) {
                        holder.day.setText(item.getDayName());
                    }
                })
                .create()
                .getAdapter();
        invocationsLabels.getListView().setNestedScrollingEnabled(false);

        invocationsStarting = RecyclerListViewBuilder.build(getActivity())
            .setView(findViewById(R.id.questionnaires_invocations_starts))
            .setAdapter(new RecyclerListViewAdapter<NotificationsDay, NotificationsDayViewHolder>())
            .setOnLayoutListener(new RecyclerListViewAdapter.OnLayoutListener() {
                @Override
                public View onCreate(ViewGroup parent) {
                    return LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.component_questionnaires_invocations_time, parent, false);
                }
            })
            .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<NotificationsDay, NotificationsDayViewHolder>() {
                @Override
                public NotificationsDayViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                    return new NotificationsDayViewHolder(adapter, view);
                }
                @Override
                public void onBind(NotificationsDayViewHolder holder, final NotificationsDay item) {
                    holder.hours.setText(item.getStartingFormatted());
                }
            })
            .setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<NotificationsDay>() {
                @Override
                public void onClick(View view, final NotificationsDay item) {
                    new QuestionnairesNotificationDayDialog(getContext(), item, true, new QuestionnairesNotificationDayDialog.OnSubmitListener() {
                        @Override
                        public void onSubmit() {
                            getInvocationsDays();
                        }

                        @Override
                        public void onCancel() {

                        }
                    }).show();
                }
            })
            .create()
            .getAdapter();
        invocationsStarting.getListView().setNestedScrollingEnabled(false);
        invocationsStarting.getListView().setVisibility(View.GONE);
        findViewById(R.id.questionnaires_invocations_starts_head).setVisibility(View.GONE);

        invocationsEnding = RecyclerListViewBuilder.build(getActivity())
            .setView(findViewById(R.id.questionnaires_invocations_ends))
            .setAdapter(new RecyclerListViewAdapter<NotificationsDay, NotificationsDayViewHolder>())
            .setOnLayoutListener(new RecyclerListViewAdapter.OnLayoutListener() {
                @Override
                public View onCreate(ViewGroup parent) {
                    return LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.component_questionnaires_invocations_time, parent, false);
                }
            })
            .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<NotificationsDay, NotificationsDayViewHolder>() {
                @Override
                public NotificationsDayViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                    return new NotificationsDayViewHolder(adapter, view);
                }
                @Override
                public void onBind(NotificationsDayViewHolder holder, final NotificationsDay item) {
                    holder.hours.setText(item.getEndingFormatted());
                }
            })
            .setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<NotificationsDay>() {
                @Override
                public void onClick(View view, final NotificationsDay item) {
                    new QuestionnairesNotificationDayDialog(getContext(), item, false, new QuestionnairesNotificationDayDialog.OnSubmitListener() {
                        @Override
                        public void onSubmit() {
                            getInvocationsDays();
                        }

                        @Override
                        public void onCancel() {

                        }
                    }).show();
                }
            })
            .create()
            .getAdapter();
        invocationsEnding.getListView().setNestedScrollingEnabled(false);

        invocationsMornings = RecyclerListViewBuilder.build(getActivity())
            .setView(findViewById(R.id.questionnaires_invocations_mornings))
            .setAdapter(new RecyclerListViewAdapter<Questionnaire, NotificationsDayViewHolder>())
            .setOnLayoutListener(new RecyclerListViewAdapter.OnLayoutListener() {
                @Override
                public View onCreate(ViewGroup parent) {
                    return LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.component_questionnaires_invocations_time, parent, false);
                }
            })
            .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<Questionnaire, NotificationsDayViewHolder>() {
                @Override
                public NotificationsDayViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                    return new NotificationsDayViewHolder(adapter, view);
                }
                @Override
                public void onBind(NotificationsDayViewHolder holder, final Questionnaire item) {
                    if(item.getStarting()!=null) {
                        String invocationTime = model.getInvocationsByDay(item);
                        if(invocationTime!=null) {
                            holder.hours.setText(invocationTime);
                        } else {
                            holder.hours.setText(Time.getHour(item.getStarting()) + " - " + Time.getHour(item.getEnding()));
                        }
                    } else {
                        holder.hours.setText("-");
                    }
                }
            })
            .setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<Questionnaire>() {
                @Override
                public void onClick(View view, final Questionnaire item) {
                    if(item.getStarting()!=null) {
                        new QuestionnairesInvocationsDialog(getActivity(), item, new QuestionnairesInvocationsDialog.OnSubmitListener() {
                            @Override
                            public void onSubmit(Questionnaire item, Integer hours, Integer minutes) {
                                model.setInvocationsByDay(item, hours, minutes, new Repository.RepositoryListener<Void>() {
                                    @Override
                                    public void onStart() {}

                                    @Override
                                    public void onDone(Void data) {
                                        getInvocationsMornings();
                                    }
                                });
                            }

                            @Override
                            public void onCancel() {
                            }

                            @Override
                            public void onReset(Questionnaire item) {
                                model.resetInvocationsByDay(item, new Repository.RepositoryListener<Void>() {
                                    @Override
                                    public void onStart() {
                                    }

                                    @Override
                                    public void onDone(Void data) {
                                        getInvocationsMornings();
                                    }
                                });
                            }
                        }).show();
                    }
                }
            })
            .create()
            .getAdapter();
        invocationsMornings.getListView().setNestedScrollingEnabled(false);

        invocationsNights = RecyclerListViewBuilder.build(getActivity())
                .setView(findViewById(R.id.questionnaires_invocations_nights))
                .setAdapter(new RecyclerListViewAdapter<Questionnaire, NotificationsDayViewHolder>())
                .setOnLayoutListener(new RecyclerListViewAdapter.OnLayoutListener() {
                    @Override
                    public View onCreate(ViewGroup parent) {
                        return LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.component_questionnaires_invocations_time, parent, false);
                    }
                })
                .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<Questionnaire, NotificationsDayViewHolder>() {
                    @Override
                    public NotificationsDayViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                        return new NotificationsDayViewHolder(adapter, view);
                    }
                    @Override
                    public void onBind(NotificationsDayViewHolder holder, final Questionnaire item) {
                        if(item.getStarting()!=null) {
                            String invocationTime = model.getInvocationsByDay(item);
                            if(invocationTime!=null) {
                                holder.hours.setText(invocationTime);
                            } else {
                                holder.hours.setText(Time.getHour(item.getStarting()) + " - " + Time.getHour(item.getEnding()));
                            }
                        } else {
                            holder.hours.setText("-");
                        }
                    }
                })
                .setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<Questionnaire>() {
                    @Override
                    public void onClick(View view, final Questionnaire item) {
                        if(item.getStarting()!=null) {
                            new QuestionnairesInvocationsDialog(getActivity(), item, new QuestionnairesInvocationsDialog.OnSubmitListener() {
                                @Override
                                public void onSubmit(Questionnaire item, Integer hours, Integer minutes) {
                                    model.setInvocationsByDay(item, hours, minutes, new Repository.RepositoryListener<Void>() {
                                        @Override
                                        public void onStart() {}

                                        @Override
                                        public void onDone(Void data) {
                                            getInvocationsNights();
                                        }
                                    });
                                }

                                @Override
                                public void onCancel() {
                                }

                                @Override
                                public void onReset(Questionnaire item) {
                                    model.resetInvocationsByDay(item, new Repository.RepositoryListener<Void>() {
                                        @Override
                                        public void onStart() {
                                        }

                                        @Override
                                        public void onDone(Void data) {
                                            getInvocationsNights();
                                        }
                                    });
                                }
                            }).show();
                        }
                    }
                })
                .create()
                .getAdapter();
        invocationsNights.getListView().setNestedScrollingEnabled(false);
        invocationsNights.getListView().setVisibility(View.GONE);
        findViewById(R.id.questionnaires_invocations_nights_head).setVisibility(View.GONE);

        // Add invoked questionnaire
        ExtendedFloatingActionButton vAdd = findViewById(R.id.add);
        vAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                startActivity(QuestionnairesTemplatesListActivity.class);
            }
        });
//        vAdd.getDrawable().mutate()
//                .setTint(getResources().getColor(R.color.colorSecondary));


        // For testing purposes
        if(Debug.isDebug()) {
            findViewById(R.id.questionnaires_all).setVisibility(View.VISIBLE);
            findViewById(R.id.questionnaires_all).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(QuestionnairesListActivity.class);
                }
            });
        } else {
    
            findViewById(R.id.questionnaires_all).setVisibility(View.GONE);
        }
    }


    private void get() {
        model.getItemAvailableNewest(new Repository.RepositoryListener<Questionnaire>() {
            @Override
            public void onStart() {
            }

            @Override
            public void onDone(Questionnaire item) {
                activeList.clear();
                if(item!=null) {
                    activeList.addItem(item);
                }
                activeList.refresh();

                if (getEmptyView() != null) {
                    getEmptyView().setVisibility(activeList.isEmpty() ?
                            View.VISIBLE : View.GONE);
                }
            }
        });
    }


    private void getInvocationsDays() {
        invocationsLabels.setItems(NotificationsDays.build().getItems());
        invocationsLabels.refresh();

        invocationsStarting.setItems(NotificationsDays.build().getItems());
        invocationsStarting.refresh();
        invocationsEnding.setItems(NotificationsDays.build().getItems());
        invocationsEnding.refresh();
    }


    private void getInvocationsMornings() {
        model.getItemsFutureWeeklyByMorning(new Repository.RepositoryListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
                getLoadingView(R.id.loadingInvocations).show();
            }

            @Override
            public void onDone(List<Questionnaire> item) {
                invocationsMornings.setItems(item);
                invocationsMornings.refresh();
                getLoadingView(R.id.loadingInvocations).hide();
            }
        });
    }

    private void getInvocationsNights() {
        model.getItemsFutureWeeklyByNight(new Repository.RepositoryListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
            }

            @Override
            public void onDone(List<Questionnaire> item) {
                invocationsNights.setItems(item);
                invocationsNights.refresh();
            }
        });
    }
}
