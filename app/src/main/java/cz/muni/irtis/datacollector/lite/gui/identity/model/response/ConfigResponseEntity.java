package cz.muni.irtis.datacollector.lite.gui.identity.model.response;

import java.util.List;

import cz.muni.irtis.datacollector.lite.gui.bursts.model.Burst;

public class ConfigResponseEntity {
    private List<Burst> bursts;

    public List<Burst> getBursts() {
        return bursts;
    }
}
