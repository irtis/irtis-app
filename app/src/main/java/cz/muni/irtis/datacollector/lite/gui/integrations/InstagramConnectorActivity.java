package cz.muni.irtis.datacollector.lite.gui.integrations;

import android.os.AsyncTask;
import android.os.Bundle;

import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.InstagramIntegration;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.connection.Request;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities.CommentsEntity;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities.MediaEntity;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities.UserEntity;

public class InstagramConnectorActivity extends ConnectorActivityBase {
    private InstagramIntegration integration;


    public InstagramConnectorActivity() {
        super(R.layout.activity_integrations_instagram_connector);
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setIntegration(integration = new InstagramIntegration(this, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onError(int code) {
                refresh();
            }

            @Override
            public void onFinished(boolean isLogged) {
                Debug.getInstance().task(getIntegration().getName(), "Connected = " + (isLogged ? "true" : "false"));
                new DownloadSampleDataTask().execute();
                refresh();
            }
        }, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onError(int code) {
            }

            @Override
            public void onFinished(boolean isLogged) {
                Debug.getInstance().task(getIntegration().getName(), "Disconnected");
                refresh();
            }
        }));
    }


    private class DownloadSampleDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            integration.getProfile(new Request.OnRequestEntityListener<UserEntity>() {
                @Override
                public void onComplete(UserEntity item) {
                    Debug.getInstance().log(integration.getName(), "getProfile = "+(item!=null ? "true" : "false"));
                }
            });

            integration.getMedia(new Request.OnRequestEntityListener<List<MediaEntity>>() {
                @Override
                public void onComplete(List<MediaEntity> item) {
                    Debug.getInstance().log(integration.getName(), "getMedia = "+(item!=null ? "true" : "false"));
                    if(item!=null) {
                        item.get(0).getId();

                        integration.getComments(item.get(0), new Request.OnRequestEntityListener<List<CommentsEntity>>() {
                            @Override
                            public void onComplete(List<CommentsEntity> item) {
                                Debug.getInstance().log(integration.getName(), "getComments = "+(item!=null ? "true" : "false"));

                            }
                        });
                    }
                }
            });
            return null;
        }
    }
}
