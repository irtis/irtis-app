package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.StepEntity;

@Dao
public interface StepsRepository extends IRepository<StepEntity> {
    public static final String TABLE_NAME = "steps";

    /**
     * Add new entity to database.
     * @param item entity
     */
    @Insert
    long insert(StepEntity item);

    @Query("SELECT * " +
            "FROM "+TABLE_NAME+" " +
            "WHERE "+TABLE_NAME+".datetime = :time")
    StepEntity getByTime(Long time);

    @Query("SELECT * " +
            "FROM "+TABLE_NAME+" " +
            "WHERE "+TABLE_NAME+".datetime < :time")
    List<StepEntity> getPrevious(Long time);

    @Query("SELECT * " +
            "FROM "+TABLE_NAME+" " +
            "WHERE "+TABLE_NAME+".datetime < :time" + " " +
            "LIMIT :offset, :count")
    List<StepEntity> getPrevious(Long time, int offset, int count);

    /**
     * {@link IRepository#deleteById(Long)}
     */
    @Query("DELETE FROM " + TABLE_NAME + " WHERE datetime = :id")
    int deleteById(Long id);



}
