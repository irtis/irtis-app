package cz.muni.irtis.datacollector.lite.gui.messages.model;

import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.messages.database.MessageEntity;

public class Message {
    private Long id;
    private String text;
    private String response;
    private Long created;
    private Long downloaded;
    private Long notified;
    private Long opened;
    private Long read;
    private Long uploaded;

    public Message(Long id) {
        this.id = id;
    }

    public Message(MessageEntity entity) {
        this.id = entity.getId();
        this.text = entity.getText();
        this.response = entity.getResponse();
        this.created = entity.getCreated();
        this.downloaded = entity.getDownloaded();
        this.notified = entity.getNotified();
        this.opened = entity.getOpened();
        this.read = entity.getRead();
        this.uploaded = entity.getUploaded();
    }

    public static Message build(Long id) {
        return new Message(id);
    }

    public static Message build(MessageEntity entity) {
        return new Message(entity);
    }


    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public boolean isResponse() {
        return response!=null;
    }

    public Long getCreated() {
        return created;
    }

    public Long getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Long downloaded) {
        this.downloaded = downloaded;
    }

    public String getDownloadedFormatted() {
        return Time.getTimeStamp(getDownloaded(), "dd.MM.yyyy HH:mm");
    }

    public Long getNotified() {
        return notified;
    }

    public void setNotified(Long notified) {
        this.notified = notified;
    }

    public boolean isNotified() {
        return notified !=null && notified >0;
    }

    public Long getOpened() {
        return opened;
    }

    public void setOpened(Long opened) {
        this.opened = opened;
    }

    public boolean isOpened() {
        return opened !=null && opened >0;
    }

    public Long getRead() {
        return read;
    }

    public void setRead(Long read) {
        this.read = read;
    }

    public boolean isRead() {
        return read !=null && read >0;
    }

    public Long getUploaded() {
        return uploaded;
    }

    public void setUploaded(Long uploaded) {
        this.uploaded = uploaded;
    }
}
