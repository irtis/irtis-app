package cz.muni.irtis.datacollector.lite.application.debug;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.Application;
import cz.muni.irtis.datacollector.lite.application.database.StorageDatabase;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.Files;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.Burst;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.BurstsModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.QuestionnairesDatabase;
import cz.muni.irtis.datacollector.lite.metrics.database.MetricsDatabase;
import okhttp3.Response;

public class DebugLogs {
    private Context context;
    private File storage;

    private DebugLogs(Context context) {
        this.context = context;
        // Create storage directory: data/data/app/cache/logs
        this.storage = new File(Application.getInstance().getApplicationContext()
                .getCacheDir(), "logs");
        if(!this.storage.exists()) {
            this.storage.mkdir();
        }
    }

    static public DebugLogs build(Context context) {
        return new DebugLogs(context);
    }

    private List<File> getFiles() {
        List<File> files = new ArrayList<File>();

        if (storage.exists()) {
            File[] items = storage.listFiles();
            for (int i = 0; i < items.length; i++) {
                files.add(items[i]);
            }
        }

        return files;
    }


    /**
     * append()
     *
     * Create log
     *
     * @param row
     * @param name
     */
    public void append(String row, String name) {
        File file = new File(storage, name+".log");

        try {
            FileWriter f = new FileWriter(file.getPath(), true);
            f.write(getCurrentTimeStamp() + "\t" + row + "\n");
            f.close();
        } catch (IOException e) {
            // @fix recursion overflow
//            Debug.getInstance()
//                .exception(e, this, file.getPath());
        }
    }

    public void create(String content, String name) {
        File file = new File(storage, name+".log");

        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.append(content);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            // @fix recursion overflow
//            Debug.getInstance()
//                .exception(e, this, file.getPath());
        }
    }


    public void upload() {
        if(Network.isConnection()) {
            Boolean isEnabled = false;
            List<Burst> bursts = BurstsModel.build(context).getItems();
            if (bursts != null) {
                Iterator<Burst> iBursts = bursts.iterator();
                while (iBursts.hasNext()) {
                    Burst burst = iBursts.next();
                    if (burst.getStarting() - (Time.DAY*2) < Time.getTime() && burst.getEnding() + (Time.DAY*2) > Time.getTime()) {
                        isEnabled = true;
                        break;
                    }
                }
            }

            if (isEnabled) {
                // Send debug files
                List<File> files = getFiles();
                if (!files.isEmpty()) {
                    Iterator<File> iFiles = files.iterator();
                    while (iFiles.hasNext()) {
                        File file = iFiles.next();
                        if (Network.isWIFI()) {
                            new UploadTask(file).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                }

                // Send sqlite files
                files = Arrays.asList(new File[]{
                        MetricsDatabase.getDatabase(context).getFile(),
                        new File(MetricsDatabase.getDatabase(context).getFile().getAbsolutePath().replace(".db", ".db-wal")),
                        new File(MetricsDatabase.getDatabase(context).getFile().getAbsolutePath().replace(".db", ".db-shm")),
                        QuestionnairesDatabase.getDatabase(context).getFile(),
                        new File(QuestionnairesDatabase.getDatabase(context).getFile().getAbsolutePath().replace(".db", ".db-wal")),
                        new File(QuestionnairesDatabase.getDatabase(context).getFile().getAbsolutePath().replace(".db", ".db-shm")),
                        StorageDatabase.getDatabase(context).getFile(),
                        new File(StorageDatabase.getDatabase(context).getFile().getAbsolutePath().replace(".db", ".db-wal")),
                        new File(StorageDatabase.getDatabase(context).getFile().getAbsolutePath().replace(".db", ".db-shm")),
                });

                Iterator<File> iFiles = files.iterator();
                while (iFiles.hasNext()) {
                    File file = iFiles.next();
                    if (file.exists() && Network.isWIFI()) {
                        new UploadTask(file, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }
        }
    }


    private class UploadTask extends AsyncTask<Void, Void, Void> {
        private File file;
        private File temp;
        private String type;
        private Boolean isDeleteFile;


        public UploadTask(File file, boolean isDeleteFile) {
            this.file = file;
            this.isDeleteFile = isDeleteFile;
            String timeStamp = getCurrentTimeStamp();
            if(file.getPath().contains("databases")) {
                this.type = "database";
            } else if(file.getName().indexOf("-")!=-1) {
                this.type = file.getName().substring(0, file.getName().indexOf("-"));
            } else {
                this.type = file.getName().replace(".log", "");
            }

            String name = type;
            if(type.equals("exception")) {
                name = file.getName().replace(".log", "");
            } else if(type.equals("database")) {
                String extension = file.getName().substring(file.getName().indexOf(".")+1);
                name = type+"-"+file.getName()
                        .replace("_", "-")
                        .replace("."+extension, "") + "-" + timeStamp + "." + extension;
            } else {
                name = type + "-"+ timeStamp;
            }

            if(type.equals("database")) {
                temp = new File(storage.getAbsolutePath() + "/" + name);
            } else {
                temp = new File(storage.getAbsolutePath() + "/" + name + ".temp");
            }

            try {
                String o = file.getAbsolutePath();
                String t = temp.getAbsolutePath();
                Files.copy(file, temp);
            } catch (IOException e) {
                Log.e("DebugsLogs", "Cannot copy "+file.getAbsolutePath());
            }
        }

        public UploadTask(File file) {
            this(file, true);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (Connection.isAvailable()) {
                Connection.build(context).postFile(
                    UrlComposer.compose("events/" + type + "/upload"),
                    temp,
                    new Connection.OnConnectionListener() {
                        @Override
                        public void onStart() {
                        }

                        @Override
                        public void onError(Exception e) {
                            temp.delete();
                        }

                        @Override
                        public void onDone(Response response, Object content) {
                            if(isDeleteFile) {
                                file.delete();
                            }
                            temp.delete();
                        }
                    }, false
                );
            } else {
                Debug.getInstance().task(this.getClass().getSimpleName(), "Service isActive unable to connect to the host");
                temp.delete();
            }
            return null;
        }
    }

    public File getStorage() {
        return storage;
    }

    public String getCurrentTimeStamp() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        Date time = new Date();
        return format.format(time);
    }
}
