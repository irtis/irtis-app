package cz.muni.irtis.datacollector.lite.gui.integrations.model.facebook;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.internal.ServerProtocol;
import com.facebook.login.DefaultAudience;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;

import java.util.Collections;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;

public class FacebookService extends ServiceBase {
    private static final int RC_SIGN_IN = 4165;

    private LoginManager loginManager;
    private CallbackManager callbackManager;
    private FacebookSignInOptions options;



    public FacebookService(Context context, FacebookSignInOptions options, final OnServiceListener onSignInListener, final OnServiceListener onSignOutListener) {
        super(context, onSignInListener, onSignOutListener);

        this.options = options;
        if(this.options==null) {
            this.options = FacebookSignInOptions.build();
        }

        loginManager = LoginManager.getInstance();
        callbackManager = CallbackManager.Factory.create();

        loginManager.setDefaultAudience(options.getDefaultAudience());
        loginManager.setLoginBehavior(LoginBehavior.NATIVE_WITH_FALLBACK);
        loginManager.setAuthType(ServerProtocol.DIALOG_REREQUEST_AUTH_TYPE);

        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if(onSignInListener!=null) {
                    onSignInListener.onFinished(loginResult != null && loginResult.getAccessToken() != null);
                }
            }
            @Override
            public void onCancel() {
                if(onSignInListener!=null) {
                    onSignInListener.onError(ERROR_CANCELED);
                }
            }
            @Override
            public void onError(FacebookException e) {
                Debug.getInstance().exception(e, this, "FacebookSignIn:failed reason="+e.getMessage());
                if(onSignInListener!=null) {
                    onSignInListener.onError(0);
                }
            }
        });

        new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken accessToken, AccessToken accessToken2) {
                if (accessToken2 == null) {
                    if(onSignOutListener!=null) {
                        onSignOutListener.onFinished(false);
                    }
                }
            }
        };
    }
    public FacebookService(Context context, FacebookSignInOptions options, OnServiceListener onSignInListener) {
        this(context, options, onSignInListener, null);
    }
    public FacebookService(Context context, FacebookSignInOptions options) {
        this(context, options, null, null);
    }
    public FacebookService(Context context) {
        this(context, null, null, null);
    }


    public AccessToken getAccessToken() {
        return AccessToken.getCurrentAccessToken();
    }

    @Override
    public boolean isConnected() {
        return getAccessToken()!=null;
    }

    public void signIn(Activity activity) {
        if(onSignInListener!=null) {
            onSignInListener.onStart();
        }
        loginManager.logIn(activity, options.getPermissions());
    }

    public void signOut() {
        if(onSignOutListener!=null) {
            onSignOutListener.onStart();
        }
        loginManager.logOut();
    }




    public void setSignInButton(final View button) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Network.isConnection()) {
                    signIn((Activity) button.getContext());
                } else {
                    if(onSignInListener!=null) {
                        onSignInListener.onError(GoogleSignInStatusCodes.NETWORK_ERROR);
                    }
                }
            }
        });
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    static class FacebookSignInOptions {
        private DefaultAudience defaultAudience;
        private List<String> permissions = Collections.emptyList();

        static public FacebookSignInOptions build() {
            return new FacebookSignInOptions();
        }

        public FacebookSignInOptions setDefaultAudience(DefaultAudience defaultAudience) {
            this.defaultAudience = defaultAudience;
            return this;
        }
        public DefaultAudience getDefaultAudience() {
            return defaultAudience;
        }

        public FacebookSignInOptions setPermissions(List<String> permissions) {
            this.permissions = permissions;
            return this;
        }
        public List<String> getPermissions() {
            return permissions;
        }
    }
}
