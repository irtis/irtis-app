package cz.muni.irtis.datacollector.lite.gui.questionnaires.model;


import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cz.muni.irtis.datacollector.lite.application.utils.Time;

public class NotificationsDay {
    public static final Integer DAY_SATURDAY = 6;
    public static final Integer DAY_SUNDAY = 7;

    private Integer day;
    private Long starting;
    private Long ending;
    private Boolean isWeekend;

    public NotificationsDay(Integer day, Long starting, Long ending, Boolean isWeekend) {
        this.day = day;
        this.starting = starting;
        this.ending = ending;
        this.isWeekend = isWeekend;
    }

    static public NotificationsDay build(Integer day, Long starting, Long ending) {
        return new NotificationsDay(day, starting, ending, isWeekend(day));
    }

    static public NotificationsDay build(Integer day) {
        return NotificationsDay.build(day, null, null);
    }

    static public boolean isWeekend(Integer day) {
        return Arrays.asList(new Integer[]{DAY_SATURDAY, DAY_SUNDAY}).contains(day);
    }

    public Integer getDay() {
        return day;
    }

    public String getDayName() {
        String name;
        if(day!=DAY_SUNDAY) {
            name = Arrays.asList(new DateFormatSymbols().getWeekdays()).get(day+1);
        } else {
            name = Arrays.asList(new DateFormatSymbols().getWeekdays()).get(1);
        }
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    public Long getStarting() {
        return starting;
    }

    public Integer getStartingHours() {
        return Integer.valueOf(format(starting,"%h"));
    }
    public Integer getStartingMinutes() {
        return Integer.valueOf(format(starting,"%m"));
    }

    public Long getEnding() {
        return ending;
    }

    public Integer getEndingHours() {
        return Integer.valueOf(format(ending,"%h"));
    }
    public Integer getEndingMinutes() {
        return Integer.valueOf(format(ending,"%m"));
    }

    public String getStartingFormatted() {
        return format(starting,"%h:%m");
    }

    public String getEndingFormatted() {
        return format(ending,"%h:%m");
    }

    public String getTimeFormatted() {
        return format(starting,"%h:%m")+" - "+format(ending, "%h:%m");
    }

    public Boolean isWeekend() {
        return isWeekend;
    }

    public Boolean isActive() {
        if(getStartingHours() <= Time.getHour() && getEndingHours() >= Time.getHour()) {
            if(getStartingHours() == Time.getHour() && getStartingMinutes() > Time.getMinute()) {
                return false;
            }
            if(getEndingHours() == Time.getHour() && getEndingMinutes() < Time.getMinute()) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * format()
     *
     * @param time
     * @param format  %h:%m
     * @return
     */
    static public String format(Long time, String format) {
        String output = "";
        time /= Time.SECOND;
        Long seconds = time % 60;
        Long minutes = (time / 60) % 60;
        Long hours = (time / 60 / 60) % 24;
        Long days = time / 60 / 60 / 24;
        if(!format.contains("%d")) {
            hours = time / 60 / 60;
        }

        Matcher matcher = Pattern.compile("(\\%h|\\%m)+?").matcher(format);
        while (matcher.find()) {
            String part = matcher.group();
            if(output.isEmpty()) {
                output = format;
            }
            if (part.equals("%h")) {
                output = output.replace(part, String.format("%02d", hours));
            } else if (part.equals("%m")) {
                output = output.replace(part, String.format("%02d", minutes));
            }
        }
        if(output.isEmpty()) {
            output = String.format("%02d:%02d", hours, minutes);
        }

        return output;
    }
}
