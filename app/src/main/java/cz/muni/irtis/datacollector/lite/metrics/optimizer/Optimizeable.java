package cz.muni.irtis.datacollector.lite.metrics.optimizer;

public interface Optimizeable {
    /**
     *
     * @return true if optimized
     */
    boolean optimize();
}
