package cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.connection;

import android.content.Context;
import android.content.SharedPreferences;

import cz.muni.irtis.datacollector.lite.Application;

public class AccessToken {
    public static final String SETTING_KEY_ACCESS_TOKEN = "instagram_access_token";

    static private AccessToken instance;
    private String token;


    public AccessToken() {
    }

    static public AccessToken getInstance() {
        if(instance==null) {
            instance = new AccessToken();
        }
        return instance;
    }

    public String getToken() {
        if(token==null) {
            token = getSettings().getString(SETTING_KEY_ACCESS_TOKEN, null);
        }
        return token;
    }

    public boolean isValid() {
        return getToken()!=null;
    }

    public void save(String token) {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putString(SETTING_KEY_ACCESS_TOKEN, token);
        editor.commit();
    }

    public void delete() {
        save(null);
        token = null;
    }


    private SharedPreferences getSettings() {
        Context context = Application.getInstance().getApplicationContext();
        SharedPreferences settings = context.getSharedPreferences("com.instagram.signin", Context.MODE_PRIVATE);

        return settings;
    }


    /**
     * AccessTokenErrorException
     *
     * Exception for token retrieval
     */
    static public class AccessTokenErrorException extends Exception {
        private int code;

        public AccessTokenErrorException(int code, String message) {
            super(message);
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }
}
