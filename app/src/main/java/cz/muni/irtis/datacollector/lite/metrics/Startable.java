package cz.muni.irtis.datacollector.lite.metrics;

/**
 * Startable
 *
 * Allows the metric to be executed by a standard execution process (MetricsManager)
 */
public interface Startable {
    void start();
}
