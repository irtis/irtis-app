package cz.muni.irtis.datacollector.lite.metrics.database.entities.phone;

import androidx.room.ColumnInfo;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.EntityBase;


abstract public class PhoneEntity extends EntityBase {
    @ColumnInfo(name = "uploaded")
    private Long uploaded;


    public PhoneEntity(Long datetime) {
        super(datetime);
    }

    public Long getUploaded() {
        return uploaded;
    }

    public void setUploaded(Long uploaded) {
        this.uploaded = uploaded;
    }
}
