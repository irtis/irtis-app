package cz.muni.irtis.datacollector.lite.metrics.synchronization.components;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.components.JobServiceBase;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.synchronization.MetricsSynchronizationService;

public class SynchronizationJob extends JobServiceBase {
    public static final int JOB_ID = 4196;
    public static final int SYNC_TIME_RETRY = 30;

    @Override
    public boolean onStartJob(JobParameters params) {
        MetricsSynchronizationService.start(getApplicationContext());
        return true;
    }


    public static void schedule(Context context) {
        schedule(context, Config.SYNCHRONIZATION_REFRESH_PERIOD);
    }

    /**
     * schedule()
     *
     * Restrictions
     * - each 2hours
     * - only wifi (it isActive checked somewhere else if this isActive met)
     * - not on low battery
     *
     * @param context
     * @param minutes
     */
    public static void schedule(Context context, int minutes) {
        ComponentName serviceComponent = new ComponentName(context, SynchronizationJob.class);

        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, serviceComponent);
        builder.setOverrideDeadline(minutes * Time.MINUTE);
        builder.setMinimumLatency((minutes-1) * Time.MINUTE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setRequiresBatteryNotLow(true);
        }
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);


        // Save the next and the very first last sync
        SharedPreferences settings = Settings.getSettings();
        SharedPreferences.Editor editor = settings.edit();
        if(settings.getLong(Settings.SYNCHRONIZATION_LAST, 0)==0) {
            editor.putLong(Settings.SYNCHRONIZATION_LAST, Time.getTime());
        }
        editor.putLong(Settings.SYNCHRONIZATION_NEXT, Time.getTime() + (minutes * Time.MINUTE));
        editor.commit();

        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(builder.build());
    }

    public static boolean isSet(Context context) {
        return JobServiceBase.isSet(context, JOB_ID);
    }
}

