package cz.muni.irtis.datacollector.lite.gui.integrations;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.google.api.services.youtube.YouTubeScopes;
import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.Subscription;

import java.io.IOException;
import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.google.GoogleAccounts;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.google.GoogleService;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.google.YoutubeIntegration;

public class YoutubeConnectorActivity extends ConnectorActivityBase {
    private GoogleAccounts accounts;
    private YoutubeIntegration integration;

    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;

    public YoutubeConnectorActivity() {
        super(R.layout.activity_integrations_connector_youtube);
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        accounts = new GoogleAccounts(this, new String[] { YouTubeScopes.YOUTUBE_READONLY });

        setIntegration(integration = new YoutubeIntegration(this, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onError(int code) {
                refresh();
            }

            @Override
            public void onFinished(boolean isLogged) {
                Debug.getInstance().task(getIntegration().getName(), "Connected = " + (isLogged ? "true" : "false"));
                new DownloadSampleDataTask().execute();
                refresh();
            }
        }, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onError(int code) {
            }

            @Override
            public void onFinished(boolean isLogged) {
                Debug.getInstance().task(getIntegration().getName(), "Disconnected");
                refresh();
            }
        }));
    }



    public void connect() {
        if (!Network.isConnection()) {

        } else if (!GoogleService.isGooglePlayServicesAvailable(this)) {
            Dialog dialog = GoogleService.getGooglePlayServicesErrorDialog(this, null);
            if(dialog!=null) {
                dialog.show();
            }
        } else if (!accounts.isAccounts()) {
            // @todo there are no accounts to be used to connect to youtube
        } else {
            getIntegration().connect(this);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==REQUEST_PERMISSION_GET_ACCOUNTS) {
        }
    }

    private class DownloadSampleDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Channel item = integration.getChannelBySelf();
                Debug.getInstance().log(integration.getName(), "getChannelBySelf = "+(item!=null ? "true" : "false"));

                List<Subscription> items = integration.getSubscriptionsBySelf();
                Debug.getInstance().log(integration.getName(), "getSubscriptionsBySelf = "+(items!=null ? "true" : "false"));

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
