package cz.muni.irtis.datacollector.lite.persistence.model;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.GsonBuilder;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Identity;
import cz.muni.irtis.datacollector.lite.metrics.MetricsService;
import cz.muni.irtis.datacollector.lite.persistence.AliveWatcherJob;
import cz.muni.irtis.datacollector.lite.persistence.PersistentService;
import okhttp3.Response;

/**
 * AliveWatcher
 *
 * This is not supposed to run on UI Thread!
 */
public class AliveWatcher {
    static private AliveWatcher instance;
    private Context context;

    // Application runtime
    private Long started;
    private Long stopped;


    private AliveWatcher(Context context) {
        this.context = context;
    }

    static public AliveWatcher build(Context context) {
        if(instance==null) {
            instance = new AliveWatcher(context);
        }
        return instance;
    }

    public static AliveWatcher getInstance() {
        if(instance!=null) {
            return instance;
        } else {
            return null;
        }
    }

    public AliveWatcher start() {
        started = Time.getTime();
        stopped = null;

        if(!AliveWatcherJob.isSet(context)) {
            AliveWatcherJob.schedule(context);
        }

        return this;
    }

    public AliveWatcher stop() {
        stopped = Time.getTime();

        if(AliveWatcherJob.isSet(context)) {
            AliveWatcherJob.cancel(context);
        }

        return this;
    }

    public void send() {
        if(Identity.getInstance().isLogged() && Network.isConnection()) {
            new SendTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            Debug.getInstance().warning(this.getClass().getSimpleName(),
                    "Unable to upload alive notification due to missing sign in");
        }
    }


    private class SendTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Connection.build(context).postJson(
                UrlComposer.compose("alive"),
                new GsonBuilder().create().toJson(Alive.build(
                    PersistentService.isRunning(context),
                    MetricsService.isRunning(context),
                    MetricsService.isMetricRunningScreenshots(context)
                )), new Connection.OnConnectionListener() {
                    @Override
                    public void onStart() {
                    }
                    @Override
                    public void onError(Exception e) {
                    }
                    @Override
                    public void onDone(Response response, Object content) {
                    }
                }, false
            );
            return null;
        }
    }
}
