package cz.muni.irtis.datacollector.lite.metrics;


import android.content.Context;
import android.util.Log;


import java.util.ArrayList;
import java.util.List;

import cz.muni.irtis.datacollector.lite.metrics.database.MetricsDatabase;
import cz.muni.irtis.datacollector.lite.metrics.model.condition.Condition;
import cz.muni.irtis.datacollector.lite.metrics.optimizer.Optimizer;

/**
 * Metric
 *
 * Metric base class. Defines the essential properties of a metric.
 * All metrics must be Runnable, Stoppable, Persistent, and Identifiable.
 * Other properties are advised to be defined on the side of particular
 * extensions of this class.
 */
abstract public class Metric implements Runnable, Stoppable, Persistent, Identifiable {
    private final String TAG = this.getClass().getSimpleName();

    private Context context;
    private int delay;
    private boolean isRunning = false;
    private MetricsDatabase db;
    private Optimizer optimizer;
    private List<Condition> prerequisitises;


    public Metric() {
    }

    public Metric(Context context, int delay) {
        this(context, delay, null);
    }

    public Metric(Context context, int delay, Optimizer optimizer) {
        this.context = context;
        this.delay = delay;
        this.optimizer = optimizer;

        prerequisitises = new ArrayList<>();
        db = MetricsDatabase.getDatabase(context);
    }


    public Context getContext() {
        return context;
    }

    public int getDelay() {
        return delay;
    }

    public MetricsDatabase getDatabase() {
        return db;
    }


    /**
     * isRunning()
     *
     * Check if the metric is being executed
     * @return
     */
    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public void setOptimizer(Optimizer optimizer) {
        this.optimizer = optimizer;
    }

    public Optimizer getOptimizer() {
        return optimizer;
    }


    /**
     * addPrerequisity()
     *
     * Add a prerequisity that conditions the metric to be executed.
     * All such are checked in isPrerequisitiesSatisfied() method which must be called manually.
     *
     * @param condition
     */
    public void addPrerequisity(Condition condition) {
        prerequisitises.add(condition);
    }

    /**
     * getPrerequisity()
     *
     * Get prerequisity by a type.
     *
     * @param type
     * @return
     */
    public <T extends Condition> T getPrerequisity(Class<T> type) {
        for (int i = 0; i < prerequisitises.size(); i++) {
            if (type.isInstance(prerequisitises.get(i))) {
                return type.cast(prerequisitises.get(i));
            }
        }
        throw new IllegalStateException("No metric of type '" + type.toString() + "' found.");
    }

    /**
     * isPrerequisitiesSatisfied()
     *
     * Check if all required prerequisities are satisfied.
     *
     * @return
     */
    protected boolean isPrerequisitiesSatisfied() {
        for (int i = 0; i < prerequisitises.size(); i++) {
            if (!prerequisitises.get(i).check(getContext())) {
                Log.d("Metric prerequisities:",
                        "`" + prerequisitises.get(i).getClass().getSimpleName() + "` condition not satisfied for `"+TAG+"`.");
                return false;
            }
        }
        return true;
    }
}