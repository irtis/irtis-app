package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.phone.PhoneSmsEntity;

@Dao
public interface PhoneSmsesRepository extends IRepository<PhoneSmsEntity> {
    public static final String TABLE_NAME = "phone_smses";

    /**
     * Add new entity to database.
     * @param item entity
     */
    @Insert
    long insert(PhoneSmsEntity item);

    /**
     * Get entity by it's PK datetime
      * @param time java style timestamp (1561544094*1000)
     * @return entity
     */
    @Query("SELECT * FROM " + TABLE_NAME + " WHERE datetime = :time")
    PhoneSmsEntity getByTime(Long time);

    /**
     * {@link IRepository#getPrevious(Long)}
     */
    @Query("SELECT * FROM " + TABLE_NAME + " " +
            "WHERE datetime < :time" + " " +
            "AND uploaded IS NULL")
    List<PhoneSmsEntity> getPrevious(Long time);

    /**
     * {@link IRepository#getPrevious(Long)}
     */
    @Query("SELECT * FROM " + TABLE_NAME + " " +
            "WHERE datetime < :time" + " " +
            "AND uploaded IS NULL" + " " +
            "LIMIT :offset, :count")
    List<PhoneSmsEntity> getPrevious(Long time, int offset, int count);

    @Query("SELECT MAX(message_date) FROM " + TABLE_NAME)
    Long getMaxMessageDatetime();

    /**
     * {@link IRepository#deleteById(Long)}
     */
//    @Query("DELETE FROM " + TABLE_NAME + " WHERE datetime = :id")
    @Query("UPDATE " + TABLE_NAME + " SET uploaded = :id WHERE datetime = :id AND uploaded IS NULL")
    int deleteById(Long id);

//    @Query("UPDATE " + TABLE_NAME + " SET uploaded = :time WHERE uploaded IS NULL")
//    void deletePrevious(Long time);
}
