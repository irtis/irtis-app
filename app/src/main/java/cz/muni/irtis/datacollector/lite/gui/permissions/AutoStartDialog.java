package cz.muni.irtis.datacollector.lite.gui.permissions;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;

import androidx.appcompat.app.AlertDialog;

import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.permissions.Permission;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionAutostart;
import cz.muni.irtis.datacollector.lite.application.widgets.Box;

/**
 * AutoStartDialog()
 *
 * On some systems (miui), we need to get autostart permission in order to run persistent service.
 *
 * The problem isActive that miui android distributions restrict all apps from automatically restarting their services by issuing autostart permission.
 * This permission setting can be found in installed application detail or in settings->permissions->autostart.
 * There are some developers who are white listed by the system.
 *
 * See https://stackoverflow.com/questions/39366231/how-to-check-miui-autostart-permission-programmatically
 */
public class AutoStartDialog extends PermissionDialogBase {

    public AutoStartDialog(Context context, Permission permission, PermissionDialogResult listener)
    {
        super(context, permission, AutoStartDialog.class.getSimpleName(), listener);
    }

    @Override
    protected AlertDialog onBuilderCreate(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getContext().getString(R.string.permissions_autostart))
            .setIcon(R.drawable.ic_autostart_black_24dp)
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent = new Intent();
                    try {
                        ComponentName component = getAutostartSettingsComponentName();
                        if(component!=null) {
                            intent.setComponent(component);
                            List<ResolveInfo> list = getContext().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                            if (list.size() > 0) {
                                getContext().startActivity(intent);
                                ((PermissionAutostart) getPermission()).setGranted();
                                if(getListener()!=null) {
                                    getListener().onDone(true);
                                }
                            }
                        } else {
                            ((PermissionAutostart) getPermission()).setGranted();
                            if(getListener()!=null) {
                                getListener().onDone(false);
                            }
                        }
                    } catch (Exception e) {
                        Debug.getInstance().exception(e);
                        ((PermissionAutostart) getPermission()).setGranted();
                        if(getListener()!=null) {
                            getListener().onDone(false);
                        }
                    }

                }
            });
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            builder.setMessage(Html.fromHtml(getContext().getString(R.string.permissions_autostart_dialog).replace("{appname}", getContext().getString(R.string.app_name)), Html.FROM_HTML_MODE_LEGACY));
        } else {
            builder.setMessage(Html.fromHtml(getContext().getString(R.string.permissions_autostart_dialog).replace("{appname}", getContext().getString(R.string.app_name))));
        }
        return builder.create();
    }


    private ComponentName getAutostartSettingsComponentName() {
        try {
            String manufacturer = android.os.Build.MANUFACTURER;
            String version = Build.VERSION.RELEASE!=null ? Build.VERSION.RELEASE.trim() : "";
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                if (!getContext().getPackageManager().queryIntentActivities(
                        new Intent().setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity")),
                        PackageManager.MATCH_DEFAULT_ONLY).isEmpty()) {
                    return new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity");
                } else {
                    return null;
                }
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                if (!getContext().getPackageManager().queryIntentActivities(
                        new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity")),
                        PackageManager.MATCH_DEFAULT_ONLY).isEmpty()) {
                    return new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity");
                } else {
                    return null;
                }
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                if (!getContext().getPackageManager().queryIntentActivities(
                        new Intent().setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity")),
                        PackageManager.MATCH_DEFAULT_ONLY).isEmpty()) {
                    return new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity");
                } else {
                    return null;
                }
            } else if ("letv".equalsIgnoreCase(manufacturer)) {
                if (!getContext().getPackageManager().queryIntentActivities(
                        new Intent().setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity")),
                        PackageManager.MATCH_DEFAULT_ONLY).isEmpty()) {
                    return new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity");
                } else {
                    return null;
                }
            } else if ("huawei".equalsIgnoreCase(manufacturer) || "honor".equalsIgnoreCase(manufacturer)) {
                if(!getContext().getPackageManager().queryIntentActivities(
                        new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.startupmgr.ui.StartupNormalAppListActivity")),
                        PackageManager.MATCH_DEFAULT_ONLY).isEmpty()) {
                    return new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.startupmgr.ui.StartupNormalAppListActivity");
                } else if (!getContext().getPackageManager().queryIntentActivities(
                        new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.appcontrol.activity.StartupAppControlActivity")),
                        PackageManager.MATCH_DEFAULT_ONLY).isEmpty()) {
                    return new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.appcontrol.activity.StartupAppControlActivity");
                } else if (!getContext().getPackageManager().queryIntentActivities(
                        new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.appcontrol.activity.StartupAppControlActivity")),
                        PackageManager.MATCH_DEFAULT_ONLY).isEmpty()) {
                    return new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity");
                } else {
                    return null;
                }
            } else if("asus".equalsIgnoreCase(manufacturer)) {
                if (!getContext().getPackageManager().queryIntentActivities(
                        new Intent().setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.autostart.AutoStartActivity")),
                        PackageManager.MATCH_DEFAULT_ONLY).isEmpty()) {
                    return new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.autostart.AutoStartActivity");
                } else {
                    return null;
                }
            }
        } catch (Exception e) {
            Debug.getInstance().exception(e);
        }

        return null;
    }

//    private static final Intent[] POWERMANAGER_INTENTS = {
//            new Intent().setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity")),
//            new Intent().setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity")),
//            new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity")),
//            new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity")),
//            new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.startupapp.StartupAppListActivity")),
//            new Intent().setComponent(new ComponentName("com.oppo.safe", "com.oppo.safe.permission.startup.StartupAppListActivity")),
//            new Intent().setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.AddWhiteListActivity")),
//            new Intent().setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.BgStartUpManager")),
//            new Intent().setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity")),
//            new Intent().setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.entry.FunctionActivity")).setData(Uri.parse("mobilemanager://function/entry/AutoStart"))
//    };
}