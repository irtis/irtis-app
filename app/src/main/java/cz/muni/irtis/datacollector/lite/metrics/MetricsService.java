package cz.muni.irtis.datacollector.lite.metrics;

import android.content.Context;

import java.lang.reflect.Constructor;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.components.TaskServiceBase;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.hooking.AccessibilityNotification;
import cz.muni.irtis.datacollector.lite.metrics.manager.MetricDeclaration;
import cz.muni.irtis.datacollector.lite.metrics.manager.MetricsManager;
import cz.muni.irtis.datacollector.lite.metrics.mediaprojection.MediaProjectionNotification;
import cz.muni.irtis.datacollector.lite.metrics.mediaprojection.MediaProjectionProvider;
import cz.muni.irtis.datacollector.lite.application.utils.Repeat;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.Burst;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.BurstsModel;
import cz.muni.irtis.datacollector.lite.persistence.model.Events;
import cz.muni.irtis.datacollector.lite.persistence.runtime.RuntimeCapture;
import cz.muni.irtis.datacollector.lite.metrics.model.screen.Screenshot;
import cz.muni.irtis.datacollector.lite.persistence.PersistentService;

/**
 * MetricsService
 *
 * Metrics service takes care of an execution of metrics in the background.
 */
public class MetricsService extends TaskServiceBase {
    public static final String TAG = MetricsService.class.getSimpleName();

    private Repeat hCheckRepeat;
    private MetricsManager manager;


    public MetricsService(Context context) {
        super(context, MetricsService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Mark service as enabled (=running)
        setServiceEnabled(true, MetricsService.class);

        // Deal with withdrawn/lost MediaProjection permission
        if (hCheckRepeat == null) {
            hCheckRepeat = Repeat.build(Config.METRICS_AUTORUN_PERIOD, new Runnable() {
                @Override
                public void run() {
                    autorun();
                }
            }, true);
        }

        Config config = Config.build();
        manager = new MetricsManager();
        if(config.getMetrics()!=null && !config.getMetrics().isEmpty()) {
            Iterator<MetricDeclaration> iEnabled = config.getMetrics().iterator();
            while (iEnabled.hasNext()) {
                MetricDeclaration declaration = iEnabled.next();
                try {
                    Constructor<?> construct = declaration.getMetric().getConstructor(Context.class, Integer.class);
                    Metric metric = (Metric) construct.newInstance(new Object[]{getContext(), declaration.getInterval()});
                    Integer interval = declaration.isOnce() ? MetricsManager.ONCE : declaration.getInterval();
                    manager.add(metric, interval);
                } catch (Exception e) {
                    Debug.getInstance().exception(e);
                }
            }
        }

        manager.start();

        RuntimeCapture.build(getContext()).start();

        PersistentService.getInstance().getNotification()
            .restart();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(manager!=null) {
            if(manager.isRunning(Screenshot.IDENTIFIER)) {
                manager.remove(Screenshot.IDENTIFIER);
            }
            manager.stop();
        }

        RuntimeCapture.build(getContext()).stop();

        if(hCheckRepeat!=null) {
            hCheckRepeat.cancel();
            hCheckRepeat = null;
        }

        PersistentService.getInstance().getNotification()
            .restart();

        setServiceEnabled(false, MetricsService.class);
    }


    /**
     * isEnabled()
     *
     * Check if the service was running before termination
     *
     * @return
     */
    static public boolean isEnabled() {
        return TaskServiceBase.isServiceEnabled(MetricsService.class);
    }

    static public void setEnabled(boolean enabled) {
        TaskServiceBase.setServiceEnabled(enabled, MetricsService.class);
    }

    /**
     * isRunning()
     *
     * Check if the service isActive running
     *
     * @param context
     * @return
     */
    static public boolean isRunning(Context context) {
        return TaskServiceBase.isServiceRunning(context, MetricsService.class);
    }


    /**
     * start()
     *
     * Run only if the there is an active burst!
     */
    public static void start(final Context context) {
        if(Config.METRICS_BURSTS_ENABLED) {
            BurstsModel.build(context).getItemClosest(new Repository.RepositoryListener<Burst>() {
                @Override
                public void onStart() {
                }
                @Override
                public void onDone(Burst item) {
                    if (item!=null && item.isActive()) {
                        if (!isRunning(context)) {
                            Events.build(context).log(Events.EVENT_CAPTURING_STARTED);
                            TaskServiceBase.startService(context, MetricsService.class);
                        }
                    } else {
                        if (isRunning(context)) {
                            TaskServiceBase.stopService(context, MetricsService.class);
                            Events.build(context).log(Events.EVENT_CAPTURING_STOPPED);
                        }
                    }
                }
            });
        } else {
            if (!isRunning(context) && isEnabled()) {
                Events.build(context).log(Events.EVENT_CAPTURING_STARTED);
                TaskServiceBase.startService(context, MetricsService.class);
            }
        }
    }

    /**
     * stop()
     *
     * Stop the service
     *
     */
    public static void stop(Context context) {
        TaskServiceBase.stopService(context, MetricsService.class);
        Events.build(context).log(Events.EVENT_CAPTURING_STOPPED);
    }

    public static void should(final Context context) {
        if(Config.METRICS_BURSTS_ENABLED) {
            BurstsModel.build(context).getItemClosest(new Repository.RepositoryListener<Burst>() {
                @Override
                public void onStart() {
                }
                @Override
                public void onDone(Burst item) {
                    if (item!=null && item.isActive()) {
                    } else {
                        if (isRunning(context)) {
                            TaskServiceBase.stopService(context, MetricsService.class);
                            Events.build(context).log(Events.EVENT_CAPTURING_STOPPED);
                        }
                    }
                }
            });
        }
    }


    public void startMetric(int identifier)  {
        if (manager != null) {
            switch (identifier) {
                case Screenshot.IDENTIFIER:
                    if (MediaProjectionProvider.getInstance().isToken()) {
                        if (!manager.isRunning(identifier)) {
                            if (!manager.has(identifier)) {
                                Metric metric = new Screenshot(getContext(), Config.METRICS_SCREENSHOTS_PERIOD);
                                manager.add(metric, MetricsManager.ONCE);
                                Events.build(getContext()).log(Events.EVENT_CAPTURING_SCREENSHOTS_STARTED);
                                Debug.getInstance().warning(MetricsService.TAG,
                                "Starting metric: "+identifier+" | Origin: added");
                            } else {
                                manager.restart();
                                Debug.getInstance().warning(MetricsService.TAG,
                                "Starting metric: "+identifier+" | Origin: restart");
                            }
                        } else {
                            Debug.getInstance().warning(MetricsService.TAG,
                            "Cannot start metric: "+identifier+" | Reason: the metric is already running");
                        }
                    } else {
                        Debug.getInstance().warning(MetricsService.TAG,
                        "Cannot start metric: "+identifier+" | Reason: the token is false");
                    }
                    break;

                default:
                    // @todo run any other metric
                    break;
            }

            PersistentService.getInstance().getNotification()
                    .restart();
        }
    }

    public void stopMetric(int identifier)  {
        manager.remove(identifier);

        if(Screenshot.IDENTIFIER == identifier) {
            Events.build(getContext()).log(Events.EVENT_CAPTURING_SCREENSHOTS_STOPPED);
        }

        PersistentService.getInstance().getNotification()
                .restart();
    }

    public boolean isMetric(int identifier)  {
        if(manager!=null) {
            return manager.has(identifier);
        } else {
            return false;
        }
    }

    public boolean isMetricRunning(int identifier)  {
        if(manager!=null) {
            return manager.isRunning(identifier);
        } else {
            return false;
        }
    }

    public boolean isMetricEnabled(int identifier) {
        return Settings.getSettings().getBoolean("metrics_"+identifier+"_enabled", false);
    }

    public void setMetricEnabled(int identifier, boolean enabled) {
        Settings.getSettings().edit()
            .putBoolean("metrics_"+identifier+"_enabled", enabled)
            .commit();
    }


    static public void startMetric(Context context, int identifier)  {
        MetricsService service = (MetricsService) TaskServiceBase.getService(MetricsService.class);
        if(service!=null) {
            service.setMetricEnabled(identifier, true);
            service.startMetric(identifier);
        } else {
            Debug.getInstance().warning(MetricsService.TAG,
            "Cannot start metric: "+identifier+" | Reason: the service is null");
        }
    }

    static public void stopMetric(Context context, int identifier)  {
        MetricsService service = (MetricsService) TaskServiceBase.getService(MetricsService.class);
        if(service!=null) {
            service.setMetricEnabled(identifier, false);
            service.stopMetric(identifier);
        }
    }

    static public boolean isMetricRunning(Context context, int identifier) {
        MetricsService service = (MetricsService) TaskServiceBase.getService(MetricsService.class);
        if(service!=null) {
            return service.isMetricRunning(identifier);
        } else {
            return false;
        }
    }

    static public boolean isMetricEnabled(Context context, int identifier) {
        MetricsService service = (MetricsService) TaskServiceBase.getService(MetricsService.class);
        if(service!=null) {
            return service.isMetricEnabled(identifier);
        } else {
            return false;
        }
    }


    static public void startMetricScreenshots(Context context) {
        startMetric(context, Screenshot.IDENTIFIER);
    }
    static public void stopMetricScreenshots(Context context) {
        stopMetric(context, Screenshot.IDENTIFIER);
    }
    static public boolean isMetricRunningScreenshots(Context context) {
        return isMetricRunning(context, Screenshot.IDENTIFIER);
    }
    static public boolean isMetricEnabledScreenshots(Context context) {
        return isMetricEnabled(context, Screenshot.IDENTIFIER);
    }


    static public List<Metric> getMetricsRunning()  {
        MetricsService service = (MetricsService) TaskServiceBase.getService(MetricsService.class);
        if(service!=null) {
            if(service.manager!=null) {
                return service.manager.getRunning();
            } else {
                return null;
            }
        } else {
            return null;
        }


    }

    private void autorun() {
        // Run screenshots if set as enabled
        if(isMetricEnabled(Screenshot.IDENTIFIER)) {
            if (!isMetricRunning(Screenshot.IDENTIFIER)) {
                if (!MediaProjectionProvider.getInstance().isGranted()) {
                    MediaProjectionNotification.build(getContext()).show();
                }
            }

            if (!PermissionsProvider.build(getContext()).isGrantedAccessibilityPermission()) {
                AccessibilityNotification.build(getContext()).show();
            }
        }

        // @todo autorun another
    }

    static public Integer getFrameNumber(Long time) {
        Integer hour = Time.getHour(time);
        if(hour >= 8 && hour < 10) {
            return 1;
        } else if(hour >= 10 && hour < 15) {
            return 2;
        } else if(hour >= 15 && hour < 20) {
            return 3;
        } else if(hour >= 20 && hour < 22) {
            return 4;
        } else {
            return null;
        }
    }
}
