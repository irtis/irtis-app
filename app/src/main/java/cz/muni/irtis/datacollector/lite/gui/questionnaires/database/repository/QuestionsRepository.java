package cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import androidx.sqlite.db.SupportSQLiteQuery;

import java.util.List;

import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionEntity;

@Dao
public interface QuestionsRepository {
    public static final String TABLE_NAME = "questionnaires_questions";

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void store(QuestionEntity item);

    @Insert
    long insert(QuestionEntity item);

    @Query("DELETE FROM "+TABLE_NAME+" WHERE questionnaire_id = :idQuestionnaire")
    void delete(Long idQuestionnaire);

    @Update
    void update(QuestionEntity item);


    @Query("SELECT * FROM " + TABLE_NAME + " WHERE questionnaire_id = :idQuestionnaire AND question_id = :idQuestion")
    QuestionEntity getItem(Long idQuestionnaire, Long idQuestion);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE questionnaire_id = :idQuestionnaire")
    List<QuestionEntity> getItems(Long idQuestionnaire);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE questionnaire_id = :idQuestionnaire LIMIT :offset, :count")
    List<QuestionEntity> getAll(Long idQuestionnaire, int offset, int count);

    @Query("SELECT COUNT(*) FROM " + TABLE_NAME + " WHERE questionnaire_id = :idQuestionnaire")
    Integer getItemsCount(Long idQuestionnaire);

    @Query("SELECT COUNT(*) FROM " + TABLE_NAME + " WHERE questionnaire_id = :idQuestionnaire AND skipped IS NOT NULL")
    Integer getItemsCountAnswers(Long idQuestionnaire);

    @Query("SELECT COUNT(*) FROM " + TABLE_NAME + " WHERE questionnaire_id = :idQuestionnaire AND skipped = 0")
    Integer getItemsCountAnswered(Long idQuestionnaire);
}
