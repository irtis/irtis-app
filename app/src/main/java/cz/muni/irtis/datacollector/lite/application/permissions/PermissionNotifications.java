package cz.muni.irtis.datacollector.lite.application.permissions;

import android.content.Context;

import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_NOTIFICATIONS;
import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_OVERLAY;

/**
 * PermissionNotifications
 *
 */
public class PermissionNotifications extends Permission {

    public PermissionNotifications(Context context, String title, String notes) {
        super(context, PERMISSION_NOTIFICATIONS, title, notes);
    }

    public boolean isGranted()
    {
        return provider.isGrantedNotificationsPermission();
    }

}
