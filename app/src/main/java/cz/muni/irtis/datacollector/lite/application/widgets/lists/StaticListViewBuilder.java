package cz.muni.irtis.datacollector.lite.application.widgets.lists;

import android.app.Activity;

import java.util.ArrayList;


/**
 * RecyclerListViewBuilder
 *
 * Builder for RecyclerListView
 */
public class StaticListViewBuilder {
    private Activity activity;
    private int resourceId;
    private ExtendedListViewAdapter adapter;

    private ExtendedListViewAdapter.OnLayoutListener onItemLayoutListener;
    private ExtendedListViewAdapter.OnHolderListener onHolderListener;
    private ExtendedListView.OnItemClickListener onItemClickListener;


    public StaticListViewBuilder(Activity activity) {
        this.activity = activity;
    }

    static public StaticListViewBuilder build(Activity activity) {
        return new StaticListViewBuilder(activity);
    }

    /**
     * setView()
     *
     * @param id resource id of the view
     * @return
     */
    public StaticListViewBuilder setView(int id) {
        this.resourceId = id;
        return this;
    }

    public StaticListViewBuilder setAdapter(ExtendedListViewAdapter adapter) {
        this.adapter = adapter;
        return this;
    }


    /**
     * setOnLayoutListener()
     * <br><br>
     * Setting the layout of item row of the recycler list view
     * <br><br>
     * Initialization of the callback should be done like this:<br>
     *      setOnLayoutListener(new ExtendedListViewAdapter.OnLayoutListener() {
     *          @Override
     *          public View onCreate(ViewGroup parent) {
     *              return LayoutInflater.from(parent.getContext())
     *                  .inflate(R.layout.list_row, parent, false);
     *          }
     *      })
     *
     * @param listener
     * @return StaticListViewBuilder
     */
    public StaticListViewBuilder setOnLayoutListener(ExtendedListViewAdapter.OnLayoutListener listener) {
        this.onItemLayoutListener = listener;
        return this;
    }

    /**
     * setOnHolderListener()
     * <br><br>
     * Setting the ViewHolder of the recycler list view
     * <br><br>
     * You need to declare ViewHolder class somewhere in your code like this:<br>
     *      public class ViewHolder extends ListViewHolderBase {
     *          TextView vTitle;
     *          public ViewHolder(ExtendedListViewAdapter adapter, View view) {
     *              super(adapter, view);
     *              vTitle = view.findViewById(R.id.title);
     *          }
     *      }
     * <br><br>
     * Initialization of the callback should be done like this:<br>
     *      setOnHolderListener(new ExtendedListViewAdapter.OnHolderListener<String, ViewHolder>() {
     *          @Override
     *          public ViewHolder onCreate(ExtendedListViewAdapter adapter, View view) {
     *              return new ViewHolder(adapter, view);
     *          }
     *
     *          @Override
     *          public void onBind(ViewHolder holder, String item) {
     *              holder.vTitle.setText(item);
     *          }
     *      })
     *
     * @param listener ExtendedListViewAdapter.OnHolderListener
     * @return StaticListViewBuilder
     */
    public StaticListViewBuilder setOnHolderListener(ExtendedListViewAdapter.OnHolderListener listener) {
        this.onHolderListener = listener;
        return this;
    }

    /**
     * setOnItemClickListener()
     * <br><br>
     * Setting the item onClick listener of the recycler list view
     * <br><br>
     * Initialization of the callback should be done like this:<br>
     *      setOnItemClickListener(new ExtendedListViewAdapter.OnItemClickListener<String>() {
     *          @Override
     *          public void onClick(View view, String item) {
     *              Debug.getInstance().box(Activity.this, "Content of the string", item);
     *          }
     *      })
     *
     * @param listener ExtendedListViewAdapter.OnItemClickListener
     * @return StaticListViewBuilder
     */
    public StaticListViewBuilder setOnItemClickListener(ExtendedListView.OnItemClickListener listener) {
        this.onItemClickListener = listener;
        return this;
    }



    /**
     * append()
     *
     * Build instance
     *
     * @return
     */
    public StaticListView create(ArrayList<Object> items) {
        StaticListView list = activity.findViewById(this.resourceId);
        if(this.adapter!=null) {
            list.setAdapter(this.adapter);
        } else {
            throw new NullPointerException("Adapter isActive null. You need to setAdapter() with an instance.");
        }
        if(this.onItemLayoutListener!=null) {
            this.adapter.setOnLayoutListener(this.onItemLayoutListener);
        }
        if(this.onHolderListener!=null) {
            this.adapter.setOnHolderListener(this.onHolderListener);
        } else {
            throw new NullPointerException("Holder listener isActive null. You need to setOnHolderListener() with the creation and binding of holder view.");
        }
        if(this.onItemClickListener!=null) {
            list.setOnItemClickListener(this.onItemClickListener);
        }
        if(items!=null) {
            this.adapter.setItems(items);
        }
        return list;
    }

    public StaticListView create() {
        return this.create(null);
    }
}
