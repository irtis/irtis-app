package cz.muni.irtis.datacollector.lite.gui.integrations;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.IntegrationBase;

abstract public class ConnectorActivityBase extends ActivityBase {
    protected IntegrationBase integration;
    protected int layoutResource;

    public ConnectorActivityBase(int layoutResource) {
        this.layoutResource = layoutResource;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutResource);

        findViewById(R.id.connect).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connect();
            }
        });
        findViewById(R.id.disconnect).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disconnect();
            }
        });
    }

    public void setIntegration(IntegrationBase integration) {
        this.integration = integration;
        refresh();
    }

    public IntegrationBase getIntegration() {
        return integration;
    }

    protected void refresh() {
        findViewById(R.id.connect).setVisibility(!integration.isConnected() ? View.VISIBLE : View.GONE);
        findViewById(R.id.disconnect).setVisibility(!integration.isConnected() ? View.GONE : View.VISIBLE);
    }

    public void connect() {
        if (Network.isConnection()) {
            getIntegration().connect(this);
        } else {
            // @todo
        }
    }

    public void disconnect() {
        getIntegration().disconnect();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        integration.onActivityResult(requestCode, resultCode, data);
    }
}
