package cz.muni.irtis.datacollector.lite.application.permissions;

import android.content.Context;

abstract public class Permission {
    protected PermissionsProvider provider;

    protected String id;
    protected String title;
    protected String description;
    protected Boolean isRequired;

    public Permission() {}
    public Permission(Context context, String id, String title, String description)
    {
        this.id = id;
        this.title = title;
        this.description = description;
        this.isRequired = true;

        this.provider = new PermissionsProvider(context, false);
    }

    public String getId() { return this.id; }

    public String getTitle() { return this.title; }

    public String getDescription() { return this.description; }

    public Boolean isRequired() {
        return isRequired;
    }

    public void setRequired(Boolean required) {
        isRequired = required;
    }

    abstract public boolean isGranted();
}
