package cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.connection;

import cz.muni.irtis.datacollector.lite.application.network.Connection;

public class Request {
    static private String BASE_URL = "https://api.instagram.com";

    public interface OnRequestListener {
        public void onDone(Response response);
    }
    public interface OnRequestEntityListener<T> {
        public void onComplete(T entity);
    }


    public Request(AccessToken token, String path, final OnRequestListener listener) {
        String url = buildUrl(path, token);
        Connection.build().getJson(url, new Connection.OnConnectionListener<String>() {
            @Override
            public void onStart() {
            }

            @Override
            public void onError(Exception e) {

            }

            @Override
            public void onDone(okhttp3.Response r, String content) {
                Response response = null;
                if(content!=null && !content.isEmpty()) {
                    response = Response.buildFromJson(content);
                }
                listener.onDone(response);
            }
        });
    }

    static public Request build(AccessToken token, String path, final OnRequestListener listener) {
        return new Request(token, path, listener);
    }

    private String buildUrl(String path, AccessToken accessToken) {
        String token = accessToken.isValid() ? accessToken.getToken() : "";
        path = path.replaceAll("^/", "");
        return BASE_URL + "/v1/" + path + "/?access_token=" + token;
    }


}
