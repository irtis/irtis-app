package cz.muni.irtis.datacollector.lite.gui.messages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.PopupMenu;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.NavigationFragmentBase;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListView;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewAdapter;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewBuilder;
import cz.muni.irtis.datacollector.lite.gui.messages.components.MessagesNotification;
import cz.muni.irtis.datacollector.lite.gui.messages.database.MessageEntity;
import cz.muni.irtis.datacollector.lite.gui.messages.model.Message;
import cz.muni.irtis.datacollector.lite.gui.messages.model.MessagesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesTemplatesListActivity;
import cz.muni.irtis.datacollector.lite.persistence.model.Events;

public class MessagesFragment extends NavigationFragmentBase {
    private RecyclerListViewAdapter list;
    private MessagesModel model;


    public MessagesFragment() {
        super(R.layout.fragment_messages);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        model = new MessagesModel(getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        model.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        RecyclerListView recyclerListView = RecyclerListViewBuilder.build(getActivity())
            .setView(findViewById(R.id.list))
            .setAdapter(new RecyclerListViewAdapter<Message, MessagesViewHolder>())
            .setOnLayoutListener(new RecyclerListViewAdapter.OnLayoutListener() {
                @Override
                public View onCreate(ViewGroup parent) {
                    return LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.component_messages_item, parent, false);
                }
            })
            .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<Message, MessagesViewHolder>() {
                @Override
                public MessagesViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                    return new MessagesViewHolder(adapter, view);
                }

                @Override
                public void onBind(MessagesViewHolder holder, final Message item) {
                    holder.time.setText(item.getDownloadedFormatted());
                    holder.text.setText(item.getText());

                    if(!item.isOpened()) {
                        model.setOpened(item, new Repository.RepositoryListener<Boolean>() {
                            @Override
                            public void onStart() {
                            }

                            @Override
                            public void onDone(Boolean data) {
                                item.setOpened(Time.getTime());
                            }
                        });
                    }

                    if(!item.isResponse()) {
                        holder.response.setVisibility(View.GONE);
                        holder.answer.setVisibility(View.VISIBLE);
                        holder.answer.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                new MessagesResponseDialog(getContext(), item, new MessagesResponseDialog.ReportResultListener() {
                                    @Override
                                    public void onDone(Boolean result) {
                                        get();
                                    }
                                }).show();
                            }
                        });
                    } else {
                        holder.answer.setVisibility(View.GONE);
                        holder.response.setVisibility(View.VISIBLE);
                        holder.response.setText(item.getResponse());
                    }

                    if(!item.isRead()) {
                        holder.read.setVisibility(View.GONE);
                        read(item);
                    } else {
                        holder.read.setVisibility(View.VISIBLE);
                    }
                }
            })
            .setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<Message>() {
                @Override
                public void onClick(View view, Message item) {
                }
            })
            .create();

        recyclerListView.setNestedScrollingEnabled(false);
        list = recyclerListView.getAdapter();

        // For testing purposes
        if(Debug.isDebug()) {
            FloatingActionButton vAdd = findViewById(R.id.add);
            vAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    final Message item = Message.build(new MessageEntity(
                        Time.getTime(),
                        "Hello, it is good to be in touch with you. We dedetcted some unusual activity in your phone. Please check that everything is OK.",
                        Time.getTime(),
                        Time.getTime(),
                        null,
                        null,
                        null));
                    model.store(item, new Repository.RepositoryListener<Boolean>() {
                        @Override
                        public void onStart() {
                        }

                        @Override
                        public void onDone(Boolean result) {
                            if (result) {
                                MessagesNotification.build(getContext(), item).show();
                                Snackbar.make(view, "A new message was successfully created", Snackbar.LENGTH_LONG).show();
                                get();
                            }
                        }
                    });
                }
            });

            vAdd.getDrawable().mutate()
                    .setTint(getResources().getColor(R.color.colorSecondary));

        } else {
            findViewById(R.id.add).setVisibility(View.GONE);
        }

        get();

        return view;
    }


    private void get() {
        model.getItems(new Repository.RepositoryListener<List<Message>>() {
            @Override
            public void onStart() {
                getLoadingView().show();
                getEmptyView().setVisibility(View.GONE);
            }

            @Override
            public void onDone(List<Message> items) {
                if(items!=null && !items.isEmpty()) {
                    list.setItems(items);
                    list.refresh();
                } else {
                    getEmptyView().setVisibility(View.VISIBLE);
                }
                getLoadingView().hide();
            }
        });
    }

    private void read(final Message item) {
        if(item!=null) {
            model.setRead(item, new Repository.RepositoryListener<Boolean>() {
                @Override
                public void onStart() {
                }
                @Override
                public void onDone(Boolean result) {
                    if(result) {
                        list.refresh();
                        Debug.getInstance().warning(MessagesService.TAG,
                            "Instance read | Value: "+(item.getText()!=null ? item.getText() : "null"));
                        Events.build(getContext()).log(Events.EVENT_MESSAGES_READ);
                    }
                }
            });
        }
    }

//    private void response(final View view, final Message item) {
//        PopupMenu popup = new PopupMenu(getActivity(), view);
//        popup.getMenuInflater().inflate(R.menu.messages, popup.getMenu());
//        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem menuItem) {
//                switch(menuItem.getItemId()) {
//                    case R.id.ok:
//                        model.setResponse(item, "Ok", new Repository.RepositoryListener<Boolean>() {
//                            @Override
//                            public void onStart() {
//
//                            }
//
//                            @Override
//                            public void onDone(Boolean response) {
//
//                            }
//                        });
//                        return true;
//
//                    default:
//                        return false;
//                }
//            }
//        });
//
//        popup.show();
//    }
}
