package cz.muni.irtis.datacollector.lite.gui.bursts;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.NavigationFragmentBase;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewAdapter;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewBuilder;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.QuestionnairesViewHolder;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.Burst;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.BurstsModel;

public class BurstsFragment extends NavigationFragmentBase {
    private RecyclerListViewAdapter listActive;
    private RecyclerListViewAdapter listFuture;
    private RecyclerListViewAdapter listPast;
    private BurstsModel model;


    public BurstsFragment() {
        super(R.layout.fragment_bursts);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        model = new BurstsModel(getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        model.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        getRefreshView().setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getActive();
                getFuture();
                getPast();
            }
        });

        getLoadingView().setBackgroundColor(
                ContextCompat.getColor(getContext(), android.R.color.transparent));

        ini();
        getActive();
        getFuture();
        getPast();

        return view;
    }

    private void ini() {
        listActive = RecyclerListViewBuilder.build(getActivity())
                .setView(findViewById(R.id.list_active))
                .setAdapter(new RecyclerListViewAdapter<Burst, QuestionnairesViewHolder>())
                .setOnLayoutListener(new RecyclerListViewAdapter.OnLayoutListener() {
                    @Override
                    public View onCreate(ViewGroup parent) {
                        return LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.component_bursts_item, parent, false);
                    }
                })
                .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<Burst, BurstsViewHolder>() {
                    @Override
                    public BurstsViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                        return new BurstsViewHolder(adapter, view);
                    }
                    @Override
                    public void onBind(BurstsViewHolder holder, final Burst item) {
                        holder.vTitle.setText(item.getTitle());
                        holder.vTime.setText(item.getTimeFormattedShort());
                        holder.vPaceCurrent.setText(item.getPaceCurrentFormatted());

                        holder.vPace.setVisibility(item.getStarting() < Time.getTime() ? View.VISIBLE : View.GONE);
                        if (item.getPacePrevious() != null) {
                            ((View) holder.vPacePrevious.getParent()).setVisibility(View.VISIBLE);
                            holder.vPacePrevious.setText(item.getPacePreviousFormatted());
                            if (item.getPacePrevious() <= item.getPaceCurrent()) {
                                holder.vPacePrevious.setText((item.getPaceCurrent()-item.getPacePrevious())+"%");
                                holder.vPacePreviousIcon.setImageDrawable(getActivity().getDrawable(R.drawable.ic_trending_up_black_24dp));
                                holder.vPacePreviousIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorGreen));
                            } else if (item.getPacePrevious() > item.getPaceCurrent()) {
                                holder.vPacePrevious.setText((item.getPacePrevious()-item.getPaceCurrent())+"%");
                                holder.vPacePreviousIcon.setImageDrawable(getActivity().getDrawable(R.drawable.ic_trending_down_black_24dp));
                                holder.vPacePreviousIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorRed));
                            }
                        } else {
                            ((View) holder.vPacePrevious.getParent()).setVisibility(View.GONE);
                        }

                        if(item.getPurse()!=null) {
                            holder.vCoinsBronze.setText(item.getPurse().getCountBronze().toString());
                            holder.vCoinsSilver.setText(item.getPurse().getCountSilver().toString());
                            holder.vCoinsGold.setText(item.getPurse().getCountGold().toString());
                        }
                        holder.vProgress.setProgress(item.getProgress());
                    }
                })
                .setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<Burst>() {
                    @Override
                    public void onClick(View view, Burst item) {
                        startActivity(BurstsDetailActivity.class, item.getId().toString());
                    }
                })
                .create()
                .getAdapter();
        listActive.getListView().setNestedScrollingEnabled(false);


        listFuture = RecyclerListViewBuilder.build(getActivity())
            .setView(findViewById(R.id.list_future))
            .setAdapter(new RecyclerListViewAdapter<Burst, QuestionnairesViewHolder>())
            .setOnLayoutListener(new RecyclerListViewAdapter.OnLayoutListener() {
                @Override
                public View onCreate(ViewGroup parent) {
                    View view = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.component_bursts_item, parent, false);
//                    view.setAlpha(0.2f);
                    return view;
                }
            })
            .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<Burst, BurstsViewHolder>() {
                @Override
                public BurstsViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                    return new BurstsViewHolder(adapter, view);
                }
                @Override
                public void onBind(BurstsViewHolder holder, final Burst item) {
                    holder.vTitle.setText(item.getTitle());
                    holder.vTime.setText(item.getTimeFormattedShort());
                    holder.vPaceCurrent.setText(item.getPaceCurrentFormatted());

                    holder.vPace.setVisibility(item.getStarting() < Time.getTime() ? View.VISIBLE : View.GONE);
                    if (item.getPacePrevious() != null) {
//                        findViewById(R.id.pacePreviousContainer).setVisibility(View.VISIBLE);
                        holder.vPacePrevious.setText(item.getPacePreviousFormatted());
                        if (item.getPacePrevious() <= item.getPaceCurrent()) {
                            holder.vPacePrevious.setText((item.getPaceCurrent()-item.getPacePrevious())+"%");
                            holder.vPacePreviousIcon.setImageDrawable(getActivity().getDrawable(R.drawable.ic_trending_up_black_24dp));
                            holder.vPacePreviousIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorGreen));
                        } else if (item.getPacePrevious() > item.getPaceCurrent()) {
                            holder.vPacePrevious.setText((item.getPacePrevious()-item.getPaceCurrent())+"%");
                            holder.vPacePreviousIcon.setImageDrawable(getActivity().getDrawable(R.drawable.ic_trending_down_black_24dp));
                            holder.vPacePreviousIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorRed));
                        }
                    } else {
//                        findViewById(R.id.pacePreviousContainer).setVisibility(View.GONE);
                    }

                    if(item.getPurse()!=null) {
                        holder.vCoinsBronze.setText(item.getPurse().getCountBronze().toString());
                        holder.vCoinsSilver.setText(item.getPurse().getCountSilver().toString());
                        holder.vCoinsGold.setText(item.getPurse().getCountGold().toString());
                    }
                    holder.vProgress.setProgress(item.getProgress());

                    holder.vHelp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(BurstsHelpActivity.class);
                        }
                    });
                }
            })
            .setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<Burst>() {
                @Override
                public void onClick(View view, Burst item) {
                    startActivity(BurstsDetailActivity.class, item.getId().toString());
                }
            })
            .create()
            .getAdapter();
        listFuture.getListView().setNestedScrollingEnabled(false);


        listPast = RecyclerListViewBuilder.build(getActivity())
                .setView(findViewById(R.id.list_past))
                .setAdapter(new RecyclerListViewAdapter<Burst, QuestionnairesViewHolder>())
                .setOnLayoutListener(new RecyclerListViewAdapter.OnLayoutListener() {
                    @Override
                    public View onCreate(ViewGroup parent) {
                        View view = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.component_bursts_item, parent, false);
//                    view.setAlpha(0.2f);
                        return view;
                    }
                })
                .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<Burst, BurstsViewHolder>() {
                    @Override
                    public BurstsViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                        return new BurstsViewHolder(adapter, view);
                    }
                    @Override
                    public void onBind(BurstsViewHolder holder, final Burst item) {
                        holder.vTitle.setText(item.getTitle());
                        holder.vTime.setText(item.getTimeFormattedShort());
                        holder.vPaceCurrent.setText(item.getPaceCurrentFormatted());

                        holder.vPace.setVisibility(item.getStarting() < Time.getTime() ? View.VISIBLE : View.GONE);
                        if (item.getPacePrevious() != null) {
//                        findViewById(R.id.pacePreviousContainer).setVisibility(View.VISIBLE);
                            holder.vPacePrevious.setText(item.getPacePreviousFormatted());
                            if (item.getPacePrevious() <= item.getPaceCurrent()) {
                                holder.vPacePrevious.setText((item.getPaceCurrent()-item.getPacePrevious())+"%");
                                holder.vPacePreviousIcon.setImageDrawable(getActivity().getDrawable(R.drawable.ic_trending_up_black_24dp));
                                holder.vPacePreviousIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorGreen));
                            } else if (item.getPacePrevious() > item.getPaceCurrent()) {
                                holder.vPacePrevious.setText((item.getPacePrevious()-item.getPaceCurrent())+"%");
                                holder.vPacePreviousIcon.setImageDrawable(getActivity().getDrawable(R.drawable.ic_trending_down_black_24dp));
                                holder.vPacePreviousIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorRed));
                            }
                        } else {
//                        findViewById(R.id.pacePreviousContainer).setVisibility(View.GONE);
                        }

                        if(item.getPurse()!=null) {
                            holder.vCoinsBronze.setText(item.getPurse().getCountBronze().toString());
                            holder.vCoinsSilver.setText(item.getPurse().getCountSilver().toString());
                            holder.vCoinsGold.setText(item.getPurse().getCountGold().toString());
                        }
                        holder.vProgress.setProgress(item.getProgress());

                        holder.vHelp.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(BurstsHelpActivity.class);
                            }
                        });
                    }
                })
                .setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<Burst>() {
                    @Override
                    public void onClick(View view, Burst item) {
                        startActivity(BurstsDetailActivity.class, item.getId().toString());
                    }
                })
                .create()
                .getAdapter();
        listPast.getListView().setNestedScrollingEnabled(false);
    }


    private void getActive() {
        model.getItemsActive(new Repository.RepositoryListener<List<Burst>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onDone(List<Burst> items) {
                listActive.setItems(items);
                listActive.refresh();
                findViewById(R.id.list_active_empty).setVisibility(listActive.isEmpty() ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void getFuture() {
        model.getItemsFuture(new Repository.RepositoryListener<List<Burst>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onDone(List<Burst> items) {
                listFuture.setItems(items);
                listFuture.refresh();
                findViewById(R.id.list_future_empty).setVisibility(listFuture.isEmpty() ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void getPast() {
        model.getItemsPast(new Repository.RepositoryListener<List<Burst>>() {
            @Override
            public void onStart() {
                getLoadingView().show();
            }

            @Override
            public void onDone(List<Burst> items) {
                listPast.setItems(items);
                listPast.refresh();
                findViewById(R.id.list_past_empty).setVisibility(listPast.isEmpty() ? View.VISIBLE : View.GONE);
                getLoadingView().hide();
                getRefreshView().setRefreshing(false);
            }
        });
    }
}
