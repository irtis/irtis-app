package cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class MediaEntity {
    private String id;
    @SerializedName("created_time")
    private Long created;
    private CaptionEntity caption;
    private UserEntity user;
    private String type;
    @SerializedName("users_in_photo")
    private List<UserEntity> usersInPhoto;
    private String filter;
    private String[] tags;
    private LocationEntity location;
    private CountsEntity comments;
    private CountsEntity likes;
    private String link;
    private ImageEntity images;
    private VideoEntity video;



    static public MediaEntity buildFromJson(String json) {
        return new GsonBuilder().create().fromJson(json, MediaEntity.class);
    }

    static public List<MediaEntity> buildListFromJson(String json) {
        return new GsonBuilder().create().fromJson(json, new TypeToken<List<MediaEntity>>() {}.getType());
    }


    public String getId() {
        return id;
    }

    public Long getCreated() {
        return created;
    }

    public CaptionEntity getCaption() {
        return caption;
    }

    public UserEntity getUser() {
        return user;
    }

    public String getType() {
        return type;
    }

    public List<UserEntity> getUsersInPhoto() {
        return usersInPhoto;
    }

    public String getFilter() {
        return filter;
    }

    public String[] getTags() {
        return tags;
    }

    public LocationEntity getLocation() {
        return location;
    }

    public Integer getComments() {
        return comments.getCount();
    }

    public Integer getLikes() {
        return likes.getCount();
    }

    public String getLink() {
        return link;
    }

    public ImageEntity getImages() {
        return images;
    }

    public VideoEntity getVideo() {
        return video;
    }
}
