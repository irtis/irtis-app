package cz.muni.irtis.datacollector.lite.gui.questionnaires.model;

import android.content.Context;

import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;

/**
 * QuestionnaireFromTemplateBuilder
 *
 *
 */
public class QuestionnaireFromTemplateBuilder {
    public static final int TYPE_WELLBEING = 271;
    public static final int TYPE_MORNING = 272;
    public static final int TYPE_NIGHT = 270;
    public static final int TYPE_MULTI_SLIDER = 280;

    static public Questionnaire build(Context context, int type) {
        switch (type) {
            default:
            case TYPE_WELLBEING:
                return Questionnaire.buildFromAssets(context, Questionnaire.getTemplatePathByIdentifier("wellbeing"));

            case TYPE_MORNING:
                return Questionnaire.buildFromAssets(context, Questionnaire.getTemplatePathByIdentifier("pilot-morning-2020"));

            case TYPE_NIGHT:
                return Questionnaire.buildFromAssets(context, Questionnaire.getTemplatePathByIdentifier("pilot-night-2020"));

            case TYPE_MULTI_SLIDER:
                return Questionnaire.buildFromAssets(context, Questionnaire.getTemplatePathByIdentifier("multiple-slider"));
        }
    }
}