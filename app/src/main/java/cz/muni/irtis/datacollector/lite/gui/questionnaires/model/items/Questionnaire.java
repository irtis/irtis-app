package cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.GsonBuilder;
import com.google.gson.InstanceCreator;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import cz.muni.irtis.datacollector.lite.Application;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.json.AnnotationExclusionStrategy;
import cz.muni.irtis.datacollector.lite.application.json.Exclude;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesService;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.AnswerJson;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.NotificationJson;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.QuestionJson;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.QuestionnaireJson;

public class Questionnaire {
    private Long id;
    private Integer type;
    private String identifier;
    private Boolean configurable;
    @SerializedName("questionnaire_id")
    private Long idQuestionnaire;
    @SerializedName("people_questionnaire_id")
    private Long idPeopleQuestionnaire;
    private String title;
    private Integer validity;
    private Integer buffer;
    @SerializedName("silence_delay")
    private Integer silenceDelay;
    private String group;
    @Exclude
    private File source;
    private List<Question> questions;
    @Exclude
    private List<Notification> notifications;

    private Long created;       // Time when the survey was created by the administrator or system
    private Long updated;       // Time when the survey was updated by the administrator
    private Long starting;      // Time when the survey can be available to the user
    private Long ending;        // Time when the survey cannot be available to the user
    private Long invoking;       // Time when the survey can become available to the user
    private Long closing;       // Time when the survey would be open for the user to answer (closing = ([starting, ending] -> NOW() + validity * 60000)

    private Long notified;      // Time when the survey was notified to the user via notification GUI
    private Long silenced;      // Time when the survey was silenced by the user
    private Long opened;        // Time when the survey was opened by the user
    private Long completed;     // Time when the survey was completed by the user

    private Long uploaded;      // Time when the survey was received from the user on the server


    static public Questionnaire buildFromInstance(Questionnaire instance, boolean initialize) {
        if(instance!=null) {
            if (initialize) {
                if (instance.getId() == null) {
                    if (instance.getCreated() == null) {
                        instance.setCreated(Time.getTime());
                    }
                    if (instance.getUpdated() == null) {
                        instance.setUpdated(Time.getTime());
                    }
                    if (instance.getStarting() == null) {
                        instance.setStarting(Time.getTime());
                    }
                    if (instance.getEnding() == null) {
                        instance.setEnding(Time.getTime() + (instance.getValidity() * Time.MINUTE));
                    }
                    if (instance.getInvoking() == null) {
                        instance.setInvoking(instance.getStarting(), instance.getEnding());
                    }
                }

                // Create Identifier
                if (instance.getIdentifier() == null) {
                    instance.setIdentifier(generateIdentifier());
                }
            }

            // After-Initialization
            // Re-arrange and re-link questions and answers
            if (instance.getQuestions() != null) {
                Iterator<Question> iQuestions = instance.getQuestions().iterator();
                int position = 1;
                while (iQuestions.hasNext()) {
                    Question question = iQuestions.next();
                    question.setQuestionnaire(instance);
                    if (question.getPosition() == null) {
                        question.setPosition(position);
                    }
                    if (question.getAnswers() != null && !question.getAnswers().isEmpty()) {
                        Iterator<Answer> iAnswers = question.getAnswers().iterator();
                        while (iAnswers.hasNext()) {
                            Answer answer = iAnswers.next();
                            answer.setQuestion(question);
                            answer.setQuestionnaire(instance);
                        }
                    }
                    position++;
                }
            }
        }

        return instance;
    }

    static public Questionnaire buildFromInstance(Questionnaire instance) {
        return buildFromInstance(instance, true);
    }

    static public Questionnaire buildFromString(String json, boolean initialize) {
        Questionnaire instance = null;
        QuestionnaireScopes scopes = null;
        try {
            instance = new GsonBuilder()
                    .setExclusionStrategies(new AnnotationExclusionStrategy())
                    .registerTypeAdapter(Questionnaire.class, new QuestionnaireInstanceCreator())
                    .create()
                    .fromJson(json, Questionnaire.class);

            scopes = new GsonBuilder()
                    .setExclusionStrategies(new AnnotationExclusionStrategy())
                    .registerTypeAdapter(Questionnaire.class, new QuestionnaireInstanceCreator())
                    .create()
                    .fromJson(json, QuestionnaireScopes.class);
        } catch (Exception e) {
            Debug.getInstance().exception(e, json);
        }

        // Initialize filters
        if(instance!=null && scopes!=null) {
            if(scopes.getFilters()!=null && !scopes.getFilters().isEmpty()) {
                // Assign defined filters
                Iterator<QuestionFilter> iFilters = scopes.getFilters().iterator();
                while (iFilters.hasNext()) {
                    QuestionFilter filter = iFilters.next();
                    Question question = instance.getQuestionById(filter.getIdOwner());
                    if(question!=null) {
                        if (question.getFilters() == null) {
                            question.setFilters(new ArrayList<QuestionFilter>());
                        }
                        question.getFilters().add(filter);
                    }
                }

                // Create transitive filters
                Iterator<Question> iQuestions = instance.getQuestions().iterator();
                while(iQuestions.hasNext()) {
                    Question question = iQuestions.next();
                    if(question.getFilters()!=null) {
                        Question previous = instance.getQuestionByPosition(question.getPosition()-1);
                        if(previous!=null && previous.getFilters()!=null) {
                            Iterator<QuestionFilter> iPreviousFilters = previous.getFilters().iterator();
                            while (iFilters.hasNext()) {
                                QuestionFilter filter = iFilters.next();
                                question.getFilters().add(
                                        QuestionFilter.build(question.getId(), filter.getIdQuestion(), filter.getIdAnswer()));
                            }

                        }
                    }
                }
            }
        }

        // Build links
        if(instance!=null) {
            Boolean isLinksUsed = false;
            Iterator<Question> iQuestions = instance.getQuestions().iterator();
            while (iQuestions.hasNext()) {
                Question question = iQuestions.next();
                if(question!=null && question.isLink()) {
                    isLinksUsed = true;
                    break;
                }
            }

            if(!isLinksUsed) {
                List<Question> collection = new ArrayList<>();
                Collections.copy(instance.getQuestions(), collection);
                Collections.sort(collection, new Comparator<Question>() {
                    @Override
                    public int compare(Question q1, Question q2) {
                        if(q1.getPosition()!=null && q2.getPosition()!=null) {
                            if(q1.getPosition() > q2.getPosition()) {
                                return 1;
                            } else if(q1.getPosition() < q2.getPosition()) {
                                return -1;
                            } else {
                                return 0;
                            }
                        } else {
                            return 0;
                        }
                    }
                });
            }

            // @todo Mapping links by position
        }

        return buildFromInstance(instance, initialize);
    }

    static public Questionnaire buildFromString(String json) {
        return buildFromString(json, true);
    }

    static public List<Questionnaire> buildListFromString(String json, boolean initialize) {
        List<Questionnaire> buffer = new ArrayList<>();
        List<Questionnaire> instances = new GsonBuilder()
                .setExclusionStrategies(new AnnotationExclusionStrategy())
                .registerTypeAdapter(Questionnaire.class, new QuestionnaireInstanceCreator())
                .create()
                .fromJson(json, new TypeToken<List<Questionnaire>>() {
                }.getType());

        List<QuestionnaireScopes> scopes = new GsonBuilder()
                .setExclusionStrategies(new AnnotationExclusionStrategy())
                .registerTypeAdapter(Questionnaire.class, new QuestionnaireInstanceCreator())
                .create()
                .fromJson(json, new TypeToken<List<QuestionnaireScopes>>() {
                }.getType());

        // Initialize filters
        if(instances!=null && !instances.isEmpty()) {
            Iterator<Questionnaire> iQuestionnaire = instances.iterator();
            int i = 0;
            while (iQuestionnaire.hasNext()) {
                Questionnaire instance = iQuestionnaire.next();
                if(scopes!=null && scopes.get(i)!=null) {
                    if(scopes.get(i).getFilters()!=null && !scopes.get(i).getFilters().isEmpty()) {
                        // Assign defined filters
                        Iterator<QuestionFilter> iFilters = scopes.get(i).getFilters().iterator();
                        while (iFilters.hasNext()) {
                            QuestionFilter filter = iFilters.next();
                            Question question = instance.getQuestionById(filter.getIdOwner());
                            if(question!=null) {
                                if (question.getFilters() == null) {
                                    question.setFilters(new ArrayList<QuestionFilter>());
                                }
                                question.getFilters().add(filter);
                            }
                        }

                        // Create transitive filters
                        Iterator<Question> iQuestions = instance.getQuestions().iterator();
                        while(iQuestions.hasNext()) {
                            Question question = iQuestions.next();
                            if (question.getFilters() != null) {
                                Question previous = instance.getQuestionByPosition(question.getPosition() - 1);
                                if(previous!=null && previous.getFilters()!=null) {
                                    Iterator<QuestionFilter> iPreviousFilters = previous.getFilters().iterator();
                                    while (iFilters.hasNext()) {
                                        QuestionFilter filter = iFilters.next();
                                        question.getFilters().add(
                                                QuestionFilter.build(question.getId(), filter.getIdQuestion(), filter.getIdAnswer()));
                                    }

                                }
                            }
                        }
                    }
                }
                buffer.add(buildFromInstance(instance, initialize));
                i++;
            }
        }

        return buffer;
    }

    static public List<Questionnaire> buildListFromString(String json) {
        return buildListFromString(json, true);
    }

    static public Questionnaire buildFromFile(File file, boolean initialize) {
        Questionnaire item = null;
        String json = null;

        try {
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String sCurrentLine;
            while ((sCurrentLine = reader.readLine()) != null) {
                builder.append(sCurrentLine).append("\n");
            }
            json = builder.toString();
        } catch (Exception e) {
            Debug.getInstance().exception(e);
        }

        if(json!=null) {
            item = buildFromString(json, initialize);
            item.setSource(file);
        }

        return item;
    }

    static public Questionnaire buildFromFile(File file) {
        return buildFromFile(file, true);
    }

    static public Questionnaire buildFromAssets(Context context, String path, boolean initialize) {
        Questionnaire item = null;
        String json = null;
        AssetManager assets = context.getAssets();
        try {
            InputStream is = assets.open(path);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (Exception e) {
            Debug.getInstance().exception(e);
        }

        if(json!=null) {
            item = buildFromString(json, initialize);
        }

        return item;
    }

    static public Questionnaire buildFromAssets(Context context, String path) {
        return buildFromAssets(context, path, true);
    }

    private static class QuestionnaireInstanceCreator implements InstanceCreator<Questionnaire> {
        @Override
        public Questionnaire createInstance(Type type) {
            return new Questionnaire();
        }
    }


    static public String getTemplatePathByIdentifier(String identifier) {
        return "questionnaires/"+identifier+".json";
    }


    public Questionnaire setId(Long id) {
        this.id = id;
        return this;
    }
    public Long getId() {
        return id;
    }

    public Integer getType() {
        return type;
    }

    public boolean isType(Integer type) {
        if(this.type!=null) {
            return this.type.equals(type);
        } else {
            return false;
        }
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setIdQuestionnaire(Long idQuestionnaire) {
        this.idQuestionnaire = idQuestionnaire;
    }

    public Long getIdQuestionnaire() {
        return idQuestionnaire;
    }

    public void setIdPeopleQuestionnaire(Long idPeopleQuestionnaire) {
        this.idPeopleQuestionnaire = idPeopleQuestionnaire;
    }

    public Long getIdPeopleQuestionnaire() {
        return idPeopleQuestionnaire;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getTitle() {
        return title;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public Question getQuestionById(Long id) {
        if(questions!=null && !questions.isEmpty()) {
            Iterator<Question> iQuestions = questions.iterator();
            while (iQuestions.hasNext()) {
                Question question = iQuestions.next();
                if (question.getId().equals(id)) {
                    return question;
                }
            }
        }

        return null;
    }


    public Question getQuestionByPosition(int position) {
        if(questions!=null && !questions.isEmpty() && position>0) {
            Iterator<Question> iQuestions = questions.iterator();
            while(iQuestions.hasNext()) {
                Question question = iQuestions.next();
                if(question.getPosition().equals(position)) {
                    return question;
                }
            }
        }
        return null;
    }

    public Integer getQuestionIndex(Question question) {
        if(questions!=null && !questions.isEmpty()) {
            Integer index = 0;
            Iterator<Question> iQuestions = questions.iterator();
            while(iQuestions.hasNext()) {
                Question q2 = iQuestions.next();
                if(q2.getPosition().equals(question.getPosition())) {
                    return index;
                }
                index++;
            }
        }
        return -1;
    }


    public Integer getValidity() {
        if(validity!=null) {
            return validity;
        } else {
            return 0;
        }
    }


    public boolean isOpenForStarting(Long time) {
        boolean isInInterval = getStarting() != null && getEnding() != null && getStarting() <= time && getEnding() >= time;
        boolean isInFrame = getStarting() != null && getEnding() != null &&
                getStarting()-(QuestionnairesModel.VALIDFRAME_HOUR_OFFSET*Time.HOUR) <= time && getEnding()+(QuestionnairesModel.VALIDFRAME_HOUR_OFFSET*Time.HOUR) >= time;
        boolean isInvoking = getInvoking() !=null && getInvoking() <= time;

        if(getInvoking()!=null) {
            return isInvoking && isInFrame;
        } else {
            return isInInterval;
        }

    }

    public boolean isOpenForStarting() {
        return isOpenForStarting(Time.getTime());
    }



    public void setClosing(Long time) {
        this.closing = time;
    }

    public void setClosing() {
        if (isOpenForStarting()) {
            setClosing(Time.getTime() + (getValidity() * Time.MINUTE));
        }
    }


    public String getClosingFormatted() {
        return Time.getTimeStamp(closing, "dd.MM.yyyy HH:mm");
    }
    public Long getClosing() {
        return closing;
    }

    public Long getClosingThreshold() {
        return getClosing();
    }


    public boolean isClosed() {
        if(getClosing()!=null) {
            if(!isCompleted()) {
                return getClosing() <= Time.getTime();
            }
        }

        return true;
    }

    public Long getClosedRemaining() {
        if(!isClosed()) {
            return getClosingThreshold() - Time.getTime();
        } else {
            return 0L;
        }
    }

    public String getClosedRemainingFormatted() {
        Long time = getClosedRemaining();
        return String.format("%02d:%02d:%02d",
            TimeUnit.MILLISECONDS.toHours(time),
            TimeUnit.MILLISECONDS.toMinutes(time) -
                    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time)),
            TimeUnit.MILLISECONDS.toSeconds(time) -
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)));
    }

    public Integer getClosedRemainingPercents() {
        long progress = Time.getTime();
        long min = getClosing() - (getValidity() * Time.MINUTE);
        long max = getClosing();
        if (min >= max || progress >= max) {
            return 0;
        }
        if (progress <= min) {
            return 100;
        }

        return (int) ((max - progress) * 100 / (max - min));
    }

    public Long getBufferTime() {
        if(isCompleted()) {
            return getCompleted() + (getBuffer() * Time.MINUTE);
        } else if(getClosing() !=null) {
            return getClosing() + (getBuffer() * Time.MINUTE);
        } else {
            return 0L;
        }
    }

    public Integer getBuffer() {
        if(buffer!=null) {
            return buffer;
        } else {
            return 0;
        }
    }

    public Integer getSilenceDelay() {
        if(silenceDelay!=null) {
            return silenceDelay;
        } else {
            return 10;
        }
    }

    public String getGroup() {
        return group;
    }

    public void setCreated(Long created) {
        this.created = created;
    }
    public Long getCreated() {
        return created;
    }
    public String getCreatedFormatted() {
        return Time.getTimeStamp(created, "dd.MM.yyyy HH:mm");
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }
    public Long getUpdated() {
        return updated;
    }
    public String getUpdatedFormatted() {
        return Time.getTimeStamp(updated, "dd.MM.yyyy HH:mm");
    }


    public String getDateFormatted() {
        return Time.getTimeStamp(starting, "dd.MM.yyyy");
    }

    public Long getStarting() {
        return starting;
    }
    public void setStarting(Long starting) {
        this.starting = starting;
    }
    public String getStartingFormatted() {
        return Time.getTimeStamp(starting, "dd.MM.yyyy HH:mm");
    }

    public String getIntervalFormatted() {
        return Time.getTimeStamp(starting, "HH:mm") + " - " + Time.getTimeStamp(ending, "HH:mm dd.MM.yyyy");
    }
    public String getIntervalHoursFormatted() {
        return Time.getTimeStamp(starting, "HH:mm") + " - " + Time.getTimeStamp(ending, "HH:mm");
    }

    public Long getEnding() {
        return ending;
    }
    public void setEnding(Long ending) {
        this.ending = ending;
    }
    public String getEndingFormatted() {
        return Time.getTimeStamp(ending, "dd.MM.yyyy HH:mm");
    }

    public Long getInvoking() {
        return invoking;
    }
    public String getInvokingFormatted() {
        return invoking != null ? Time.getTimeStamp(invoking, "dd.MM.yyyy HH:mm") : "";
    }
    public String getInvokingHoursFormatted() {
        return invoking != null ? Time.getTimeStamp(invoking, "HH:mm") : "";
    }
    public void setInvoking(Long invoking) {
        this.invoking = invoking;
    }

    public void setInvoking(Long starting, Long ending) {
        Random random = new Random();
        Integer min = Long.valueOf(starting/1000).intValue();
        Integer max = Long.valueOf(ending/1000).intValue();
        Long invoking = Long.valueOf((max - min > 0 ? random.nextInt(max - min) : 0) + min) * 1000;
        this.invoking = invoking;
    }


    public Long getNotified() {
        return notified;
    }
    public void setNotified(Long notified) {
        this.notified = notified;
    }
    public String getNotifiedFormatted() {
        return Time.getTimeStamp(notified, "dd.MM.yyyy HH:mm");
    }
    public boolean isNotified() {
        return notified!=null && notified>0;
    }

    public Long getSilenced() {
        return silenced;
    }

    public void setSilenced(Long silenced) {
        this.silenced = silenced;
    }

    public Long getOpened() {
        return opened;
    }
    public void setOpened(Long opened) {
        this.opened = opened;
    }
    public String getOpenedFormatted() {
        return Time.getTimeStamp(opened, "dd.MM.yyyy HH:mm");
    }
    public boolean isOpened() {
        return opened!=null && opened>0;
    }

    public void setCompleted() {
        this.completed = Time.getTime();
    }
    public void setCompleted(Long completed) {
        this.completed = completed;
    }
    public boolean isCompleted() {
        return completed!=null && completed>0;
    }
    public Long getCompleted() {
        return completed;
    }
    public String getCompletedFormatted() {
        return Time.getTimeStamp(completed, "dd.MM.yyyy HH:mm");
    }

    public boolean isFinished() {
        return isCompleted() || isClosed();
    }

    public void setUploaded(Long uploaded) {
        this.uploaded = uploaded;
    }
    public void setUploaded() {
        setUploaded(System.currentTimeMillis());
    }
    public boolean isUploaded() {
        return uploaded!=null && uploaded!=0;
    }
    public Long getUploaded() {
        return uploaded;
    }

    public Boolean getConfigurable() {
        return configurable;
    }

    public Boolean isConfigurable() {
        return configurable;
    }

    public void setConfigurable(Boolean configurable) {
        this.configurable = configurable;
    }

    public void setSource(File source) {
        this.source = source;
    }
    public File getSource() {
        return source;
    }


    static public String generateIdentifier() {
        String identifier = UUID.randomUUID().toString();
        return identifier;
    }

    /**
     * isFromTemplate()
     *
     * Check if the questionnaire is from assets template
     *
     * @param context
     * @return
     */
    public boolean isFromTemplate(Context context) {
        if(getIdentifier()!=null) {
            if(isType(QuestionnairesModel.TYPE_INVOKED)) {
                return true;
            } else {
                AssetManager assets = context.getAssets();
                try {
                    assets.open(getTemplatePathByIdentifier(getIdentifier()));
                    return true;
                } catch (Exception e) {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    public boolean equals(Object obj) {
        Questionnaire object = (Questionnaire) obj;
        if(!this.id.equals(object.getId())) {
            return false;
        }
        if(!this.identifier.equals(object.getIdentifier())) {
            return false;
        }
        if(!this.title.equals(object.getTitle())) {
            return false;
        }
        if(!this.created.equals(object.getCreated())) {
            return false;
        }
        if(!this.updated.equals(object.getUpdated())) {
            return false;
        }
        if(!this.notified.equals(object.getNotified())) {
            return false;
        }
        if(!this.silenced.equals(object.getSilenced())) {
            return false;
        }
        if(!this.opened.equals(object.getOpened())) {
            return false;
        }
        if(!this.completed.equals(object.getCompleted())) {
            return false;
        }
        if(this.uploaded!=null) {
            if (!this.uploaded.equals(object.getUploaded())) {
                return false;
            }
        }
        if(this.configurable!=null) {
            if (!this.configurable.equals(object.getConfigurable())) {
                return false;
            }
        }
        if(this.source!=null) {
            if (!this.source.equals(object.getSource())) {
                return false;
            }
        }

        Integer matches = 0;
        Iterator<Question> iQuestions = questions.iterator();
        while(iQuestions.hasNext()) {
            Question question = iQuestions.next();
            Iterator<Question> iObjectQuestions = object.getQuestions().iterator();
            while(iObjectQuestions.hasNext()) {
                Question objectQuestion = iObjectQuestions.next();
                if(question.equals(objectQuestion)) {
                    matches++;
                    break;
                }
            }
        }
        if(!matches.equals(questions.size())) {
            return false;
        }

        return true;
    }

    public String toString() {
        return new GsonBuilder()
            .setExclusionStrategies(new AnnotationExclusionStrategy())
            .create()
            .toJson(this);
    }

    public File toFile() {
        File file = null;
        String content = toString();
        if(content!=null) {
            File directory = new File(Application.getInstance().getApplicationContext()
                    .getCacheDir(), "questionnaires");
            if(!directory.exists()) {
                directory.mkdir();
            }

            if(getIdPeopleQuestionnaire()!=null) {
                file = new File(directory, getIdentifier() + "-id-" +
                        getIdPeopleQuestionnaire().toString() + ".json");
            } else {
                file = new File(directory, getIdentifier()+"-template.json");
            }

            try {
                if(file.createNewFile()) {
                    Debug.getInstance().log(QuestionnairesService.TAG, "File created: " + file);
                } else {
                    Debug.getInstance().log(QuestionnairesService.TAG, "File exists: " + file);
                }
            } catch (IOException e) {
                Debug.getInstance().exception(e, this, file.getPath());
            }

            try {
                if(!file.exists()) {
                    Debug.getInstance().exception(
                            new FileNotFoundException("Questionnaire.toFile(): file not exists"), this, file.getPath());
                }
                FileWriter f = new FileWriter(file.getPath(), false);
                f.write(content);
                f.close();
            } catch (Exception e) {
                Debug.getInstance().exception(e, this, file.getPath());
            }
        } else {
            Debug.getInstance().exception(new IllegalStateException("Questionnaire.toFile(): cannot get toString() content"), this);
        }

        return file;
    }

    public QuestionnaireJson toRequest() {
        List<QuestionJson> questions = new ArrayList<>();
        List<AnswerJson> answers = new ArrayList<>();
        List<NotificationJson> notifications = new ArrayList<>();

        if(getQuestions()!=null && !getQuestions().isEmpty()) {
            Iterator<Question> iQuestions = getQuestions().iterator();
            while (iQuestions.hasNext()) {
                Question question = iQuestions.next();
                questions.add(new QuestionJson(question));

                Iterator<Answer> iAnswers = question.getAnswers().iterator();
                while (iAnswers.hasNext()) {
                    AnswerJson jAnswer = new AnswerJson(iAnswers.next());
                    switch (question.getType()) {
                        default:
                            jAnswer.setIdAnswer(null);
                            break;

                        case Question.TYPE_CHECK:
                        case Question.TYPE_SCALE:
                            break;
                    }

                    answers.add(jAnswer);
                }
            }
        }

        if(getNotifications()!=null && !getNotifications().isEmpty()) {
            Iterator<Notification> iNotifications = getNotifications().iterator();
            while (iNotifications.hasNext()) {
                Notification notification = iNotifications.next();
                notifications.add(new NotificationJson(notification));
            }
        }

        QuestionnaireJson request = new QuestionnaireJson(getIdPeopleQuestionnaire(), getIdQuestionnaire(),
            getCreated(), getUpdated(), getStarting(), getEnding(), getInvoking(), getClosing(), getNotified(), getSilenced(), getOpened(), getCompleted(),
            questions, answers, notifications);
        return request;
    }
}