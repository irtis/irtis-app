package cz.muni.irtis.datacollector.lite.gui.questionnaires.components;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dinuscxj.progressbar.CircleProgressBar;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewAdapter;

public class QuestionnairesViewHolder extends RecyclerListViewAdapter.ItemsListViewHolderBase {
    public CircleProgressBar vRemainingProgress;
    public TextView vRemainingValue;
    public TextView vTime;
    public TextView vTitle;
    public Button vPostpone;

    public QuestionnairesViewHolder(RecyclerListViewAdapter adapter, View view) {
        super(adapter, view);
        vRemainingProgress = view.findViewById(R.id.remaining_progress);
        vRemainingValue = view.findViewById(R.id.remaining_value);
        vTime = view.findViewById(R.id.time);
        vTitle = view.findViewById(R.id.title);
        vPostpone = view.findViewById(R.id.postpone);
    }
}
