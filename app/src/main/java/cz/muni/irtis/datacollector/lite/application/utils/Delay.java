package cz.muni.irtis.datacollector.lite.application.utils;


import android.os.Handler;

public class Delay
{
	static public Delay build(long time, Runnable closure) {
		return new Delay(time, closure);
	}

	public Delay(long time, Runnable closure) {
		new Handler().postDelayed(closure, time);
	}
}
