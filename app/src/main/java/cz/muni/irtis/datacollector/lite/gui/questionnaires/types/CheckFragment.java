package cz.muni.irtis.datacollector.lite.gui.questionnaires.types;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import java.util.Iterator;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewAdapter;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewBuilder;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;

public class CheckFragment extends TypeFragmentBase {
    private RecyclerListViewAdapter list;


    public CheckFragment(Context context, Question question, boolean editable) {
        super(context, question, R.layout.activity_questionnaires_form_type_list, editable);
    }

    public class ViewHolder extends RecyclerListViewAdapter.ItemsListViewHolderBase {
        CheckBox vCheck;
        public ViewHolder(RecyclerListViewAdapter adapter, View view) {
            super(adapter, view);
            vCheck = view.findViewById(R.id.check);
        }
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        list = RecyclerListViewBuilder.build(getActivity())
            .setView(view.findViewById(R.id.list))
            .setAdapter(new RecyclerListViewAdapter<String, CheckFragment.ViewHolder>(R.layout.activity_questionnaires_form_type_check))
            .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<Answer, ViewHolder>() {
                @Override
                public ViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                    return new ViewHolder(adapter, view);
                }

                @Override
                public void onBind(ViewHolder holder, Answer item) {
                    holder.vCheck.setText(item.getText());
                    holder.vCheck.setChecked(item.isSelected());
                    holder.vCheck.setEnabled(isEditable());
                }
            })
            .setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<Answer>() {
                @Override
                public void onClick(View view, Answer item) {
                    toggle(item);
                }
            })
            .create().getAdapter();


        list.addItems(getQuestion().getAnswers());

        return view;
    }

    /**
     * toggle()
     * @param item
     */
    public void toggle(Answer item) {
        if(isEditable()) {
            select(item, !item.isSelected());
            if(item.isSelected() && item.getExclusions()!=null && !item.getExclusions().isEmpty()) {
                Iterator<Long> iExclusions = item.getExclusions().iterator();
                while(iExclusions.hasNext()) {
                    Long id = iExclusions.next();
                    Answer answer = getQuestion().getAnswerById(id);
                    select(answer, false);
                }
            }

            refresh();
        }
    }

    private void select(Answer item, boolean selected) {
        item.setSelected(selected);
        item.setValue(selected ? "1" : null);
        ViewHolder holder = (ViewHolder) list.getViewHolder(item);
        if(holder!=null && holder.vCheck!=null) {
            holder.vCheck.setChecked(selected);
        }
    }
}
