package cz.muni.irtis.datacollector.lite.gui.integrations.model.google;

import android.content.Context;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import com.google.api.services.youtube.YouTubeScopes;

import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;

public class GoogleServiceBuilder {

    static public GoogleService buildYoutube(Context context, ServiceBase.OnServiceListener onSignInListener, ServiceBase.OnServiceListener onSignOutListener) {
        GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(
                    new Scope(YouTubeScopes.YOUTUBE),
                    new Scope(YouTubeScopes.YOUTUBE_READONLY),
                    new Scope("https://www.googleapis.com/auth/youtube.force-ssl")
                )
//                .requestIdToken(context.getString(R.string.google_client_id))
                .requestEmail()
                .build();

        return new GoogleService(context, options, onSignInListener, onSignOutListener);
    }

    static public GoogleService buildYoutube(Context context) {
        return buildYoutube(context, null, null);
    }
}
