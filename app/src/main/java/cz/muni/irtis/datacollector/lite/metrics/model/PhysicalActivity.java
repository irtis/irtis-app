package cz.muni.irtis.datacollector.lite.metrics.model;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.ActivityTransitionEvent;
import com.google.android.gms.location.ActivityTransitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.Arrays;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities.CommentsEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.ActivityEntity;
import cz.muni.irtis.datacollector.lite.metrics.optimizer.Optimizer;
import cz.muni.irtis.datacollector.lite.metrics.model.util.physicalactivity.BackgroundDetectedActivitiesService;
import cz.muni.irtis.datacollector.lite.metrics.model.util.physicalactivity.RecognizedActivity;
import cz.muni.irtis.datacollector.lite.metrics.Metric;

/**
 * Capture physical activity
 */
public class PhysicalActivity extends Metric {
    private final String TAG = this.getClass().getSimpleName();
    public static final int IDENTIFIER = 257;

    private PhysicalActivityBroadcastReceiver receiver;

    /**
     *
     * @param context
     * @param delay
     */
    public PhysicalActivity(Context context, Integer delay) {
        super(context, delay,
            Optimizer.buildScheduled(context, delay, Time.HOUR*2, Time.MINUTE*15));
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "Activity";
    }

    /**
     * Register location updates per delay from starting intent
     */
    @Override
    public void run() {
        if(getOptimizer().isActive()) {
            if (isRunning()) {
                stop();
            }
        } else {
            if (!isRunning()) {
                start();
            }
        }
    }

    public void start() {
        if (!isRunning()) {
            createReceiver();
            setRunning(true);
        }
    }

    @Override
    public void stop() {
        if(isRunning()) {
            destroyReceiver();
            setRunning(false);
        }
    }


    @Override
    public long save(Object... params) {
        Integer type = (Integer) params[0];
        Integer confidence = (Integer) params[1];

        try {
            Debug.getInstance().log(TAG, RecognizedActivity.toString(type)+" ("+confidence.toString()+")");
            return getDatabase().getPhysicalActivityRepository().insert(
                    new ActivityEntity(Time.getTime(), RecognizedActivity.toString(type), confidence));
        } catch (Exception e) {
            Debug.getInstance().exception(e);
            return -1;
        }
    }

    private void createReceiver() {
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(receiver = new PhysicalActivityBroadcastReceiver(),
                new IntentFilter("activity"));
        Intent intent = new Intent(getContext(), BackgroundDetectedActivitiesService.class);
        intent.putExtra("delay", getDelay());
        getContext().startService(intent);
    }

    private void destroyReceiver() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(receiver);
        Intent intent = new Intent(getContext(), BackgroundDetectedActivitiesService.class);
        getContext().stopService(intent);
    }

    public class PhysicalActivityBroadcastReceiver extends BroadcastReceiver {
        public PhysicalActivityBroadcastReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if ("activity".equals(intent.getAction())) {
                int type = intent.getIntExtra("type", -1);
                int confidence = intent.getIntExtra("confidence", 0);

                save(type, confidence);
                if(type == DetectedActivity.ON_FOOT) {
                    List<DetectedActivity> probables = new GsonBuilder().create()
                            .fromJson(intent.getStringExtra("probables"), new TypeToken<List<DetectedActivity>>() {}.getType());
                    if(probables!=null && !probables.isEmpty() && probables.get(1)!=null) {
                        save(probables.get(1).getType(), probables.get(1).getConfidence());
                    }
                }
            }
        }
    }
}
