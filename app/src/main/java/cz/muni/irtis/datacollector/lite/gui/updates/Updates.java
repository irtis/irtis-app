package cz.muni.irtis.datacollector.lite.gui.updates;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import androidx.core.content.FileProvider;

import java.io.File;
import java.util.Arrays;

import cz.muni.irtis.datacollector.lite.application.Applications;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;

public class Updates {
    Context context;

    public Updates(Context context) {
        this.context = context;
    }

    static public Updates build(Context context) {
        return new Updates(context);
    }

    public File getDirectory() {
        String path = context.getCacheDir().getAbsolutePath()+"/updates";
        File directory = new File(path);
        if(!directory.exists()) {
            directory.mkdir();
        }

        return directory;
    }

    public File create(String name) {
        try {
            File file = new File(getDirectory().getAbsolutePath()+"/"+name);
            file.createNewFile();
            return file;
        } catch (Exception e) {
            Debug.getInstance().exception(e, this);
            return null;
        }
    }

    public File getNewest() {
        File directory = getDirectory();
        File[] files = directory.listFiles();
        Arrays.sort(files);
        if(files.length!=0) {
            return files[0];
        } else {
            return null;
        }
    }

    public void delete() {
        File directory = getDirectory();
        File[] files = directory.listFiles();
        for(int i=0; i<files.length; i++) {
            files[i].delete();
        }
    }

    public boolean isUpdate() {
        File directory = getDirectory();
        File[] files = directory.listFiles();
        for(int i=0; i<files.length; i++) {
            File file = files[i];
            if(file!=null) {
                Integer version = Integer.valueOf(file.getName().replace(".apk", ""));
                if(version > Applications.getPackageInfo(context).versionCode) {
                    return true;
                }
            }
        }

        return false;
    }


    public Intent createInstallDialogIntent() {
        Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
        Uri uri = Uri.fromFile(getNewest());
        if (Build.VERSION.SDK_INT >= 24) {
            uri = FileProvider.getUriForFile(context, context.getPackageName(), getNewest());
        }
        intent.setDataAndType(uri, "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        return intent;
    }
}
