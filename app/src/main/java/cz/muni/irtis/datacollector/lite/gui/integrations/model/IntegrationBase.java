package cz.muni.irtis.datacollector.lite.gui.integrations.model;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;

abstract public class IntegrationBase {
    private Context context;

    public IntegrationBase(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }


    /**
     * isConnected()
     *
     * Check if the integration connection to a service isActive established
     *
     * @return
     */
    abstract public boolean isConnected();

    /**
     * connect()
     *
     * Connect from the integrated service
     *
     * @param activity
     */
    abstract public void connect(Activity activity);

    /**
     * disconnect()
     *
     * Disconnect from the integrated service
     */
    abstract public void disconnect();

    /**
     * isApplication()
     *
     * Check if the integration has an service client app on the device
     *
     * @return
     */
    abstract public boolean isApplication();

    /**
     * getName()
     *
     * Get name of the service
     *
     * @return
     */
    abstract public String getName();

    /**
     * getImage()
     *
     * Get image of the service
     *
     * @return
     */
    abstract public Drawable getImage();

    /**
     * onActivityResult()
     *
     * Process results from authorization/signin activities of the integrated service
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    abstract public void onActivityResult(int requestCode, int resultCode, Intent data);
}
