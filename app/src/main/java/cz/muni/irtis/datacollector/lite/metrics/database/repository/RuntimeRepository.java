package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.RuntimeEntity;

@Dao
public interface RuntimeRepository extends IRepository<RuntimeEntity> {
    public static final String TABLE_NAME = "runtime";

    /**
     * Add new entity to database.
     * @param item entity
     */
    @Insert
    long insert(RuntimeEntity item);

    @Query("UPDATE " + TABLE_NAME + " SET stopped = :stopped WHERE datetime = :time")
    void update(Long time, Long stopped);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE type = :type ORDER BY datetime DESC LIMIT 1")
    RuntimeEntity getLast(Integer type);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long store(RuntimeEntity item);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE type = :type")
    List<RuntimeEntity> getByType(Integer type);

    /**
     * {@link IRepository#getPrevious(Long)}
     */
    @Query("SELECT * FROM " + TABLE_NAME + " " +
            "WHERE datetime < :time" + " " +
//            "AND uploaded IS NULL"
    "")
    List<RuntimeEntity> getPrevious(Long time);

    /**
     * {@link IRepository#getPrevious(Long)}
     */
    @Query("SELECT * FROM " + TABLE_NAME + " " +
            "WHERE datetime < :time" + " " +
            "AND stopped IS NOT NULL" + " " +
            "AND uploaded IS NULL" + " " +
            "LIMIT :offset, :count")
    List<RuntimeEntity> getPrevious(Long time, int offset, int count);

    /**
     * {@link IRepository#deleteById(Long)}
     */
    @Query("UPDATE " + TABLE_NAME + " SET uploaded = :id WHERE datetime = :id AND uploaded IS NULL")
    int deleteById(Long id);

//    @Query("DELETE FROM " + TABLE_NAME + " " +
//            "WHERE " + TABLE_NAME + ".datetime IN (" +
//            "SELECT " + TABLE_NAME + ".datetime FROM " + TABLE_NAME + " " +
//            "WHERE datetime < :time" + " " +
//            "LIMIT :offset, :count)")
//    void deletePrevious(Long time, int offset, int count);
}
