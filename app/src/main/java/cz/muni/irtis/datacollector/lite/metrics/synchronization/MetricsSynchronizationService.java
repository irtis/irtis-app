package cz.muni.irtis.datacollector.lite.metrics.synchronization;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.components.TaskServiceBase;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.synchronization.components.SynchronizationJob;
import cz.muni.irtis.datacollector.lite.metrics.synchronization.components.SynchronizationNotification;
import cz.muni.irtis.datacollector.lite.persistence.model.Events;

public class MetricsSynchronizationService extends TaskServiceBase {
    private SynchronizationNotification notification;
    private MetricsSynchronizationManager synchronization;

    public MetricsSynchronizationService(Context context) {
        super(context, MetricsSynchronizationService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        notification = SynchronizationNotification.build(getContext());
        synchronization = new MetricsSynchronizationManager(getContext(), new MetricsSynchronizationManager.OnSynchronizationListener() {
            @Override
            public void onStart() {
                notification.show();
                Events.build(getContext()).log(Events.EVENT_SYNCHRONIZATION_PROCESSING);
            }

            @Override
            public void onError(int error) {
                switch (error) {
                    default:
                        SynchronizationJob.schedule(getContext(), Config.SYNCHRONIZATION_REFRESH_PERIOD / 2);
                        break;
                }
                notification.hide();
                stop(getContext());
            }

            @Override
            public void onDone(Boolean result, Long elapsed) {
                if (result) {
                    // Save last sync setting
                    SharedPreferences settings = Settings.getSettings();
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putLong(Settings.SYNCHRONIZATION_LAST, Time.getTime());
                    editor.commit();

                    // Reschedule
                    SynchronizationJob.schedule(getContext());
                }
                notification.hide();
                stop(getContext());

                // Log elapsed time
                DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                symbols.setGroupingSeparator(',');
                formatter.setDecimalFormatSymbols(symbols);
                Debug.getInstance().task(getType().getSimpleName(), "Service elapsed: " + formatter.format(elapsed));
            }
        });

        Connection.isAvailable(new Connection.IsAvailableListener() {
            @Override
            public void onSuccess() {
                synchronization.start();
            }

            @Override
            public void onFailure() {
                Debug.getInstance().task(getType().getSimpleName(), "Service is unable to connect to the host");
                SynchronizationJob.schedule(getContext(), Config.SYNCHRONIZATION_REFRESH_PERIOD / 2);
                notification.hide();
                stop(getContext());
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    /**
     * isRunning()
     * <p>
     * Check if the service isActive running
     *
     * @param context
     * @return
     */
    static public boolean isSyncing(Context context) {
        return TaskServiceBase.isServiceRunning(context, MetricsSynchronizationService.class);
    }

    /**
     * start()
     * <p>
     * Start the service
     *
     * @param context
     */
    public static void start(Context context) {
        TaskServiceBase.startService(context, MetricsSynchronizationService.class);
    }

    /**
     * stop()
     * <p>
     * Stop the service
     *
     * @param context
     */
    public static void stop(Context context) {
        TaskServiceBase.stopService(context, MetricsSynchronizationService.class);
    }

    public static boolean isOutOfSynchronization() {
        Long lastSync = Settings.getSettings().getLong(Settings.SYNCHRONIZATION_LAST, 0);
        Long nextSyncSchedule = lastSync + (Config.SYNCHRONIZATION_REFRESH_PERIOD * Time.MINUTE);
        return nextSyncSchedule < Time.getTime();
    }
}
