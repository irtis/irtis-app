package cz.muni.irtis.datacollector.lite.gui.bursts;

import android.os.Bundle;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.TextActivityBase;


public class BurstsHelpActivity extends TextActivityBase {
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setTitle(R.string.bursts);

        setContent(getString(R.string.bursts_help), new OnSetContentListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onDone() {

            }
        });
    }
}
