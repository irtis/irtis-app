package cz.muni.irtis.datacollector.lite.gui.messages.components;


import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.NotificationBase;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.MainActivity;
import cz.muni.irtis.datacollector.lite.gui.messages.MessagesFragment;
import cz.muni.irtis.datacollector.lite.gui.messages.MessagesService;
import cz.muni.irtis.datacollector.lite.gui.messages.model.Message;
import cz.muni.irtis.datacollector.lite.gui.messages.model.MessagesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesService;
import cz.muni.irtis.datacollector.lite.persistence.model.Events;

public class MessagesNotification extends NotificationBase {
    private static final int NOTIFICATION_IDENTIFIER = 50;

    private NotificationCompat.Builder builder;
    private MessagesModel model;
    private Message message;

    private MessagesNotification(Context context, Message message) {
        super(context);
        model = new MessagesModel(context);
        this.message = message;
    }

    static public MessagesNotification build(Context context, Message message) {
        return new MessagesNotification(context, message);
    }

    @Override
    public Notification create() {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(MainActivity.PARAM_SELECTED_FRAGMENT, MessagesFragment.class.getSimpleName());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pending = PendingIntent.getActivity(
                context, NOTIFICATION_IDENTIFIER, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder = new NotificationCompat.Builder(context, CHANNEL_HIGH)
            .setSmallIcon(R.drawable.ic_message_grey_24dp)
            .setSubText(context.getString(R.string.messages))
            .setContentTitle(context.getString(R.string.messagesNew))
            .setContentText(message.getText())
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message.getText()))
            .setContentIntent(pending)
            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
            .setAutoCancel(true)
            .setOnlyAlertOnce(true);

        Debug.getInstance().warning(MessagesService.TAG,
                "Notification created | Value: "+(message.getText()!=null ? message.getText() : "null"));

        return notification = builder.build();
    }

    public void show() {
        manager.notify((int) (NOTIFICATION_IDENTIFIER+message.getId()), create());
        if(!message.isNotified()) {
            model.setNotified(message, Time.getTime());
        }
        Events.build(context).log(Events.EVENT_MESSAGES_NOTIFIED);
    }

    public void hide() {
        if(notification!=null){
            manager.cancel((int) (NOTIFICATION_IDENTIFIER+message.getId()));
        }
    }
}