package cz.muni.irtis.datacollector.lite.gui.permissions;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;

import androidx.appcompat.app.AlertDialog;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.permissions.Permission;

public class BatteryOptimizationDialog extends PermissionDialogBase {

    public BatteryOptimizationDialog(Context context, Permission permission)
    {
        super(context, permission, BatteryOptimizationDialog.class.getSimpleName());
    }

    @Override
    protected AlertDialog onBuilderCreate(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getContext().getString(R.string.permissions_batteryoptimization))
                .setIcon(R.drawable.ic_battery_60_grey_24dp)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        getContext().startActivity(intent);
                    }
                });
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            builder.setMessage(Html.fromHtml(getContext().getString(R.string.permissions_batteryoptimization_dialog).replace("{appname}", getContext().getString(R.string.app_name)), Html.FROM_HTML_MODE_LEGACY));
        } else {
            builder.setMessage(Html.fromHtml(getContext().getString(R.string.permissions_batteryoptimization_dialog).replace("{appname}", getContext().getString(R.string.app_name))));
        }
        return builder.create();
    }

    public void show() {
        if(!getPermission().isGranted()) {
            Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            intent.setData(Uri.parse("package:" + getContext().getPackageName()));
            getContext().startActivity(intent);
        } else {
            getDialog().show();
        }
    }
}