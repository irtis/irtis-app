package cz.muni.irtis.datacollector.lite.application.utils;

import org.joda.time.format.DateTimeFormat;

import java.util.Calendar;

public class Time {
    public static final long SECOND = 1000;
    public static final long MINUTE = 60*SECOND;
    public static final long HOUR = 60*MINUTE;
    public static final long DAY = 24*HOUR;
    public static final long WEEK = 7*DAY;
    public static final long MONTH = 30*DAY;


    public static long getTime() {
        return System.currentTimeMillis();
    }

    public static Long getTime(int year, int month, int day, int hour, int minute, int second) {
        if(day<=0) {
            month -= 1;
            day = getDaysInMonth(year, month) - day;
        }
        if(month<=0) {
            year -= 1;
            month = 12 - month;
        }

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month-1);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTimeInMillis();
    }

    public static Long getTime(int year, int month, int day) {
        return getTime(year, month, day, 0, 0, 0);
    }

    public static int getTimeUnix() {
        return (int) (long) (getTime() / 1000L);
    }

    public static String getTimeStamp(long time, String format) {
        return DateTimeFormat.forPattern(format).print(time);
    }
    public static String getTimeStamp(long time) {
        return getTimeStamp(time, "yyyy-MM-dd HH:mm:ss");
    }
    public static String getTimeStamp(String format) {
        return getTimeStamp(getTime(), format);
    }
    public static String getTimeStamp() {
        return getTimeStamp("yyyy-MM-dd HH:mm:ss");
    }


    static public Integer getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }
    static public Integer getYear(Integer time) {
        return Integer.valueOf(getTimeStamp(time, "yyyy"));
    }
    static public Integer getYear(Long time) {
        return Integer.valueOf(getTimeStamp(time, "yyyy"));
    }

    static public Integer getMonth() {
        return Calendar.getInstance().get(Calendar.MONTH)+1;
    }
    static public Integer getMonth(Integer time) {
        return Integer.valueOf(getTimeStamp(time, "MM"));
    }
    static public Integer getMonth(Long time) {
        return Integer.valueOf(getTimeStamp(time, "MM"));
    }

    static public Integer getDay() {
        return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    }
    static public Integer getDay(Integer time) {
        return Integer.valueOf(getTimeStamp(time,"dd"));
    }
    static public Integer getDay(Long time) {
        return Integer.valueOf(getTimeStamp(time,"dd"));
    }

    static public Integer getDayOfWeek() {
        return Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
    }
    static public Integer getDayOfWeek(Integer time) {
        return Integer.valueOf(getTimeStamp(time,"e"));
    }
    static public Integer getDayOfWeek(Long time) {
        return Integer.valueOf(getTimeStamp(time,"e"));
    }

    static public Integer getHour() {
        return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
    }
    static public Integer getHour(Integer time) {
        return Integer.valueOf(getTimeStamp(time, "HH"));
    }
    static public Integer getHour(Long time) {
        return Integer.valueOf(getTimeStamp(time, "HH"));
    }

    static public Integer getMinute() {
        return Calendar.getInstance().get(Calendar.MINUTE);
    }
    static public Integer getMinute(Integer time) {
        return Integer.valueOf(getTimeStamp(time, "mm"));
    }
    static public Integer getMinute(Long time) {
        return Integer.valueOf(getTimeStamp(time, "mm"));
    }


    /**
     * isDayTime()
     *
     * From 06:00 - 22:00
     *
     * @return
     */
    static public Boolean isDayTime(Integer starting, Integer ending) {
        int hour = Time.getHour();
        return hour >= starting && hour <= ending;
    }

    static public Boolean isDayTime() {
        return isDayTime(7, 21);
    }

    /**
     * isNightTime()
     *
     * From 22:00 - 06:00
     *
     * @return
     */
    static public Boolean isNightTime() {
        return !isDayTime();
    }

    static public Integer getElapsedDays(Integer time) {
        Integer days = (int) Math.ceil(Float.valueOf(time)/Float.valueOf(DAY/1000));
        return days;
    }

    static public int getDaysInMonth(int year, int month) {
        if(month<=0) {
            year -= 1;
            month = 12 + month;
        }

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month-1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    static public int getDaysInMonth(String year, String month) {
        return getDaysInMonth(Integer.valueOf(year), Integer.valueOf(month));
    }
}


