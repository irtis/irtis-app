package cz.muni.irtis.datacollector.lite.metrics.model;

import android.annotation.SuppressLint;
import android.content.Context;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.LocationEntity;
import cz.muni.irtis.datacollector.lite.metrics.model.condition.IsLocationOn;
import cz.muni.irtis.datacollector.lite.metrics.Metric;
import cz.muni.irtis.datacollector.lite.metrics.optimizer.Optimizer;

/**
 * Capture location
 *
 * @deprecated due GooglePlay's location requirements policy which we are not able to meet
 */
public class Location extends Metric {
    private final String TAG = this.getClass().getSimpleName();
    public static final int IDENTIFIER = 256;

    private FusedLocationProviderClient fusedLocationClient;
    LocationCallback locationCallback;

    private int distance;

    /**
     *
     * @param context
     * @param delay  Minimum metric collection interval (in mills)
     */
    public Location(Context context, Integer delay) {
        super(context, delay,
            Optimizer.buildExponential(context, delay, Time.MINUTE*30));

        distance = 0;

        addPrerequisity(new IsLocationOn());
        initLocationCallback();
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "GPS";
    }

    /**
     * Register location updates delay from starting intent
     */
    @Override
    @SuppressWarnings({"MissingPermission"})
    public void run() {
        if (isPrerequisitiesSatisfied()) {
            if (getOptimizer().isActive()) {
                if (isRunning()) {
                    stop();
                }
            } else {
                if (!isRunning()) {
                    start();
                }
            }
        } else {
            setRunning(false);
        }
    }

    public void start() {
        initLocationRequest();
        setRunning(true);
    }

    /**
     * Stop receiving location updates
     */
    @Override
    public void stop() {
        fusedLocationClient.removeLocationUpdates(locationCallback);
        setRunning(false);
    }

    @Override
    public long save(Object... params) {
        Double latitude = (Double) params[0];
        Double longitude = (Double) params[1];
        try {
            Debug.getInstance().log(TAG, latitude+", "+longitude);
            return getDatabase().getLocationRepository().insert(
                    new LocationEntity(Time.getTime(), latitude, longitude));
        } catch (Exception e) {
            Debug.getInstance().exception(e);
            return -1;
        }
    }



    private void onLocationChanged(android.location.Location location) {
        save((double) Math.round(location.getLatitude() * 10000d) / 10000d,
                (double) Math.round(location.getLongitude() * 10000d) / 10000d);
    }

    @SuppressLint("MissingPermission")
    private void initLocationCallback() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult != null) {
                    for (android.location.Location location : locationResult.getLocations()) {
                        if (location != null) {
                            onLocationChanged(location);
                        }
                    }
                }
            }
        };
    }

    @SuppressWarnings({"MissingPermission"})
    private void initLocationRequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(getDelay());
        locationRequest.setFastestInterval(getDelay());
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback,
               getContext().getMainLooper());

        // Save last known location
        // See https://developers.google.com/android/reference/com/google/android/gms/location/FusedLocationProviderClient#getLastLocation()
        fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<android.location.Location>() {
            @Override
            public void onSuccess(android.location.Location location) {
                if(location!=null) {
                    onLocationChanged(location);
                }
            }
        });
    }
}
