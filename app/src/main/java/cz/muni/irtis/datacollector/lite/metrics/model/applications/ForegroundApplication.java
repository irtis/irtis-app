package cz.muni.irtis.datacollector.lite.metrics.model.applications;

import android.app.ActivityManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;

import com.rvalerio.fgchecker.AppChecker;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationForegroundEntity;
import cz.muni.irtis.datacollector.lite.metrics.model.condition.IsScreenOn;
import cz.muni.irtis.datacollector.lite.metrics.model.condition.IsUsageStatsAllowed;
import cz.muni.irtis.datacollector.lite.metrics.Metric;

public class ForegroundApplication extends Metric {
    private static final String TAG = ForegroundApplication.class.getSimpleName();
    public static final int IDENTIFIER = 247;

    private AppChecker applications;

    public ForegroundApplication(Context context, Integer delay) {
        super(context, delay);

        applications = new AppChecker();

        addPrerequisity(new IsScreenOn());
        addPrerequisity(new IsUsageStatsAllowed());
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "Application";
    }

    @Override
    public void run() {
        if (!isPrerequisitiesSatisfied()) {
            return;
        }

        setRunning(true);

        String packageName = applications.getForegroundApp(getContext());
        save(packageName);
    }

    @Override
    public void stop() {
        setRunning(false);
    }

    @Override
    public long save(Object... params) {
        String packageName = (String) params[0];

        // save new apps
        long appId1 = getAppId(packageName);
        if (appId1 <= 0) {
            appId1 = saveNewAppName(packageName);
        }

        if (appId1 != -1) {
            try {
                Long time = Time.getTime();
                Debug.getInstance().log(TAG, packageName);
                return getDatabase().getForegroundApplicationRepository().insert(
                        new ApplicationForegroundEntity(time, appId1));
            } catch (Exception e) {
                Debug.getInstance().exception(e);
                return -1;
            }
        }

        return -1;
    }

    private long getAppId(String name) {
        ApplicationEntity entity = getDatabase().getApplicationRepository().getIdByName(name);
        if(entity!=null) {
            return entity.getId();
        } else {
            return -1;
        }
    }

    private long saveNewAppName(String name) {
        return getDatabase().getApplicationRepository().insert(new ApplicationEntity(Time.getTime(), name));
    }
}
