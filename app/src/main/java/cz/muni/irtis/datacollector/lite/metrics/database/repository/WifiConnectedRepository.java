package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi.WifiConnectedEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi.WifiEntity;

@Dao
public interface WifiConnectedRepository extends IRepository<WifiEntity> {
    public static final String TABLE_NAME = "wifis_connected";

    /**
     * Add new entity to database.
     * @param item entity
     */
    @Insert
    long insert(WifiConnectedEntity item);

    /**
     * Get entity by it's PK datetime
      * @param time java style timestamp (1561544094*1000)
     * @return entity
     */
    @Query("SELECT "+TABLE_NAME+".datetime, "+ WifiRepository.TABLE_NAME+".ssid " +
            "FROM "+TABLE_NAME + " JOIN " + WifiRepository.TABLE_NAME + " ON " + WifiRepository.TABLE_NAME + ".datetime = " + TABLE_NAME + ".wifis_ssid_id" + " " +
            "WHERE "+TABLE_NAME+".datetime = :time")
    WifiEntity getByTime(Long time);

    /**
     * {@link IRepository#getPrevious(Long)}
     */
    @Query("SELECT "+TABLE_NAME+".datetime, "+ WifiRepository.TABLE_NAME+".ssid " +
            "FROM "+TABLE_NAME + " JOIN " + WifiRepository.TABLE_NAME + " ON " + WifiRepository.TABLE_NAME + ".datetime = " + TABLE_NAME + ".wifis_ssid_id" + " " +
            "WHERE "+TABLE_NAME+".datetime < :time")
    List<WifiEntity> getPrevious(Long time);

    /**
     * {@link IRepository#getPrevious(Long, int, int)}
     */
    @Query("SELECT "+TABLE_NAME+".datetime, "+ WifiRepository.TABLE_NAME+".ssid " +
            "FROM "+TABLE_NAME + " JOIN " + WifiRepository.TABLE_NAME + " ON " + WifiRepository.TABLE_NAME + ".datetime = " + TABLE_NAME + ".wifis_ssid_id" + " " +
            "WHERE "+TABLE_NAME+".datetime < :time" + " " +
            "LIMIT :offset, :count")
    List<WifiEntity> getPrevious(Long time, int offset, int count);

    /**
     * {@link IRepository#deleteById(Long)}
     */
    @Query("DELETE FROM " + TABLE_NAME + " WHERE datetime = :id")
    int deleteById(Long id);

//    @Query("DELETE FROM " + TABLE_NAME + " " +
//            "WHERE " + TABLE_NAME + ".datetime IN (" +
//            "SELECT " + TABLE_NAME + ".datetime FROM " + TABLE_NAME + " " +
//            "WHERE datetime < :time" + " " +
//            "LIMIT :offset, :count)")
//    void deletePrevious(Long time, int offset, int count);
}