package cz.muni.irtis.datacollector.lite.persistence.runtime;

import android.content.Context;

/**
 * RuntimeApplication
 */
public class RuntimeApplication extends RuntimeBase {
    private RuntimeApplication(Context context) {
        super(context);
    }

    static public RuntimeApplication build(Context context) {
        return new RuntimeApplication(context);
    }

    @Override
    public void start() {
        started(TYPE_APPLICATION);
    }

    @Override
    public void stop() {
        stopped(TYPE_APPLICATION);
    }
}
