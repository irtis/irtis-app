package cz.muni.irtis.datacollector.lite.application.permissions;

import android.content.Context;

import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_ACCESSIBILITY;

/**
 * PermissionAccessibility
 *
 */
public class PermissionAccessibility extends Permission {

    public PermissionAccessibility(Context context, String title, String notes) {
        super(context, PERMISSION_ACCESSIBILITY, title, notes);
        setRequired(false);
    }

    public boolean isGranted()
    {
        return provider.isGrantedAccessibilityPermission();
    }

}
