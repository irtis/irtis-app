package cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities;

public class FileEntity {
    private String url;
    private Integer width;
    private Integer height;

    public String getUrl() {
        return url;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }
}
