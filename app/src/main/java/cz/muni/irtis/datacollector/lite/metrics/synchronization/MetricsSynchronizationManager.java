package cz.muni.irtis.datacollector.lite.metrics.synchronization;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.MetricsDatabase;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.EntityBase;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenshotEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ApplicationsBackgroundRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ApplicationsForegroundRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.IRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.PhoneCallsRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.PhoneSmsesRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.RuntimeRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.WifiAvailableRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.WifiConnectedRepository;
import okhttp3.Response;

/**
 * SyncManager
 *
 * The class isActive designed for synchronous execution
 *
 */
public class MetricsSynchronizationManager {
    private final String TAG = this.getClass().getSimpleName();

    public static final int RESULT_OK = 1;
    public static final int RESULT_BAD = 0;

    public static final int STATE_STARTED = 10;
    public static final int STATE_FINISHED = 20;

    public static final int TASK_SUCCESS = 148;
    public static final int TASK_FAILURE = 389;

    public static final int ERROR_NETWORK = 568;
    public static final int ERROR_WIFI = 841;
    public static final int ERROR_SERVER_IS_BUSY = 379;
    public static final int ERROR_CANCELLED = 894;


    private Context context;
    private boolean isInProgress;
    private ManagerTask hManagerTask;
    private MetricsDatabase database;
    private Gson gson;

    public interface SyncRunnable {
        void run(MetricsDatabase database, Long time);
    }


    public interface OnSynchronizationListener {
        public void onStart();
        public void onError(int error);
        public void onDone(Boolean result, Long elapsed);
    }
    private OnSynchronizationListener listener;

    public MetricsSynchronizationManager(Context context, OnSynchronizationListener listener) {
        this.context = context;
        this.listener = listener;
        database = MetricsDatabase.getDatabase(context);
        gson = new Gson();
    }

    public void start() {
        if(!isInProgress) {
            hManagerTask = new ManagerTask();
            hManagerTask.execute();
        }
    }

    public void stop() {
        if(isInProgress) {
            hManagerTask.cancel(true);
            hManagerTask = null;
            isInProgress = false;
        }
    }

    public boolean isInProgress() {
        return isInProgress;
    }


    /**
     * isServerBusy()
     *
     * Check if the server isActive able to process our request for synchronization
     *
     * @todo server select request
     *
     * @return
     */
    private boolean isServerBusy() {
//        Connection.build(context).get();
        return false;
    }

    private class ManagerTask extends AsyncTask<Void, Void, Integer> {
        long tBegin;

        @Override
        protected void onPreExecute() {
            tBegin = Time.getTime();
            isInProgress = true;
            listener.onStart();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            long time = Time.getTime();
            if(Network.isConnection()) {
                if(Network.isWIFI()) {
                    if(!isServerBusy()) {

                        // The list of metrics to be synchronized
                        // The reason for iterability isActive to be able to step in with some insurance operations such as isCancel
                        List<SyncRunnable> metrics = Arrays.asList(new SyncRunnable[] {
                            // Applications
                            new SyncRunnable() { // Foreground application
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getForegroundApplicationRepository(),
                                            UrlComposer.compose("metrics/applications/foreground/upload"), time);
                                }
                            },
                            new SyncRunnable() { // Foreground application 2
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getForegroundApplicationRepository2(),
                                            UrlComposer.compose("metrics/applications/foreground2/upload"), time);
                                }
                            },
                            new SyncRunnable() { // Background application
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getBackgroundApplicationRepository(),
                                            UrlComposer.compose("metrics/applications/background/upload"), time);
                                }
                            },

                            // Runtime
                            new SyncRunnable() {
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getRuntimeRepository(),
                                            UrlComposer.compose("metrics/runtime/upload"), time);
                                }
                            },

                            // Screen
                            new SyncRunnable() {
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getScreenRepository(),
                                            UrlComposer.compose("metrics/screen/screens/upload"), time);
                                }
                            },
                            new SyncRunnable() {
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getScreenTapsRepository(),
                                            UrlComposer.compose("metrics/screen/taps/upload"), time);
                                }
                            },
                            new SyncRunnable() {
                                public void run(MetricsDatabase database, Long time) { // Screenshots
                                    syncScreenshotMetric(database.getScreenshotRepository(),
                                            UrlComposer.compose("metrics/screen/screenshots/upload"), time);
                                }
                            },
                            new SyncRunnable() {
                                public void run(MetricsDatabase database, Long time) { // Metadata
                                    syncMetric(database.getScreenshotsMetadataRepository(),
                                            UrlComposer.compose("metrics/screen/screenshots/metadata/upload"), time);
                                }
                            },


                            new SyncRunnable() { // Battery state
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getBatteryStateRepository(),
                                            UrlComposer.compose("metrics/battery/upload"), time);
                                }
                            },
                            new SyncRunnable() { // Location
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getLocationRepository(),
                                            UrlComposer.compose("metrics/locations/upload"), time);
                                }
                            },
                            new SyncRunnable() { // Physical activity
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getPhysicalActivityRepository(),
                                            UrlComposer.compose("metrics/activities/upload"), time);
                                }
                            },
                            new SyncRunnable() { // Headphones
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getHeadphonesRepository(),
                                            UrlComposer.compose("metrics/headphones/upload"), time);
                                }
                            },
                            new SyncRunnable() { // Playback
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getPlaybackRepository(),
                                            UrlComposer.compose("metrics/playback/upload"), time);
                                }
                            },
                            new SyncRunnable() { // Steps
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getStepsRepository(),
                                            UrlComposer.compose("metrics/steps/upload"), time);
                                }
                            },
                            new SyncRunnable() { // Notifications
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getNotificationsRepository(),
                                            UrlComposer.compose("metrics/notifications/upload"), time);
                                }
                            },

                            // Phone
                            new SyncRunnable() { // Call history
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getCallHistoryRepository(),
                                            UrlComposer.compose("metrics/phone/calls/upload"), time);
                                }
                            },
                            new SyncRunnable() { // Sms conversation
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getSmsConversationRepository(),
                                            UrlComposer.compose("metrics/phone/smses/upload"), time);
                                }
                            },

                            // Wifis
                            new SyncRunnable() {
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getAvailableWifiRepository(),
                                            UrlComposer.compose("metrics/wifi/available/upload"), time);
                                }
                            },
                            new SyncRunnable() {
                                public void run(MetricsDatabase database, Long time) {
                                    syncMetric(database.getConnectedWifiRepository(),
                                            UrlComposer.compose("metrics/wifi/connected/upload"), time);
                                }
                            },
                        });


                        Iterator<SyncRunnable> iterator = metrics.iterator();
                        while(iterator.hasNext()) {
                            // Check if the sync isActive not cancelled for whatever reason
                            if(isCancelled()) {
                                return ERROR_CANCELLED;
                            }
                            try {
                                // Do the synchronization
                                SyncRunnable sync = iterator.next();
                                sync.run(database, time);
                            } catch (Exception e) {
                                Debug.getInstance().exception(e);
                            }
                        }
                    } else {
                        return ERROR_SERVER_IS_BUSY;
                    }
                } else {
                    return ERROR_WIFI;
                }
            } else {
                return ERROR_NETWORK;
            }

            return RESULT_OK;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if(result.equals(RESULT_OK)) {
                listener.onDone(true, Time.getTime()-tBegin);
            } else {
                Debug.getInstance().error(TAG, result.toString());
                listener.onError(result);
            }
            isInProgress = false;
        }
    }

    /**
     * Synchronize metric
     *
     * @param repository
     * @param url
     * @param time
     */
    private void syncMetric(final IRepository repository, String url, long time) {
        List<EntityBase> items = null;
        int offset = 0;
        int count = 1000;
        while(!(items = repository.getPrevious(time, offset, count)).isEmpty()) {
            String json = gson.toJson(items);
            if(Network.isWIFI()) {
                if (Connection.isAvailable()) {
                    SyncConnection syncConnection = new SyncConnection(url, time);
                    Connection.build(context).postJson(url, json, syncConnection, false);
                    if (syncConnection.isSuccessful()) {
                        if (repository instanceof PhoneCallsRepository ||
                                repository instanceof PhoneSmsesRepository ||
                                repository instanceof RuntimeRepository ||
                                repository instanceof WifiAvailableRepository ||
                                repository instanceof WifiConnectedRepository ||
                                repository instanceof ApplicationsForegroundRepository ||
                                repository instanceof ApplicationsBackgroundRepository) {
                            final Iterator<EntityBase> iItems = items.iterator();
                            database.runInTransaction(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    int affected = 0;
                                    while (iItems.hasNext()) {
                                        affected += repository.deleteById(iItems.next().getId());
                                    }
                                    return null;
                                }
                            });
                        } else {
                            repository.delete(items);
                        }
                    } else {
                        Debug.getInstance().task(this.getClass().getSimpleName(), "Server failed to receive the data");
                        return;
                    }
                } else {
                    Debug.getInstance().task(this.getClass().getSimpleName(), "Service is unable to connect to the host");
                    return;
                }
            } else {
                Debug.getInstance().task(this.getClass().getSimpleName(), "Service is unable to send date due non-wifi connection");
                return;
            }
        }
    }

    /**
     * Synchronize screenshot metric = JSON & Files
     *
     * @param repository
     * @param url
     * @param time
     */
    private void syncScreenshotMetric(final IRepository repository, String url, long time) {
        List<ScreenshotEntity> items = null;
        int offset = 0;
        int count = 1000;
        while(!(items = repository.getPrevious(time, offset, count)).isEmpty()) {
            Iterator<ScreenshotEntity> iItems = items.iterator();
            while (iItems.hasNext()) {
                ScreenshotEntity entity = iItems.next();
                File file = new File(entity.getDeviceUrl());
                if (file.exists()) {
                    if(Network.isWIFI()) {
                        if (Connection.isAvailable()) {
                            SyncConnection syncConnectionFile = new SyncConnection(url, time);
                            Connection.build(context).postFile(url, file, syncConnectionFile, false);
                            if (syncConnectionFile.isSuccessful()) {
                                file.delete();
                                repository.delete(entity);
                            } else {
                                Debug.getInstance().task(this.getClass().getSimpleName(), "Server failed to receive the data");
                                return;
                            }
                        } else {
                            Debug.getInstance().task(this.getClass().getSimpleName(), "Service is unable to connect to the host");
                            return;
                        }
                    } else {
                        Debug.getInstance().task(this.getClass().getSimpleName(), "Service is unable to send date due non-wifi connection");
                        return;
                    }
                } else {
                    repository.delete(entity);
                }
            }
        }
    }


    private class SyncConnection implements Connection.OnConnectionListener<String> {
        private String url;
        private long time;
        private boolean isSuccessful;

        public SyncConnection(String url, long time) {
            this.url = url;
            this.time = time;
            isSuccessful = false;
            Debug.getInstance().log(TAG, "Task started: "+url);
        }

        @Override
        public void onStart() {}
        @Override
        public void onError(Exception e) {}
        @Override
        public void onDone(Response response, String content) {
            if (response != null) {
                switch (response.code()) {
                    default:    // Errors
                        String message = "";
                        if(content!=null) {
                            try {
                                if (content != null) {
                                    JSONObject json = new JSONObject(content);
                                    message = url+": failed ("+response.code() + (json.getString("message")!=null ? ": " + json.getString("message") : "")+")";
                                }
                            } catch (JSONException e) {
                                Debug.getInstance().exception(e, this);
                                message = url+": failed ("+response.code() + (!response.message().isEmpty() ? ": " + response.message() : "")+")";
                            }
                        } else {
                            message = url+": failed ("+response.code() + (!response.message().isEmpty() ? ": " + response.message() : "")+")";
                        }
                        Debug.getInstance().error(TAG, message);
                        break;
                    case 200:
                        Debug.getInstance().log(TAG, "Task finished: "+url+" (result="+response.code()+")");
                        isSuccessful = true;
                        break;
                }
            }
        }

        public boolean isSuccessful() {
            return isSuccessful;
        }
    }
}

