package cz.muni.irtis.datacollector.lite.metrics.model;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.Metric;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.StepEntity;

/**
 * Playback
 *
 */
public class Steps extends Metric {
    private static final String TAG = Steps.class.getSimpleName();
    public static final int IDENTIFIER = 260;

    private Long lastTime;

    private SensorManager manager;
    private SensorEventListener listener;

    public Steps(Context context, Integer delay) {
        super(context, delay);

        manager = (SensorManager) getContext().getSystemService(Context.SENSOR_SERVICE);
        lastTime = 0L;
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "Playback";
    }

    @Override
    public void run() {
        setRunning(true);

        if(getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER)) {
            Sensor sensor = manager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
            if (sensor != null) {
                manager.registerListener(listener = new SensorEventListener() {
                    @Override
                    public void onSensorChanged(SensorEvent event) {
                        Float steps = event.values[0];
                        save(steps.intValue());
                    }

                    @Override
                    public void onAccuracyChanged(Sensor sensor, int accuracy) {

                    }
                }, sensor, SensorManager.SENSOR_DELAY_UI);
            }
        } else {
            Debug.getInstance().log(TAG, "System does not support FEATURE_SENSOR_STEP_COUNTER");
        }
    }

    @Override
    public void stop() {
        setRunning(false);
        if(listener!=null) {
            manager.unregisterListener(listener);
        }
    }

    @Override
    public long save(Object... params) {
        Integer steps = (Integer) params[0];

        if(lastTime+getDelay() < Time.getTime()) {
            lastTime = Time.getTime();
            try {
                Debug.getInstance().log(TAG, steps.toString());
                return getDatabase().getStepsRepository().insert(new StepEntity(
                        Time.getTime(),
                        steps
                ));
            } catch (Exception e) {
                Debug.getInstance().exception(e);
                return -1;
            }
        }

        return -1;
    }
}
