package cz.muni.irtis.datacollector.lite.gui.updates;


import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.NotificationBase;

public class UpdateNotification extends NotificationBase {
    private static final int IDENTIFIER = 81610;
    private Updates updates;

    private NotificationCompat.Builder builder;

    private UpdateNotification(Context context) {
        super(context);

        updates = new Updates(context);
    }

    static public UpdateNotification build(Context context) {
        return new UpdateNotification(context);
    }

    @Override
    public Notification create() {
        Intent intent = new Intent(context, UpdateInstallActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
            context, IDENTIFIER, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        builder = new NotificationCompat.Builder(context, CHANNEL_HIGH)
            .setSmallIcon(R.drawable.ic_file_download_black_24dp)
            .setSubText(context.getString(R.string.updates_notification_type))
            .setContentTitle(context.getString(R.string.updates_notification_title))
            .setContentText(context.getString(R.string.updates_notification_notes))
            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setOnlyAlertOnce(true);

        return notification = builder.build();
    }

    public void show() {
        manager.notify(IDENTIFIER, create());
    }

    public void hide() {
        if(notification!=null){
            manager.cancel(IDENTIFIER);
        }
    }

}