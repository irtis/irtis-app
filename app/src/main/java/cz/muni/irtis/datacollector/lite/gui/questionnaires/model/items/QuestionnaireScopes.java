package cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuestionnaireScopes {
    @SerializedName("answer_links")
    private List<QuestionFilter> filters;


    public List<QuestionFilter> getFilters() {
        return filters;
    }
}