package cz.muni.irtis.datacollector.lite.application.widgets;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class ExtendedProgressBar extends ProgressBar {
    private ViewPager mViewpager;

    public ExtendedProgressBar(Context context) {
        super(context);
    }

    public ExtendedProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExtendedProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ExtendedProgressBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setViewPager(@Nullable ViewPager viewPager) {
        mViewpager = viewPager;
        if (mViewpager != null && mViewpager.getAdapter() != null) {
            mViewpager.removeOnPageChangeListener(mInternalPageChangeListener);
            mViewpager.addOnPageChangeListener(mInternalPageChangeListener);
            mInternalPageChangeListener.onPageSelected(mViewpager.getCurrentItem());
            setMax(mViewpager.getAdapter().getCount());
        }
    }

    public DataSetObserver getDataSetObserver() {
        return mInternalDataSetObserver;
    }

    private final ViewPager.OnPageChangeListener mInternalPageChangeListener =
        new ViewPager.OnPageChangeListener() {

            @Override public void onPageScrolled(int position, float positionOffset,
                                                 int positionOffsetPixels) {
            }

            @Override public void onPageSelected(int position) {

                if (mViewpager.getAdapter() == null
                        || mViewpager.getAdapter().getCount() <= 0) {
                    return;
                }
                setProgress(position);
            }

            @Override public void onPageScrollStateChanged(int state) {
            }
        };

    private final DataSetObserver mInternalDataSetObserver = new DataSetObserver() {
        @Override public void onChanged() {
            super.onChanged();
            if (mViewpager == null) {
                return;
            }
            PagerAdapter adapter = mViewpager.getAdapter();
            int newCount = adapter != null ? adapter.getCount() : 0;
            int currentCount = getProgress();
            if (newCount == currentCount) {
                // No change
                return;
            } else if (getMax() < newCount) {
                setProgress(mViewpager.getCurrentItem());
            } else {
                setProgress(getMax());
            }
            setMax(mViewpager.getAdapter().getCount());
        }
    };
}
