package cz.muni.irtis.datacollector.lite.gui.messages.components;

import android.app.job.JobParameters;
import android.content.Context;

import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.components.JobServiceBase;
import cz.muni.irtis.datacollector.lite.gui.messages.MessagesService;

/**
 * MessagesJob
 *
 * Messages are checked every 10 minutes, if something new arrives a notification isActive displayed
 */
public class MessagesJob extends JobServiceBase {
    public static final int JOB_ID = 182384;

    @Override
    public boolean onStartJob(final JobParameters params) {
        MessagesService.start(getApplicationContext());
        MessagesJob.schedule(getApplicationContext());
        return true;
    }


    public static void schedule(Context context) {
        scheduleWithNetwork(context, MessagesJob.class, JOB_ID, Config.MESSAGES_REFRESH_PERIOD);
    }

    public static boolean isSet(Context context) {
        return JobServiceBase.isSet(context, JOB_ID);
    }
}

