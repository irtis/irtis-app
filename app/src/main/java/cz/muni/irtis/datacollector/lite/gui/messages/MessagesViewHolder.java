package cz.muni.irtis.datacollector.lite.gui.messages;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewAdapter;

/**
 * ViewHolderUsers
 * <p>
 * Author: Michal Schejbal<br>
 * Created: 13.12.2019
 */
public class MessagesViewHolder extends RecyclerListViewAdapter.ItemsListViewHolderBase {
    public TextView time;
    public TextView text;
    public TextView response;
    public Button answer;
    public ImageView read;

    public MessagesViewHolder(RecyclerListViewAdapter adapter, View view) {
        super(adapter, view);
        time = view.findViewById(R.id.time);
        text = view.findViewById(R.id.text);
        response = view.findViewById(R.id.response);
        read = view.findViewById(R.id.read);
        answer = view.findViewById(R.id.answer);
    }
}