package cz.muni.irtis.datacollector.lite.gui.questionnaires;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;


import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.application.widgets.ExtendedViewPager;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.widgets.QuestionnairesProgressBar;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesViewModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.types.FinishFragment;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.types.TypeFragmentBase;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.types.TypeFragmentBuilder;
import cz.muni.irtis.datacollector.lite.persistence.model.Events;
import me.relex.circleindicator.CircleIndicator;


public class QuestionnairesFormActivity extends ActivityBase {
    public static final String PARAM_ITEM_ID = "identifier";

    private Questionnaire questionnaire;
    private QuestionnairesModel model;

    private ExtendedViewPager pager;
    private ViewPagerFragmentAdapter adapter;

    private CircleIndicator indicator;
    private QuestionnairesProgressBar vProgress;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(null);
        setContentView(R.layout.activity_questionnaires_form);

        Long itemId = getIntent().getLongExtra(PARAM_ITEM_ID, -1);

        pager = findViewById(R.id.pager);
        adapter = new ViewPagerFragmentAdapter(getSupportFragmentManager());

        pager.setAdapter(adapter);
        pager.setSwipePagingEnabled(false);
        pager.setSmoothScroll(true);
        pager.setOffscreenPageLimit(1);

        indicator = findViewById(R.id.indicator);
        indicator.setViewPager(pager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());
        indicator.setVisibility(Debug.isDebug() ? View.VISIBLE : View.GONE);

        vProgress = findViewById(R.id.progress);
        vProgress.setViewPager(pager);
        vProgress.getProgressDrawable().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        adapter.registerDataSetObserver(vProgress.getDataSetObserver());

        model = QuestionnairesModel.build(this);

        if(itemId!=null && itemId!=-1) {
            get(itemId);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(questionnaire!=null) {
            if(!questionnaire.isCompleted()) {
                model.store(questionnaire, new Repository.RepositoryListener<Boolean>() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onDone(Boolean data) {
                    }
                });
            }
            Debug.getInstance().warning(QuestionnairesService.TAG,
                "Instance closed | Value: "+(questionnaire.getIdentifier()!=null ? questionnaire.getIdentifier() : "null"));
        }
    }

    private void get(Long id) {
        model.getItem(id, new Repository.RepositoryListener<Questionnaire>() {
            @Override
            public void onStart() {
            }

            @Override
            public void onDone(Questionnaire item) {
                questionnaire = item;
                pager.setSwipePagingEnabled(questionnaire.isCompleted());
                indicator.setVisibility(questionnaire.isCompleted() ? View.VISIBLE : View.GONE);
                vProgress.setVisibility(questionnaire.isCompleted() ? View.GONE : View.VISIBLE);

                Iterator<Question> iQuestions = questionnaire.getQuestions().iterator();
                while(iQuestions.hasNext()) {
                    Question question = iQuestions.next();
                    if(questionnaire.isCompleted()) {
                        if(question.isCompleted()) {
                            adapter.addItem(TypeFragmentBuilder.build(QuestionnairesFormActivity.this, question, false));
                        }
                    } else {
                        adapter.addItem(TypeFragmentBuilder.build(QuestionnairesFormActivity.this, question, true));
                    }
                }

//                // Debug
//                adapter.addItem(new FinishFragment(QuestionnairesFormActivity.this, questionnaire));

                if(!questionnaire.isFinished()) {
                    adapter.addItem(new FinishFragment(QuestionnairesFormActivity.this, questionnaire));
                }

                if(!questionnaire.isOpened()) {
                    QuestionnairesModel model = new QuestionnairesModel(QuestionnairesFormActivity.this);
                    model.setOpened(questionnaire, Time.getTime());
                }

                Events.build(QuestionnairesFormActivity.this).log(Events.EVENT_QUESTIONNAIRES_OPENED);

                Debug.getInstance().warning(QuestionnairesService.TAG,
                        "Instance opened | Value: "+(questionnaire.getIdentifier()!=null ? questionnaire.getIdentifier() : "null"));
            }
        });
    }

    /**
     * ViewPagerFragmentAdapter
     */
    private class ViewPagerFragmentAdapter extends ExtendedViewPager.ExtendedViewPagerFragmentAdapter {
        public ViewPagerFragmentAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addItem(TypeFragmentBase fragment) {
            this.items.add(fragment);
            fragment.setViewPager(pager);
            this.notifyDataSetChanged();
        }
    }


    static public void start(Context context, Questionnaire item) {
        Intent i = new Intent(context, QuestionnairesFormActivity.class);
        i.putExtra(PARAM_ITEM_ID, item.getId());
        context.startActivity(i);
    }
}
