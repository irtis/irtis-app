package cz.muni.irtis.datacollector.lite.metrics.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import cz.muni.irtis.datacollector.lite.metrics.database.repository.LocationsRepository;

@Entity(tableName = LocationsRepository.TABLE_NAME)
public class LocationEntity extends EntityBase {
    @ColumnInfo(name = "latitude")
    private Double latitude;
    @ColumnInfo(name = "longitude")
    private Double longitude;

    public LocationEntity(Long datetime, Double latitude, Double longitude) {
        super(datetime);
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}
