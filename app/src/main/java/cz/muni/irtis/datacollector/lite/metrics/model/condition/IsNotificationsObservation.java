package cz.muni.irtis.datacollector.lite.metrics.model.condition;

import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationManagerCompat;

import cz.muni.irtis.datacollector.lite.metrics.model.util.ScreenState;

public class IsNotificationsObservation implements Condition {
    @Override
    public boolean check(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            if (NotificationManagerCompat.getEnabledListenerPackages(context).contains(context.getPackageName())) {
               return true;
            }
        }

        return false;
    }
}
