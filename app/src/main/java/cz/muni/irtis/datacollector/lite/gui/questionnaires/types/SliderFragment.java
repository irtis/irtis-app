package cz.muni.irtis.datacollector.lite.gui.questionnaires.types;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;

import java.util.Iterator;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;

public class SliderFragment extends TypeFragmentBase {
    public SliderFragment(Context context, Question question, boolean editable) {
        super(context, question, R.layout.activity_questionnaires_form_type_slider, editable);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        final SeekBar slider = view.findViewById(R.id.slider);
        slider.setMax(getQuestion().getDensity());
        slider.setProgress(slider.getMax() / 2);
        slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                Answer answer = getQuestion().getAnswer();
                answer.setValue(String.valueOf(progress));
                refresh();
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });
        slider.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Answer answer = getQuestion().getAnswer();
                answer.setValue(String.valueOf(slider.getProgress()));
                slider.getProgressDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                slider.getThumb().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
                refresh();
                return false;
            }
        });
        slider.setEnabled(isEditable());
        if (getQuestion().getAnswer().isValue()) {
            slider.setProgress(Integer.valueOf(getQuestion().getAnswer().getValue()));
        } else {
            slider.setProgressTintList(ColorStateList.valueOf(Color.TRANSPARENT));
            slider.setProgressBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorGreyDark)));
            slider.getThumb().setColorFilter(getResources().getColor(R.color.colorGreyDark), PorterDuff.Mode.SRC_ATOP);
        }

        FlexboxLayout vLabelsContainer = view.findViewById(R.id.labels);
        Iterator<Answer> iLabels = getQuestion().getAnswers().iterator();
        while(iLabels.hasNext()) {
            Answer label = iLabels.next();
            TextView vElement = new TextView(vLabelsContainer.getContext());
            vElement.setText(label.getText());
            vLabelsContainer.addView(vElement);
        }

        return view;
    }
}
