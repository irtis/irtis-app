package cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.connection;

import org.json.JSONException;
import org.json.JSONObject;

public class Response {
    private String data;
    private String pagination;
    private String meta;

    public Response(String data, String pagination, String meta) {
        this.data = data;
        this.pagination = pagination;
        this.meta = meta;
    }

    static public Response buildFromJson(String json) {
        try {
            JSONObject response = new JSONObject(json);
            return new Response(
                response.getString("data"),
                response.has("pagination") ? response.getString("pagination") : null,
                response.has("meta") ? response.getString("meta") : null
            );
        } catch (JSONException e) {
            return null;
        }
    }

    public String getData() {
        return data;
    }
}
