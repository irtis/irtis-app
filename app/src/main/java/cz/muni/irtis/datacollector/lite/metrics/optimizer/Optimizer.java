package cz.muni.irtis.datacollector.lite.metrics.optimizer;

import android.content.Context;

import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.model.util.ScreenState;

/**
 * Optimizer
 *
 * Basic time unit is SECOND
 *
 */
public class Optimizer {
    private static final int TYPE_OFF = 1;
    private static final int TYPE_CONSTANT = 2;
    private static final int TYPE_LINEAR = 3;
    private static final int TYPE_EXPONENTIAL = 4;
    private static final int TYPE_SCHEDULED = 5;

    private Context context;
    private int type;

    private long time;
    private long offset;
    private long iteration;
    private long current;
    private long start;
    private long after;
    private long max;

    private Optimizer(Context context, int type) {
        this.context = context;
        this.type = type;
        reset();
    }

    static public Optimizer build(Context context) {
        return new Optimizer(context, TYPE_OFF);
    }

    static public Optimizer buildOff(Context context) {
        Optimizer instance = new Optimizer(context, TYPE_OFF);
        return instance;
    }

    static public Optimizer buildConstant(Context context, long length) {
        Optimizer instance = new Optimizer(context, TYPE_CONSTANT);
        instance.offset = length/Time.SECOND;
        return instance;
    }

    static public Optimizer buildLinear(Context context, long delay, long max) {
        Optimizer instance = new Optimizer(context, TYPE_LINEAR);
        instance.offset = delay/Time.SECOND;
        instance.max = max/Time.SECOND;
        return instance;
    }

    static public Optimizer buildExponential(Context context, long delay, long max) {
        Optimizer instance = new Optimizer(context, TYPE_EXPONENTIAL);
        instance.offset = delay/Time.SECOND;
        instance.max = max/Time.SECOND;
        return instance;
    }

    static public Optimizer buildScheduled(Context context, long delay, long after, long max) {
        Optimizer instance = new Optimizer(context, TYPE_SCHEDULED);
        instance.offset = delay/Time.SECOND;
        instance.after = after;
        instance.max = max/Time.SECOND;
        return instance;
    }


    public void reset() {
        current = 1;
        iteration = 1;
        time = Time.getTime();
        start = time + after;
    }

    public boolean isActive() {
        if(isDisplayOff()) {
            switch (type) {
                default:
                case TYPE_OFF:
                    return true;

                case TYPE_CONSTANT:
                    if(time < Time.getTime()) {
                        time = Time.getTime() + offset * Time.SECOND;
//                        Debug.getInstance().log("Optimization",
//                                Time.getTimeStamp(time, "HH:mm:ss")+" | offset:"+offset);
                        return false;
                    } else {
                        return true;
                    }

                case TYPE_LINEAR:
                    if(time < Time.getTime()) {
                        if(current < max) {
                            current = offset * iteration;
                        }
                        if(current < max) {
                            time = Time.getTime() + current * Time.SECOND;
                            iteration++;
                        } else {
                            time = Time.getTime() + max * Time.SECOND;
                        }
//                        Debug.getInstance().log("Optimization",
//                                Time.getTimeStamp(time, "HH:mm:ss")+" | current: "+current+", iteration: "+iteration+", offset:"+offset+", max: "+max);
                        return false;
                    } else {
                        return true;
                    }

                case TYPE_EXPONENTIAL:
                    if(time < Time.getTime()) {
                        if(current < max) {
                            current *= offset;
                        }
                        if(current < max) {
                            time = Time.getTime() + current * Time.SECOND;
                            iteration++;
                        } else {
                            time = Time.getTime() + max * Time.SECOND;
                        }
//                        Debug.getInstance().log("Optimization",
//                                Time.getTimeStamp(time, "HH:mm:ss")+" | current: "+current+", iteration: "+iteration+", offset:"+offset+", max: "+max);
                        return false;
                    } else {
                        return true;
                    }

                case TYPE_SCHEDULED:
                    if(start < Time.getTime()) {
                        if (time < Time.getTime()) {
                            if(current < max) {
                                current *= offset;
                            }
                            if(current < max) {
                                time = Time.getTime() + current * Time.SECOND;
                                iteration++;
                            } else {
                                time = Time.getTime() + max * Time.SECOND;
                            }
//                            Debug.getInstance().log("Optimization",
//                                    Time.getTimeStamp(time, "HH:mm:ss")+" | current: "+current+", iteration: "+iteration+", offset:"+offset+", max: "+max+", start: "+start);
                            return false;
                        } else {
                            return true;
                        }
                    } else{
                        return false;
                    }
            }
        } else {
            reset();
            return false;
        }
    }

    private boolean isDisplayOff() {
        return !ScreenState.isOn(context);
    }
}
