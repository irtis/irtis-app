package cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram;

import android.content.Context;

import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;

public class InstagramServiceBuilder {
    static public InstagramService build(Context context, ServiceBase.OnServiceListener onSignInListener, ServiceBase.OnServiceListener onSignOutListener) {
        return new InstagramService(context, onSignInListener, onSignOutListener);
    }

    static public InstagramService build(Context context) {
        return build(context, null, null);
    }
}
