package cz.muni.irtis.datacollector.lite.gui.questionnaires.model;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.Arrays;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesService;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.notifications.QuestionnairesNotification;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;

import static android.os.AsyncTask.THREAD_POOL_EXECUTOR;


/**
 * QuestionnairesViewModel
 *
 * @deprecated
 */
public class QuestionnairesViewModel extends AndroidViewModel {
    private QuestionnairesModel model;
    MutableLiveData<List<Questionnaire>> data;

    private interface OnGetLoaderListener {
        void onStart();
        List<Questionnaire> onTask();
        void onDone(List<Questionnaire> items);
    }

    public QuestionnairesViewModel(Application application) {
        super(application);
        model = new QuestionnairesModel(application);
        data = new MutableLiveData<List<Questionnaire>>();
    }

    public LiveData<List<Questionnaire>> getHandler() {
        return data;
    }


    public void getItems(final int offset, final int count) {
        new GetLoaderTask(new OnGetLoaderListener() {
            @Override
            public void onStart() { }
            @Override
            public List<Questionnaire> onTask() {
                return model.getItems(offset, count);
            }
            @Override
            public void onDone(List<Questionnaire> items) {
                data.setValue(items);
            }
        }).executeOnExecutor(THREAD_POOL_EXECUTOR);
    }
    public void getItems(int count) {
        getItems(0, count);
    }


    public void getItemsAvailable(final long time, final int offset, final int count) {
        new GetLoaderTask(new OnGetLoaderListener() {
            @Override
            public void onStart() { }
            @Override
            public List<Questionnaire> onTask() {
                return model.getItemsAvailable(offset, count);
            }
            @Override
            public void onDone(List<Questionnaire> items) {
                data.setValue(items);
            }
        }).executeOnExecutor(THREAD_POOL_EXECUTOR);
    }
    public void getItemsAvailable(long time, int count) {
        getItemsAvailable(time,0, count);
    }

    public void getItemsNewestAvailable() {
        new GetLoaderTask(new OnGetLoaderListener() {
            @Override
            public void onStart() { }
            @Override
            public List<Questionnaire> onTask() {
                Questionnaire item = model.getItemAvailableNewest();
                if(item!=null && !item.isClosed()) {
                    return Arrays.asList(new Questionnaire[]{
                        item
                    });
                } else {
                    return null;
                }
            }
            @Override
            public void onDone(List<Questionnaire> items) {
                data.setValue(items);
            }
        }).executeOnExecutor(THREAD_POOL_EXECUTOR);
    }


    public void getItem(final Long identifier) {
        new GetLoaderTask(new OnGetLoaderListener() {
            @Override
            public void onStart() { }
            @Override
            public List<Questionnaire> onTask() {
                List<Questionnaire> items = Arrays.asList(model.getItem(identifier));
                return items;
            }
            @Override
            public void onDone(List<Questionnaire> items) {
                data.setValue(items);
            }
        }).executeOnExecutor(THREAD_POOL_EXECUTOR);
    }

    public void storeItem(final Questionnaire item) {
        new GetLoaderTask(new OnGetLoaderListener() {
            @Override
            public void onStart() { }
            @Override
            public List<Questionnaire> onTask() {
                model.store(item);
                return null;
            }
            @Override
            public void onDone(List<Questionnaire> items) {
                if(item.isCompleted()) {
                    if (QuestionnairesNotification.isBuilt(item)) {
                        QuestionnairesNotification.cancel(getApplication().getBaseContext(), item);
                    }
                    Debug.getInstance().warning(QuestionnairesService.TAG,
                        "Instance completed | Value: "+(item.getIdentifier()!=null ? item.getIdentifier() : "null"));
                }
            }
        }).executeOnExecutor(THREAD_POOL_EXECUTOR);
    }

    public void postpone(final Questionnaire item, final int validity) {
        new GetLoaderTask(new OnGetLoaderListener() {
            @Override
            public void onStart() { }
            @Override
            public List<Questionnaire> onTask() {
                model.setPostponed(item, validity);
                return null;
            }
            @Override
            public void onDone(List<Questionnaire> items) {
                if (QuestionnairesNotification.isBuilt(item)) {
                    QuestionnairesNotification.update(item);
                }
            }
        }).executeOnExecutor(THREAD_POOL_EXECUTOR);
    }


    private class GetLoaderTask extends AsyncTask<String, String, List<Questionnaire>>  {
        private OnGetLoaderListener listener;

        public GetLoaderTask(OnGetLoaderListener listener) {
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listener.onStart();
        }
        @Override
        protected List<Questionnaire> doInBackground(String... folder)  {
            return listener.onTask();
        }
        @Override
        protected void onPostExecute(List<Questionnaire> items) {
            listener.onDone(items);
        }
    }
}
