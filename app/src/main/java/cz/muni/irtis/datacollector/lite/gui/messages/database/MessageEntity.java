package cz.muni.irtis.datacollector.lite.gui.messages.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import cz.muni.irtis.datacollector.lite.application.database.EntityBase;
import cz.muni.irtis.datacollector.lite.gui.messages.model.Message;

@Entity(
    tableName = MessagesRepository.TABLE_NAME
)
public class MessageEntity extends EntityBase {
    @PrimaryKey
    @ColumnInfo(name = "id")
    private Long id;
    @ColumnInfo(name = "text")
    private String text;
    @ColumnInfo(name = "response")
    private String response;
    @ColumnInfo(name = "created")
    private Long created;
    @ColumnInfo(name = "downloaded")
    private Long downloaded;
    @ColumnInfo(name = "notified")
    private Long notified;
    @ColumnInfo(name = "opened")
    private Long opened;
    @ColumnInfo(name = "read")
    private Long read;
    @ColumnInfo(name = "uploaded")
    private Long uploaded;


    public MessageEntity(Long id, String text, Long created, Long downloaded, Long notified, Long opened, Long read) {
        this.id = id;
        this.text = text;
        this.created = created;
        this.downloaded = downloaded;
        this.notified = notified;
        this.opened = opened;
        this.read = read;
    }

    public MessageEntity(Message item) {
        this.id = item.getId();
        this.text = item.getText();
        this.response = item.getResponse();
        this.created = item.getCreated();
        this.downloaded = item.getDownloaded();
        this.notified = item.getNotified();
        this.opened = item.getOpened();
        this.read = item.getRead();
        this.uploaded = item.getUploaded();
    }

    public static MessageEntity build(Message item) {
        return new MessageEntity(item);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Long downloaded) {
        this.downloaded = downloaded;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Long getNotified() {
        return notified;
    }

    public void setNotified(Long notified) {
        this.notified = notified;
    }

    public Long getOpened() {
        return opened;
    }

    public void setOpened(Long opened) {
        this.opened = opened;
    }

    public Long getRead() {
        return read;
    }

    public void setRead(Long read) {
        this.read = read;
    }

    public Long getUploaded() {
        return uploaded;
    }

    public void setUploaded(Long uploaded) {
        this.uploaded = uploaded;
    }
}
