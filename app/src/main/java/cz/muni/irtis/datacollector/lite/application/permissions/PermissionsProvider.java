package cz.muni.irtis.datacollector.lite.application.permissions;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.PowerManager;
import android.text.TextUtils;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.Applications;
import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.BooleanSet;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesService;
import cz.muni.irtis.datacollector.lite.metrics.model.condition.IsUsageStatsAllowed;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.ACTIVITY_RECOGNITION;
import static android.Manifest.permission.BIND_ACCESSIBILITY_SERVICE;
import static android.Manifest.permission.PACKAGE_USAGE_STATS;
import static android.Manifest.permission.READ_CALL_LOG;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_BOOT_COMPLETED;
import static android.Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS;
import static android.Manifest.permission.SYSTEM_ALERT_WINDOW;



/**
 * PermissionsProvider
 *
 * Provides an interface to interact with application permissions
 *
 * Usage:
 *  - construct
 *  - call:
 *      - setOnHolderListener
 *      - request
 *      - in onRequestPermissionsResult() call result
 */
public class PermissionsProvider {
    public static final String TAG = PermissionsProvider.class.getSimpleName();

    private Context context;
    private Boolean isLogs;

    private List<Permission> permissions;

    // Ordinary permissions
    static final public String PERMISSION_BOOT      = RECEIVE_BOOT_COMPLETED;   // Permission to start the app at system boot

    // Dangerous permissions (https://developer.android.com/guide/topics/permissions/overview#dangerous_permissions)
    static final public String PERMISSION_LOCATION  = ACCESS_FINE_LOCATION;     // Permission to access device GPS location
    static final public String PERMISSION_CONTACTS  = READ_CONTACTS;            // Permission to read phone contacts
    static final public String PERMISSION_CALLS     = READ_CALL_LOG;            // Permission to read phone call logs
    static final public String PERMISSION_SMS       = READ_SMS;                 // Permission to read phone sms messages
    static final public String PERMISSION_OVERLAY   = SYSTEM_ALERT_WINDOW;      // Permission to show view above all apps

    // Specific permissions
    static final public String PERMISSION_USAGE     = PACKAGE_USAGE_STATS;  // Permissions to get applications usage statistics
    static final public String PERMISSION_AUTOSTART = "irtis.permission.AUTOSTART"; // MIUI permission to automatically run/restart foreground services
    static final public String PERMISSION_ADVANCED_BATTERY_OPTIMIZATION = "irtis.permission.ADVANCED_BATTERY_OPTIMIZATION"; // Custom specifics per manufacturer permission to automatically run/restart foreground services
    static final public String PERMISSION_NOTIFICATIONS = "irtis.permission.NOTIFICATIONS";
    static final public String PERMISSION_ACTIVITY_RECOGNITION = ACTIVITY_RECOGNITION;
    static final public String PERMISSION_BATTERYOPTIMIZATION = REQUEST_IGNORE_BATTERY_OPTIMIZATIONS; // Permission to get better foreground services persistence
    static final public String PERMISSION_ACCESSIBILITY = BIND_ACCESSIBILITY_SERVICE; // Permission to get contents of the current application


    public interface PermissionsListener {
        public void onDenied();
        public void onGranted();
    }
    private PermissionsListener listener;



    public PermissionsProvider(Context context, Boolean isLogs) {
        this.context = context;
        this.isLogs = isLogs;
        this.permissions = new ArrayList<>();
    }


    static public PermissionsProvider build(Context context) {
        return new PermissionsProvider(context, false);
    }

    static public PermissionsProvider build(Context context, Boolean isLogs) {
        return new PermissionsProvider(context, isLogs);
    }


    /**
     * getItemsOrdinaries()
     *
     * Returns list of ordinary permissions
     *
     * @return
     */
    public String[] getItemsOrdinaries() {
        return new String[]{
            PERMISSION_BOOT
        };
    }
    /**
     * getItemsDangerous()
     *
     * Returns list of dangerous permissions
     *
     * @return
     */
    public String[] getItemsDangerous() {
        return new String[]{
            PERMISSION_LOCATION,
            PERMISSION_CONTACTS,
            PERMISSION_CALLS,
            PERMISSION_SMS
        };
    }
    /**
     * getItemsSpecial()
     *
     * Returns list of special permissions
     *
     * @return
     */
    public String[] getItemsSpecial()
    {
        return new String[]{
            PERMISSION_USAGE,
            PERMISSION_AUTOSTART,
            PERMISSION_BATTERYOPTIMIZATION,
            PERMISSION_ADVANCED_BATTERY_OPTIMIZATION,
            PERMISSION_OVERLAY,
            PERMISSION_NOTIFICATIONS
        };
    }

    /**
     * request()
     *
     * The result must be processed by onRequestPermissionsResult() method
     * and result() method
     *
     * @param permissions
     * @param code
     */
    public PermissionsProvider request(String[] permissions, int code) {
        String[] denied = getPermissionsDenied(permissions);
        if(denied.length>0) {
            ActivityCompat.requestPermissions(
                (Activity) this.context,
                denied,
                code
            );
        } else if(listener!=null) {
            result(permissions);
        }

        return this;
    }

    /**
     * request()
     *
     * The result must be processed by onRequestPermissionsResult() method
     * and result() method
     *
     * @param permission
     * @param code
     */
    public PermissionsProvider request(String permission, int code) {
        if(!this.isGranted(permission)) {
            ActivityCompat.requestPermissions(
                (Activity) this.context,
                new String[] {permission},
                code
            );
        } else if(listener!=null) {
            result(permission);
        }

        return this;
    }

    public PermissionsProvider request(String permission) {
        return request(permission, 0);
    }


    /**
     * result()
     *
     * Process the result of a granting process (system interacting with the user)
     * returned by onRequestPermissionsResult() method
     */
    public void result(String[] permissions) {
        if(checkPermissions(permissions)) {
            this.listener.onGranted();
        } else {
            this.listener.onDenied();
        }
    }

    public void result(String permissions) {
        if(isGranted(permissions)) {
            this.listener.onGranted();
        } else {
            this.listener.onDenied();
        }
    }


    public Permission add(Permission permission) {
        this.permissions.add(permission);
        return permission;
    }


    /**
     * Check if the list of permission if granted
     *
     * @param permissions
     * @return
     */
    public boolean checkPermissions(String[] permissions) {
        if(permissions!=null && permissions.length>0) {
            for (String permission : permissions) {
                if (!checkPermission(permission)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if a certain permission isActive granted
     *
     * @param permission
     * @return
     */
    public boolean checkPermission(String permission) {
        if(permission.equals(PERMISSION_USAGE)) {
            return this.isGrantedUsagePermission();
        } else if(permission.equals(PERMISSION_AUTOSTART)) {
            return this.isGrantedAutostartPermission();
        } else if(permission.equals(PERMISSION_BATTERYOPTIMIZATION)) {
            return this.isGrantedBatteryOptimizationPermission();
        } else if(permission.equals(PERMISSION_ADVANCED_BATTERY_OPTIMIZATION)) {
            return this.isGrantedAdvancedBatteryOptimizationPermission();
        } else if(permission.equals(PERMISSION_OVERLAY)) {
            return this.isGrantedOverlayPermission();
        } else if(permission.equals(PERMISSION_NOTIFICATIONS)) {
            return this.isGrantedNotificationsPermission();
        } else {
            return ContextCompat.checkSelfPermission(context, permission) ==
                    PackageManager.PERMISSION_GRANTED;
        }
    }

    /**
     * Get the sub-setItem of permissions that are granted
     *
     * @param wanted
     * @return
     */
    public String[] getPermissionsGranted(String[] wanted) {
        ArrayList<String> result = new ArrayList<>();
        for (String permission : wanted) {
            if (checkPermission(permission)) {
                result.add(permission);
            }
        }
        return result.toArray(new String[result.size()]);
    }

    /**
     * Get the sub-setItem of permissions that are denied
     *
     * @param wanted
     * @return
     */
    public String[] getPermissionsDenied(String[] wanted) {
        ArrayList<String> result = new ArrayList<>();
        for (String permission : wanted) {
            if (!checkPermission(permission)) {
                result.add(permission);
            }
        }
        return result.toArray(new String[result.size()]);
    }

    public boolean isGranted(String permission)
    {
        return this.checkPermission(permission);
    }

    public boolean isGranted(String[] permissions)
    {
        return this.checkPermissions(permissions);
    }

    public boolean isGranted() {
        // Logs
        if (permissions != null && !permissions.isEmpty()) {
            BooleanSet isGranted = BooleanSet.build(permissions.size());
            Iterator<Permission> iPermission = permissions.iterator();
            while (iPermission.hasNext()) {
                Permission permission = iPermission.next();
                Debug.getInstance().log(TAG, permission.getId() + " = " + (isGranted(permission.getId()) ? "true" : "false"));
            }
        }

        if(isLogs) {
            Debug.getInstance().log(TAG, "isGrantedUsagePermission() = " + (isGrantedUsagePermission() ? "true" : "false"));
            Debug.getInstance().log(TAG, "isGrantedBatteryOptimizationPermission() = " + (isGrantedBatteryOptimizationPermission() ? "true" : "false"));
            Debug.getInstance().log(TAG, "isGrantedOverlayPermission() = " + (isGrantedOverlayPermission() ? "true" : "false"));
            Debug.getInstance().log(TAG, "isGrantedNotificationsPermission() = " + (isGrantedNotificationsPermission() ? "true" : "false"));
            Debug.getInstance().log(TAG, "isGrantedActivityRecognitionPermission() = " + (isGrantedActivityRecognitionPermission() ? "true" : "false"));
            Debug.getInstance().log(TAG, "isGrantedAccessibilityPermission() = " + (isGrantedAccessibilityPermission() ? "true" : "false"));
            Debug.getInstance().log(TAG, "isGrantedUsagePermission() = " + (isGrantedUsagePermission() ? "true" : "false"));

            if (isRequiredAutostartPermission()) {
                Debug.getInstance().log(TAG, "isGrantedAutostartPermission() = " + (isGrantedAutostartPermission() ? "true" : "false"));
            }
            if (isRequiredAdvancedBatteryOptimizationPermission()) {
                Debug.getInstance().log(TAG, "isGrantedAdvancedBatteryOptimizationPermission() = " + (isGrantedAdvancedBatteryOptimizationPermission() ? "true" : "false"));
            }
        }

        // Real checking
        if(Config.REQUEST_PERMISSIONS) {
            if (permissions != null && !permissions.isEmpty()) {
                BooleanSet isGranted = BooleanSet.build(permissions.size());
                Iterator<Permission> iPermission = permissions.iterator();
                while (iPermission.hasNext()) {
                    Permission permission = iPermission.next();
                    if (isGranted(permission.getId())) {
                        isGranted.setTrue();
                    }
                }
                if (isGranted.isFalse()) {
                    return false;
                }
            }
            if (!isGrantedUsagePermission()) {
                return false;
            }
            if (isRequiredAutostartPermission() && !isGrantedAutostartPermission()) {
                return false;
            }
            if (isRequiredBatteryOptimizationPermission() && !isGrantedBatteryOptimizationPermission()) {
                return false;
            }
            if (isRequiredAdvancedBatteryOptimizationPermission() && !isGrantedAdvancedBatteryOptimizationPermission()) {
                return false;
            }
            if (!isGrantedOverlayPermission()) {
                return false;
            }
            if (!isGrantedNotificationsPermission()) {
                return false;
            }
            if (!isGrantedActivityRecognitionPermission()) {
                return false;
            }
            if (!isGrantedAccessibilityPermission() && Applications.isFirstStart()) {
                return false;
            }
        }

        return true;
    }

    public boolean isGrantedUsagePermission()
    {
        return IsUsageStatsAllowed.isPackageUsagePermissionGranted(context);
    }

    public boolean isGrantedAutostartPermission()
    {
        SharedPreferences settings = Settings.getSettings();
        return settings.getBoolean("permission_"+PERMISSION_AUTOSTART.toLowerCase(), false);
    }

    public boolean isGrantedBatteryOptimizationPermission()
    {
        PowerManager power = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return power.isIgnoringBatteryOptimizations(context.getPackageName());
        } else {
            return true;
        }
    }

    public boolean isGrantedAdvancedBatteryOptimizationPermission()
    {
        SharedPreferences settings = Settings.getSettings();
        return settings.getBoolean("permission_"+PERMISSION_ADVANCED_BATTERY_OPTIMIZATION.toLowerCase(), false);
    }

    public boolean isGrantedOverlayPermission()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return android.provider.Settings.canDrawOverlays(context);
        } else {
            return true;
        }
    }

    public boolean isGrantedAccessibilityPermission() {
        try {
            if(android.provider.Settings.Secure.getInt(context.getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED)==1) {
                String settingValue = android.provider.Settings.Secure.getString(context.getContentResolver(),
                        android.provider.Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
                if (settingValue != null) {
                    TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');
                    TextUtils.SimpleStringSplitter splitter = mStringColonSplitter;
                    splitter.setString(settingValue);
                    while (splitter.hasNext()) {
                        String service = splitter.next();
                        if (service.equalsIgnoreCase(
                                "cz.muni.irtis.datacollector.lite/cz.muni.irtis.datacollector.lite.metrics.hooking.HookingService")) {
                            return true;
                        }
                    }
                }
            }
        } catch (android.provider.Settings.SettingNotFoundException e) {
        }

        return false;
    }


    public boolean isGrantedNotificationsPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            if (NotificationManagerCompat.getEnabledListenerPackages(context).contains(context.getPackageName())) {
                return true;
            }
        }

        return false;
    }


    public boolean isGrantedActivityRecognitionPermission() {
        if(isRequiredActivityRecognitionPermission()) {
            return ContextCompat.checkSelfPermission(context, Manifest.permission.ACTIVITY_RECOGNITION) == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }


    /**
     * isRequiredAutostartPermission()
     *
     * Check if `autostart` permission might be required
     *
     * @return
     */
    public boolean isRequiredAutostartPermission() {
        try {
            String manufacturer = android.os.Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                return true;
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                return true;
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                return true;
            } else if ("letv".equalsIgnoreCase(manufacturer)) {
                return true;
            } else if ("huawei".equalsIgnoreCase(manufacturer) || "honor".equalsIgnoreCase(manufacturer)) {
                return true;
            } else if ("asus".equalsIgnoreCase(manufacturer)) {
                return true;
            }
        } catch (Exception e) {
            Debug.getInstance().exception(e);
        }

        return false;
    }

    public boolean isRequiredBatteryOptimizationPermission() {
        boolean isVersion = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
        boolean isActivityCheck1 = !context.getPackageManager().queryIntentActivities(new Intent(android.provider.Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS),
                PackageManager.MATCH_DEFAULT_ONLY).isEmpty();
//        boolean isActivityCheck2 = true;
//        boolean isActivityCheck2 = !context.getPackageManager().queryIntentActivities(
//                new Intent().setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$HighPowerApplicationsActivity")),
//                PackageManager.MATCH_DEFAULT_ONLY).isEmpty();

        boolean isManufacturerSpecific = true;
        if(android.os.Build.MANUFACTURER.equalsIgnoreCase("huawei")) {
            if(Build.VERSION.RELEASE.compareTo("4")<=0) {
                isManufacturerSpecific = false;
            }
        }

        return isVersion && isActivityCheck1 && isManufacturerSpecific;
    }

    public boolean isRequiredAdvancedBatteryOptimizationPermission() {
        String manufacturer = android.os.Build.MANUFACTURER;
        if ("xiaomi".equalsIgnoreCase(manufacturer)) {
            try {
                Intent intent = new Intent();
                ComponentName component = new ComponentName("com.miui.powerkeeper", "com.miui.powerkeeper.ui.HiddenAppsContainerManagementActivity");
                if(component!=null) {
                    intent.setComponent(component);
                    List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                    if (list.size() > 0) {
                        return true;
                    }
                }
            } catch (Exception e) {
                Debug.getInstance().exception(e);
            }
        }

        return false;
    }

    public boolean isRequiredOverlayPermission() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public boolean isRequiredNotificationsPermission() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
    }

    public boolean isRequiredActivityRecognitionPermission() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q;
//        return true;
    }


    /**
     * setGrantedAutostartPermission()
     *
     * Set autostart permission granted
     *
     * This setItem can not be determined at 100% due to lack of system feedback on its setting
     */
    public void setGrantedAutostartPermission()
    {
        SharedPreferences settings = Settings.getSettings();
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("permission_"+PERMISSION_AUTOSTART.toLowerCase(), true);
        editor.commit();
    }

    public void setGrantedAdvancedBatteryOptimizationPermission()
    {
        SharedPreferences settings = Settings.getSettings();
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("permission_"+PERMISSION_ADVANCED_BATTERY_OPTIMIZATION.toLowerCase(), true);
        editor.commit();
    }

    public PermissionsProvider setListener(PermissionsListener listener) {
        this.listener = listener;
        return this;
    }
}
