package cz.muni.irtis.datacollector.lite.metrics.database.entities;

import androidx.annotation.NonNull;
import androidx.room.PrimaryKey;

import cz.muni.irtis.datacollector.lite.application.utils.Time;

public abstract class EntityBase {
    @PrimaryKey
    @NonNull
    private Long datetime;      // Serves as an ID

    public EntityBase(Long datetime) {
        this.datetime = datetime;
    }

    public Long getDatetime() {
        return datetime;
    }
    public String getDatetimeFormatted() {
        return Time.getTimeStamp(getDatetime());
    }

    public Long getId() {
        return getDatetime();
    }

    public void setId(@NonNull Long datetime) {
        this.datetime = datetime;
    }
}
