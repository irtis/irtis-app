package cz.muni.irtis.datacollector.lite.application.debug;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import cz.muni.irtis.datacollector.lite.Application;


public class UnhandledExceptions implements Thread.UncaughtExceptionHandler {
    private Thread.UncaughtExceptionHandler defaultHandler;

    public UnhandledExceptions() {
        // Get default handler
        this.defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
    }

    /**
     * bind()
     *
     * Use this to initialize the handler
     */
    static public void bind() {
        Thread.setDefaultUncaughtExceptionHandler(new UnhandledExceptions());
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        // Extract the stacktrace
        final Writer stringBuffSync = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringBuffSync);
        e.printStackTrace(printWriter);
        String stacktrace = stringBuffSync.toString();
        printWriter.close();

        save(stacktrace);

        // Continue with default behaviour
        defaultHandler.uncaughtException(t, e);
    }

    private void save(String stack) {
        DebugLogs logs = DebugLogs.build(Application.getInstance().getApplicationContext());
        logs.create(stack, "exception-"+logs.getCurrentTimeStamp());
    }
}
