package cz.muni.irtis.datacollector.lite.gui.integrations.model.facebook;

import android.content.Context;

import com.facebook.login.DefaultAudience;

import java.util.Arrays;

import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;

/**
 * FacebookServiceBuilder
 * 
 * See https://developers.facebook.com/docs/facebook-login/permissions
 */
public class FacebookServiceBuilder {

    static public FacebookService buildFacebook(Context context, ServiceBase.OnServiceListener onSignInListener, ServiceBase.OnServiceListener onSignOutListener) {
        FacebookService.FacebookSignInOptions options = FacebookService.FacebookSignInOptions.build()
                .setDefaultAudience(DefaultAudience.NONE)
                .setPermissions(Arrays.asList(
                    // Personal
                    "email",
                    "user_age_range",
                    "user_birthday",
                    "user_gender",
                    "user_hometown",
                    "user_location",
                    // AliveWatcher
                    "user_location",
                    // Friends & likes
                    "user_friends",
                    "user_likes",
                    // Content - restricted to a limited setItem of partners
                    "user_photos",
                    "user_videos",
                    "user_posts",
                    "user_tagged_places",
                    "user_events",
                    // Groups
                    "groups_access_member_info"
                ));

        return new FacebookService(context, options, onSignInListener, onSignOutListener);
    }

    static public FacebookService buildFacebook(Context context) {
        return buildFacebook(context, null, null);
    }


    static public FacebookService buildMessengerBusiness(Context context, ServiceBase.OnServiceListener onSignInListener, ServiceBase.OnServiceListener onSignOutListener) {
        FacebookService.FacebookSignInOptions options = FacebookService.FacebookSignInOptions.build()
                .setDefaultAudience(DefaultAudience.NONE)
                .setPermissions(Arrays.asList(
                        "pages_messaging"
                ));

        return new FacebookService(context, options, onSignInListener, onSignOutListener);
    }
    static public FacebookService buildMessengerBusiness(Context context) {
        return buildMessengerBusiness(context, null, null);
    }


    static public FacebookService buildInstagramBusiness(Context context, ServiceBase.OnServiceListener onSignInListener, ServiceBase.OnServiceListener onSignOutListener) {
        FacebookService.FacebookSignInOptions options = FacebookService.FacebookSignInOptions.build()
                .setDefaultAudience(DefaultAudience.NONE)
                .setPermissions(Arrays.asList(
                    "instagram_basic",
                    "instagram_content_publish",
                    "instagram_manage_comments",
                    "instagram_manage_insights"
                ));

        return new FacebookService(context, options, onSignInListener, onSignOutListener);
    }
    static public FacebookService buildInstagramBusiness(Context context) {
        return buildInstagramBusiness(context, null, null);
    }
}
