package cz.muni.irtis.datacollector.lite.gui.questionnaires.types;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TimePicker;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;

public class TimeSpinnerFragment extends TypeFragmentBase {
    protected TimePicker vPicker;
    protected EditText vText;

    public TimeSpinnerFragment(Context context, Question question, boolean editable) {
        super(context, question, R.layout.activity_questionnaires_form_type_time_spinner, editable);
    }

    public TimeSpinnerFragment(Context context, Question question, int contentLayoutId, boolean editable) {
        super(context, question, contentLayoutId, editable);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        vPicker = view.findViewById(R.id.picker);
        vPicker.setIs24HourView(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            vPicker.setHour(12);
            vPicker.setMinute(0);
        } else {
            vPicker.setCurrentHour(12);
            vPicker.setCurrentMinute(0);
        }

        vPicker.setEnabled(isEditable());
        vPicker.setVisibility(isEditable() ? View.VISIBLE : View.GONE);
        String[] time = getQuestion().getAnswer().isValue() ? getQuestion().getAnswer().getValue().split(":") : new String[]{"0", "0"};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            vPicker.setHour(Integer.valueOf(time[0]));
            vPicker.setMinute(Integer.valueOf(time[1]));
        } else {
            vPicker.setCurrentHour(Integer.valueOf(time[0]));
            vPicker.setCurrentMinute(Integer.valueOf(time[1]));
        }

        vPicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hour, int minute) {
                Answer answer = getQuestion().getAnswer();
                answer.setValue(String.format("%02d", hour) + ":" + String.format("%02d", minute));
                refresh();
            }
        });

        vText = view.findViewById(R.id.text);
        vText.setVisibility(isEditable() ? View.GONE : View.VISIBLE);
        vText.setEnabled(false);
        if (getQuestion().getAnswer().isValue()) {
            vText.setText(getQuestion().getAnswer().getValue());
        }

        return view;
    }
}
