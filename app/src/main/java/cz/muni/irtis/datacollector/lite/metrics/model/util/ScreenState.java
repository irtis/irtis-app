package cz.muni.irtis.datacollector.lite.metrics.model.util;

import android.content.Context;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.os.PowerManager;
import android.view.Display;

import cz.muni.irtis.datacollector.lite.application.utils.Time;

/**
 *
 * Always-On Display Issue
 * https://stackoverflow.com/questions/60216558/detect-always-on-display-when-trying-to-determine-if-the-screen-is-off
 */
public class ScreenState {
    private static Long lasttime = Time.getTime();

    /**
     * Is the screen of the device on.
     * @param context the context
     * @return true when the default (hardware) isActive on
     */
    public static boolean isOn(Context context) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            DisplayManager dm = (DisplayManager) context.getSystemService(Context.DISPLAY_SERVICE);
            boolean screenOn = false;
            for (Display display : dm.getDisplays()) {
                if(display.getDisplayId() == Display.DEFAULT_DISPLAY) {
                    if (display.getState() == Display.STATE_ON) {
                        lasttime = Time.getTime();
                        screenOn = true;
                        break;
                    }
                }
            }
            return screenOn;
        } else {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            //noinspection deprecation
            return pm.isScreenOn();
        }
    }

    public static Long getLastTimeOn() {
        return lasttime;
    }
}
