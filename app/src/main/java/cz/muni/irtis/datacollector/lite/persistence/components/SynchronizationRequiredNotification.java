package cz.muni.irtis.datacollector.lite.persistence.components;


import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import cz.muni.irtis.datacollector.lite.gui.MainActivity;
import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.NotificationBase;

public class SynchronizationRequiredNotification extends NotificationBase {
    private static final int IDENTIFIER = 85473;

    private NotificationCompat.Builder builder;

    private SynchronizationRequiredNotification(Context context) {
        super(context);

    }

    static public SynchronizationRequiredNotification build(Context context) {
        return new SynchronizationRequiredNotification(context);
    }

    @Override
    public Notification create() {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(MainActivity.PARAM_START_SYNC, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pending = PendingIntent.getActivity(
                context, IDENTIFIER, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder = new NotificationCompat.Builder(context, CHANNEL_HIGH)
            .setSmallIcon(R.drawable.ic_sync_grey_24dp)
            .setSubText(context.getString(R.string.persistence_notification_syncronization_type))
            .setContentTitle(context.getString(R.string.persistence_notification_syncronization_title))
            .setContentText(context.getString(R.string.persistence_notification_syncronization_text))
            .setContentIntent(pending)
            .setCategory(NotificationCompat.CATEGORY_ERROR)
            .setAutoCancel(true)
            .setOnlyAlertOnce(true);

        return notification = builder.build();
    }

    public void show() {

        manager.notify(IDENTIFIER, create());
    }

    public void hide() {
        if(notification!=null){
            manager.cancel(IDENTIFIER);
        }
    }
}