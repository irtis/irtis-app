package cz.muni.irtis.datacollector.lite.gui.messages.database;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import cz.muni.irtis.datacollector.lite.application.database.EntityRepositoryBase;

@Dao
public interface MessagesRepository extends EntityRepositoryBase<MessageEntity> {
    public static final String TABLE_NAME = "messages";

    @Query("SELECT * FROM "+TABLE_NAME+" ORDER BY downloaded DESC")
    List<MessageEntity> getItems();

    @Query("SELECT * FROM "+TABLE_NAME+" WHERE read IS NULL ORDER BY created DESC LIMIT 3")
    List<MessageEntity> getItemsNewest();

    @Query("SELECT COUNT(*) FROM "+TABLE_NAME+" WHERE read IS NULL")
    Integer getItemsNewestCount();

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE id = :id")
    MessageEntity getItem(Long id);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE uploaded IS NULL")
    List<MessageEntity> getNotUploaded();
}
