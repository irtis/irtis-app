package cz.muni.irtis.datacollector.lite.application.components;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.os.Build;

public abstract class NotificationBase {

    public static final String CHANNEL_MIN = "channel_min";
    public static final String CHANNEL_LOW = "channel_low";
    public static final String CHANNEL_HIGH = "channel_high";
    public static final String CHANNEL_TOP = "channel_top";
    public static final String CHANNEL_PERSISTENT = "channel_persistent";

    protected Context context;
    protected NotificationManager manager;
    protected Notification notification;

    public NotificationBase(Context context) {
        this.context = context;
        this.manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.initNotificationChannels();
        }
    }

    abstract public Notification create();

    @TargetApi(Build.VERSION_CODES.O)
    private void initNotificationChannels() {
        NotificationChannel channel = null;

        channel = new NotificationChannel(CHANNEL_MIN, CHANNEL_MIN, NotificationManager.IMPORTANCE_MIN);
//        channel.
        manager.createNotificationChannel(channel);

        manager.createNotificationChannel(
                new NotificationChannel(CHANNEL_LOW, CHANNEL_LOW, NotificationManager.IMPORTANCE_LOW));

        channel = new NotificationChannel(CHANNEL_HIGH, CHANNEL_HIGH, NotificationManager.IMPORTANCE_HIGH);
        channel.enableVibration(true);
        channel.enableLights(true);
        manager.createNotificationChannel(channel);

        channel = new NotificationChannel(CHANNEL_TOP, CHANNEL_TOP, NotificationManager.IMPORTANCE_HIGH);
        channel.enableVibration(true);
        channel.enableLights(true);
        channel.setBypassDnd(true);
        manager.createNotificationChannel(channel);

        channel = new NotificationChannel(CHANNEL_PERSISTENT, CHANNEL_PERSISTENT, NotificationManager.IMPORTANCE_HIGH);
        channel.enableVibration(false);
        channel.enableLights(false);
        channel.setShowBadge(false);
        manager.createNotificationChannel(channel);
    }
}
