package cz.muni.irtis.datacollector.lite.gui.questionnaires.components;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.NumberPicker;

import androidx.appcompat.app.AlertDialog;

import cz.muni.irtis.datacollector.R;

public class QuestionnairesPostponeDialog extends Dialog {
    private AlertDialog dialog;
    private String[] values = new String[] {
        "10","20","30","40","50","60"
    };
    private NumberPicker vPicker;
    private OnSubmitListener listener;

    public interface OnSubmitListener {
        void onSubmit(int number);
        void onCancel();
    }

    public QuestionnairesPostponeDialog(Context context, OnSubmitListener listener) {
        super(context);
        this.listener = listener;
        this.dialog = onBuilderCreate(null);
    }


    protected AlertDialog onBuilderCreate(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View vContent = getLayoutInflater().inflate(R.layout.activity_questionnaires_postpone, null);

        vPicker = vContent.findViewById(R.id.picker);
        vPicker.setWrapSelectorWheel(true);
        vPicker.setMinValue(0);
        vPicker.setMaxValue(values.length-1);
        vPicker.setDisplayedValues(values);
        vPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected number from picker
            }
        });

        builder
            .setView(vContent)
            .setPositiveButton("Ok", new OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    listener.onSubmit(Integer.valueOf(values[vPicker.getValue()]));
                    close();
                }
            })
            .setNegativeButton("Cancel", new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    listener.onCancel();
                    close();
                }
            })
            .setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        listener.onCancel();
                        close();
                    }
                    return true;
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    listener.onCancel();
                    close();
                }
            });

        return builder.create();
    }

    public void show() { this.dialog.show(); }

    public void hide() { this.dialog.hide(); }

    public void close() { this.dialog.hide(); }

    public AlertDialog getDialog() { return this.dialog; }
}