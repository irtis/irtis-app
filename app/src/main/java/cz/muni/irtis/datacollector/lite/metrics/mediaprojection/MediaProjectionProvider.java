package cz.muni.irtis.datacollector.lite.metrics.mediaprojection;


import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Method;

import cz.muni.irtis.datacollector.lite.application.Applications;
import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;

import static android.content.Context.MEDIA_PROJECTION_SERVICE;

/**
 * MediaProjectionProvider
 *
 * Provider for connection to MediaProjection system service
 *
 */
public class MediaProjectionProvider {
    private static final String TAG = MediaProjectionProvider.class.getSimpleName();
    static private final int REQUEST_CODE = 7845;

    static private MediaProjectionProvider instance = null;
    private MediaProjectionToken token;

    MediaProjectionManager manager;
    MediaProjection projection;


    /**
     * MediaProjectionProvider()
     *
     * Initialize via getInstance() method
     *
     */
    public MediaProjectionProvider() {
    }

    /**
     * getInstance()
     *
     * Singleton getter for the class
     *
     * @return
     */
    static public MediaProjectionProvider getInstance() {
        if(instance==null) {
            instance = new MediaProjectionProvider();
        }
        return instance;
    }

    /**
     * request()
     *
     * This should be always run from an activity
     *
     * @param activity
     */
    public void request(Activity activity) {
        this.stop();

        if(token==null) {
            token = new MediaProjectionToken();
        }

        // Try to get binder token (handle) to MediaProjection via IPC call
        if(!token.isValid()) {
            Intent handle = MediaProjectionIPCTokenBuilder.build(activity).getToken();
            if(handle!=null) {
                token.setHandle(handle);
            }
        }

        // Initialize MediaProjection
        manager = (MediaProjectionManager) activity.getSystemService(MEDIA_PROJECTION_SERVICE);
        if(isToken()) {
            try {
                projection = manager.getMediaProjection(Activity.RESULT_OK, token.getHandle());
                ((ActivityBase) activity).onActivityResult(REQUEST_CODE, Activity.RESULT_OK, token.getHandle());
            } catch (Exception e) {
                Debug.getInstance().exception(e, this);
                Debug.getInstance().warning(MediaProjectionProvider.TAG, "Failed to getMediaProjection()");
                token.invalidate();
                activity.startActivityForResult(manager.createScreenCaptureIntent(), REQUEST_CODE);
            }
        } else {
            activity.startActivityForResult(manager.createScreenCaptureIntent(), REQUEST_CODE);
        }
    }


    public void start() {

    }

    public void stop() {
        if(projection!=null) {
            projection.stop();
            projection = null;
        }
    }


    public boolean isToken() {
        return token!=null && token.isValid();
    }

    public boolean isGranted() {
        if(isToken()) {
            try {
                manager.getMediaProjection(Activity.RESULT_OK, token.getHandle());
                return true;
            } catch (Exception e) {
            }
        }
        return false;
    }


    public MediaProjection getProjection() {
        return projection;
    }


    public boolean onActivityResult(int request, int result, Intent data) {
        if (request == REQUEST_CODE) {
            if (token!=null && result==Activity.RESULT_OK) {
                if (token.isValid()) {

                } else {
                    token.setHandle(data);
                    projection = manager.getMediaProjection(Activity.RESULT_OK, token.getHandle());
                }
                return true;
            } else if (result == Activity.RESULT_CANCELED) {
                token = null;
            }
        }

        return false;
    }

    /**
     * MediaProjectionBuilder
     *
     * Builder helping to establish IPC link avoiding authentication by the user
     *
     * Communicates
     *
     * @experimental
     */
    static private class MediaProjectionIPCTokenBuilder {
        private Context context;

        static public MediaProjectionIPCTokenBuilder build(Context context) {
            return new MediaProjectionIPCTokenBuilder(context);
        }

        public MediaProjectionIPCTokenBuilder(Context context) {
            this.context = context;
        }

        public Intent getToken() {
            ApplicationInfo info = Applications.getApplicationInfo(context);
            IBinder binder = getServiceBinder();

            Intent intent = null;
            if(hasProjectionPermission(binder, info.uid, info.packageName)) {
                IMediaProjection projection = createProjection(binder, info.uid, info.packageName);
                if(projection!=null) {
                    intent = new Intent();
                    intent.putExtra("android.media.projection.extra.EXTRA_MEDIA_PROJECTION", (Parcelable) projection.asBinder());
                }
            }

            return intent;
        }

        private boolean hasProjectionPermission(IBinder binder, int uid, String packageName) {
            Parcel data = Parcel.obtain();
            Parcel reply = Parcel.obtain();
            boolean result = false;
            try {
                data.writeInterfaceToken("android.media.projection.IMediaProjectionManager");
                data.writeInt(uid);
                data.writeString(packageName);
                binder.transact(IBinder.FIRST_CALL_TRANSACTION + 0, data, reply, 0);
                reply.readException();
                result = (0!=reply.readInt());
            } catch (Exception e) {
                Debug.getInstance().exception(e, this);
            } finally {
                reply.recycle();
                data.recycle();
            }

            return result;
        }

        private IMediaProjection createProjection(IBinder binder, int uid, String packageName) {
            Parcel data = Parcel.obtain();
            Parcel reply = Parcel.obtain();
            IMediaProjection projection = null;
            try {
                data.writeInterfaceToken("android.media.projection.IMediaProjectionManager");
                data.writeInt(uid);
                data.writeString(packageName);
                data.writeInt(0);
                data.writeInt(0);
                binder.transact(IBinder.FIRST_CALL_TRANSACTION + 1, data, reply, 0);
                reply.readException();
                projection = IMediaProjection.Stub.asInterface(reply.readStrongBinder());
            } catch (Exception e) {
                Debug.getInstance().exception(e, this);
            }  finally {
                reply.recycle();
                data.recycle();
            }

            return projection;
        }

        private IBinder getServiceBinder() {
            IBinder binder = null;
            try {
                Class classServiceManager = Class.forName("android.os.ServiceManager");
                Method methodGetService = classServiceManager.getMethod("getService", new Class[]{String.class});
                if (methodGetService != null) {
                    Object result = methodGetService.invoke(classServiceManager, new Object[]{Context.MEDIA_PROJECTION_SERVICE});
                    if (result != null) {
                        binder = (IBinder) result;
                    }
                }
            } catch (Exception e) {
                Debug.getInstance().exception(e, this);
            }

            return binder;
        }

        private Intent getPermissionDialogIntent() {
            Intent intent = new Intent();
            intent.setComponent(ComponentName.unflattenFromString(
                    "com.android.systemui/com.android.systemui.media.MediaProjectionPermissionActivity"));
            return intent;
        }
    }
}
