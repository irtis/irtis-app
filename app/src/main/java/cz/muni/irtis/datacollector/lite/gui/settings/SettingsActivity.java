package cz.muni.irtis.datacollector.lite.gui.settings;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;

public class SettingsActivity extends ActivityBase {
    public static final String SELECTED_SCREEN = "screen";
    public static final int SCREEN_SYNC = 1;
    public static final int SCREEN_METRICS = 2;

    /**
     * onCreate()
     *
     * Display proper settings screen fragment base on received input
     *
     * @param bundle
     */
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_settings);

        // Handle refresh fragment
        Fragment fragment = null;
        switch (getIntent().getIntExtra(SELECTED_SCREEN, SCREEN_SYNC)) {
            case SCREEN_SYNC:
                fragment = Fragment.instantiate(this, SyncFragment.class.getName());
                break;
            case SCREEN_METRICS:
                fragment = Fragment.instantiate(this, MetricsFragment.class.getName());
                break;
        }

        if(fragment!=null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment, fragment);
            transaction.commit();
        }
    }
}
