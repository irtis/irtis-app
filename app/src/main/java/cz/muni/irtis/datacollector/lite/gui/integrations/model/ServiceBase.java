package cz.muni.irtis.datacollector.lite.gui.integrations.model;

import android.content.Context;


abstract public class ServiceBase {
    public static final int ERROR_NETWORK_NOT_AVAILABLE = 9753;

    public static final int ERROR_ACCOUNT_NOT_EXISTING = 5548;
    public static final int ERROR_ACCOUNT_IS_EXISTING = 8735;
    public static final int ERROR_ACCOUNT_NOT_ACTIVATED = 1942;

    public static final int ERROR_ACCESS_DENIED = 5498;

    public static final int ERROR_PASSWORD_WRONG = 7367;

    public static final int ERROR_CANCELED = 3217;

    protected Context context;

    public interface OnServiceListener {
        public void onStart();
        public void onError(int code);
        public void onFinished(boolean isLogged);
    }
    protected OnServiceListener onSignInListener;
    protected OnServiceListener onSignOutListener;


    public ServiceBase(Context context) {
        this.context = context;
    }
    public ServiceBase(Context context, OnServiceListener onSignInListener) {
        this.context = context;
        this.onSignInListener = onSignInListener;
    }
    public ServiceBase(Context context, OnServiceListener onSignInListener, OnServiceListener onSignOutListener) {
        this.context = context;
        this.onSignInListener = onSignInListener;
        this.onSignOutListener = onSignOutListener;
    }

    abstract public boolean isConnected();
//    abstract public void signIn();


    public Context getContext() {
        return context;
    }

    public void setOnSignInListener(OnServiceListener listener) {
        this.onSignInListener = listener;
    }

    public void setOnSignOutListener(OnServiceListener listener) {
        this.onSignOutListener = listener;
    }
}
