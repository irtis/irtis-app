package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenEntity;

@Dao
public interface ScreenRepository extends IRepository<ScreenEntity> {
    public static final String TABLE_NAME = "screen";

    /**
     * Add new entity to database.
     * @param item entity
     */
    @Insert
    long insert(ScreenEntity item);

    @Query("SELECT * " +
            "FROM "+TABLE_NAME+" " +
            "WHERE "+TABLE_NAME+".datetime = :time")
    ScreenEntity getByTime(Long time);

    @Query("SELECT * " +
            "FROM "+TABLE_NAME+" " +
            "WHERE "+TABLE_NAME+".datetime < :time")
    List<ScreenEntity> getPrevious(Long time);

    @Query("SELECT * " +
            "FROM "+TABLE_NAME+" " +
            "WHERE "+TABLE_NAME+".datetime < :time" + " " +
            "LIMIT :offset, :count")
    List<ScreenEntity> getPrevious(Long time, int offset, int count);

    /**
     * {@link IRepository#deleteById(Long)}
     */
    @Query("DELETE FROM " + TABLE_NAME + " WHERE datetime = :id")
    int deleteById(Long id);



}
