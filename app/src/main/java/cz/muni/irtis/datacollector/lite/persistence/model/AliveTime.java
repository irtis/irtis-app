package cz.muni.irtis.datacollector.lite.persistence.model;

public class AliveTime {
    private Long started;
    private Long stopped;

    public AliveTime(Long started, Long stopped) {
        this.started = started;
        this.stopped = stopped;
    }

    static public AliveTime build(Long started, Long stopped) {
        return new AliveTime(started, stopped);
    }

    public AliveTime(Long started) {
        this.started = started;
    }
}
