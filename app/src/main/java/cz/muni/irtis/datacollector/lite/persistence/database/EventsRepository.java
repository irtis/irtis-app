package cz.muni.irtis.datacollector.lite.persistence.database;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import cz.muni.irtis.datacollector.lite.application.database.EntityRepositoryBase;

@Dao
public interface EventsRepository extends EntityRepositoryBase<EventEntity> {
    public static final String TABLE_NAME = "events";

    @Query("SELECT * FROM "+TABLE_NAME+" WHERE uploaded IS NULL")
    List<EventEntity> getItemsNotUploaded();
}
