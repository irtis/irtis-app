package cz.muni.irtis.datacollector.lite.metrics.model.util;

import android.os.Environment;
import android.os.StatFs;

public class Storage {

    static public long getFreeSpace() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        long blockSize = stat.getBlockSizeLong();
        long blockCount = stat.getAvailableBlocksLong();
        long size = blockCount * blockSize;
        return size;
    }

    static public boolean isFreeSpace() {
        double megaBytes = getFreeSpace() / (1024 * 1024);
        return megaBytes > 100;
    }

    static public boolean isZeroSpace() {
        return getFreeSpace() == 0;
    }
}
