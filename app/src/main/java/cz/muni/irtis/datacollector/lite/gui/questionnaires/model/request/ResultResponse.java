package cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request;

public class ResultResponse {
    private Boolean result;
    private String message;

    public ResultResponse(Boolean result, String message) {
        this.result = result;
        this.message = message;
    }

    public ResultResponse(Boolean result) {
        this.result = result;
    }

    public Boolean getResult() {
        return result;
    }

    public Boolean isSuccessful() {
        return result;
    }

    public String getMessage() {
        return message;
    }
}
