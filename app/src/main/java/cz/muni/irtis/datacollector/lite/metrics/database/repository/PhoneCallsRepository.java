package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.phone.PhoneCallEntity;

@Dao
public interface PhoneCallsRepository extends IRepository<PhoneCallEntity> {
    public static final String TABLE_NAME = "phone_calls";

    /**
     * Add new entity to database.
     * @param item entity
     */
    @Insert
    long insert(PhoneCallEntity item);

    /**
     * Get entity by it's PK datetime
      * @param time java style timestamp (1561544094*1000)
     * @return entity
     */
    @Query("SELECT * FROM " + TABLE_NAME + " WHERE datetime = :time")
    PhoneCallEntity getByTime(Long time);

    /**
     * {@link IRepository#getPrevious(Long)}
     */
    @Query("SELECT * FROM " + TABLE_NAME + " " +
            "WHERE datetime < :time" + " " +
            "AND uploaded IS NULL")
    List<PhoneCallEntity> getPrevious(Long time);

    /**
     * {@link IRepository#getPrevious(Long)}
     */
    @Query("SELECT * FROM " + TABLE_NAME + " " +
            "WHERE datetime < :time" + " " +
            "AND uploaded IS NULL" + " " +
            "LIMIT :offset, :count")
    List<PhoneCallEntity> getPrevious(Long time, int offset, int count);

    @Query("SELECT MAX(call_date) FROM " + TABLE_NAME)
    Long getMaxCallDatetime();

    /**
     * {@link IRepository#deleteById(Long)}
     */
    @Query("UPDATE " + TABLE_NAME + " SET uploaded = :id WHERE datetime = :id AND uploaded IS NULL")
    int deleteById(Long id);

//    @Query("UPDATE " + TABLE_NAME + " SET uploaded = :time WHERE uploaded IS NULL")
//    void deletePrevious(Long time, int offset, int count);
}
