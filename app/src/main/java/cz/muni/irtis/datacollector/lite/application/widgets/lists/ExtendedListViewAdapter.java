package cz.muni.irtis.datacollector.lite.application.widgets.lists;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * ExtendedListViewAdapter
 *
 */
public class ExtendedListViewAdapter<T> extends BaseAdapter {
	private Integer idItemLayout;
	private List<T> items;


	/**
	 * ViewHolder
	 */
	static abstract public class ViewHolder {
		public ViewHolder(View view) {
			view.setTag(this);
		}
	}


	public interface OnHolderListener<T, H> {
		/**
		 * Create holder for row views
		 * @param adapter
		 * @param view
		 * @return
		 */
		public ViewHolder onCreate(ExtendedListViewAdapter adapter, View view);

		/**
		 * Fill holder with item data
		 * @param holder
		 * @param item
		 */
		public void onBind(H holder, T item);
	}
	private OnHolderListener onHolderListener;

	public interface OnLayoutListener {
		/**
		 * Create layout view from idItemLayout or any other specific way
		 * @param parent
		 * @return
		 */
		public View onCreate(ViewGroup parent);
	}
	private OnLayoutListener onLayoutListener;



	public ExtendedListViewAdapter(Integer resource) {
		this.items = new ArrayList<>();
		this.setItemLayout(resource);
	}

	public ExtendedListViewAdapter() {
		this(0);
	}


	@Override
	public int getCount() {
		if(this.items!=null) {
			return this.items.size();
		} else {
			return 0;
		}
	}

	public T getItem(int position) {
		return this.items.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return 0;
	}

	public List<T> getItems() {
		return this.items;
	}

	public void addItem(T item, boolean refresh) {
		this.items.add(item);
		if(refresh) {
			this.refresh();
		}
	}
	public void addItem(T item) {
		this.addItem(item, false);
	}

	public void addItems(List<T> items, boolean refresh) {
		this.items.addAll(items);
		if(refresh) {
			this.refresh();
		}
	}
	public void addItems(List<T> items) {
		this.addItems(items, false);
	}

	public void setItems(List<T> items, boolean refresh) {
		this.items = items;
		if(refresh) {
			this.refresh();
		}
	}
	public void setItems(List<T> items) {
		this.setItems(items, false);
	}

	public void remove(int position) { this.items.remove(position); }
	public void remove(T item) { this.items.remove(item); }

	public boolean isEmpty() {
		return this.items!=null && !this.items.isEmpty();
	}

	public void clear() {
		this.items.clear();
	}

	public void refresh() {
		notifyDataSetChanged();
	}


	public Integer getItemLayout()
	{
		return this.idItemLayout;
	}
	public void setItemLayout(Integer resource) {this.idItemLayout = resource;}


	@Override
	public View getView(int position, android.view.View view, ViewGroup parent) {
		ViewHolder holder = null;
		if(view==null) {
			if (onLayoutListener != null) {
				view = onLayoutListener.onCreate(parent);
			} else if (this.idItemLayout > 0) {
				view = LayoutInflater.from(parent.getContext())
						.inflate(this.idItemLayout, parent, false);
			}

			holder = this.onHolderListener.onCreate(ExtendedListViewAdapter.this, view);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		T item = this.items.get(position);
		this.onHolderListener.onBind(holder, item);

		return view;
	}


	public void setOnLayoutListener(OnLayoutListener listener) {
		this.onLayoutListener = listener;
	}

	public void setOnHolderListener(OnHolderListener listener)
	{
		this.onHolderListener = listener;
	}
}
