package cz.muni.irtis.datacollector.lite.gui.identity.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import cz.muni.irtis.datacollector.lite.Application;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.ConnectionResult;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.identity.model.response.ConfigResponseEntity;
import cz.muni.irtis.datacollector.lite.gui.identity.model.response.GenerateResponseEntity;
import cz.muni.irtis.datacollector.lite.gui.identity.model.response.TokenResponseEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.ResultResponse;
import okhttp3.Response;

/**
 * Identity
 *
 * Client identity
 */
public class Identity extends Repository {
    static private Identity instance;

    private String token;

    public Identity(Context context) {
        super(context);
    }

    public interface OnIdentityGenerateListener {
        void onStart();
        void onError();
        void onDone(String login, String password);
    }
    public interface OnIdentitySignInListener {
        void onStart();
        void onDone(boolean isLogged);
    }

    static public Identity getInstance() {
        if(instance==null) {
            instance = new Identity(Application.getInstance().getApplicationContext());
        }
        return instance;
    }

    public boolean isLogged() {
        return getToken()!=null;
    }

    public boolean isToken() {
        return token!=null;
    }

    public String getToken() {
        if(token==null) {
            String sett = getSettings().getString("token", null);
            token = sett!=null && !sett.isEmpty() ? sett : null;
        }
        return token;
    }

    public void setToken(String token) {
        this.token = token;
        getSettings().edit().putString("token", token).commit();
    }

    public void isSignedIn(final OnIdentitySignInListener listener) {
        // @todo Test requred!!
//        run(new TaskListener<Boolean>() {
//            @Override
//            public void onStart() {
//                listener.onStart();
//            }
//
//            @Override
//            public Boolean onTask() {
//                return isSignedIn();
//            }
//
//            @Override
//            public void onDone(Boolean result) {
//                listener.onDone(result);
//            }
//        });
        Connection.build(getContext()).getJson(
            UrlComposer.compose("/identity/issignedin"),
            new Connection.OnConnectionListener<String>() {
                @Override
                public void onStart() {
                    listener.onStart();
                }
                @Override
                public void onError(Exception e) {}
                @Override
                public void onDone(Response response, String content) {
                    Boolean isSigned = false;
                    if (content != null && !content.isEmpty()) {
                        isSigned = new GsonBuilder().create().fromJson(content, Boolean.class);
                    }
                    listener.onDone(isSigned);
                }
            }
        );
    }

    /**
     * isSignedIn()
     *
     * @endpoint identity/issignedin
     * @return
     */
    public boolean isSignedIn() {
        String content = Connection.build(getContext()).getJson(
                UrlComposer.compose("/identity/issignedin"));

        Boolean isSigned = false;
        if (content != null && !content.isEmpty()) {
            isSigned = new GsonBuilder().create().fromJson(content, Boolean.class);
        }

        return isSigned;
    }



    public void signIn(String login, String password, final OnIdentitySignInListener listener) {
        Credentials credentials = Credentials.build(login, password);
        credentials.setDevice(DeviceIdentity.build()
            .setDevice(android.os.Build.MODEL)
            .setApi(android.os.Build.VERSION.SDK));

        Connection.build(getContext()).postJson(
            UrlComposer.compose("/identity/signin"),
            credentials.toString(),
            new Connection.OnConnectionListener<String>() {
                @Override
                public void onStart() {
                    listener.onStart();
                }
                @Override
                public void onError(Exception e) {
                }
                @Override
                public void onDone(Response response, String content) {
                    if(content!=null && !content.isEmpty()) {
                        TokenResponseEntity item = new GsonBuilder().create().fromJson(content, TokenResponseEntity.class);
                        setToken(item.getToken());
                    }
                    listener.onDone(isLogged());
                }
            }
        );
    }

    /**
     * signIn()
     *
     * @endpoint /identity/signin
     * @param login
     * @param password
     */
    public boolean signIn(String login, String password) {
        Credentials credentials = Credentials.build(login, password);
        credentials.setDevice(DeviceIdentity.build()
            .setDevice(android.os.Build.MODEL)
            .setApi(android.os.Build.VERSION.SDK));

        ConnectionResult result = Connection.build(getContext()).postJson(
                UrlComposer.compose("/identity/signin"),
                credentials.toString());

        if (result != null && result.isResult() && result.isContent()) {
            TokenResponseEntity item = result.getContent(new TypeToken<TokenResponseEntity>() {
            });
            if(item!=null) {
                setToken(item.getToken());
            } else {
                setToken(null);
            }
            return isLogged();
        }

        return false;
    }


    /**
     * signOut()
     *
     * @deprecated
     */
    public void signOut(final OnIdentitySignInListener listener) {
        setToken(null);
        Connection.build(getContext()).getJson(
            UrlComposer.compose("identity/signout"),
            new Connection.OnConnectionListener() {
                @Override
                public void onStart() {
                    listener.onStart();
                }
                @Override
                public void onError(Exception e) {}
                @Override
                public void onDone(Response response, Object content) {
                    listener.onDone(true);
                }
            }
        );
    }

    /**
     * signOut()
     *
     * @endpoint /identity/signout
     */
    public boolean signOut() {
        String content = Connection.build(getContext()).getJson(
                UrlComposer.compose("/identity/signout"));

        Boolean isSigned = false;
        if (content != null && !content.isEmpty()) {
            isSigned = new GsonBuilder().create().fromJson(content, Boolean.class);
        }

        setToken(null);

        return isSigned;
    }


    /**
     * generate()
     *
     * @deprecated
     */
    public void generate(final OnIdentityGenerateListener listener) {
        Connection.build(getContext()).postJson(
            UrlComposer.compose("identity/generate"),
            DeviceIdentity.build()
                .setDevice(android.os.Build.MODEL)
                .setApi(android.os.Build.VERSION.SDK)
                .toString(),
            new Connection.OnConnectionListener<String>() {
                @Override
                public void onStart() {
                    listener.onStart();
                }
                @Override
                public void onError(Exception e) {
                    Debug.getInstance().exception(e, this);
                }
                @Override
                public void onDone(Response response, String content) {
                    GenerateResponseEntity item = new GsonBuilder().create().fromJson(content, GenerateResponseEntity.class);
                    if(item!=null) {
                        setToken(item.getToken());
                        listener.onDone(item.getLogin(), item.getPassword());
                    } else {
                        listener.onError();
                    }
                }
            }
        );
    }

    public void downloadConfig(final RepositoryListener<ConfigResponseEntity> listener) {
        run(new TaskListener<ConfigResponseEntity>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public ConfigResponseEntity onTask() {
                String content = Connection.build(getContext()).getJson(UrlComposer.compose("identity/config/download") );
                if(content!=null && !content.isEmpty()) {
                    return new GsonBuilder().create().fromJson(content, ConfigResponseEntity.class);
                } else {
                    return null;
                }
            }

            @Override
            public void onDone(ConfigResponseEntity result) {
                listener.onDone(result);
            }
        });
    }

    private SharedPreferences getSettings() {
        Context context = Application.getInstance().getApplicationContext();
        SharedPreferences settings = context.getSharedPreferences(context.getPackageName()+".identity", Context.MODE_PRIVATE);

        return settings;
    }
}