package cz.muni.irtis.datacollector.lite.application.permissions;

import android.content.Context;

import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_USAGE;


public class PermissionUsage extends Permission {

    public PermissionUsage(Context context, String title, String notes)
    {
        super(context, PERMISSION_USAGE, title, notes);
    }

    public boolean isGranted()
    {
        return provider.isGrantedUsagePermission();
    }

}
