package cz.muni.irtis.datacollector.lite.metrics.model.applications;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import java.util.List;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.metrics.Metric;

/**
 * @todo Work in progress...
 */
public class InstalledApplication extends Metric {
    public static final int IDENTIFIER = 248;
    private PackageManager packageManager;

    public InstalledApplication(Context context) {
        super(context, 0);
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "Installed";
    }

    @Override
    public void run() {
        setRunning(true);
        packageManager = getContext().getPackageManager();

        List<ApplicationInfo> packages = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);

        for (ApplicationInfo packageInfo : packages) {
            if (packageInfo.FLAG_SYSTEM != 1) {
                Log.d("InstalledApplication", "Installed package :" + packageInfo.packageName);
            }
        }
    }

    @Override
    public void stop() {
        setRunning(false);
    }

    @Override
    public long save(Object... params) {
        try {
            return 0;
        } catch (Exception e) {
            Debug.getInstance().exception(e);
            return -1;
        }
    }
}
