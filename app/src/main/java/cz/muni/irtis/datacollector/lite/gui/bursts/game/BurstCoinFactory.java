package cz.muni.irtis.datacollector.lite.gui.bursts.game;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class BurstCoinFactory {
    static public BurstCoin build(Integer value) {
       switch (value) {
           default: return null;
           case 10: return new BurstCoinGold();
           case 5: return new BurstCoinSilver();
           case 1: return new BurstCoinBronze();
       }
    }
}

