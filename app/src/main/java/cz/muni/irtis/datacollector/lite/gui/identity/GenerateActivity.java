package cz.muni.irtis.datacollector.lite.gui.identity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.application.widgets.Box;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Credentials;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Identity;

public class GenerateActivity extends ActivityBase {
    private View vLoading;
    private View vForm;

    private EditText vName;
    private EditText vEmail;
    private EditText vPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate);

        vLoading = findViewById(R.id.loading);
        vForm = findViewById(R.id.form);

        vName = findViewById(R.id.name);
        vEmail = findViewById(R.id.email);
        vPhone = findViewById(R.id.phone);

        vForm.setVisibility(View.GONE);

        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid()) {
                    generate();
                } else {
                    Snackbar.make(v, getString(R.string.signin_generate_credentials_error), Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    private boolean isValid() {
        boolean isResult = true;
        /*
        isResult &= !vName.getText().toString().isEmpty();
        isResult &= !vEmail.getText().toString().isEmpty();
        isResult &= !vPhone.getText().toString().isEmpty();

        if(!vEmail.getText().toString().isEmpty()) {
            Pattern pattern = Pattern.compile("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", Pattern.CASE_INSENSITIVE);
            isResult &= pattern.matcher(vEmail.getText().toString()).find();
        }

        if(!vPhone.getText().toString().isEmpty()) {
            Pattern pattern = Pattern.compile("^(\\+420)? ?[1-9][0-9]{2} ?[0-9]{3} ?[0-9]{3}$", Pattern.CASE_INSENSITIVE);
            isResult &= pattern.matcher(vPhone.getText().toString()).find();
        }
        */
        return isResult;
    }

    private void generate() {
        if(Network.isConnection()) {
            Identity.getInstance().generate(
                new Identity.OnIdentityGenerateListener() {
                    @Override
                    public void onStart() {
                        vLoading.setVisibility(View.VISIBLE);
                        vForm.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        vLoading.setVisibility(View.GONE);
                        vForm.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onDone(String login, String password) {
                        vLoading.setVisibility(View.GONE);
                        vForm.setVisibility(View.VISIBLE);

                        startActivity(CredentialsActivity.class, Credentials.build(login, password).toString());
                    }
                }
            );
        } else {
            Box.ok(this, getString(R.string.network_no_connection),
                    getString(R.string.network_no_connection_suggestion));
        }
    }
}
