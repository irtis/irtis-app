package cz.muni.irtis.datacollector.lite.metrics.model.condition;

import android.content.Context;

import cz.muni.irtis.datacollector.lite.application.network.Network;

public class IsWifiOn implements Condition {
    @Override
    public boolean check(Context context) {
        return Network.isWIFI();
    }
}
