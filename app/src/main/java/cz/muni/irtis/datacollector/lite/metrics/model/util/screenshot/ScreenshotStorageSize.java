package cz.muni.irtis.datacollector.lite.metrics.model.util.screenshot;

import android.content.Context;
import android.os.AsyncTask;

import java.io.File;

import static android.os.AsyncTask.THREAD_POOL_EXECUTOR;

/**
 * StorageSize
 *
 * Gets the total amount of used space by the application data
 *
 */
public class ScreenshotStorageSize {
    private Context context;
    private boolean isAsyncFinished;

    public interface GetSizeAsyncListener {
        public void onDone(long bits);
    }
    private GetSizeAsyncListener listener;


    public ScreenshotStorageSize(Context context) {
        this.isAsyncFinished = true;
        this.context = context;
    }

    static public ScreenshotStorageSize build(Context context) {
        return new ScreenshotStorageSize(context);
    }

    /**
     * getSize()
     *
     * Get size of the used space
     *
     * @param folder
     * @return
     */
    private long getSize(File folder) {
        String path = folder.getAbsolutePath();
        long length = 0;
        File[] files = folder.listFiles();
        if(files!=null) {
            int count = files.length;
            for (int i = 0; i < count; i++) {
                if (files[i].isFile()) {
                    length += files[i].length();
                } else {
                    length += getSize(files[i]);
                }
            }
        }
        return length;
    }
    private long getSize() {
        return this.getSize(new File(context.getCacheDir(), "screenshots"));
    }

    /**
     * getSizeAsync()
     *
     * Get size of the used space in an async run
     *
     * @param folder
     * @param listener
     */
    public void getSizeAsync(File folder, GetSizeAsyncListener listener) {
        if(this.isAsyncFinished) {
            this.listener = listener;
            new GetStorageTask().executeOnExecutor(THREAD_POOL_EXECUTOR, folder);
        }
    }
    public void getSizeAsync(GetSizeAsyncListener listener) {
        getSizeAsync(new File(context.getCacheDir(), "/screenshots"), listener);
    }


    private class GetStorageTask extends AsyncTask<File, File, Long> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Long doInBackground(File... folder)
        {
            return getSize(folder[0]);
        }
        @Override
        protected void onPostExecute(Long bits) {
            listener.onDone(bits);
        }
    }

    static public long toMB(long bits) {
        int mb = 1024*1024;
        return bits < mb ? 0 : bits / mb;
    }
    static public long toGB(long bits) {
        int gb = 1024*1024*1024;
        return bits < gb ? 0 : bits / gb;
    }
}
