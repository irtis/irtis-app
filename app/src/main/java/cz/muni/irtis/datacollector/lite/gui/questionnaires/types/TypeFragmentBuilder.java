package cz.muni.irtis.datacollector.lite.gui.questionnaires.types;

import android.content.Context;

import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;

public class TypeFragmentBuilder {

    static public TypeFragmentBase build(Context context, Question question, boolean editable) {
        switch (question.getType()) {
            default:
            case Question.TYPE_TEXT:
                return new TextFragment(context, question, editable);
            case Question.TYPE_NUMBER:
                return new NumberFragment(context, question, editable);
            case Question.TYPE_CHECK:
                return new CheckFragment(context, question, editable);
            case Question.TYPE_SCALE:
                return new ScaleFragment(context, question, editable);
            case Question.TYPE_SLIDER:
                return new SliderFragment(context, question, editable);
            case Question.TYPE_MULTIPLE_SLIDER:
                return new MultipleSliderFragment(context, question, editable);
            case Question.TYPE_SPEECH:
                return new SpeechFragment(context, question, editable);
            case Question.TYPE_TIME_SPINNER:
                return new TimeSpinnerFragment(context, question, editable);
            case Question.TYPE_TIME_CLOCK:
                return new TimeClockFragment(context, question, editable);
            case Question.TYPE_TIME_DATE:
                return new TimeDateSpinnerFragment(context, question, editable);
            case Question.TYPE_TIME_BIRTHDATE:
                return new TimeBirthDateSpinnerFragment(context, question, editable);
            case Question.TYPE_TIME_HOURSMINUTES:
                return new TimeHoursMinutesSpinnerFragment(context, question, editable);
        }
    }
}
