package cz.muni.irtis.datacollector.lite.metrics.model.condition;

import android.content.Context;

public interface Condition {
    boolean check(Context context);
}
