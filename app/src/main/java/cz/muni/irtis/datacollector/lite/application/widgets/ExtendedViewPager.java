package cz.muni.irtis.datacollector.lite.application.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import cz.muni.irtis.datacollector.lite.application.components.FragmentBase;

public class ExtendedViewPager extends ViewPager {
    private boolean isSwipePaging;
    private boolean isSmoothScroll;
    private Stack<Integer> history;

    public ExtendedViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        isSwipePaging = true;
        isSmoothScroll = true;
        history = new Stack<>();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isSwipePaging) {
            return super.onTouchEvent(event);
        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (isSwipePaging) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    public void setSwipePagingEnabled(boolean enabled) {
        this.isSwipePaging = enabled;
    }

    public void setSmoothScroll(boolean enabled) {
        isSmoothScroll = enabled;
    }

    @Override
    public void setCurrentItem(int item) {
        history.push(getCurrentItem());
        super.setCurrentItem(item, isSmoothScroll);
    }

    public void setPreviousItem() {
        if(!history.isEmpty()) {
            int item = history.pop();
            super.setCurrentItem(item, isSmoothScroll);
        }
    }

    public int getPosition() {
        return getCurrentItem();
    }

    public int getPosition(FragmentBase item) {
        return ((ExtendedViewPagerFragmentAdapter) getAdapter()).getPosition(item);
    }

    public int getPositionPrevious() {
        if(!history.isEmpty()) {
            return history.lastElement();
        } else {
            return -1;
        }
    }

    public boolean isFirstItem(FragmentBase item) {
        int p = getPosition(item);
        return getPosition(item)==0;
    }

    public boolean isLastItem(FragmentBase item) {
        return getPosition(item)+1==getAdapter().getCount();
    }

    public boolean isHistory() {
        return !history.isEmpty();
    }

    public int getItemsCount() {
        return getAdapter().getCount();
    }


    static public class ExtendedViewPagerFragmentAdapter extends FragmentPagerAdapter {
        protected List<FragmentBase> items;

        public ExtendedViewPagerFragmentAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
            items = new ArrayList<FragmentBase>();
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            if(items!=null && !items.isEmpty()) {
                return items.get(position);
            } else {
                return null;
            }
        }

        public int getPosition(FragmentBase item) {
            return items.indexOf(item);
        }


        @Override
        public int getCount() {
            return items!=null ? items.size() : 0;
        }


    }
}
