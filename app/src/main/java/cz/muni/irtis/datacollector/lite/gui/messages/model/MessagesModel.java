package cz.muni.irtis.datacollector.lite.gui.messages.model;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.ConnectionResult;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import cz.muni.irtis.datacollector.lite.application.utils.BooleanSet;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.application.widgets.Box;
import cz.muni.irtis.datacollector.lite.gui.messages.database.MessageEntity;
import cz.muni.irtis.datacollector.lite.gui.messages.database.MessagesRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.QuestionnaireJson;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.ResultResponse;
import cz.muni.irtis.datacollector.lite.persistence.model.Events;

import static android.os.AsyncTask.THREAD_POOL_EXECUTOR;

public class MessagesModel extends Repository {
    private MessagesRepository repository;


    static public MessagesModel build(Context context) {
        return new MessagesModel(context);
    }

    public MessagesModel(Context context) {
        super(context);
        this.repository = getDatabase().getMessagesRepository();
    }

    public void store(final Message item, final RepositoryListener<Boolean> listener) {
        run(new TaskListener<Boolean>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public Boolean onTask() {
                repository.store(MessageEntity.build(item));
                return true;
            }
            @Override
            public void onDone(Boolean result) {
                listener.onDone(result);
            }
        });
    }

    public void setNotified(final Message item, final RepositoryListener<Boolean> listener) {
        run(new TaskListener<Boolean>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public Boolean onTask() {
                setNotified(item, Time.getTime());
                return true;
            }
            @Override
            public void onDone(Boolean result) {
                listener.onDone(result);
            }
        });
    }

    public void setNotified(Message item, long time) {
        item.setNotified(time);
        repository.store(MessageEntity.build(item));
    }


    public void setOpened(final Message item, final RepositoryListener<Boolean> listener) {
        run(new TaskListener<Boolean>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public Boolean onTask() {
                setOpened(item, Time.getTime());
                return true;
            }
            @Override
            public void onDone(Boolean result) {
                listener.onDone(result);
            }
        });
    }

    public void setOpened(Message item, long time) {
        item.setOpened(time);
        repository.store(MessageEntity.build(item));
    }


    public void setRead(final Message item, final RepositoryListener<Boolean> listener) {
        run(new TaskListener<Boolean>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public Boolean onTask() {
                setRead(item, Time.getTime());
                return true;
            }
            @Override
            public void onDone(Boolean result) {
                listener.onDone(result);
            }
        });
    }

    public void setRead(Message item, long time) {
        item.setRead(time);
        repository.store(MessageEntity.build(item));
    }


    public void setUploaded(final Message item, final RepositoryListener<Boolean> listener) {
        run(new TaskListener<Boolean>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public Boolean onTask() {
                setUploaded(item, Time.getTime());
                return true;
            }
            @Override
            public void onDone(Boolean result) {
                listener.onDone(result);
            }
        });
    }

    public void setUploaded(Message item, long time) {
        item.setUploaded(time);
        repository.store(MessageEntity.build(item));
    }


    public void setResponse(final Message item, final String response, final RepositoryListener<Boolean> listener) {
        run(new TaskListener<Boolean>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public Boolean onTask() {
                setResponse(item, response);
                return true;
            }
            @Override
            public void onDone(Boolean result) {
                listener.onDone(result);
            }
        });
    }

    public void setResponse(Message item, String response) {
        item.setResponse(response);
        item.setUploaded(null); // In order to upload the message to the server again
        repository.store(MessageEntity.build(item));
    }


    public void getItems(final RepositoryListener<List<Message>> listener) {
        run(new TaskListener<List<Message>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public List<Message> onTask() {
                List<Message> messages = new ArrayList<>();
                List<MessageEntity> entities = getDatabase().getMessagesRepository().getItems();
                if(entities!=null && !entities.isEmpty()) {
                    Iterator<MessageEntity> iMessageEntities = entities.iterator();
                    while (iMessageEntities.hasNext()) {
                        MessageEntity entity = iMessageEntities.next();
                        messages.add(Message.build(entity));
                    }
                    return messages;
                } else {
                    return null;
                }
            }
            @Override
            public void onDone(List<Message> items) {
                listener.onDone(items);
            }
        });
    }

    public void getItemsNewest(final RepositoryListener<List<Message>> listener) {
        run(new TaskListener<List<Message>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public List<Message> onTask() {
                List<Message> messages = new ArrayList<>();
                List<MessageEntity> entities = getDatabase().getMessagesRepository().getItemsNewest();
                if(entities!=null && !entities.isEmpty()) {
                    Iterator<MessageEntity> iMessageEntities = entities.iterator();
                    while (iMessageEntities.hasNext()) {
                        MessageEntity entity = iMessageEntities.next();
                        messages.add(Message.build(entity));
                    }
                    return messages;
                } else {
                    return null;
                }
            }
            @Override
            public void onDone(List<Message> items) {
                listener.onDone(items);
            }
        });
    }

    public void getItemsNewestCount(final RepositoryListener<Integer> listener) {
        run(new TaskListener<Integer>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public Integer onTask() {
                return getDatabase().getMessagesRepository().getItemsNewestCount();
            }
            @Override
            public void onDone(Integer items) {
                listener.onDone(items);
            }
        });
    }

    public List<Message> getItemsNotUploaded() {
        List<MessageEntity> entities = repository.getNotUploaded();
        if(entities!=null) {
            List<Message> items = new ArrayList<>();
            Iterator<MessageEntity> i = entities.iterator();
            while(i.hasNext()) {
                MessageEntity entity = i.next();
                items.add(Message.build(entity));
            }
            return items;
        } else {
            return null;
        }
    }


    public void getItem(final Message item, final RepositoryListener<Message> listener) {
        run(new TaskListener<Message>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public Message onTask() {
                MessageEntity entity = getDatabase().getMessagesRepository().getItem(item.getId());
                if(entity!=null) {
                    return Message.build(entity);
                } else {
                    return null;
                }
            }
            @Override
            public void onDone(Message item) {
                listener.onDone(item);
            }
        });
    }


    /**
     * download
     *
     * Download and store messages from the server
     *
     * @param listener
     */
    public void download(final RepositoryListener<List<Message>> listener) {
//        runDebug(new TaskListener<List<Message>>() {
//            @Override
//            public void onStart() {
//                listener.onStart();
//            }
//
//            @Override
//            public List<Message> onTask() {
//                String result = Connection.build(getContext())
//                    .getJson(UrlComposer.compose("/messages/download"));
//
//                if(result!=null) {
//                    List<Message> items = new GsonBuilder().create().fromJson(result, new TypeToken<List<Message>>(){}.getType());
//                    if(items!=null && !items.isEmpty()) {
//                        Iterator<Message> iMessages = items.iterator();
//                        while (iMessages.hasNext()) {
//                            Message item = iMessages.next();
//                            item.setDownloaded(Time.getTime());
//                            repository.store(MessageEntity.build(item));
//                        }
//                    }
//                    return items;
//                } else {
//                    return null;
//                }
//            }
//
//            @Override
//            public void onDone(List<Message> result) {
//                listener.onDone(result);
//            }
//        });
        new DownloadTask(listener).executeOnExecutor(THREAD_POOL_EXECUTOR);
    }

    public class DownloadTask extends AsyncTask<Void, Void, List<Message>> {
        private RepositoryListener<List<Message>> listener;

        public DownloadTask(RepositoryListener<List<Message>> listener) {
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            listener.onStart();
        }

        @Override
        protected List<Message> doInBackground(Void... nul) {
            return download();
        }

        @Override
        protected void onPostExecute(List<Message> items) {
            listener.onDone(items);
        }
    }

    /**
     * download()
     *
     * @endpoint /messages/download
     * @return
     */
    public List<Message> download() {
        String result = Connection.build(getContext())
                .getJson(UrlComposer.compose("/messages/download"));

        if(result!=null) {
            List<Message> items = new GsonBuilder().create().fromJson(result, new TypeToken<List<Message>>() {
            }.getType());
            if (items != null && !items.isEmpty()) {
                Iterator<Message> iMessages = items.iterator();
                while (iMessages.hasNext()) {
                    Message item = iMessages.next();
                    item.setDownloaded(Time.getTime());
                    repository.store(MessageEntity.build(item));
                }
            }
            return items;
        } else {
            return null;
        }
    }


    public void upload(final RepositoryListener<Boolean> listener) {
//        run(new TaskListener<Boolean>() {
//            @Override
//            public void onStart() {
//                listener.onStart();
//            }
//
//            @Override
//            public Boolean onTask() {
//                List<Message> items = getItemsNotUploaded();
//                if (items != null && !items.isEmpty()) {
//
//                    List<Message> temporals = new ArrayList<>();
//                    Iterator<Message> iItems = items.iterator();
//                    while(iItems.hasNext()) {
//                        Message temporal = iItems.next();
//                        if(temporal.isRead()) {
//                            temporals.add(temporal);
//                        }
//                    }
//                    items = temporals;
//
//                    BooleanSet results = BooleanSet.build(items.size());
//                    iItems = items.iterator();
//                    while(iItems.hasNext()) {
//                        Message item = iItems.next();
//                        MessageJson json = new MessageJson(item);
//
//                        ConnectionResult result = Connection.build(getContext()).postJson(
//                                UrlComposer.compose("/messages/upload"),
//                                json.toString());
//
//                        if(result!=null && result.isResult() && result.isContent()) {
//                            ResultResponse response = result.getContent(new TypeToken<ResultResponse>(){});
//                            if (response != null && response.isSuccessful()) {
//                                setUploaded(item, Time.getTime());
//                                results.setTrue();
//                            }
//                        }
//                    }
//
//                    return results.isTrue();
//                } else {
//                    return false;
//                }
//            }
//
//            @Override
//            public void onDone(Boolean result) {
//                listener.onDone(result);
//            }
//        });
        new UploadTask(listener).executeOnExecutor(THREAD_POOL_EXECUTOR);
    }

    public class UploadTask extends AsyncTask<Void, Void, Boolean> {
        private RepositoryListener<Boolean> listener;

        public UploadTask(RepositoryListener<Boolean> listener) {
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            listener.onStart();
        }

        @Override
        protected Boolean doInBackground(Void... nul) {
            List<Message> items = getItemsNotUploaded();
            if (items != null && !items.isEmpty()) {
                List<Message> temporals = new ArrayList<>();
                Iterator<Message> iItems = items.iterator();
                while(iItems.hasNext()) {
                    Message temporal = iItems.next();
                    if(temporal.isRead()) {
                        temporals.add(temporal);
                    }
                }
                items = temporals;

                BooleanSet results = BooleanSet.build(items.size());
                iItems = items.iterator();
                while(iItems.hasNext()) {
                    Message item = iItems.next();
                    if(upload(item)) {
                        results.setTrue();
                    }
                }

                return results.isTrue();
            } else {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            listener.onDone(result);
        }
    }

    public void upload(final Message item, final RepositoryListener<Boolean> listener) {
        run(new TaskListener<Boolean>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public Boolean onTask() {
                return upload(item);
            }
            @Override
            public void onDone(Boolean result) {
                listener.onDone(result);
            }
        });
    }

    /**
     * upload()
     *
     * @endpoint /messages/upload
     * @param item
     */
    public boolean upload(Message item) {
        MessageJson json = new MessageJson(item);

        ConnectionResult result = Connection.build(getContext()).postJson(
                UrlComposer.compose("/messages/upload"),
                json.toString());

        if(result!=null && result.isResult() && result.isContent()) {
            ResultResponse response = result.getContent(new TypeToken<ResultResponse>(){});
            if (response != null && response.isSuccessful()) {
                setUploaded(item, Time.getTime());
                return true;
            }
        }

        return false;
    }
}
