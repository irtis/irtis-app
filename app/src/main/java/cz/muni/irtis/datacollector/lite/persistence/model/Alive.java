package cz.muni.irtis.datacollector.lite.persistence.model;


public class Alive {
    private Boolean application;    // Indicates if the application is running
    private Boolean capture;        // Indicates if the capturing is running
    private Boolean screenshots;    // Indicates if the capturing is running

    private Alive(Boolean application, Boolean capture, Boolean screenshots) {
        this.application = application;
        this.capture = capture;
        this.screenshots = screenshots;
    }

    static public Alive build(Boolean application, Boolean capture, Boolean screenshots) {
        return new Alive(application, capture, screenshots);
    }
}
