package cz.muni.irtis.datacollector.lite.metrics.database.entities.screen;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.EntityBase;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ScreenshotsRepository;

@Entity(tableName = ScreenshotsRepository.TABLE_NAME)
public class ScreenshotEntity extends EntityBase {
    @ColumnInfo(name = "device_url")
    @SerializedName("device_url")
    private String deviceUrl;

    public ScreenshotEntity(Long datetime, String deviceUrl) {
        super(datetime);
        this.deviceUrl = deviceUrl;
    }

    public String getDeviceUrl() {
        return deviceUrl;
    }
}
