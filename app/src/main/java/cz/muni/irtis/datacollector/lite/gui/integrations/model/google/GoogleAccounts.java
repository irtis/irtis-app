package cz.muni.irtis.datacollector.lite.gui.integrations.model.google;

import android.Manifest;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.ExponentialBackOff;

import java.util.Arrays;

import cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;

import static android.app.Activity.RESULT_OK;

/**
 * GoogleAccounts
 *
 * Listing account requires android.permission.GET_ACCOUNTS permission in the manifest file
 */
public class GoogleAccounts {
    static private final String SETTING_ACCOUNT_NAME = "google_account_selected";
    static private final int REQUEST_PERMISSION_GET_ACCOUNTS = 1894;
    static private final int REQUEST_ACCOUNT_PICKER = 9841;

    private Context context;

    private GoogleAccountCredential credential;
    private PermissionsProvider permissions;

    public GoogleAccounts(Context context, String[] scopes) {
        this.context = context;
        permissions = new PermissionsProvider(context, false);
        credential = GoogleAccountCredential.usingOAuth2(
            context, Arrays.asList(scopes))
            .setBackOff(new ExponentialBackOff());
    }

    /**
     * isAccounts()
     *
     * Check if there isActive any google account
     *
     * @return
     */
    public boolean isAccounts() {
        return credential.getAllAccounts()!=null;
    }

    /**
     * selectAccount()
     *
     * Select particular account name
     *
     * @param accountName
     */
    public void selectAccount(String accountName) {
        credential.setSelectedAccountName(accountName);
    }

    public GoogleAccountCredential getCredential() {
        return credential;
    }


    public void startChooseActivity(Activity activity) {
        if (permissions.isGranted(Manifest.permission.GET_ACCOUNTS)) {
            String accountName = Settings.getSettings().getString(SETTING_ACCOUNT_NAME, null);
            if (accountName != null) {
                selectAccount(accountName);
            } else {
                activity.startActivityForResult(credential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
            }
        } else {
            // Request the GET_ACCOUNTS permission via a user dialog
            permissions.request(Manifest.permission.GET_ACCOUNTS, REQUEST_PERMISSION_GET_ACCOUNTS);
        }
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==REQUEST_PERMISSION_GET_ACCOUNTS) {

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null && data.getExtras() != null) {
                    String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        SharedPreferences.Editor editor = Settings.getSettings().edit();
                        editor.putString(SETTING_ACCOUNT_NAME, accountName);
                        editor.apply();
                        selectAccount(accountName);
                    }
                }
                break;
        }
    }
}
