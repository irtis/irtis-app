package cz.muni.irtis.datacollector.lite.application.permissions;

import android.content.Context;

import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_ACTIVITY_RECOGNITION;
import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_NOTIFICATIONS;

/**
 * PermissionActivityRecognition
 *
 */
public class PermissionActivityRecognition extends Permission {

    public PermissionActivityRecognition(Context context, String title, String notes) {
        super(context, PERMISSION_ACTIVITY_RECOGNITION, title, notes);
    }

    public boolean isGranted()
    {
        return provider.isGrantedActivityRecognitionPermission();
    }

}
