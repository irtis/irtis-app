package cz.muni.irtis.datacollector.lite.gui.integrations.model.google;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;


import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;

/**
 * GoogleService
 *
 * Connector for authorized access to google services
 *
 * When you want to make a call to one of the Google APIs provided in the
 * Google Play services library (such as Google Sign-in and Drive), you need to
 * append an instance of one the API client objects, which are subclasses of GoogleApi.
 *
 * The ApiException status code indicates the detailed failure reason.
 * See GoogleSignInStatusCodes class reference for more information.
 *
 * https://developers.google.com/android/guides/setup
 * https://developers.google.com/identity/sign-in/android/start-integrating
 *
 * Backend authentication:
 * https://developers.google.com/identity/sign-in/android/backend-auth
 */
public class GoogleService extends ServiceBase {
    private static final int RC_SIGN_IN = 4165;

    private GoogleSignInOptions options;
    private GoogleApiClient api;

    public GoogleService(Context context, GoogleSignInOptions options, OnServiceListener onSignInListener, OnServiceListener onSignOutListener) {
        super(context, onSignInListener, onSignOutListener);

        this.options = options;
        if(this.options==null) {
            this.options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
        }

        api = new GoogleApiClient.Builder(context)
            .addApi(Auth.GOOGLE_SIGN_IN_API, options)
            .build();
    }
    public GoogleService(Context context, GoogleSignInOptions options, OnServiceListener onSignInListener) {
        this(context, null, onSignInListener, null);
    }
    public GoogleService(Context context, OnServiceListener onSignInListener, OnServiceListener onSignOutListener) {
        this(context, null, onSignInListener, onSignOutListener);
    }
    public GoogleService(Context context, OnServiceListener onSignInListener) {
        this(context, null, onSignInListener, null);
    }
    public GoogleService(Context context) {
        this(context, null, null, null);
    }


    public boolean isConnected() {
        return getAccount()!=null;
    }

    public void signIn(Activity activity) {
        onSignInListener.onStart();
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(api);
        activity.startActivityForResult(intent, RC_SIGN_IN);
    }

    public void signIn() {
        onSignInListener.onStart();
        Auth.GoogleSignInApi.silentSignIn(api).addStatusListener(new PendingResult.StatusListener() {
            @Override
            public void onComplete(Status status) {
                onSignInListener.onFinished(isConnected());
            }
        });
    }

    public void signOut() {
        onSignOutListener.onStart();
        GoogleSignInClient client = GoogleSignIn.getClient(getContext(), options);
        client.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                onSignOutListener.onFinished(isConnected());
            }
        });
    }

    public void connect(final OnServiceListener listener) {
        listener.onStart();
        api.connect();
    }

    public void disconnect() {
        onSignOutListener.onStart();
        GoogleSignInClient client = GoogleSignIn.getClient(getContext(), options);
        client.revokeAccess().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                onSignOutListener.onFinished(isConnected());
            }
        });
    }

    public GoogleSignInAccount getAccount() {
        return GoogleSignIn.getLastSignedInAccount(getContext());
    }

    public GoogleApiClient getClient() {
        return api;
    }

    /**
     * isGooglePlayServicesAvailable()
     *
     * Check that Google Play services APK isActive installed and up to date.
     *
     * @return true if Google Play Services isActive available and up to
     *     date on this device; false otherwise.
     */
    static public boolean isGooglePlayServicesAvailable(Context context) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int code = apiAvailability.isGooglePlayServicesAvailable(context);
        switch (code) {
            case ConnectionResult.SUCCESS:
                return true;

            case ConnectionResult.SERVICE_MISSING:
                Debug.getInstance().exception(new IllegalStateException("GooglePlayServices service isActive missing"));
                return false;
            case ConnectionResult.SERVICE_DISABLED:
                Debug.getInstance().exception(new IllegalStateException("GooglePlayServices service disable"));
                return false;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                Debug.getInstance().exception(new IllegalStateException("GooglePlayServices service needs an update"));
                return false;
            default:
                Debug.getInstance().exception(new IllegalStateException("GooglePlayServices error"));
                return false;
        }
    }

    /**
     * getGooglePlayServicesErrorDialog()
     *
     * The method returns a Dialog you should show, which provides an appropriate message about
     * the error and provides an action that takes the user to Google Play Store to install the update
     *
     * @param activity
     * @param cancelListener
     * @return Dialog
     */
    static public Dialog getGooglePlayServicesErrorDialog(Activity activity, DialogInterface.OnCancelListener cancelListener) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int code = apiAvailability.isGooglePlayServicesAvailable(activity);
        return apiAvailability.getErrorDialog(activity, code, code, cancelListener);
    }


    public void setSignInButton(final View button) {
        button.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                if(Network.isConnection()) {
                    if(GoogleService.isGooglePlayServicesAvailable(getContext())) {
                        signIn((Activity) button.getContext());
                    } else {
                        Dialog dialog = GoogleService.getGooglePlayServicesErrorDialog((Activity) button.getContext(), new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {

                            }
                        });

                        if(dialog!=null) {
                            dialog.show();
                        }
                    }
                } else {
                    onSignInListener.onError(GoogleSignInStatusCodes.NETWORK_ERROR);
                }
            }
        });
//        button.setSize(SignInButton.SIZE_WIDE);
//        button.setScopes(options.getScopeArray());
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RC_SIGN_IN:
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    onSignInListener.onFinished(true);
                } catch (ApiException e) {
                    Debug.getInstance().exception(e, this, "GoogleSignIn:failed code=" + e.getStatusCode()
                            + ", reason="+GoogleSignInStatusCodes.getStatusCodeString(e.getStatusCode()));
                    onSignInListener.onError(e.getStatusCode());
                }
                break;

            // Google Play services errors
            case ConnectionResult.SERVICE_MISSING:
                break;
            case ConnectionResult.SERVICE_DISABLED:
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                break;
        }
    }
}
