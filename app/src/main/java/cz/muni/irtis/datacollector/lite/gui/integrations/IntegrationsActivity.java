package cz.muni.irtis.datacollector.lite.gui.integrations;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.ExtendedListView;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.ExtendedListViewAdapter;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.StaticListViewBuilder;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.IntegrationBase;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.facebook.FacebookIntegration;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.google.YoutubeIntegration;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.InstagramIntegration;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.twitter.TwitterIntegration;


public class IntegrationsActivity extends ActivityBase {
    public static final String DISPLAY          = "display";
    public static final int DISPLAY_REQUIRE     = 735;
    public static final int DISPLAY_OVERVIEW    = 672;

    private ExtendedListViewAdapter list;

    static private class ViewHolder extends ExtendedListViewAdapter.ViewHolder {
        public ImageView vIcon;
        public TextView vTitle;
        public Switch vButton;
        public TextView vNotes;

        public ViewHolder(View view) {
            super(view);
            this.vIcon = (ImageView) view.findViewById(R.id.icon);
            this.vTitle = (TextView) view.findViewById(R.id.title);
            this.vButton = (Switch) view.findViewById(R.id.button);
            this.vNotes = (TextView) view.findViewById(R.id.notes);
        }
    }

    static private class ListItem {
        private IntegrationBase integration;
        private Class activityClass;

        public ListItem(IntegrationBase integration, Class activityClass) {
            this.integration = integration;
            this.activityClass = activityClass;
        }

        public IntegrationBase getIntegration() {
            return integration;
        }

        public Class getActivityClass() {
            return activityClass;
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_integrations);

        switch (this.getIntent().getIntExtra(DISPLAY, DISPLAY_REQUIRE)) {
            default:
            case DISPLAY_REQUIRE:
                break;
            case DISPLAY_OVERVIEW:
                findViewById(R.id.submit).setVisibility(View.GONE);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                break;
        }

        iniLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();
        list.refresh();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);

            case android.R.id.home:
                this.finish();
                return true;
        }
    }

    public void iniLayout() {
        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        list = StaticListViewBuilder.build(this)
                .setView(R.id.list)
                .setAdapter(new ExtendedListViewAdapter<ListItem>(R.layout.activity_integrations_list_row))
                .setOnItemClickListener(new ExtendedListView.OnItemClickListener<ListItem>() {
                    @Override
                    public void onClick(ExtendedListViewAdapter adapter, ListItem integration) {
                        startActivity(integration.getActivityClass());
                        list.refresh();
                    }
                })
                .setOnHolderListener(new ExtendedListViewAdapter.OnHolderListener<ListItem, ViewHolder>() {
                    @Override
                    public ExtendedListViewAdapter.ViewHolder onCreate(ExtendedListViewAdapter adapter, View view) {
                        return new ViewHolder(view);
                    }

                    @Override
                    public void onBind(ViewHolder holder, ListItem item) {
                        holder.vIcon.setImageDrawable(item.getIntegration().getImage());
                        holder.vTitle.setText(item.getIntegration().getName());
                        holder.vButton.setChecked(item.getIntegration().isConnected());
                    }
                })
                .create()
                .getAdapter();

        list.addItem(new ListItem(new YoutubeIntegration(this), YoutubeConnectorActivity.class));
        list.addItem(new ListItem(new FacebookIntegration(this), FacebookConnectorActivity.class));
//        list.addItem(new ListItem(new MessengerIntegration(this), MessengerConnectorActivity.class));
        list.addItem(new ListItem(new InstagramIntegration(this), InstagramConnectorActivity.class));
        list.addItem(new ListItem(new TwitterIntegration(this), TwitterConnectorActivity.class));

        list.refresh();
    }
}
