package cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request;

import com.google.gson.annotations.SerializedName;

import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;

public class QuestionJson {
    @SerializedName("questionnaire_id")
    private Long idQuestionnaire;
    @SerializedName("question_id")
    private Long idQuestion;
    private Integer elapsed;
    private Boolean skipped;

    public QuestionJson(QuestionEntity entity) {
        this.idQuestionnaire = entity.getIdQuestionnaire();
        this.idQuestion = entity.getIdQuestion();
        this.elapsed = entity.getElapsed();
        this.skipped = entity.getSkipped();
    }

    public QuestionJson(Question item) {
        this.idQuestionnaire = item.getQuestionnaire().getIdQuestionnaire();
        this.idQuestion = item.getId();
        this.elapsed = item.getElapsed();
        this.skipped = item.getSkipped();
    }

    public void setIdQuestionnaire(Long idQuestionnaire) {
        this.idQuestionnaire = idQuestionnaire;
    }

    public void setIdQuestion(Long idQuestion) {
        this.idQuestion = idQuestion;
    }

    public void setElapsed(Integer elapsed) {
        this.elapsed = elapsed;
    }

    public Long getIdQuestionnaire() {
        return idQuestionnaire;
    }

    public Long getIdQuestion() {
        return idQuestion;
    }

    public Integer getElapsed() {
        return elapsed;
    }

    public Boolean getSkipped() {
        return skipped;
    }
}
