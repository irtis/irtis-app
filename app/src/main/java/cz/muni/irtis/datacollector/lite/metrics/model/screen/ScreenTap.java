package cz.muni.irtis.datacollector.lite.metrics.model.screen;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.Metric;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenTapEntity;

/**
 * ActiveScreen
 *
 * This metrics takes care of screen on/off state
 *
 * The Taps are filtered by the set delay
 */
public class ScreenTap extends Metric {
    private static final String TAG = ScreenTap.class.getSimpleName();
    public static final int IDENTIFIER = 246;

    private WindowManager manager;
    private View view;
    private Long eventTimeOut;

    public ScreenTap(Context context, Integer delay) {
        super(context, delay);
        manager = (WindowManager) context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        eventTimeOut = 0L;
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "Taps";
    }

    @Override
    public void run() {
        createView();
        setRunning(true);
    }

    @Override
    public void stop() {
        destroyView();
        setRunning(false);
    }

    @Override
    public long save(Object... params) {
        Integer x = (Integer) params[0];
        Integer y = (Integer) params[1];

        try {
            Debug.getInstance().log(TAG, x + ", " + y);
            return getDatabase().getScreenTapsRepository().insert(new ScreenTapEntity(
                Time.getTime(),
                x, y
            ));
        } catch (Exception e) {
            Debug.getInstance().exception(e);
            return -1;
        }
    }


    private void createView() {
        if(view==null) {
            // Create fragment view
            view = new View(getContext());
            view.setOnTouchListener(new OnTouchListener());
//        view.setBackgroundColor(Color.RED);

            int LAYOUT_FLAG;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
            } else {
                LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
            }

            // Define window management
            WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                    1, 1,
                    LAYOUT_FLAG,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                            WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                            WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSPARENT
            );
            params.gravity = Gravity.LEFT | Gravity.TOP;

            manager.addView(view, params);
        }
    }

    private void destroyView() {
        if(view!=null) {
            manager.removeView(view);
            view = null;
        }
    }


    /**
     * OnTouchListener
     *
     * Handling touch events for the fragment
     *
     */
    public class OnTouchListener implements View.OnTouchListener {
        private final GestureDetector gestureDetector = new GestureDetector(new GestureListener());

        public boolean onTouch(View v, MotionEvent event) {
            if(eventTimeOut<Time.getTime()) {
                save((int) event.getX(),(int) event.getY());
                eventTimeOut = Time.getTime()+getDelay();
            }
            return false;
        }

        private final class GestureListener extends GestureDetector.SimpleOnGestureListener {
            private static final int SWIPE_THRESHOLD = 100;
            private static final int SWIPE_VELOCITY_THRESHOLD = 100;

            @Override
            public void onLongPress(MotionEvent e) {
                onOnLongPress();
                super.onLongPress(e);
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                onOnDoubleTab();
                return super.onDoubleTap(e);
            }

            @Override
            public boolean onDown(MotionEvent e) {
                return super.onDown(e);
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                onOnSingleTapUp();
                return super.onSingleTapUp(e);
            }


            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                boolean result = false;
                try {
                    float diffY = e2.getY() - e1.getY();
                    float diffX = e2.getX() - e1.getX();
                    if (Math.abs(diffX) > Math.abs(diffY)) {
                        if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffX > 0) result = onSwipeRight();
                            else result = onSwipeLeft();
                        }
                    } else {
                        if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffY > 0) result = onSwipeBottom();
                            else result = onSwipeTop();
                        }
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }

                return result;
            }
        }

        public boolean onOnLongPress() {
            return false;
        }

        public boolean onOnDoubleTab() {
            return false;
        }

        public boolean onOnSingleTapUp() {
            return false;
        }

        public boolean onSwipeRight() {
            return false;
        }

        public boolean onSwipeLeft() {
            return false;
        }

        public boolean onSwipeTop() {
            return false;
        }

        public boolean onSwipeBottom() {
            return false;
        }
    }
}
