package cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items;

import androidx.annotation.Nullable;

import java.util.List;

import cz.muni.irtis.datacollector.lite.application.json.Exclude;

public class Answer {
    @Exclude
    private Question question;
    @Exclude
    private Questionnaire questionnaire;
    private Long id;
    private Long link;
    private List<Long> exclusions;
    private String text;
    private String value;
    private Boolean selected;
    private Integer elapsed;

    public Answer(Question question, Long id, String text) {
        this.question = question;
        this.id = id;
        this.text = text;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }


    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public boolean isValue() {
        return value!=null && !value.isEmpty();
    }


    public Boolean isSelected() { return selected!=null && selected.equals(true); }

    public void setSelected() {
        this.selected = true;
    }
    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Boolean getSelected() {
        return selected;
    }

    public boolean isLink() {
        return link !=null && link >0;
    }

    public List<Long> getExclusions() {
        return exclusions;
    }



    /**
     * isLink()
     *
     * Get link id of the corresponding following question depending on the refresh answer
     * @return
     */
    public Long getLink() {
        return link;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        Answer object = (Answer) obj;
        if(!this.id.equals(object.getId())) {
            return false;
        }

        if(this.link !=null) {
            if (!this.link.equals(object.getLink())) {
                return false;
            }
        } else {
            if(this.link ==null && this.link !=null) {
                return false;
            }
        }

        if(this.value!=null) {
            if (!this.value.equals(object.getValue())) {
                return false;
            }
        } else {
            if(this.value==null && this.value!=null) {
                return false;
            }
        }

        if(this.selected!=null) {
            if (!this.selected.equals(object.getSelected())) {
                return false;
            }
        } else {
            if(this.selected==null && this.selected!=null) {
                return false;
            }
        }

        return true;
    }
}
