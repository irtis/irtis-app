package cz.muni.irtis.datacollector.lite.gui.bursts.model;

import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.bursts.game.BurstPurse;
import cz.muni.irtis.datacollector.lite.persistence.database.BurstEntity;

public class Burst {
    private Long id;
    private String identifier;
    private String title;
    private Long starting;
    private Long ending;
    private Integer paceCurrent;
    private Integer paceAverage;
    private Integer pacePrevious;
    private BurstPurse purse;


    public Burst(Long id) {
        this.id = id;
    }

    public Burst(BurstEntity entity) {
        this.id = entity.getId();
        this.identifier = entity.getIdentifier();
        this.title = entity.getTitle();
        this.starting = entity.getStarting();
        this.ending = entity.getEnding();
    }

    public static Burst build(Long id) {
        return new Burst(id);
    }

    public static Burst build(BurstEntity entity) {
        return new Burst(entity);
    }


    public Long getId() {
        return id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getTitle() {
        return title;
    }

    public Long getStarting() {
        return starting;
    }

    public void setStarting(Long starting) {
        this.starting = starting;
    }

    public Long getEnding() {
        return ending;
    }

    public void setEnding(Long ending) {
        this.ending = ending;
    }

    public boolean isActive() {
        return starting <= Time.getTime() && ending >= Time.getTime();
    }

    public String getTimeFormatted() {
        return Time.getTimeStamp(starting, "HH:mm dd.MM.yyyy") + " - " + Time.getTimeStamp(ending, "HH:mm dd.MM.yyyy");
    }

    public String getTimeFormattedShort() {
        return Time.getTimeStamp(starting, "dd.MM.") + " - " + Time.getTimeStamp(ending, "dd.MM.yyyy");
    }

    public Integer getTimeRemaining() {
        if(isActive()) {
            return Time.getElapsedDays((int) (getEnding() - Time.getTime()) / 1000);
        } else if(getStarting() > Time.getTime()) {
            return Time.getElapsedDays((int) (getEnding() - getStarting()) / 1000);
        } else {
            return 0;
        }
    }

    public String getTimeRemainingFormatted() {
        if(getTimeRemaining() > 0) {
            return getTimeRemaining().toString() + " " + (getTimeRemaining() > 1 ? "days" : "day");
        } else {
            return "0 days";
        }
    }


    public Integer getPaceCurrent() {
        return paceCurrent;
    }
    public String getPaceCurrentFormatted() {
        return paceCurrent !=null ? paceCurrent.toString() + "%" : "0%";
    }
    public void setPaceCurrent(Integer pace) {
        this.paceCurrent = pace;
    }

    public Integer getPaceAverage() {
        return paceAverage;
    }
    public String getPaceAverageFormatted() {
        return paceAverage !=null ? paceAverage.toString() + "%" : "0%";
    }
    public void setPaceAverage(Integer pace) {
        this.paceAverage = pace;
    }

    public Integer getPacePrevious() {
        return pacePrevious;
    }
    public String getPacePreviousFormatted() {
        return pacePrevious !=null ? pacePrevious.toString() + "%" : "0%";
    }
    public void setPacePrevious(Integer pace) {
        this.pacePrevious = pace;
    }


    public Integer getProgress() {
        if(isActive()) {
            Float percent = Float.valueOf((float) (Time.getTime() - getStarting()) / (getEnding() - getStarting())) * 100;
            return percent.intValue();
        } else {
            if(ending < Time.getTime()) {
                return 100;
            } else {
                return 0;
            }
        }
    }

    public BurstPurse getPurse() {
        return purse;
    }

    public void setPurse(BurstPurse purse) {
        this.purse = purse;
    }

}
