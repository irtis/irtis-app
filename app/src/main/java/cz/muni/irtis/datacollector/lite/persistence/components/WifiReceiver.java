package cz.muni.irtis.datacollector.lite.persistence.components;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;

import cz.muni.irtis.datacollector.lite.application.debug.DebugLogs;
import cz.muni.irtis.datacollector.lite.application.utils.Delay;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.synchronization.MetricsSynchronizationService;

/**
 * WifiTurnOrReceiver
 *
 * When WIFI is turned on, we use the opportunity to upload every essential data to the server
 */
public class WifiReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(final Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION)) {
            if (intent.getBooleanExtra(WifiManager.EXTRA_SUPPLICANT_CONNECTED, false)) {
                if(MetricsSynchronizationService.isOutOfSynchronization()) {
                    Delay.build(Time.SECOND * 15, new Runnable() {
                        @Override
                        public void run() {
                            // Debug
                            DebugLogs.build(context).upload();

                            // Synchronization
                            MetricsSynchronizationService.start(context);
                        }
                    });
                }
            }
        }
    }
}
