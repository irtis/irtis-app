package cz.muni.irtis.datacollector.lite.gui.questionnaires;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListView;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewAdapter;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewBuilder;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesViewModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;


public class QuestionnairesListActivity extends AppCompatActivity {
    private RecyclerListViewAdapter list;
    private QuestionnairesModel model;

    static public class ViewHolder extends RecyclerListViewAdapter.ItemsListViewHolderBase {
        public ImageView vCheck;
        public TextView vTime;
        public TextView vTitle;

        public ViewHolder(RecyclerListViewAdapter adapter, View view) {
            super(adapter, view);
            vCheck = view.findViewById(R.id.check);
            vTime = view.findViewById(R.id.time);
            vTitle = view.findViewById(R.id.title);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaires);

        model = new QuestionnairesModel(this);

        // Build ListView with listeners (getting adapter as reference)
        list = RecyclerListViewBuilder.build(this)
            .setView(R.id.list)
            .setAdapter(new RecyclerListViewAdapter<Questionnaire, ViewHolder>(R.layout.activity_questionnaires_list_row))
            .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<Questionnaire, ViewHolder>() {
                @Override
                public ViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                    return new ViewHolder(adapter, view);
                }

                @Override
                public void onBind(ViewHolder holder, Questionnaire item) {
                    holder.vCheck.setVisibility(item.isCompleted() ? View.VISIBLE : View.INVISIBLE);
                    holder.vTime.setText(item.getDateFormatted() + " " + item.getIntervalHoursFormatted() + " | " + item.getInvokingHoursFormatted());
                    holder.vTitle.setText(item.getTitle());
                }
            })
            .setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<Questionnaire>() {
                @Override
                public void onClick(View view, Questionnaire item) {
                    QuestionnairesFormActivity.start(QuestionnairesListActivity.this, item);
                }
            })
            .setOnScrollListener(new RecyclerListView.OnScrollListener() {
                @Override
                public void onScroll(Integer count) {
                    get(count);
                }
            })
            .create()
            .getAdapter();

        // Set the first contents of the ListView
        get();
    }


    public void get(int offset) {
        model.getItems(offset, 15, new Repository.RepositoryListener<List<Questionnaire>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onDone(List<Questionnaire> items) {
                if(items!=null && !items.isEmpty()) {
                    list.addItems(items, true);
                }
            }
        });
    }
    public void get() {
        list.clear();
        list.refresh();
        get(0);
    }

}
