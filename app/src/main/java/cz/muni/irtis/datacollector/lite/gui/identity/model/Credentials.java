package cz.muni.irtis.datacollector.lite.gui.identity.model;

import com.google.gson.GsonBuilder;

public class Credentials {
    private String login;
    private String password;
    private String device;
    private String api;

    private Credentials(String login, String password) {
        this.login = login;
        this.password = password;
    }

    static public Credentials build(String login, String password) {
        return new Credentials(login, password);
    }

    static public Credentials build(String json) {
        return new GsonBuilder().create().fromJson(json, Credentials.class);
    }

    public String toString() {
        return new GsonBuilder().create().toJson(this, Credentials.class);
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setDevice(DeviceIdentity device) {
        this.device = device.getDevice();
        this.api = device.getApi();
    }

    public String getDevice() {
        return device;
    }

    public String getApi() {
        return api;
    }
}
