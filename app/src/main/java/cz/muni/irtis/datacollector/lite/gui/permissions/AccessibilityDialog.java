package cz.muni.irtis.datacollector.lite.gui.permissions;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;

import androidx.appcompat.app.AlertDialog;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.permissions.Permission;

public class AccessibilityDialog extends PermissionDialogBase {

    public AccessibilityDialog(Context context, Permission permission) {
        super(context, permission, AccessibilityDialog.class.getSimpleName());
    }

    @Override
    protected AlertDialog onBuilderCreate(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getContext().getString(R.string.permissions_accessibility))
                .setIcon(R.drawable.ic_accessibility_grey_24dp)
                .setPositiveButton("OK", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        getContext().startActivity(intent);
                    }
                });
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            builder.setMessage(Html.fromHtml(getContext().getString(R.string.permissions_hooking_dialog).replace("{appname}", getContext().getString(R.string.app_name)), Html.FROM_HTML_MODE_LEGACY));
        } else {
            builder.setMessage(Html.fromHtml(getContext().getString(R.string.permissions_hooking_dialog).replace("{appname}", getContext().getString(R.string.app_name))));
        }
        return builder.create();
    }
}