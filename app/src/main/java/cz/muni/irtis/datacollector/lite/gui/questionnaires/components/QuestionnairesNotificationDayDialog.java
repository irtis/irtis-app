package cz.muni.irtis.datacollector.lite.gui.questionnaires.components;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.NumberPicker;

import androidx.appcompat.app.AlertDialog;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.NotificationsDay;

public class QuestionnairesNotificationDayDialog extends Dialog {
    private AlertDialog dialog;
    private NotificationsDay item;
    private OnSubmitListener listener;
    private Boolean isStarting;

    NumberPicker pickerHours;
    NumberPicker pickerMinutes;

    public interface OnSubmitListener {
        void onSubmit();

        void onCancel();
    }


    public QuestionnairesNotificationDayDialog(Context context, NotificationsDay item, boolean isStarting, OnSubmitListener listener) {
        super(context);
        this.item = item;
        this.listener = listener;
        this.isStarting = isStarting;
        this.dialog = onBuilderCreate(null);
    }


    protected AlertDialog onBuilderCreate(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View vContent;

        if (isStarting) {
            vContent = getLayoutInflater().inflate(R.layout.component_questionnaires_notifications_time_starting, null);

            pickerHours = iniHours((NumberPicker) vContent.findViewById(R.id.morningHours));
            pickerMinutes = iniMinutes((NumberPicker) vContent.findViewById(R.id.morningMinutes));

            pickerHours.setValue(item.getStartingHours());
            pickerMinutes.setValue(item.getStartingMinutes());

        } else {
            vContent = getLayoutInflater().inflate(R.layout.component_questionnaires_notifications_time_ending, null);

            pickerHours = iniHours((NumberPicker) vContent.findViewById(R.id.nightHours));
            pickerMinutes = iniMinutes((NumberPicker) vContent.findViewById(R.id.nightMinutes));

            pickerHours.setValue(item.getEndingHours());
            pickerMinutes.setValue(item.getEndingMinutes());
        }

        builder
            .setView(vContent)
            .setTitle(getContext().getString(isStarting ? R.string.questionnaires_invocations_starting_title : R.string.questionnaires_invocations_ending_title))
            .setPositiveButton("Ok", new OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    long time = pickerHours.getValue() * Time.HOUR + pickerMinutes.getValue() * Time.MINUTE;
                    long min;
                    long max;
                    if (isStarting) {
                        min = (!item.isWeekend() ? Config.QUESTIONNAIRES_WEEK_MORNING_MIN : Config.QUESTIONNAIRES_WEEKEND_MORNING_MIN);
                        max = (!item.isWeekend() ? Config.QUESTIONNAIRES_WEEK_MORNING_MAX : Config.QUESTIONNAIRES_WEEKEND_MORNING_MAX);
                    } else {
                        min = (!item.isWeekend() ? Config.QUESTIONNAIRES_WEEK_NIGHT_MIN : Config.QUESTIONNAIRES_WEEKEND_NIGHT_MIN);
                        max = (!item.isWeekend() ? Config.QUESTIONNAIRES_WEEK_NIGHT_MAX : Config.QUESTIONNAIRES_WEEKEND_NIGHT_MAX);
                    }
                    if (time < min) {
                        time = min;
                    }

                    if (time > max) {
                        time = max;
                    }

                    if (isStarting) {
                        Settings.getSettings()
                                .edit()
                                .putLong(Settings.QUESTIONNAIRES_MORNING + "_" + item.getDay(), time)
                                .commit();
                    } else {
                        Settings.getSettings()
                                .edit()
                                .putLong(Settings.QUESTIONNAIRES_NIGHT + "_" + item.getDay(), time)
                                .commit();
                    }
                    listener.onSubmit();
                    close();
                }
            })
            .setNegativeButton("Cancel", new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    listener.onCancel();
                    close();
                }
            })
            .setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        listener.onCancel();
                        close();
                    }
                    return true;
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    listener.onCancel();
                    close();
                }
            });

        return builder.create();
    }

    private NumberPicker iniHours(NumberPicker view) {
        view.setWrapSelectorWheel(true);
        view.setTag(item.isWeekend());
        if (!item.isWeekend()) {
            view.setMinValue((int) ((isStarting ? Config.QUESTIONNAIRES_WEEK_MORNING_MIN : Config.QUESTIONNAIRES_WEEK_NIGHT_MIN) / Time.HOUR));
            view.setMaxValue((int) ((isStarting ? Config.QUESTIONNAIRES_WEEK_MORNING_MAX : Config.QUESTIONNAIRES_WEEK_NIGHT_MAX) / Time.HOUR));
        } else {
            view.setMinValue((int) ((isStarting ? Config.QUESTIONNAIRES_WEEKEND_MORNING_MIN : Config.QUESTIONNAIRES_WEEKEND_NIGHT_MIN) / Time.HOUR));
            view.setMaxValue((int) ((isStarting ? Config.QUESTIONNAIRES_WEEKEND_MORNING_MAX : Config.QUESTIONNAIRES_WEEKEND_NIGHT_MAX) / Time.HOUR));
        }
        view.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
            }
        });
        return view;
    }

    private NumberPicker iniMinutes(NumberPicker view) {
        view.setWrapSelectorWheel(true);
        view.setMinValue(0);
        view.setMaxValue(59);
        view.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
            }
        });
        return view;
    }

    public void show() { this.dialog.show(); }

    public void hide() { this.dialog.hide(); }

    public void close() { this.dialog.hide(); }

    public AlertDialog getDialog() { return this.dialog; }
}
