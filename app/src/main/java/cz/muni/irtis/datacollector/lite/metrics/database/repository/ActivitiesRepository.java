package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.ActivityEntity;

@Dao
public interface ActivitiesRepository extends IRepository<ActivityEntity> {
    public static final String TABLE_NAME = "activities";

    /**
     * Add new entity to database.
     * @param item entity
     */
    @Insert
    long insert(ActivityEntity item);

    /**
     * Get entity by it's PK datetime
      * @param time java style timestamp (1561544094*1000)
     * @return entity
     */
    @Query("SELECT * FROM " + TABLE_NAME + " WHERE datetime = :time")
    ActivityEntity getByTime(Long time);

    /**
     * {@link IRepository#getPrevious(Long time)}
     */
    @Query("SELECT * FROM " + TABLE_NAME + " " +
            "WHERE datetime < :time")
    List<ActivityEntity> getPrevious(Long time);

    /**
     * {@link IRepository#getPrevious(Long time, int offset, int count)}
     */
    @Query("SELECT * FROM " + TABLE_NAME + " " +
            "WHERE datetime < :time" + " " +
            "LIMIT :offset, :count")
    List<ActivityEntity> getPrevious(Long time, int offset, int count);

    @Query("DELETE FROM " + TABLE_NAME + " WHERE datetime = :id")
    int deleteById(Long id);

//    @Query("DELETE FROM " + TABLE_NAME + " " +
//            "WHERE " + TABLE_NAME + ".datetime IN (" +
//            "SELECT " + TABLE_NAME + ".datetime FROM " + TABLE_NAME + " " +
//            "WHERE datetime < :time" + " " +
//            "LIMIT :offset, :count)")
//    void deletePrevious(Long time, int offset, int count);
}
