package cz.muni.irtis.datacollector.lite.application.network;

import android.content.Context;
import android.os.LocaleList;

import java.util.Locale;

import cz.muni.irtis.datacollector.lite.Application;
import cz.muni.irtis.datacollector.lite.application.Config;
import okhttp3.Headers;

/**
 * IdentificationBuilder()
 *
 * Build implicit headers to be added to the communication with the server
 */
public class IdentificationBuilder {
    static public Headers buildHeaders(Context context) {
        Headers.Builder builder = new Headers.Builder();

        // Add client locale (language code)
        if(context!=null) {
            Locale locale = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                LocaleList lItems = null;
                lItems = context.getResources().getConfiguration().getLocales();
                locale = lItems.get(0);
            } else {
                locale = context.getResources().getConfiguration().locale;
            }

            if (locale != null) {
                String localeString = locale.toString().replace("_", "-") + "," + locale.getLanguage();
                builder.add("Accept-Language", localeString);
            }
        }

        // Add client app identification
        builder.add("Application-Name", Application.getInstance().getApplicationPackage().packageName);
        builder.add("Application-Version-Name", Application.getInstance().getApplicationPackage().versionName);
        builder.add("Application-Version-Code", String.valueOf(Application.getInstance().getApplicationPackage().versionCode));
        builder.add("Application-Platform", "Android");
        builder.add("Application-Identifier", Config.SERVER_APPLICATION_IDENTIFIER);
        builder.add("Application-Key", Config.SERVER_APPLICATION_KEY);

        return builder.build();
    }
}
