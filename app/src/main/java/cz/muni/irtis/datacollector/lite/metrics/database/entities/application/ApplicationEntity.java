package cz.muni.irtis.datacollector.lite.metrics.database.entities.application;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.EntityBase;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ApplicationsRepository;

@Entity(tableName = ApplicationsRepository.TABLE_NAME)
public class ApplicationEntity extends EntityBase {
    @ColumnInfo(name = "name")
    private String name;

    public ApplicationEntity(Long datetime, String name) {
        super(datetime);
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
