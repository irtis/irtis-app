package cz.muni.irtis.datacollector.lite.metrics.database.entities.screen;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.EntityBase;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ScreenTapsRepository;

@Entity(tableName = ScreenTapsRepository.TABLE_NAME)
public class ScreenTapEntity extends EntityBase {
    @ColumnInfo(name = "x")
    private Integer x;
    @ColumnInfo(name = "y")
    private Integer y;

    public ScreenTapEntity(Long datetime, Integer x, Integer y) {
        super(datetime);
        this.x = x;
        this.y = y;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }
}
