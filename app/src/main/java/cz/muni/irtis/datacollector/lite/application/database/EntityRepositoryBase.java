package cz.muni.irtis.datacollector.lite.application.database;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Update;

public interface EntityRepositoryBase<T extends EntityBase> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void store(T item);

    @Update
    void update(T item);

    @Delete
    void delete(T item);
}
