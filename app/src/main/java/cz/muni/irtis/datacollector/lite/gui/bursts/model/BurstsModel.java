package cz.muni.irtis.datacollector.lite.gui.bursts.model;

import android.content.Context;
import android.util.Pair;

import androidx.sqlite.db.SimpleSQLiteQuery;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.database.StorageDatabase;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.bursts.game.BurstCoin;
import cz.muni.irtis.datacollector.lite.gui.bursts.game.BurstCoinBronze;
import cz.muni.irtis.datacollector.lite.gui.bursts.game.BurstCoinDeserializer;
import cz.muni.irtis.datacollector.lite.gui.bursts.game.BurstCoinGold;
import cz.muni.irtis.datacollector.lite.gui.bursts.game.BurstCoinSilver;
import cz.muni.irtis.datacollector.lite.gui.bursts.game.BurstPurse;
import cz.muni.irtis.datacollector.lite.gui.identity.model.response.ConfigResponseEntity;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities.MediaEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.QuestionnairesDatabase;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionnaireEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.QuestionnairesRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.persistence.database.BurstEntity;
import cz.muni.irtis.datacollector.lite.persistence.database.BurstsRepository;

public class BurstsModel extends Repository {
    public BurstsModel(Context context) {
        super(context);
    }

    static public BurstsModel build(Context context) {
        return new BurstsModel(context);
    }

    public void store(final Burst item, final RepositoryListener<Boolean> listener) {
        run(new TaskListener<Boolean>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public Boolean onTask() {
                store(item);
                return true;
            }
            @Override
            public void onDone(Boolean result) {
                listener.onDone(result);
            }
        });
    }

    public void store(Burst item) {
        getDatabase().getBurstsRepository().store(BurstEntity.build(item));
    }

    public void storeFromIdentityConfig(ConfigResponseEntity config) {
        if(config!=null && config.getBursts()!=null && !config.getBursts().isEmpty()) {
            BurstsModel bursts = BurstsModel.build(getContext());
            Iterator<Burst> iBurst = config.getBursts().iterator();
            while (iBurst.hasNext()) {
                Burst burst = iBurst.next();
                bursts.store(burst);
            }
        }
    }

    public void getItems(final RepositoryListener<List<Burst>> listener) {
        run(new TaskListener<List<Burst>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public List<Burst> onTask() {
                return getItems();
            }
            @Override
            public void onDone(List<Burst> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Burst> getItems() {
        List<Burst> bursts = new ArrayList<>();
        List<BurstEntity> entities = getDatabase().getBurstsRepository().getItems();
        if(entities!=null && !entities.isEmpty()) {
            Iterator<BurstEntity> iBurstEntities = entities.iterator();
            while (iBurstEntities.hasNext()) {
                BurstEntity entity = iBurstEntities.next();
                Burst item = Burst.build(entity);
                item.setPurse(getBurstPurse(item));
                item.setPaceCurrent(getBurstPaceCurrent(item));
                item.setPaceAverage(getBurstPaceAverage(item));
                item.setPacePrevious(getBurstPacePrevious(item));
                bursts.add(item);
            }
            return bursts;
        } else {
            return null;
        }
    }


    public void getItemsActive(final RepositoryListener<List<Burst>> listener) {
        run(new TaskListener<List<Burst>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public List<Burst> onTask() {
                return getItemsActive();
            }
            @Override
            public void onDone(List<Burst> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Burst> getItemsActive() {
        List<Burst> bursts = new ArrayList<>();
        List<BurstEntity> entities = getDatabase().getBurstsRepository().getItemsActive(Time.getTime());
        if(entities!=null && !entities.isEmpty()) {
            Iterator<BurstEntity> iBurstEntities = entities.iterator();
            while (iBurstEntities.hasNext()) {
                BurstEntity entity = iBurstEntities.next();
                Burst item = Burst.build(entity);
                Boolean isFromStorage = false;
                if(Network.isConnection()) {
                    Burst bServer = getItemFromServer(item);
                    if(bServer!=null) {
                        item = bServer;
                    } else {
                        isFromStorage = true;
                    }
                }
                if(isFromStorage) {
                    item.setPurse(getBurstPurse(item));
                    item.setPaceCurrent(getBurstPaceCurrent(item));
                    item.setPaceAverage(getBurstPaceAverage(item));
                    item.setPacePrevious(getBurstPacePrevious(item));
                }
                bursts.add(item);
            }
            return bursts;
        } else {
            return null;
        }
    }

    public void getItemsFuture(final RepositoryListener<List<Burst>> listener) {
        run(new TaskListener<List<Burst>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public List<Burst> onTask() {
                return getItemsFuture();
            }
            @Override
            public void onDone(List<Burst> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Burst> getItemsFuture() {
        List<Burst> bursts = new ArrayList<>();
        List<BurstEntity> entities = getDatabase().getBurstsRepository().getItemsFuture(Time.getTime());
        if(entities!=null && !entities.isEmpty()) {
            Iterator<BurstEntity> iBurstEntities = entities.iterator();
            while (iBurstEntities.hasNext()) {
                BurstEntity entity = iBurstEntities.next();
                Burst item = Burst.build(entity);
                Boolean isFromStorage = false;
                if(Network.isConnection()) {
                    Burst bServer = getItemFromServer(item);
                    if(bServer!=null) {
                        item = bServer;
                    } else {
                        isFromStorage = true;
                    }
                }
                if(isFromStorage) {
                    item.setPurse(getBurstPurse(item));
                    item.setPaceCurrent(getBurstPaceCurrent(item));
                    item.setPaceAverage(getBurstPaceAverage(item));
                    item.setPacePrevious(getBurstPacePrevious(item));
                }
                bursts.add(item);
            }
            return bursts;
        } else {
            return null;
        }
    }

    public void getItemsPast(final RepositoryListener<List<Burst>> listener) {
        run(new TaskListener<List<Burst>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public List<Burst> onTask() {
                return getItemsPast();
            }
            @Override
            public void onDone(List<Burst> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Burst> getItemsPast() {
        List<Burst> bursts = new ArrayList<>();
        List<BurstEntity> entities = getDatabase().getBurstsRepository().getItemsPast(Time.getTime());
        if(entities!=null && !entities.isEmpty()) {
            Iterator<BurstEntity> iBurstEntities = entities.iterator();
            while (iBurstEntities.hasNext()) {
                BurstEntity entity = iBurstEntities.next();
                Burst item = Burst.build(entity);
                Boolean isFromStorage = false;
                if(Network.isConnection()) {
                    Burst bServer = getItemFromServer(item);
                    if(bServer!=null) {
                        item = bServer;
                    } else {
                        isFromStorage = true;
                    }
                }
                if(isFromStorage) {
                    item.setPurse(getBurstPurse(item));
                    item.setPaceCurrent(getBurstPaceCurrent(item));
                    item.setPaceAverage(getBurstPaceAverage(item));
                    item.setPacePrevious(getBurstPacePrevious(item));
                }
                bursts.add(item);
            }
            return bursts;
        } else {
            return null;
        }
    }

    public void getItemsNonActive(final RepositoryListener<List<Burst>> listener) {
        run(new TaskListener<List<Burst>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public List<Burst> onTask() {
                return getItemsNonActive();
            }
            @Override
            public void onDone(List<Burst> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Burst> getItemsNonActive() {
        List<Burst> bursts = new ArrayList<>();
        List<BurstEntity> entities = getDatabase().getBurstsRepository().getItemsNonActive(Time.getTime());
        if(entities!=null && !entities.isEmpty()) {
            Iterator<BurstEntity> iBurstEntities = entities.iterator();
            while (iBurstEntities.hasNext()) {
                BurstEntity entity = iBurstEntities.next();
                Burst item = Burst.build(entity);
                item.setPurse(getBurstPurse(item));
                item.setPaceCurrent(getBurstPaceCurrent(item));
                item.setPaceAverage(getBurstPaceAverage(item));
                item.setPacePrevious(getBurstPacePrevious(item));
                bursts.add(item);
            }
            return bursts;
        } else {
            return null;
        }
    }

    public void getItem(final Burst item, final RepositoryListener<Burst> listener) {
        run(new TaskListener<Burst>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Burst onTask() {
                return getItem(item);
            }

            @Override
            public void onDone(Burst item) {
                listener.onDone(item);
            }
        });
    }

    public Burst getItem(Burst item) {
        BurstEntity entity = getDatabase().getBurstsRepository().getItem(item.getId());
        if (entity != null) {
            item = Burst.build(entity);
            item.setPurse(getBurstPurse(item));
            item.setPaceCurrent(getBurstPaceCurrent(item));
            item.setPaceAverage(getBurstPaceAverage(item));
            item.setPacePrevious(getBurstPacePrevious(item));
            return item;
        } else {
            return null;
        }
    }

    public void getItemFromServer(final Burst item, final RepositoryListener<Burst> listener) {
        run(new TaskListener<Burst>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Burst onTask() {
                return getItemFromServer(item);
            }

            @Override
            public void onDone(Burst item) {
                listener.onDone(item);
            }
        });
    }

    public Burst getItemFromServer(Burst item) {
        String content = Connection.build(getContext()).getJson(
            UrlComposer.compose("identity/bursts/detail",
                Arrays.asList(
                    new Pair<String, String>("burst", item.getId().toString())
                )));

        if (content != null && !content.isEmpty()) {
            item = new GsonBuilder()
                .registerTypeAdapter(
                    BurstCoin.class,
                    new BurstCoinDeserializer()
                )
                .create()
                .fromJson(content, new TypeToken<Burst>() {}.getType());

            if(item!=null && item.getPurse()!=null && item.getPurse().isCoins()) {
                return item;
            } else {
                return getItem(item);
            }
        } else {
            return null;
        }
    }



    public void getItemClosest(final RepositoryListener<Burst> listener) {
        run(new TaskListener<Burst>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Burst onTask() {
                BurstEntity entity = getDatabase().getBurstsRepository().getItemClosest(Time.getTime());
                if(entity!=null) {
                    Burst item = Burst.build(entity);
                    item.setPurse(getBurstPurse(item));
                    item.setPaceCurrent(getBurstPaceCurrent(item));
                    item.setPaceAverage(getBurstPaceAverage(item));
                    item.setPacePrevious(getBurstPacePrevious(item));
                    return item;
                } else {
                    return null;
                }
            }
            @Override
            public void onDone(Burst item) {
                listener.onDone(item);
            }
        });
    }

    public void getBurstPurse(final Burst item, final RepositoryListener<BurstPurse> listener) {
        run(new TaskListener<BurstPurse>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public BurstPurse onTask() {
                return getBurstPurse(item);
            }
            @Override
            public void onDone(BurstPurse item) {
                listener.onDone(item);
            }
        });
    }

    public void getBurstPurse(final Burst item, final Long starting, final Long ending, final RepositoryListener<BurstPurse> listener) {
        run(new TaskListener<BurstPurse>() {
            @Override
            public void onStart() {
                listener.onStart();
            }
            @Override
            public BurstPurse onTask() {
                return getBurstPurse(item, starting, ending);
            }
            @Override
            public void onDone(BurstPurse item) {
                listener.onDone(item);
            }
        });
    }

    public BurstPurse getBurstPurse(Burst item, Long starting, Long ending) {
        return getBurstPurseFromStorage(item, starting, ending);
    }

    public BurstPurse getBurstPurseFromStorage(Burst item, Long starting, Long ending) {
        BurstPurse purse = BurstPurse.build();
        if(item!=null) {
            for (Long i = starting; i < ending; i += Time.DAY) {
                Long start = i;
                Long end = i + Time.DAY;
                SimpleSQLiteQuery query = new SimpleSQLiteQuery(
                        "SELECT " + QuestionnairesRepository.TABLE_NAME + ".* " +
                                "FROM " + QuestionnairesRepository.TABLE_NAME + " " +
                                "WHERE " + QuestionnairesRepository.TABLE_NAME + ".type IN (" + QuestionnairesModel.TYPE_REGULAR + ") " +
                                "AND " + QuestionnairesRepository.TABLE_NAME + ".starting >= " + start.toString() + " " +
                                "AND " + QuestionnairesRepository.TABLE_NAME + ".ending <= " + end.toString() + " " +
                                "ORDER BY " + QuestionnairesRepository.TABLE_NAME + ".starting ASC");

                List<QuestionnaireEntity> entities = QuestionnairesDatabase.getDatabase(getContext()).getQuestionnairesRepository()
                        .getItemsByQuery(query);
                if (entities != null && !entities.isEmpty()) {
                    int completed = 0;
                    int notified = 0;
                    int total = entities.size();
                    Iterator<QuestionnaireEntity> iEntities = entities.iterator();
                    while (iEntities.hasNext()) {
                        QuestionnaireEntity entity = iEntities.next();
                        if (entity.isCompleted()) {
                            boolean isCompletedWithAnswers = false;
                            Integer countAnswers = QuestionnairesDatabase.getDatabase(getContext()).getQuestionsRepository()
                                    .getItemsCountAnswers(entity.getId());
                            Integer countAnswered = QuestionnairesDatabase.getDatabase(getContext()).getQuestionsRepository()
                                    .getItemsCountAnswered(entity.getId());

                            if (countAnswers == 1) {
                                isCompletedWithAnswers = countAnswered == 1;
                            } else if (countAnswers == 2) {
                                isCompletedWithAnswers = countAnswered >= 1;
                            } else {
                                float percent = (Float.valueOf(countAnswered) / Float.valueOf(countAnswers)) * 100;
                                isCompletedWithAnswers = percent >= 30.0f;
                            }

                            if (isCompletedWithAnswers) {
                                completed++;
                                purse.put(BurstCoinBronze.build());
                            }
                        }

                        if (entity.isNotified()) {
                            notified++;
                        }
                    }

                    if (completed != 0) {
                        if (completed >= (total - (total / 4))) { // 75% completed questionnaires
                            purse.put(BurstCoinSilver.build());
                        }
                        if (completed == total) { // 100% completed questionnaires
                            purse.put(BurstCoinGold.build());
                        }
                    }
                }
            }
        }

        // Get special
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
                "SELECT " + BurstsRepository.TABLE_NAME + ".* " +
                        "FROM " + BurstsRepository.TABLE_NAME + " " +
                        "WHERE " + BurstsRepository.TABLE_NAME + ".starting > " + ending.toString() + " " +
                        "ORDER BY " + BurstsRepository.TABLE_NAME + ".starting ASC");

        List<BurstEntity> entitiesBursts = StorageDatabase.getDatabase(getContext()).getBurstsRepository()
                .getItemsByQuery(query);

        if(entitiesBursts!=null & !entitiesBursts.isEmpty()) {
            ending = entitiesBursts.get(0).getStarting();
        } else {
            ending += Time.WEEK;
        }

        query = new SimpleSQLiteQuery(
                "SELECT " + QuestionnairesRepository.TABLE_NAME + ".* " +
                        "FROM " + QuestionnairesRepository.TABLE_NAME + " " +
                        "WHERE " + QuestionnairesRepository.TABLE_NAME + ".type IN (" + QuestionnairesModel.TYPE_SPECIAL + ") " +
                        "AND " + QuestionnairesRepository.TABLE_NAME + ".starting >= " + starting.toString() + " " +
                        "AND " + QuestionnairesRepository.TABLE_NAME + ".ending <= " + ending.toString() + " " +
                        "ORDER BY " + QuestionnairesRepository.TABLE_NAME + ".starting ASC");

        List<QuestionnaireEntity> entities = QuestionnairesDatabase.getDatabase(getContext()).getQuestionnairesRepository()
                .getItemsByQuery(query);
        if (entities != null && !entities.isEmpty()) {
            Iterator<QuestionnaireEntity> iEntities = entities.iterator();
            while (iEntities.hasNext()) {
                QuestionnaireEntity entity = iEntities.next();
                if (entity.isCompleted()) {
                    boolean isCompletedWithAnswers = false;
                    Integer countAnswers = QuestionnairesDatabase.getDatabase(getContext()).getQuestionsRepository()
                            .getItemsCountAnswers(entity.getId());
                    Integer countAnswered = QuestionnairesDatabase.getDatabase(getContext()).getQuestionsRepository()
                            .getItemsCountAnswered(entity.getId());

                    if (countAnswers == 1) {
                        isCompletedWithAnswers = countAnswered == 1;
                    } else if (countAnswers == 2) {
                        isCompletedWithAnswers = countAnswered >= 1;
                    } else {
                        float percent = (Float.valueOf(countAnswered) / Float.valueOf(countAnswers)) * 100;
                        isCompletedWithAnswers = percent >= 30.0f;
                    }

                    if (isCompletedWithAnswers) {
                        purse.put(BurstCoinGold.build());
                    }
                }
            }
        }

        return purse;
    }

//    public BurstPurse getBurstPurseFromServer(Context context, Burst item, Long starting, Long ending) {
//        item = getBurst(context,)
//    }

    public Integer getBurstPaceCurrent(Burst item) {
        Long time = 0L;
        if(item.isActive()) {
            time = Time.getTime();
        } else if(item.getEnding() < Time.getTime()) {
            time = item.getEnding();
        } else if(item.getStarting() > Time.getTime()) {
            time = item.getStarting();
        }

        Long starting = new DateTime(time).withTimeAtStartOfDay().getMillis()-Time.DAY*2;
        Long ending = new DateTime(time).withTimeAtStartOfDay().getMillis()+Time.DAY;

        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
            "SELECT " + QuestionnairesRepository.TABLE_NAME + ".* " +
            "FROM " + QuestionnairesRepository.TABLE_NAME + " " +
            "WHERE " + QuestionnairesRepository.TABLE_NAME + ".type IN (" + QuestionnairesModel.TYPE_REGULAR + ") " +
            "AND " + QuestionnairesRepository.TABLE_NAME + ".starting >= " + starting.toString() + " " +
            "AND " + QuestionnairesRepository.TABLE_NAME + ".ending <= " + ending.toString() + " " +
            "ORDER BY " + QuestionnairesRepository.TABLE_NAME + ".starting ASC");

        List<QuestionnaireEntity> entities = QuestionnairesDatabase.getDatabase(getContext()).getQuestionnairesRepository()
                .getItemsByQuery(query);
        if (entities != null && !entities.isEmpty()) {
            int completed = 0;
            int count = entities.size();
            Iterator<QuestionnaireEntity> iEntities = entities.iterator();
            while (iEntities.hasNext()) {
                QuestionnaireEntity entity = iEntities.next();
                if(entity.isCompleted()) {
                    boolean isCompletedWithAnswers;
                    Integer countAnswers = QuestionnairesDatabase.getDatabase(getContext()).getQuestionsRepository()
                            .getItemsCountAnswers(entity.getId());
                    Integer countAnswered = QuestionnairesDatabase.getDatabase(getContext()).getQuestionsRepository()
                            .getItemsCountAnswered(entity.getId());

                    if(countAnswers == 1) {
                        isCompletedWithAnswers = countAnswered == 1;
                    } else if(countAnswers == 2) {
                        isCompletedWithAnswers = countAnswered >= 1;
                    } else {
                        isCompletedWithAnswers = countAnswered >= countAnswers / 3;
                    }

                    if(isCompletedWithAnswers) {
                        completed++;
                    }
                }
            }

            float percent = (Float.valueOf(completed) / Float.valueOf(count)) * 100;
            return (int) percent;
        } else {
            return 0;
        }
    }

    public Integer getBurstPaceAverage(Burst item) {
        Long starting = new DateTime(item.getStarting()).withTimeAtStartOfDay().getMillis()-Time.DAY*2;
        Long ending = new DateTime(item.getEnding()).withTimeAtStartOfDay().getMillis()+Time.DAY;

        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
            "SELECT " + QuestionnairesRepository.TABLE_NAME + ".* " +
            "FROM " + QuestionnairesRepository.TABLE_NAME + " " +
            "WHERE " + QuestionnairesRepository.TABLE_NAME + ".type IN (" + QuestionnairesModel.TYPE_REGULAR + ") " +
            "AND " + QuestionnairesRepository.TABLE_NAME + ".starting >= " + starting.toString() + " " +
            "AND " + QuestionnairesRepository.TABLE_NAME + ".ending <= " + ending.toString() + " " +
            "ORDER BY " + QuestionnairesRepository.TABLE_NAME + ".starting ASC");

        List<QuestionnaireEntity> entities = QuestionnairesDatabase.getDatabase(getContext()).getQuestionnairesRepository()
                .getItemsByQuery(query);
        if (entities != null && !entities.isEmpty()) {
            int completed = 0;
            int count = entities.size();
            Iterator<QuestionnaireEntity> iEntities = entities.iterator();
            while (iEntities.hasNext()) {
                QuestionnaireEntity entity = iEntities.next();
                if(entity.isCompleted()) {
                    boolean isCompletedWithAnswers = false;
                    Integer countAnswers = QuestionnairesDatabase.getDatabase(getContext()).getQuestionsRepository()
                            .getItemsCountAnswers(entity.getId());
                    Integer countAnswered = QuestionnairesDatabase.getDatabase(getContext()).getQuestionsRepository()
                            .getItemsCountAnswered(entity.getId());

                    if(countAnswers == 1) {
                        isCompletedWithAnswers = countAnswered == 1;
                    } else if(countAnswers == 2) {
                        isCompletedWithAnswers = countAnswered >= 1;
                    } else {
                        isCompletedWithAnswers = countAnswered >= countAnswers / 3;
                    }

                    if(isCompletedWithAnswers) {
                        completed++;
                    }
                }
            }

            float percent = (Float.valueOf(completed) / Float.valueOf(count)) * 100;
            return (int) percent;
        } else {
            return 0;
        }
    }

    public Integer getBurstPacePrevious(Burst item) {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
            "SELECT " + BurstsRepository.TABLE_NAME + ".* " +
            "FROM " + BurstsRepository.TABLE_NAME + " " +
            "WHERE " + BurstsRepository.TABLE_NAME + ".starting < " + item.getStarting().toString() + " " +
            "ORDER BY " + BurstsRepository.TABLE_NAME + ".starting DESC");

        List<BurstEntity> entities = StorageDatabase.getDatabase(getContext()).getBurstsRepository()
                .getItemsByQuery(query);
        if (entities != null && !entities.isEmpty()) {
            BurstEntity entity = entities.get(0);
            return getBurstPaceAverage(Burst.build(entity));
        } else {
            return null;
        }
    }

    private BurstPurse getBurstPurse(Burst item) {
        Long starting = Time.getTime(Time.getYear(item.getStarting()), Time.getMonth(item.getStarting()), Time.getDay(item.getStarting()), 0, 0, 0);
//        Long ending = starting + Time.DAY - Time.SECOND;
        Long ending = Time.getTime(Time.getYear(item.getEnding()), Time.getMonth(item.getEnding()), Time.getDay(item.getEnding()), 29, 59, 59);
        return getBurstPurse(item, starting, ending);
    }
}
