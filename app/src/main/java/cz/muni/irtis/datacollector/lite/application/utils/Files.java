package cz.muni.irtis.datacollector.lite.application.utils;

import android.os.Build;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Files {

    static public File copy(File source, File target) throws IOException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            java.nio.file.Files.copy(source.toPath(), target.toPath());
            return target;
        } else {
            try (
                InputStream in = new BufferedInputStream(
                    new FileInputStream(source));
                OutputStream out = new BufferedOutputStream(
                    new FileOutputStream(target))) {

                byte[] buffer = new byte[1024];
                int lengthRead;
                while ((lengthRead = in.read(buffer)) > 0) {
                    out.write(buffer, 0, lengthRead);
                    out.flush();
                }
            }
            return target;
        }
    }
}
