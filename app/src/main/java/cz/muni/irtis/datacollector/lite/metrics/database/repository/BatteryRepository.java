package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.BatteryEntity;

@Dao
public interface BatteryRepository extends IRepository<BatteryEntity> {
    public static final String TABLE_NAME = "battery";

    /**
     * Add new entity to database.
     * @param item entity
     */
    @Insert
    long insert(BatteryEntity item);

    /**
     * Get entity by it's PK datetime
      * @param time java style timestamp (1561544094*1000)
     * @return entity
     */
    @Query("SELECT * FROM " + TABLE_NAME + " WHERE datetime = :time")
    BatteryEntity getByTime(Long time);

    /**
     * {@link IRepository#getPrevious(Long)}
     */
    @Query("SELECT * FROM " + TABLE_NAME + " " +
            "WHERE datetime < :time")
    List<BatteryEntity> getPrevious(Long time);

    /**
     * {@link IRepository#getPrevious(Long, int, int)}
     */
    @Query("SELECT * FROM " + TABLE_NAME + " " +
            "WHERE datetime < :time" + " " +
            "LIMIT :offset, :count")
    List<BatteryEntity> getPrevious(Long time, int offset, int count);

    /**
     * {@link IRepository#deleteById(Long)}
     */
    @Query("DELETE FROM " + TABLE_NAME + " WHERE datetime = :id")
    int deleteById(Long id);


//    @Query("DELETE FROM " + TABLE_NAME + " " +
//            "WHERE " + TABLE_NAME + ".datetime IN (" +
//                "SELECT " + TABLE_NAME + ".datetime FROM " + TABLE_NAME + " " +
//                "WHERE datetime < :time" + " " +
//                "LIMIT :offset, :count)")
//    void deletePrevious(Long time, int offset, int count);
}
