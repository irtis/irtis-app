package cz.muni.irtis.datacollector.lite.persistence.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import cz.muni.irtis.datacollector.lite.application.database.EntityBase;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.Burst;

@Entity(
    tableName = BurstsRepository.TABLE_NAME
)
public class BurstEntity extends EntityBase {
    @PrimaryKey
    @ColumnInfo(name = "id")
    private Long id;
    @ColumnInfo(name = "identifier")
    private String identifier;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "starting")
    private Long starting;
    @ColumnInfo(name = "ending")
    private Long ending;


    public BurstEntity(Long id, String identifier, Long starting, Long ending) {
        this.id = id;
        this.identifier = identifier;
        this.starting = starting;
        this.ending = ending;
    }

    public BurstEntity(Burst item) {
        this.id = item.getId();
        this.identifier = item.getIdentifier();
        this.title = item.getTitle();
        this.starting = item.getStarting();
        this.ending = item.getEnding();
    }

    public static BurstEntity build(Burst item) {
        return new BurstEntity(item);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getStarting() {
        return starting;
    }

    public void setStarting(Long starting) {
        this.starting = starting;
    }

    public Long getEnding() {
        return ending;
    }

    public void setEnding(Long ending) {
        this.ending = ending;
    }
}
