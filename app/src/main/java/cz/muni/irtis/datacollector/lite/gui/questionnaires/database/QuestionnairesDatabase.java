package cz.muni.irtis.datacollector.lite.gui.questionnaires.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.io.File;

import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionnaireNotificationEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.AnswersRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.QuestionnairesNotificationsRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.QuestionnairesRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.QuestionsRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.AnswerEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionnaireEntity;

/**
 * Room ORM DB access class
 *
 * Documentation:
 *  - https://developer.android.com/training/data-storage/room
 *  - https://developer.android.com/reference/android/arch/persistence/room/package-summary
 *
 */
@Database(
    version = 1,
    entities = {
        QuestionnaireEntity.class,
        QuestionEntity.class,
        AnswerEntity.class,
        QuestionnaireNotificationEntity.class
    },
    exportSchema = false
)
public abstract class QuestionnairesDatabase extends RoomDatabase {
    private static volatile QuestionnairesDatabase INSTANCE;

    public abstract QuestionnairesRepository getQuestionnairesRepository();
    public abstract QuestionsRepository getQuestionsRepository();
    public abstract AnswersRepository getAnswersRepository();
    public abstract QuestionnairesNotificationsRepository getQuestionnairesNotificationsRepository();


    /**
     * Get database instance. If none exists, append new & return it.
     * @param context app context
     * @return database instance
     */
    public static QuestionnairesDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (QuestionnairesDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        QuestionnairesDatabase.class, "questionnaires_database.db")
                        .allowMainThreadQueries()
//                        .addMigrations(MIGRATION_2_3)
                        .build();
                }
            }
        }
        return INSTANCE;
    }

    public long getSize() {
        return getFile().length();
    }

    public File getFile() {
        return new File(getOpenHelper().getReadableDatabase().getPath());
    }

//    /**
//     * @date 31.03.2020
//     * @changes just one column
//     */
//    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE "+QuestionnairesRepository.TABLE_NAME+" ADD COLUMN questionnaire_id INTEGER");
//        }
//    };

//    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE "+QuestionnairesRepository.TABLE_NAME+" ADD COLUMN invoking INTEGER");
//        }
//    };
}
