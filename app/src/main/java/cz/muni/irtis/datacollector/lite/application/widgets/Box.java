package cz.muni.irtis.datacollector.lite.application.widgets;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;

import androidx.appcompat.app.AlertDialog;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;

public class Box {
	static private void show(AlertDialog dialog, Context context) {
		try {
			if(!((Activity) context).isFinishing()) {
				dialog.show();
			}
		} catch(Exception e) {
			Debug.getInstance().exception(e, context);
		}
	}

	/**
	 * ok()
	 *
	 * Dialog with OK button
	 *
	 * @param context
	 * @param title
	 * @param content
	 * @param listener
	 */
	static public void ok(Context context, String title, String content, DialogInterface.OnClickListener listener)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setPositiveButton("Ok", listener);

		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			builder.setInverseBackgroundForced(true);
		}

		builder.setTitle(title);
		if(content!=null) {
			builder.setMessage(content);
		}


		Box.show(builder.create(), context);
	}

	static public void ok(Context context, String title, String content)
	{
		Box.ok(context, title, content, null);
	}


	static public void yesno(Context context, String title, String content, DialogInterface.OnClickListener listener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setPositiveButton(context.getResources().getString(R.string.yes), listener);
		builder.setNegativeButton(context.getResources().getString(R.string.no), null);

		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			builder.setInverseBackgroundForced(true);
		}

		builder.setTitle(title);
		if(content!=null) {
			builder.setMessage(content);
		}

		Box.show(builder.create(), context);
	}
}
