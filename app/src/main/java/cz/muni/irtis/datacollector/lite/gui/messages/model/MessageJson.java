package cz.muni.irtis.datacollector.lite.gui.messages.model;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.AnswerJson;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.NotificationJson;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.QuestionJson;

public class MessageJson {
    private Long id;
    private String text;
    private String response;
    private Long created;
    private Long updated;
    private Long downloaded;
    private Long notified;
    private Long opened;
    private Long read;
    private Long uploaded;

    public MessageJson(Message item) {
        this.id = item.getId();
        this.text = item.getText();
        this.response = item.getResponse();
        this.created = item.getCreated();
        this.updated = item.getUploaded();
        this.downloaded = item.getDownloaded();
        this.notified = item.getNotified();
        this.opened = item.getOpened();
        this.read = item.getRead();
        this.uploaded = item.getUploaded();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public Long getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Long downloaded) {
        this.downloaded = downloaded;
    }

    public Long getNotified() {
        return notified;
    }

    public void setNotified(Long notified) {
        this.notified = notified;
    }

    public Long getOpened() {
        return opened;
    }

    public void setOpened(Long opened) {
        this.opened = opened;
    }

    public Long getRead() {
        return read;
    }

    public void setRead(Long read) {
        this.read = read;
    }

    public Long getUploaded() {
        return uploaded;
    }

    public void setUploaded(Long uploaded) {
        this.uploaded = uploaded;
    }

    public String toString() {
        return new GsonBuilder().create().toJson(this, new TypeToken<MessageJson>(){}.getType());
    }
}
