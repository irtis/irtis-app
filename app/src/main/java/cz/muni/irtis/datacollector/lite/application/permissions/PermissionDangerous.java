package cz.muni.irtis.datacollector.lite.application.permissions;

import android.content.Context;

public class PermissionDangerous extends Permission {

    public PermissionDangerous(Context context, String id, String title, String description)
    {
        super(context, id, title, description);
    }

    public boolean isGranted()
    {
        return provider.isGranted(this.id);
    }

}
