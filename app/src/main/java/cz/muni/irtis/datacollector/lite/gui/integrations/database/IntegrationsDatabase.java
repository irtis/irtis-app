package cz.muni.irtis.datacollector.lite.gui.integrations.database;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.io.File;

/**
 * Room ORM DB access class
 *
 * Documentation:
 *  - https://developer.android.com/training/data-storage/room
 *  - https://developer.android.com/reference/android/arch/persistence/room/package-summary
 *
 */
//@Database(
//version = 1,
//entities = {
//
//}
//)
public abstract class IntegrationsDatabase extends RoomDatabase {
    private static volatile IntegrationsDatabase INSTANCE;


    /**
     * Get database instance. If none exists, append new & return it.
     * @param context app context
     * @return database instance
     */
    public static IntegrationsDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (IntegrationsDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    IntegrationsDatabase.class, "integrations_database.db")
                    .allowMainThreadQueries()
                    .build();
                }
            }
        }
        return INSTANCE;
    }

    public long getSize() {
        return new File(getOpenHelper().getReadableDatabase().getPath()).length();
    }
}
