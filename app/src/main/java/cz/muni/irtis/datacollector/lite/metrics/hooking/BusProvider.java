package cz.muni.irtis.datacollector.lite.metrics.hooking;

import com.squareup.otto.Bus;

public class BusProvider {
    private static Bus instance;

    static public Bus build() {
        if(instance ==null) {
            instance = new Bus();
        }
        return instance;
    }
}
