package cz.muni.irtis.datacollector.lite.gui.questionnaires.types;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;

public class TextFragment extends TypeFragmentBase {
    protected EditText vText;

    public TextFragment(Context context, Question question, boolean editable) {
        super(context, question, R.layout.activity_questionnaires_form_type_text, editable);
    }

    public TextFragment(Context context, Question question, int contentLayoutId, boolean editable) {
        super(context, question, contentLayoutId, editable);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        vText = view.findViewById(R.id.text);
        vText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence content, int start, int before, int count) {
                Answer answer = getQuestion().getAnswer();
                answer.setValue(content!=null && content.length()>0 ? content.toString() : null);
                refresh();
            }
            @Override
            public void afterTextChanged(Editable s) { }
        });
        vText.setEnabled(isEditable());
        if(getQuestion().getAnswer().isValue()) {
            vText.setText(getQuestion().getAnswer().getValue());
        }

        return view;
    }
}
