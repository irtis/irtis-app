package cz.muni.irtis.datacollector.lite.gui.integrations;

import android.os.Bundle;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.facebook.MessengerIntegration;

public class MessengerConnectorActivity extends ConnectorActivityBase {
    public MessengerConnectorActivity() {
        super(R.layout.activity_integrations_connector_messenger);
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setIntegration(new MessengerIntegration(this, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onError(int code) {
                refresh();
            }

            @Override
            public void onFinished(boolean isLogged) {
                refresh();
            }
        }, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onError(int code) {
            }

            @Override
            public void onFinished(boolean isLogged) {
                refresh();
            }
        }));
    }
}
