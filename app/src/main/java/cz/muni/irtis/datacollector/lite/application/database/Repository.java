package cz.muni.irtis.datacollector.lite.application.database;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static android.os.AsyncTask.THREAD_POOL_EXECUTOR;

abstract public class Repository {
    private Context context;
    private StorageDatabase database;
    private boolean isAsynchronous;
    private List<RepositoryTask> running;

    public interface TaskListener<T> {
        void onStart();
        T onTask();
        void onDone(T result);
    }

    public interface RepositoryListener<T> {
        void onStart();
        void onDone(T data);
    }


    public Repository(Context context) {
        this.context = context;
        this.isAsynchronous = true;
        this.running = new ArrayList<>();
        this.database = StorageDatabase.getDatabase(getContext());
    }

    public Context getContext() {
        return context;
    }

    public StorageDatabase getDatabase() {
        return database;
    }

    public boolean isAsynchronous() {
        return isAsynchronous;
    }

    public void setAsynchronous(boolean asynchronous) {
        isAsynchronous = asynchronous;
    }

    public void cancel() {
        if(running!=null && !running.isEmpty()) {
            Iterator<RepositoryTask> iRunning = running.iterator();
            while (iRunning.hasNext()) {
                RepositoryTask task = iRunning.next();
                task.cancel(true);
            }
            running.clear();
        }
    }


    public Repository run(TaskListener listener) {
        RepositoryTask task = new RepositoryTask(listener);
        if(isAsynchronous) {
            task.executeOnExecutor(THREAD_POOL_EXECUTOR);
        } else {
            task.onPreExecute();
            Object result = task.doInBackground(new Void[]{});
            task.onPostExecute(result);
        }
        return this;
    }

    public class RepositoryTask extends AsyncTask<Void, Void, Object> {
        protected TaskListener listener;

        public RepositoryTask(TaskListener listener) {
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            running.add(this);
            listener.onStart();
        }
        @Override
        protected Object doInBackground(Void... nul) {
            return listener.onTask();
        }
        @Override
        protected void onPostExecute(Object result) {
            listener.onDone(result);
            running.remove(this);
        }
    }


    public Repository runDebug(TaskListener listener) {
        RepositoryTaskDebug task = new RepositoryTaskDebug(listener);
        if(isAsynchronous) {
            task.executeOnExecutor(THREAD_POOL_EXECUTOR);
        } else {
            task.onPreExecute();
            Object result = task.doInBackground(new Void[]{});
            task.onPostExecute(result);
        }
        return this;
    }

    public class RepositoryTaskDebug extends RepositoryTask {
        public RepositoryTaskDebug(TaskListener listener) {
            super(listener);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            running.add(this);
            listener.onStart();
        }
        @Override
        protected Object doInBackground(Void... nul) {
            return listener.onTask();
        }
        @Override
        protected void onPostExecute(Object result) {
            listener.onDone(result);
            running.remove(this);
        }
    }
}
