package cz.muni.irtis.datacollector.lite.application.network;

import android.content.Context;
import android.os.AsyncTask;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

//import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.internal.Util;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;
import okio.Source;

/**
 * Connection
 *
 * Simple framework for communicating with network services
 *
 *  SSL checkers: https://geekflare.com/ssl-test-certificate/#2-SSL-Checker
 */
public class Connection {
    static private final String TAG = "Connection";

    private Context context;
    private NetworkTask task;

    public interface OnConnectionListener<T> {
        public void onStart();
        public void onError(Exception e);
        public void onDone(Response response, T content);
    }

    public interface OnConnectionFileListener extends OnConnectionListener<File> {
        public File onCreateFile(String name);
    }



    Connection(Context context) {
        this.context = context;
    }

    static public Connection build(Context context) {
        return new Connection(context);
    }
    static public Connection build() {
        return new Connection(null);
    }

    /**
     * isAvailable()
     *
     * Best effort isActive made to try to reach the host, but firewalls and server configuration
     * may block requests resulting in a unreachable status while some specific ports may be accessible.
     * A typical implementation will use ICMP ECHO REQUESTs if the privilege can be obtained,
     * otherwise it will try to establish a TCP connection on port 7 (Echo) of the destination host.
     *
     * @return
     */
    public static boolean isAvailable() {
        String url = UrlComposer.BASE_URL;
        if(Network.isConnection()) {
            try {
                URL hUrl = new URL(url);
                SocketAddress address = new InetSocketAddress(hUrl.getHost(), hUrl.getPort());
                Socket socket = new Socket();
                boolean result = true;
                try {
                    socket.connect(address, 5000);
                } catch (SocketTimeoutException e) {
                    Debug.getInstance().exception(e);
                    result = false;
                } catch (IOException e) {
                    Debug.getInstance().exception(e);
                    result = false;
                } finally {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        Debug.getInstance().exception(e);
                    }
                    return result;
                }
            } catch (MalformedURLException e) {
                Debug.getInstance().exception(e);
                return false;
            }
        } else {
            return false;
        }
    }

    public static void isAvailable(final IsAvailableListener listener) {
        new IsAvailableTask(listener).execute();
    }


    /**
     * get()
     *
     * Get content from the server (Asynchronous)
     *
     * @param url
     * @param listener
     */
    public void getJson(String url, OnConnectionListener listener, boolean async) {
        Request request = new Request.Builder()
                .url(url)
                .headers(IdentificationBuilder.buildHeaders(context))
                .build();

        if(task==null) {
            Debug.getInstance().log(TAG, "GET: "+url);
            task = new NetworkTask(url, request, listener).start(async);
        }
    }

    public void getJson(String url, OnConnectionListener listener) {
        getJson(url, listener, true);
    }

    /**
     * get()
     *
     * Get content from the server
     *
     * Use this method with caution and not on UI thread!
     *
     * @param url
     * @return
     */
    public String getJson(String url) {
        final StringBuilder builder = new StringBuilder();
        getJson(url, new OnConnectionListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onError(Exception e) { }

            @Override
            public void onDone(Response response, Object content) {
                if(response!=null) {
                    builder.append((String) content);
                }
            }
        }, false);

        String result = builder.toString();
        return result.isEmpty() ? null : result;
    }
    

    /**
     * postJson()
     *
     * Put stream content to the server
     *
     * @param url endpoint to upload request to
     * @param content body content - valid json
     * @param listener listens for response
     * @param async start async
     */
    public void postJson(String url, String content, OnConnectionListener listener, boolean async) {
        RequestBody body = RequestBody.create(MediaType.get("application/json; charset=utf-8"), content);
        Request request = new Request.Builder()
            .url(url)
            .headers(IdentificationBuilder.buildHeaders(context))
            .post(body)
            .build();

        if(task==null) {
            Debug.getInstance().log(TAG, "PUT: "+url);
            task = new NetworkTask(url, request, listener).start(async);
        }
    }
    public void postJson(String url, String content, OnConnectionListener listener) {
        postJson(url, content, listener, true);
    }

    public ConnectionResult postJson(String url, String content) {
        final List<ConnectionResult> resulter = new ArrayList<>();
        postJson(url, content, new OnConnectionListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onError(Exception e) { }

            @Override
            public void onDone(Response response, Object content) {
                resulter.add(new ConnectionResult(response, content));
            }
        }, false);

        return !resulter.isEmpty() ? resulter.get(0) : null;
    }

//    public Response postJson(String url, String content) {
//        RequestBody body = RequestBody.create(MediaType.get("application/json; charset=utf-8"), content);
//        Request request = new Request.Builder()
//                .url(url)
//                .headers(IdentificationBuilder.buildHeaders(context))
//                .post(body)
//                .build();
//
//        if(task==null) {
//            Debug.getInstance().log(TAG, "PUT: "+url);
//            task = new NetworkTask(url, request).start(t);
//        }
//    }

    public void getFile(String url, OnConnectionListener<File> listener, boolean async) {
        Request request = new Request.Builder()
            .url(url)
            .headers(IdentificationBuilder.buildHeaders(context))
            .build();

        if(task==null) {
            Debug.getInstance().log(TAG, "GET: "+url);
            task = new NetworkTask(url, request, listener).start(async);
        }
    }

    public void getFile(String url, OnConnectionListener<File> listener) {
        getFile(url, listener, false);
    }


    /**
     * postFile()
     *
     * Put file content to the server
     *
     * @param url
     * @param file
     * @param listener
     */
    public void postFile(String url, File file, OnConnectionListener listener, boolean async) {
        String type = getMimeType(file);
        RequestBody body = null;
        if(type.contains("json")) {
            InputStream inputStream = null;
            try {
                inputStream = new FileInputStream(file);
            } catch (FileNotFoundException e) { }

            body = create(MediaType.get(type), inputStream);
        } else {
            body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("file", file.getName(),
                        RequestBody.create(MediaType.get(type), file))
                .build();
        }

        Request request = new Request.Builder()
            .url(url)
            .headers(IdentificationBuilder.buildHeaders(context))
            .post(body)
            .build();


        if (task == null) {
            Debug.getInstance().log(TAG, "PUT: " + url);
            task = new NetworkTask(url, request, listener).start(async);
        }
    }

    public void postFile(String url, File file, OnConnectionListener listener) {
        postFile(url, file, listener, true);
    }

    public boolean postFile(String url, File file) {
        final StringBuilder builder = new StringBuilder();
        postFile(url, file, new OnConnectionListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onError(Exception e) { }

            @Override
            public void onDone(Response response, Object content) {
                if(response!=null) {
                    builder.append("true");
                }
            }
        }, false);

        return !builder.toString().isEmpty();
    }


    private static RequestBody create(final @Nullable MediaType contentType, final InputStream inputStream) {
        if (inputStream == null) throw new NullPointerException("inputStream == null");

        return new RequestBody() {
            @Override public @Nullable MediaType contentType() {
                return contentType;
            }

            @Override public long contentLength() {
                try {
                    return inputStream.available() == 0 ? -1 : inputStream.available();
                } catch (IOException e) {
                    return -1;
                }
            }

            @Override public void writeTo(BufferedSink sink) throws IOException {
                Source source = null;
                try {
                    source = Okio.source(inputStream);
                    sink.writeAll(source);
                } finally {
                    Util.closeQuietly(source);
                }
            }
        };
    }

    /**
     * NetworkTask
     *
     * Nested AsyncTask responsible for getting data from/to server
     */
    private class NetworkTask extends AsyncTask<Void, Void, Void> {
        String url;
        Request request;
        Response response;
        Object content;
        OnConnectionListener listener;

        public NetworkTask(String url, Request request, OnConnectionListener listener) {
            this.url = url;
            this.request = request;
            this.listener = listener;
        }

        public NetworkTask start(boolean async) {
            Debug.getInstance().log(TAG, "onStart()");
            this.listener.onStart();
            if(async) {
                this.execute(); // Asynchronous execution
            } else {
                doInBackground((Void) null);
                onPostExecute((Void) null);
            }
            return this;
        }
        public NetworkTask start() {
            return this.start(true);
        }

        public void stop() {
            this.cancel(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                if(!this.isCancelled()) {
                    OkHttpClient.Builder builder = new OkHttpClient.Builder();
//                    if (Debug.isDebug()) {
//                        builder.addInterceptor(new OkHttpProfilerInterceptor());
//                    }
                    if(url.contains("10.0.2.2")) {
                        setTrustManager(builder);
                        builder
                            .hostnameVerifier(getHostnameVerifier());
                    }
                    OkHttpClient client = builder
                        .connectTimeout(5, TimeUnit.MINUTES)
                        .writeTimeout(5, TimeUnit.MINUTES)
                        .readTimeout(1, TimeUnit.MINUTES)
                        .build();
                    response = client.newCall(request).execute();

                    if(response!=null && response.isSuccessful()) {
                        if(response.body().contentType()!=null) {
                            String type = response.body().contentType().toString();
                            if(type.contains("application/vnd.android.package-archive")) {
                                BufferedSource source = response.body().source();
                                if(listener instanceof OnConnectionFileListener) {
                                    String name = response.header("Content-disposition")
                                            .replaceFirst("(?i)^.*filename=\"?([^\"]+)\"?.*$", "$1");
                                    File file = ((OnConnectionFileListener) listener).onCreateFile(name);
                                    if(file!=null) {
                                        BufferedSink sink = Okio.buffer(Okio.sink(file));
                                        sink.writeAll(source);
                                        sink.close();
                                        content = file;
                                    }
                                } else {
                                    content = source;
                                }

                            } else if(type.contains("application/json")) {
                                content = response.body().string();
                            }
                        } else {
                            if(listener instanceof OnConnectionFileListener) {
                                content = null;
                            } else {
                                content = response.body().string();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Debug.getInstance().exception(e, this);
                listener.onError(e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void params) {
            Debug.getInstance().log(TAG, "onDone()");
            if(response!=null) {
                try {
                    listener.onDone(response, content);
                } catch (Exception e) {
                    Debug.getInstance().exception(e, this);
                    listener.onError(e);
                }
            } else {
                listener.onDone(response, content);
            }
        }
    }




    private String getMimeType(@NonNull File file) {
        String type = null;
        final String url = file.toString();
        final String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
        }
        if (type == null) {
            type = "image/*"; // fallback type. You might setItem it to */*
        }
        return type;
    }


    public void setTrustManager(OkHttpClient.Builder builder) {
        try {
            KeyStore keystore = getKeyStore(context);

            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keystore);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);

            X509TrustManager trustManager = (X509TrustManager) tmf.getTrustManagers()[0];

            builder.sslSocketFactory(sslContext.getSocketFactory(), trustManager);
        } catch (Exception e) {
            Debug.getInstance().exception(e, this);
        }
    }


    private KeyStore getKeyStore(Context context) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        InputStream caInput = context.getResources().openRawResource(R.raw.tomcat_client);
        Certificate ca;
        try {
            ca = cf.generateCertificate(caInput);
        } finally {
            caInput.close();
        }

        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);

        return keyStore;
    }

    /**
     * getHostnameVerifier()
     *
     * @return
     */
    private HostnameVerifier getHostnameVerifier() {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                boolean isValid = Arrays.asList(new String[] {
                        "10.0.2.2",             // Emulator localhost
                        "irtis.fi.muni.cz",      // Irtis public server
                }).contains(hostname);

                Debug.getInstance().log(TAG, "Host validation: " + hostname + " = " + (isValid ? "true" : "false"));

                return isValid;
            }
        };
    }


    public interface IsAvailableListener {
        void onSuccess();
        void onFailure();
    }

    static private class IsAvailableTask extends AsyncTask<Void, Void, Boolean> {
        IsAvailableListener listener;
        IsAvailableTask(IsAvailableListener listener) {
            this.listener = listener;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return isAvailable();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result) {
                listener.onSuccess();
            } else {
                listener.onFailure();
            }
        }
    }

}
