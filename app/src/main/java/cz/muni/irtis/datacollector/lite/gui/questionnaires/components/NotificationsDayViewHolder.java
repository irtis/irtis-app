package cz.muni.irtis.datacollector.lite.gui.questionnaires.components;

import android.view.View;
import android.widget.TextView;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewAdapter;

public class NotificationsDayViewHolder extends RecyclerListViewAdapter.ItemsListViewHolderBase {
    public TextView day;
    public TextView hours;

    public NotificationsDayViewHolder(RecyclerListViewAdapter adapter, View view) {
        super(adapter, view);
        day = view.findViewById(R.id.day);
        hours = view.findViewById(R.id.hours);
    }
}
