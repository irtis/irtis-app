package cz.muni.irtis.datacollector.lite.metrics.model.wifi;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;

import java.util.List;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi.WifiConnectedEntity;

/**
 * Capture available WiFi SSIDs
 */
public class ConnectedWifi extends WifiBase {
    private final String TAG = this.getClass().getSimpleName();
    public static final int IDENTIFIER = 254;

    public ConnectedWifi(Context context, Integer delay) {
        super(context, delay);
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "Wifi connected";
    }

    // save connected wifi
    public long save(List<String> available, String connected) {
        if (connected != null) {
            long connectedId = getWifiSsid(connected);
            if (connectedId > 0) {
                Debug.getInstance().log(TAG, connected);

                WifiConnectedEntity entity = new WifiConnectedEntity(Time.getTime(), connectedId);
                try {
                    return getDatabase().getConnectedWifiRepository().insert(entity);
                } catch (SQLiteConstraintException exception) {
                    Debug.getInstance().exception(exception, null, "Time: "+Time.getTime());
                }
            }
        }

        return 0;
    }
}
