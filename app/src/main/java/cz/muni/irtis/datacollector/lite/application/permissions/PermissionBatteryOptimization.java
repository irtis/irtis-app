package cz.muni.irtis.datacollector.lite.application.permissions;

import android.content.Context;

import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_BATTERYOPTIMIZATION;

/**
 * PermissionBatteryOptimization
 *
 * See https://developer.android.com/reference/android/provider/Settings.html#ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
 */
public class PermissionBatteryOptimization extends Permission {

    public PermissionBatteryOptimization(Context context, String title, String notes)
    {
        super(context, PERMISSION_BATTERYOPTIMIZATION, title, notes);
    }

    public boolean isGranted()
    {
        return provider.isGrantedBatteryOptimizationPermission();
    }

}
