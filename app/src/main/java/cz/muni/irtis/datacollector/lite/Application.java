package cz.muni.irtis.datacollector.lite;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;

import androidx.appcompat.app.AppCompatDelegate;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.debug.UnhandledExceptions;
import cz.muni.irtis.datacollector.lite.application.widgets.Box;


public class Application extends android.app.Application {
	static private Application instance;

	/**
	 * getInstance()
	 * 
	 * Get instance of Application
	 * 
	 * @return Application
	 */
	static public Application getInstance()
	{
		return Application.instance;
	}

	/**
	 * onCreate()
	 *
	 * Called before the first components of the application starts\
	 *
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		Application.instance = this; // This must be the first statement

		Debug.getInstance().log("Application", "onCreate()");

		// Bind exceptions
		UnhandledExceptions.bind();

//		AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.getDefaultNightMode());
	}

	/**
	 * onLowMemory()
	 *
	 * Called when the Android system requests that the application cleans up memory
	 *
	 */
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		Debug.getInstance().log("Application", "onLowMemory()");
	}

    /**
     * onTrimMemory()
	 *
	 * Called when the Android system requests that the application cleans up memory.
	 *
     * @param level indicator in which position the application isActive
     */
	@Override
	public void onTrimMemory(int level) {
		super.onTrimMemory(level);
		Debug.getInstance().log("Application", "onTrimMemory() {level = "+level+"}");
	}

	/**
	 * onTerminate
	 *
	 * Called only in debug mode
	 */
	@Override
	public void onTerminate() {
		super.onTerminate();
		Debug.getInstance().log("Application", "onTerminate()");
	}

	/**
	 * getInflater()
	 * 
	 * @return LayoutInflater
	 */
	public LayoutInflater getInflater() {
		return (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}


	/**
	 * Get information about package like version, code
	 * @return
	 */
	public PackageInfo getApplicationPackage() {
		try {
			PackageInfo info = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
			return info;
		} catch (Exception e) {
			Debug.getInstance().exception(e, this);
			return null;
		}
	}
}
