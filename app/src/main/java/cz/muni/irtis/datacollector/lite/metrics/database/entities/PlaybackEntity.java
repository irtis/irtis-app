package cz.muni.irtis.datacollector.lite.metrics.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import cz.muni.irtis.datacollector.lite.metrics.database.repository.PlaybackRepository;

@Entity(tableName = PlaybackRepository.TABLE_NAME)
public class PlaybackEntity extends EntityBase {
    @ColumnInfo(name = "active")
    private Boolean active;

    public PlaybackEntity(Long datetime, Boolean active) {
        super(datetime);
        this.active = active;
    }

    public Boolean getActive() {
        return active;
    }
}
