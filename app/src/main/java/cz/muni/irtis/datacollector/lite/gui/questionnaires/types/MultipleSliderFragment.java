package cz.muni.irtis.datacollector.lite.gui.questionnaires.types;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities.MediaEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Label;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;

public class MultipleSliderFragment extends TypeFragmentBase {
    public MultipleSliderFragment(Context context, Question question, boolean editable) {
        super(context, question, R.layout.activity_questionnaires_form_type_slider, editable);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        ViewGroup vContainer = view.findViewById(R.id.container);
        vContainer.removeAllViews();

        List<Answer> answers = getQuestion().getAnswers();
        if(answers!=null && !answers.isEmpty()) {
            Iterator<Answer> iAnswers = answers.iterator();
            while (iAnswers.hasNext()) {
                final Answer answer = iAnswers.next();

                Label label = new GsonBuilder().create()
                        .fromJson(answer.getText(), new TypeToken<Label>() {}.getType());

                View vSlider = LayoutInflater.from(getContext())
                        .inflate(R.layout.activity_questionnaires_form_type_slider, null);

                TextView vLabel = vSlider.findViewById(R.id.label);
                vLabel.setText(label.getText());
                vLabel.setVisibility(View.VISIBLE);

                final SeekBar slider = vSlider.findViewById(R.id.slider);
                slider.setMax(getQuestion().getDensity());
                slider.setProgress(slider.getMax() / 2);
                slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                        answer.setValue(String.valueOf(progress));

                        refresh();
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) { }
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) { }
                });
                slider.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        answer.setValue(String.valueOf(slider.getProgress()));
                        slider.getProgressDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
                        slider.getThumb().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
                        refresh();
                        return false;
                    }
                });
                slider.setEnabled(isEditable());

                if (answer.isValue()) {
                    slider.setProgress(Integer.valueOf(answer.getValue()));
                } else {
                    slider.setProgressTintList(ColorStateList.valueOf(Color.TRANSPARENT));
                    slider.setProgressBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorGreyDark)));
                    slider.getThumb().setColorFilter(getResources().getColor(R.color.colorGreyDark), PorterDuff.Mode.SRC_ATOP);
                }

                FlexboxLayout vLabelsContainer = vSlider.findViewById(R.id.labels);
                Iterator<Answer> iLabels = label.getAnswers().iterator();
                while(iLabels.hasNext()) {
                    Answer point = iLabels.next();
                    TextView vElement = new TextView(vLabelsContainer.getContext());
                    vElement.setText(point.getText());
                    vLabelsContainer.addView(vElement);
                }

                vContainer.addView(vSlider);
            }
        }

        return view;
    }
}
