package cz.muni.irtis.datacollector.lite.metrics.model.applications;

import android.app.ActivityManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;

import com.rvalerio.fgchecker.AppChecker;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.Metric;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationForegroundEntity2;
import cz.muni.irtis.datacollector.lite.metrics.model.condition.IsScreenOn;
import cz.muni.irtis.datacollector.lite.metrics.model.condition.IsUsageStatsAllowed;

public class ForegroundApplication2 extends Metric {
    private static final String TAG = ForegroundApplication2.class.getSimpleName();
    public static final int IDENTIFIER = 262;

    private AppChecker applications;

    public ForegroundApplication2(Context context, Integer delay) {
        super(context, delay);

        applications = new AppChecker();

        addPrerequisity(new IsScreenOn());
        addPrerequisity(new IsUsageStatsAllowed());
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "Application";
    }

    @Override
    public void run() {
        if (!isPrerequisitiesSatisfied()) {
            return;
        }

        setRunning(true);

        String topPackageName = "none";
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityManager am = (ActivityManager) getContext().getSystemService(Context.ACTIVITY_SERVICE);
            ActivityManager.RunningTaskInfo foregroundTaskInfo = am.getRunningTasks(1).get(0);
            topPackageName = foregroundTaskInfo.topActivity.getPackageName();
        } else {
            UsageStatsManager usage = (UsageStatsManager) getContext().getSystemService(Context.USAGE_STATS_SERVICE);
            long time = System.currentTimeMillis();

            List<UsageStats> stats = usage.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 1000, time);
            if (stats != null) {
                SortedMap<Long, UsageStats> runningTasks = new TreeMap<>();
                for (UsageStats usageStats : stats) {
                    runningTasks.put(usageStats.getLastTimeUsed(), usageStats);
                }
                if (!runningTasks.isEmpty()) {
                    UsageStats lastTask = runningTasks.get(runningTasks.lastKey());
                    if (lastTask != null) {
                        topPackageName = lastTask.getPackageName();
                    }
                }
            }
        }
        if (!"none".equals(topPackageName)) {
            save(topPackageName);
        }
    }

    @Override
    public void stop() {
        setRunning(false);
    }

    @Override
    public long save(Object... params) {
        String current = (String) params[0];

        // save new apps
        long appId = getAppId(current);
        if (appId <= 0) {
            appId = saveNewAppName(current);
        }

        try {
            Debug.getInstance().log(TAG, current);
            return getDatabase().getForegroundApplicationRepository2().insert(
                    new ApplicationForegroundEntity2(Time.getTime(), appId));
        } catch (Exception e) {
            Debug.getInstance().exception(e);
            return -1;
        }
    }

    private long getAppId(String name) {
        ApplicationEntity entity = getDatabase().getApplicationRepository().getIdByName(name);
        if(entity!=null) {
            return entity.getId();
        } else {
            return -1;
        }
    }

    private long saveNewAppName(String name) {
        return getDatabase().getApplicationRepository().insert(new ApplicationEntity(Time.getTime(), name));
    }
}
