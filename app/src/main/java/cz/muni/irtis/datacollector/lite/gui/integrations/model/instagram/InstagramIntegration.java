package cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;

import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.Applications;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.IntegrationBase;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.connection.Request;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.connection.Response;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities.CommentsEntity;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities.MediaEntity;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities.UserEntity;

public class InstagramIntegration extends IntegrationBase {
    private InstagramService instagram;

    public InstagramIntegration(Context context, final ServiceBase.OnServiceListener onSignInListener, final ServiceBase.OnServiceListener onSignOutListener) {
        super(context);
        instagram = InstagramServiceBuilder.build(context, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
                onSignInListener.onStart();
            }

            @Override
            public void onError(int code) {
                onSignInListener.onError(code);
            }

            @Override
            public void onFinished(boolean isLogged) {
                onSignInListener.onFinished(isLogged);
            }
        }, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
                onSignOutListener.onStart();
            }

            @Override
            public void onError(int code) {
                onSignOutListener.onError(code);
            }

            @Override
            public void onFinished(boolean isLogged) {
                onSignOutListener.onFinished(isLogged);
            }
        });
    }
    public InstagramIntegration(Context context) {
        super(context);
        instagram = InstagramServiceBuilder.build(context);
    }

    static public InstagramIntegration build(Context context) {
        return new InstagramIntegration(context);
    }

    @Override
    public boolean isConnected() {
        return instagram.isConnected();
    }

    /**
     * isApplication()
     *
     * Check if youtube app isActive installed on the device
     *
     * @return
     */
    @Override
    public boolean isApplication() {
        return Applications.isApplicationInstalled(getContext(), "com.instagram.android");
    }

    @Override
    public String getName() {
        return getContext().getString(R.string.instagram_integration_title);
    }

    @Override
    public Drawable getImage() {
        return getContext().getDrawable(R.drawable.integration_instagram_icon);
    }

    public void connect(Activity activity) {
        instagram.signIn(activity);
    }

    public void disconnect() {
        instagram.signOut();
    }

    public InstagramService getService() {
        return instagram;
    }


    /**
     * getProfile()
     *
     * Get the profile of the current user
     *
     * @param listener
     */
    public void getProfile(final Request.OnRequestEntityListener<UserEntity> listener) {
        Request.build(
            instagram.getAccessToken(),
            "/users/self",
            new Request.OnRequestListener() {
                @Override
                public void onDone(Response response) {
                    UserEntity item = UserEntity.buildFromJson(response.getData());
                    listener.onComplete(item);
                }
            }
        );
    }

    /**
     * getMedia()
     *
     * Get the most recent media of the current user
     *
     * @param listener
     */
    public void getMedia(final Request.OnRequestEntityListener<List<MediaEntity>> listener) {
        Request.build(
            instagram.getAccessToken(),
            "/users/self/media/recent",
            new Request.OnRequestListener() {
                @Override
                public void onDone(Response response) {
                    List<MediaEntity> items = MediaEntity.buildListFromJson(response.getData());
                    listener.onComplete(items);
                }
            }
        );
    }

    /**
     * getComments()
     *
     * Get comments for a particular media
     *
     * @param media
     * @param listener
     */
    public void getComments(MediaEntity media, final Request.OnRequestEntityListener<List<CommentsEntity>> listener) {
        Request.build(
                instagram.getAccessToken(),
                "/media/"+media.getId()+"/comments",
                new Request.OnRequestListener() {
                    @Override
                    public void onDone(Response response) {
                        List<CommentsEntity> items = CommentsEntity.buildListFromJson(response.getData());
                        listener.onComplete(items);
                    }
                }
        );
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        instagram.onActivityResult(requestCode, resultCode, data);
    }
}
