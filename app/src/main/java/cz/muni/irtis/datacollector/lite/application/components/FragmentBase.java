package cz.muni.irtis.datacollector.lite.application.components;


import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

/**
 * FragmentBase
 *
 * - https://developer.android.com/guide/components/fragments.html
 */
public class FragmentBase extends Fragment {
	private int contentLayoutId;
	private Context context;
	private View view;

	private boolean isVisible = false;
	private boolean isCreated = false;


	public FragmentBase() {
		super();
	}

	public FragmentBase(int contentLayoutId) {
		super();
		this.contentLayoutId = contentLayoutId;
	}

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
		View view = super.onCreateView(inflater, container, bundle);
		if(view==null && contentLayoutId!=0) {
			view = LayoutInflater.from(getContext()).inflate(contentLayoutId, null);
		}

		isCreated = true;
		this.view = view;
		return view;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		isCreated = false;
		isVisible = false;
	}

	/**
	 * Called when the visible state to user has been changed.
	 */
	public void onVisibleView(boolean visible) {
	}

	public <T extends View> T findViewById(int id) {
		if(getView()!=null) {
			return getView().findViewById(id);
		} else {
			return null;
		}
	}

	@Override
	public View getView() {
		return view;
	}

	public AssetManager getAssets() {
		return this.getActivity().getAssets();
	}

	public LayoutInflater getInflater() {
		return (LayoutInflater) this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}


	public void startActivity(Class<?> c) {
		super.startActivity(new Intent(getActivity(), c));
	}


	public void startActivity(Class<?> c, String data) {
		Intent i = new Intent(getActivity(), c);
		i.putExtra("data", data);
		super.startActivity(i);
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public Context getContext() {
		if(this.context!=null) {
			return this.context;
		} else {
			return super.getContext();
		}
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (!isVisible && isVisibleToUser) {
			isVisible = true;
		}
		onVisibleView(isVisibleToUser);
	}
}
