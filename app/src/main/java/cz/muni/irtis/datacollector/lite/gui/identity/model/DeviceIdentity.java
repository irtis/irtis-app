package cz.muni.irtis.datacollector.lite.gui.identity.model;

import com.google.gson.GsonBuilder;

public class DeviceIdentity {
    private String device;
    private String api;

    private DeviceIdentity() {
    }

    static public DeviceIdentity build() {
        return new DeviceIdentity();
    }

    static public DeviceIdentity build(String json) {
        return new GsonBuilder().create().fromJson(json, DeviceIdentity.class);
    }

    public String toString() {
        return new GsonBuilder().create().toJson(this, DeviceIdentity.class);
    }

    public String getDevice() {
        return device;
    }

    public DeviceIdentity setDevice(String device) {
        this.device = device;
        return this;
    }

    public String getApi() {
        return api;
    }

    public DeviceIdentity setApi(String api) {
        this.api = api;
        return this;
    }
}
