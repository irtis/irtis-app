package cz.muni.irtis.datacollector.lite.persistence.model;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.auth.api.signin.internal.Storage;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cz.muni.irtis.datacollector.lite.application.database.StorageDatabase;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.ConnectionResult;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.Burst;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.BurstsModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionnaireEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.ResultResponse;
import cz.muni.irtis.datacollector.lite.metrics.MetricsService;
import cz.muni.irtis.datacollector.lite.persistence.database.EventEntity;
import cz.muni.irtis.datacollector.lite.persistence.database.EventsRepository;

public class Events {
    static public final Integer EVENT_CAPTURING_STARTED = 1;
    static public final Integer EVENT_CAPTURING_STOPPED = 2;
    static public final Integer EVENT_CAPTURING_SCREENSHOTS_STARTED = 3;
    static public final Integer EVENT_CAPTURING_SCREENSHOTS_STOPPED = 4;

    static public final Integer EVENT_QUESTIONNAIRES_NOTIFIED = 5;
    static public final Integer EVENT_QUESTIONNAIRES_POSTPONED = 6;
    static public final Integer EVENT_QUESTIONNAIRES_SILENCED = 7;
    static public final Integer EVENT_QUESTIONNAIRES_DOWNLOADED = 8;
    static public final Integer EVENT_QUESTIONNAIRES_UPLOADED = 9;
    static public final Integer EVENT_QUESTIONNAIRES_OPENED = 10;
    static public final Integer EVENT_QUESTIONNAIRES_COMPLETED = 11;
    static public final Integer EVENT_QUESTIONNAIRES_TEMPLATES_DOWNLOADED = 18;
    static public final Integer EVENT_QUESTIONNAIRES_INVOCATION_ACTIVE = 19;
    static public final Integer EVENT_QUESTIONNAIRES_INVOCATION_INACTIVE = 20;
    static public final Integer EVENT_QUESTIONNAIRES_INVOCATION_EMPTY = 21;
    static public final Integer EVENT_QUESTIONNAIRES_INVOCATION_NOTHINGTOSHOW = 22;


    static public final Integer EVENT_STORAGE_INSUFFICIENT_SPACE = 12;

    static public final Integer EVENT_SYNCHRONIZATION_PROCESSING = 13;

    static public final Integer EVENT_MESSAGES_DOWNLOADED = 14;
    static public final Integer EVENT_MESSAGES_NOTIFIED = 15;
    static public final Integer EVENT_MESSAGES_READ = 16;
    static public final Integer EVENT_MESSAGES_UPLOADED = 17;


    private Context context;
    private EventsRepository repository;


    private Events(Context context) {
        this.context = context;
        this.repository = StorageDatabase.getDatabase(context).getEventsRepository();
    }

    static public Events build(Context context) {
        return new Events(context);
    }

    public Context getContext() {
        return context;
    }

    public void log(Integer type) {
        repository.store(new EventEntity(type, Time.getTime()));
        Debug.getInstance().warning("Events", getEventName(type) + " ("+type.toString()+")");
    }

    public void upload() {
        if(Network.isConnection()) {
            // Check if the time is around the burst
            Boolean isEnabled = false;
            List<Burst> bursts =  BurstsModel.build(getContext()).getItems();
            if (bursts != null) {
                Iterator<Burst> iBursts = bursts.iterator();
                while (iBursts.hasNext()) {
                    Burst burst = iBursts.next();
                    if(burst.getStarting()-Time.DAY < Time.getTime() && burst.getEnding()+Time.DAY > Time.getTime()) {
                        isEnabled = true;
                        break;
                    }
                }
            }

            if(isEnabled) {
                List<EventEntity> items = repository.getItemsNotUploaded();
                if (items != null && !items.isEmpty()) {
                    new UploadTask(items).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        }
    }

    public class UploadTask extends AsyncTask<Void, Void, Object> {
        private List<EventEntity> items;

        public UploadTask(List<EventEntity> items) {
            this.items = items;
        }

        @Override
        protected Object doInBackground(Void... nul) {
            Iterator<EventEntity> i = items.iterator();
            while(i.hasNext()) {
                EventEntity item = i.next();
                String content = EventJson.build(item.getIdentifier(), item.getTime()).toString();
                ConnectionResult result = Connection.build(getContext()).postJson(
                        UrlComposer.compose("/events/events/upload"),
                        content);

                if(result!=null && result.isResult() && result.isContent()) {
                    ResultResponse response = result.getContent(new TypeToken<ResultResponse>(){});
                    if (response != null && response.isSuccessful()) {
                        item.setUploaded(Time.getTime());
                        repository.store(item);
                    }
                }
            }

            return null;
        }
    }


    public Map<String, Integer> getEventsList() {
        Map<String, Integer> list = new HashMap<String, Integer>();

        for (Field field : Events.class.getDeclaredFields()) {
            int modifiers = field.getModifiers();
            if(Modifier.isStatic(modifiers) && Modifier.isFinal(modifiers) ) {
                try {
                    list.put(field.getName(), (Integer) field.get(field.getType()));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    public String getEventName(Integer type) {
        Map<String, Integer> list = getEventsList();
        Iterator<Map.Entry<String, Integer>> iList = list.entrySet().iterator();
        while (iList.hasNext()) {
            Map.Entry<String, Integer> item = iList.next();
            if(item.getValue().equals(type)) {
                return item.getKey();
            }
        }
        return null;
    }
}
