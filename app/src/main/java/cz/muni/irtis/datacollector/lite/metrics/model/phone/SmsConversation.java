package cz.muni.irtis.datacollector.lite.metrics.model.phone;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Telephony;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.phone.PhoneSmsEntity;
import cz.muni.irtis.datacollector.lite.metrics.Metric;
import cz.muni.irtis.datacollector.lite.metrics.model.util.sms.SmsRecord;

/**
 * SmsConversation
 *
 * @deprecated due GooglePlay's location requirements policy which we are not able to meet
 */
public class SmsConversation extends Metric {
    private final String TAG = this.getClass().getSimpleName();
    public static final int IDENTIFIER = 250;

    public SmsConversation(Context context, Integer delay) {
        super(context, delay);
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "SMS";
    }

    @Override
    public void run() {
        setRunning(true);
        long latestSaved = getMaxMessageDate();
        String incommingAddress = "content://sms/inbox";
        String sentAddress = "content://sms/sent";

        addMessages(latestSaved, incommingAddress, SmsRecord.INCOMING);
        addMessages(latestSaved, sentAddress, SmsRecord.SENT);
    }

    @Override
    public void stop() {
        setRunning(false);
    }

    private void addMessages(long latestSaved, String address, String direction) {
        Uri receivedMessages = Uri.parse(address);
        Cursor c = getContext().getContentResolver().query(
                receivedMessages, null, "DATE > " + latestSaved, null, null);

        if (c != null && c.getCount() > 0) {
            while (c.moveToNext()) {
                SmsRecord record = new SmsRecord();
                record.setPhoneNumber(c.getString(c.getColumnIndex(Telephony.Sms.ADDRESS)));
                record.setContent(c.getString(c.getColumnIndex(Telephony.Sms.BODY)));
                record.setType(direction);
                record.setMessageDate(c.getLong(c.getColumnIndex(Telephony.Sms.DATE)));

                save(record);
            }
            c.close();
        }
    }

    @Override
    public long save(Object... params) {
        SmsRecord record = (SmsRecord) params[0];


        try {
            Debug.getInstance().log(TAG, record.getPhoneNumber());
            return getDatabase().getSmsConversationRepository().insert(new PhoneSmsEntity(
                Time.getTime(),
                record.getPhoneNumber(),
                record.getType(),
                record.getContent(),
                record.getMessageDate()
            ));
        } catch (Exception e) {
            Debug.getInstance().exception(e);
            return -1;
        }
    }


    /**
     * Get latest message date
     * @return long (INTEGER) milliseconds since epoch
     */
    public long getMaxMessageDate() {
        Long time = getDatabase().getSmsConversationRepository().getMaxMessageDatetime();
        if (time!=null) {
            return time;
        } else {
            return Time.getTime();
        }
    }
}
