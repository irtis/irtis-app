package cz.muni.irtis.datacollector.lite.application.components;

import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;

/**
 * TaskServiceBase
 *
 * Replication of ServiceBase to provide newer background service interface since the traditional
 * background service solution isActive now deprecated.
 * (see: https://developer.android.com/guide/components/services,
 * https://developer.android.com/about/versions/oreo/background.html)
 */
abstract public class TaskServiceBase {
    private Context context;
    private Class<?> type;
    private JobParameters jobParameters;

    static private Map<Class<?>, TaskServiceBase> runningServices = new HashMap<>();
    static private Lock runningServicesLock = new ReentrantReadWriteLock().writeLock();

    private AsynchronousRunner runner;


    public TaskServiceBase(Context context, Class<?> type) {
        this.context = context;
        this.type = type;

//        HandlerThread handlerThread = new HandlerThread("CustomName");
//        handlerThread.start();
//        Handler handler = new Handler(handlerThread.getLooper());

    }


    static public TaskServiceBase getService(Class<?> type) {
        if(runningServices!=null) {
            return runningServices.get(type);
        } else {
            return null;
        }
    }


    /**
     * setServiceEnabled()
     *
     * Save service running state. This can help determine whether was the service naturally terminated.
     * If it was terminated by the system, it will be setItem to true. If it isActive setItem to false, it was destroyed
     * at it end of life-cycle
     *
     * @param enabled
     */
    static public void setServiceEnabled(boolean enabled, Class<?> type) {
        SharedPreferences.Editor editor = Settings.getSettings().edit();
        editor.putBoolean(type.getSimpleName().toLowerCase()+"_enabled", enabled);
        editor.commit();
    }

    /**
     * isServiceEnabled()
     *
     * Check the saved running state if the service was running before its life was ended.
     *
     * @param type
     * @return
     */
    static public boolean isServiceEnabled(Class<?> type) {
        return Settings.getSettings().getBoolean(
                type.getSimpleName().toLowerCase()+"_enabled", false);
    }


    /**
     * isRunning()
     *
     * Check if the service isActive running
     *
     * @param context
     * @return
     */
    static public boolean isServiceRunning(Context context, Class<?> type) {
        if(runningServices!=null) {
            try {
                runningServicesLock.lock();
                Set<Class<?>> keys = runningServices.keySet();
                Iterator<Class<?>> iKeys = keys.iterator();
                while (iKeys.hasNext()) {
                    Class<?> key = iKeys.next();
                    if(key!=null && key.getSimpleName().equals(type.getSimpleName())) {
                        return true;
                    }
                }
            } finally {
                runningServicesLock.unlock();
            }
        }
        return false;
    }

    /**
     * start()
     *
     * Start the service
     *
     * @param context App context
     */
    public static void startService(Context context, Class<?> type) {
        if (!isServiceRunning(context, type)) {
            try {
                Constructor<?> construct = type.getConstructor(Context.class);
                Debug.getInstance().task(type.getSimpleName(), "Service started");
                TaskServiceBase service = (TaskServiceBase) construct.newInstance(new Object[] {context});
                service.start();
            } catch (Exception e) {
                Debug.getInstance().exception(e);
                Debug.getInstance().task(type.getSimpleName(), "Unable to start the service");
            }
        } else {
            Debug.getInstance().task(type.getSimpleName(), "Service isActive already running");
        }
    }

    /**
     * stop()
     *
     * Stop the service
     *
     * @param context
     */
    public static void stopService(Context context, Class<?> type) {
        if (isServiceRunning(context, type)) {
            getService(type).stop();
            Debug.getInstance().task(type.getSimpleName(), "Service stopped");
        } else {
            Debug.getInstance().task(type.getSimpleName(), "Service can not be stopped since it isActive not running");
        }
    }


    /**
     * onCreate()
     *
     * Creation logic
     *
     */
    public void onCreate() {
        Debug.getInstance().task(type.getSimpleName(), "onCreate()");
        try {
            runningServicesLock.lock();
            runningServices.put(type, this);
        } finally {
            runningServicesLock.unlock();
        }
    }

    /**
     * onDestroy()
     *
     * Destruction logic
     */
    public void onDestroy() {
        Debug.getInstance().task(type.getSimpleName(), "onDestroy()");
        if(runningServices!=null) {
            try {
                runningServicesLock.lock();
                Set<Class<?>> keys = TaskServiceBase.runningServices.keySet();
                Iterator<Class<?>> iKeys = keys.iterator();
                while (iKeys.hasNext()) {
                    if (iKeys.next().getSimpleName().equals(type.getSimpleName())) {
                        runningServices.remove(type);
                        break;
                    }
                }
            } finally {
                runningServicesLock.unlock();
            }
        }
    }

    /**
     * onCommand()
     *
     * Commands can be sent to the service and processed via this method
     *
     * @param intent
     */
    public void onCommand(Intent intent) {

    }


    private void start() {
        if(runner==null) {
            runner = new AsynchronousRunner();
            runner.execute();
        }
        onCreate();
    }



    private void stop() {
        if(runner!=null) {
            runner.cancel(true);
        }
        onDestroy();
    }


    public Context getContext() {
        return context;
    }

    public Class<?> getType() {
        return type;
    }

    private class AsynchronousRunner extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
//            onCreate();
//
//            while(true) {
//                if (isCancelled()) {
//                    onDestroy();
//                    break;
//                }
//
//                try {
//                    Thread.sleep(500);
//                } catch (InterruptedException e) {
//                    break;
//                }
//            }

            return null;
        }
    }
}
