package cz.muni.irtis.datacollector.lite.gui.questionnaires;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Pair;

import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.components.TaskServiceBase;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.ConnectionResult;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.QuestionnairesJob;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.NotificationsDay;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.NotificationsDays;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnaireFromTemplateBuilder;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.notifications.QuestionnairesNotification;
import cz.muni.irtis.datacollector.lite.metrics.MetricsService;
import cz.muni.irtis.datacollector.lite.persistence.model.Events;
import okhttp3.Response;

public class QuestionnairesService extends TaskServiceBase {
    public static final String TAG = QuestionnairesService.class.getSimpleName();

    public static final String SETTING_MORNING = "questionnaire_morning";
    public static final String SETTING_NIGHT = "questionnaire_night";

    public static final String SETTING_SYNC_LASTTIME = "questionnaire_sync_lasttime";

    private QuestionnairesModel model;

    public QuestionnairesService(Context context) {
        super(context, QuestionnairesService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        model = new QuestionnairesModel(getContext());

        NotificationsDay today = NotificationsDays.build().getItemByToday();

        Debug.getInstance().log(TAG, "today = " + (Time.getTimeStamp(today.getStarting(), "HH:mm:ss") + " - " + Time.getTimeStamp(today.getEnding(), "HH:mm:ss")));
        Debug.getInstance().log(TAG, "today = " + today.getTimeFormatted());
        Debug.getInstance().log(TAG, "today.isActive() = " + (today.isActive() ? "true" : "false"));
        Debug.getInstance().log(TAG, "today.isActive(): " + today.getStartingHours().toString() + " <= " + Time.getHour() + " && " + today.getEndingHours().toString() + " >= " + Time.getHour() + " = " + (today.getStartingHours() <= Time.getHour() && today.getEndingHours() >= Time.getHour() ? "true" : "false"));
        Debug.getInstance().log(TAG, "today.isActive(): " + today.getStartingMinutes().toString() + " > " + Time.getMinute() + " = " + (today.getStartingMinutes() > Time.getMinute() ? "true" : "false"));
        Debug.getInstance().log(TAG, "today.isActive(): " + today.getEndingMinutes().toString() + " < " + Time.getMinute() + " = " + (today.getEndingMinutes() < Time.getMinute() ? "true" : "false"));
        Debug.getInstance().log(TAG, "MetricsService.isRunning(getContext()) = " + (MetricsService.isRunning(getContext()) ? "true" : "false"));

        if(today.isActive()) { // && MetricsService.isRunning(getContext())) { // this particular part of the condition is no longer required since every questionnaire is exactly scheduled
            Debug.getInstance().log(TAG, "invoke = true");
            Events.build(getContext()).log(Events.EVENT_QUESTIONNAIRES_INVOCATION_ACTIVE);
            invoke();
        } else {
            Debug.getInstance().log(TAG, "invoke = false");
            Events.build(getContext()).log(Events.EVENT_QUESTIONNAIRES_INVOCATION_INACTIVE);
        }

        // Synchronization - run each 20 mins
        Long syncLastTimeExecution = Settings.getSettings().getLong(SETTING_SYNC_LASTTIME, 0);
        if(Time.getTime() > syncLastTimeExecution + Config.QUESTIONNAIRES_REFRESH_SYNC_PERIOD*Time.MINUTE) {
            download(); // Download server surveys
            upload(); // Upload filled surveys back to the server
            templates(); // Download templates
            Settings.getSettings().edit().putLong(SETTING_SYNC_LASTTIME, Time.getTime()).commit();
        }

        // Schedule the service for a new run
//        QuestionnairesJob.schedule(getContext());

        // Stop service
        stop(getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * start()
     *
     * Start the service
     *
     * @param context
     */
    public static void start(Context context) {
        TaskServiceBase.startService(context, QuestionnairesService.class);
    }

    /**
     * stop()
     *
     * Stop the service
     *
     * @param context
     */
    public static void stop(Context context) {
        TaskServiceBase.stopService(context, QuestionnairesService.class);
    }

    /**
     * isRunning()
     *
     * Check if the service is running
     *
     * @param context
     * @return
     */
    static public boolean isRunning(Context context) {
        return TaskServiceBase.isServiceRunning(context, QuestionnairesService.class);
    }


    /**
     * createMorningSurvey()
     *
     * This survey isActive invoked between 6-12 every day
     *
     * @deprecated
     */
    public void createMorningSurvey() {
        DateTime now = DateTime.now();
        DateTime from = new DateTime(now.year().get(), now.monthOfYear().get(), now.dayOfMonth().get(),8,0);
        DateTime to = new DateTime(now.year().get(), now.monthOfYear().get(), now.dayOfMonth().get(),12,0);
        if(Time.getTime()>from.getMillis() && Time.getTime()<to.getMillis()) {
            if(!Settings.getSettings().getBoolean(SETTING_MORNING, false)) {
                Questionnaire item = QuestionnaireFromTemplateBuilder.build(
                        getContext(), QuestionnaireFromTemplateBuilder.TYPE_MORNING);
                model.store(item);
                if (QuestionnairesNotification.isShowable(item)) {
                    QuestionnairesNotification.build(getContext(), item).show();
                }
                Settings.getSettings().edit().putBoolean(SETTING_MORNING, true).commit();

                Debug.getInstance().warning(QuestionnairesService.TAG,
                    "Instance created | Value: "+(item.getIdentifier()!=null ? item.getIdentifier() : "null"));
            }
        } else {
            Settings.getSettings().edit().putBoolean(SETTING_MORNING, false).commit();
        }
    }

    /**
     * createNightSurvey()
     *
     * This survey isActive invoked between 20-24 every day
     *
     * @deprecated
     */
    public void createNightSurvey() {
        DateTime now = DateTime.now();
        DateTime from = new DateTime(now.year().get(), now.monthOfYear().get(), now.dayOfMonth().get(), 19, 0);
        DateTime to = new DateTime(now.year().get(), now.monthOfYear().get(), now.dayOfMonth().get(), 23, 59);
        if (Time.getTime() > from.getMillis() && Time.getTime() < to.getMillis()) {
            if (!Settings.getSettings().getBoolean(SETTING_NIGHT, false)) {
                Questionnaire item = QuestionnaireFromTemplateBuilder.build(
                        getContext(), QuestionnaireFromTemplateBuilder.TYPE_NIGHT);
                model.store(item);
                if (QuestionnairesNotification.isShowable(item)) {
                    QuestionnairesNotification.build(getContext(), item).show();
                }
                Settings.getSettings().edit().putBoolean(SETTING_NIGHT, true).commit();

                Debug.getInstance().warning(QuestionnairesService.TAG,
                    "Instance created | Value: "+(item.getIdentifier()!=null ? item.getIdentifier() : "null"));
            }
        } else {
            Settings.getSettings().edit().putBoolean(SETTING_NIGHT, false).commit();
        }
    }

    private void invoke() {
        model.getItemsToInvoke(new Repository.RepositoryListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
            }

            @Override
            public void onDone(List<Questionnaire> items) {
                if(items!=null && !items.isEmpty()) {
                    Questionnaire itemToShow = null;
                    Iterator<Questionnaire> iItems = items.iterator();
                    while (iItems.hasNext()) {
                        Questionnaire item = iItems.next();
                        if (item.getClosing()==null && item.isOpenForStarting()) {
                            Questionnaire last = model.getItemLastByClosing();
                            Boolean isInitializable = true;
                            if(last!=null) {
                                isInitializable = Time.getTime() > last.getBufferTime();
                            }
                            if(isInitializable) {
                                item.setClosing();
                                model.store(item);
                                if (itemToShow == null) {
                                    itemToShow = item;
                                }
                            }
                        }
                    }

                    if(itemToShow!=null && QuestionnairesNotification.isShowable(itemToShow)) {
                        QuestionnairesNotification.build(getContext(), itemToShow).show();
                    } else {
                        Events.build(getContext()).log(Events.EVENT_QUESTIONNAIRES_INVOCATION_NOTHINGTOSHOW);
                    }
                } else {
                    Events.build(getContext()).log(Events.EVENT_QUESTIONNAIRES_INVOCATION_EMPTY);
                }
            }
        });
    }

    /**
     * downloadSurveys()
     *
     * Download JSON content, parse content
     *
     */
    public void download() {
        if(Network.isConnection()) {
            Connection.build(getContext()).getJson(
                UrlComposer.compose("/questionnaires/person/download"),
                new Connection.OnConnectionListener<String>() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onError(Exception e) {
                    }

                    @Override
                    public void onDone(Response response, String content) {
                        Debug.getInstance().log(TAG, "Download started");
                        if (content != null && !content.isEmpty()) {
                            List<Questionnaire> items = Questionnaire.buildListFromString(content, false);
                            if(items!=null && !items.isEmpty()) {
                                // Get the items about to be removed
                                List<Questionnaire> itemsRemoved = model.getItemsFuture();

                                // Remove all that were not yet initiated
                                model.deleteByFuture();

                                Iterator<Questionnaire> iItems = items.iterator();
                                while (iItems.hasNext()) {
                                    Questionnaire item = iItems.next();

                                    // Get stored copy of the item
                                    Questionnaire itemStored = null;
                                    Iterator<Questionnaire> iRemoved = itemsRemoved.iterator();
                                    while(iRemoved.hasNext()) {
                                        Questionnaire itemRemoved = iRemoved.next();
                                        if(item.getIdPeopleQuestionnaire().equals(itemRemoved.getIdPeopleQuestionnaire())) {
                                            itemStored = itemRemoved;
                                            break;
                                        }
                                    }

                                    // Store the item
                                    model.store(item);

                                    // Adjust invoking time
                                    if(item.getInvoking()==null) {
//                                        if(itemStored!=null) {
//                                            model.setInvoking(item, itemStored.getInvoking()); // set to the previously stored value
//                                        } else
                                        if(model.getInvocationsByDay(item)!=null) {
                                            model.setInvocationByDay(item); // set the local time determined by the user's settings
                                        } else {
                                            model.setInvoking(item); // set the random time
                                        }
                                    } else {
                                        if(model.getInvocationsByDay(item)!=null) {
                                            model.setInvocationByDay(item); // set the local time determined by the user's settings
                                        }
                                    }
                                }

                                // Log download event
                                Events.build(getContext()).log(Events.EVENT_QUESTIONNAIRES_DOWNLOADED);
                            }
                        }
                        Debug.getInstance().log(TAG, "Download finished");
                    }
                }, true
            );
        }
    }

    /**
     * uploadSurveys()
     *
     * Upload completed survey to the serve.
     * If successfully sent, setItem upload time to the db (will mark the survey as sent)
     */
    public void upload() {
        if(Network.isConnection()) {
            List<Questionnaire> items = model.getItemsNotUploaded();
            if (items != null && !items.isEmpty()) {
                new UploadTask(items).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }
    }

    public class UploadTask extends AsyncTask<Void, Void, Object> {
        private List<Questionnaire> items;

        public UploadTask(List<Questionnaire> items) {
            this.items = items;
        }

        @Override
        protected Object doInBackground(Void... nul) {
            Debug.getInstance().log(TAG, "Upload started");
            Iterator<Questionnaire> i = items.iterator();
            while(i.hasNext()) {
                Questionnaire item = i.next();
                if(item.isClosed()) {
                    model.upload(item);
                }
            }

            Debug.getInstance().log(TAG, "Upload finished");
            Events.build(getContext()).log(Events.EVENT_QUESTIONNAIRES_UPLOADED);
            return null;
        }
    }



    /**
     * uploadSurveys()
     *
     * Upload completed survey to the serve.
     * If successfully sent, setItem upload time to the db (will mark the survey as sent)
     */
    public void templates() {
        if(Network.isConnection()) {
            new DownloadTemplatesTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    public class DownloadTemplatesTask extends AsyncTask<Void, Void, Object> {
        public DownloadTemplatesTask() {
        }

        @Override
        protected Object doInBackground(Void... nul) {
            Long updated = 0L;
//            List<Questionnaire> templates = model.getItemsTemplates();
//            if(templates!=null && !templates.isEmpty()) {
//                Iterator<Questionnaire> iTemplates = templates.iterator();
//                while(iTemplates.hasNext()) {
//                    Questionnaire template = iTemplates.next();
//                    if(updated < template.getUpdated()) {
//                        updated = template.getUpdated();
//                    }
//                }
//            }

            Connection.build(getContext())
                .getJson(UrlComposer.compose("/questionnaires/templates",
                    Arrays.asList(
                        new Pair<String, String>("time", updated.toString()),
                        new Pair<String, String>("type", String.valueOf(QuestionnairesModel.TYPE_INVOKED))
                    )),
                    new Connection.OnConnectionListener<String>() {
                        @Override
                        public void onStart() {}
                        @Override
                        public void onError(Exception e) {}
                        @Override
                        public void onDone(Response response, String content) {
                            ConnectionResult result = ConnectionResult.build(response, content);
                            if(result!=null && result.isResult() && result.isContent()) {
                                List<Questionnaire> items = result.getContent(new TypeToken<List<Questionnaire>>(){});
                                if (items!=null && !items.isEmpty()) {
                                    Iterator<Questionnaire> iItems = items.iterator();
                                    while(iItems.hasNext()) {
                                        Questionnaire item = iItems.next();
                                        item.toFile();
                                    }
                                }
                            }

                            Events.build(getContext()).log(Events.EVENT_QUESTIONNAIRES_TEMPLATES_DOWNLOADED);
                        }
                }, false);

            return null;
        }
    }
}
