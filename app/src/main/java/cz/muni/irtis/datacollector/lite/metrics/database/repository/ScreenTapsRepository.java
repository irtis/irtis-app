package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenTapEntity;

@Dao
public interface ScreenTapsRepository extends IRepository<ScreenTapEntity> {
    public static final String TABLE_NAME = "screen_taps";

    /**
     * Add new entity to database.
     * @param item entity
     */
    @Insert
    long insert(ScreenTapEntity item);

    @Query("SELECT * " +
            "FROM "+TABLE_NAME+" " +
            "WHERE "+TABLE_NAME+".datetime = :time")
    ScreenTapEntity getByTime(Long time);

    @Query("SELECT * " +
            "FROM "+TABLE_NAME+" " +
            "WHERE "+TABLE_NAME+".datetime < :time")
    List<ScreenTapEntity> getPrevious(Long time);

    @Query("SELECT * " +
            "FROM "+TABLE_NAME+" " +
            "WHERE "+TABLE_NAME+".datetime < :time" + " " +
            "LIMIT :offset, :count")
    List<ScreenTapEntity> getPrevious(Long time, int offset, int count);

    /**
     * {@link IRepository#deleteById(Long)}
     */
    @Query("DELETE FROM " + TABLE_NAME + " WHERE datetime = :id")
    int deleteById(Long id);



}
