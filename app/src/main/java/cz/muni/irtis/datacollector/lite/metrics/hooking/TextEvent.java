package cz.muni.irtis.datacollector.lite.metrics.hooking;

import android.view.accessibility.AccessibilityNodeInfo;

public class TextEvent {
    private String content;
    private String resource;

    private TextEvent(String content, String resource) {
        this.content = content;
        this.resource = resource;
    }

    static public TextEvent build(AccessibilityNodeInfo node) {
        return new TextEvent(node.getText().toString(), node.getViewIdResourceName());
    }

    static public TextEvent build(String content) {
        return new TextEvent(content, null);
    }

    public String getContent() {
        return content;
    }
}
