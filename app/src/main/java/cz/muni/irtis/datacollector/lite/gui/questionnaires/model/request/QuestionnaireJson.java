package cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class QuestionnaireJson {
    private Long id;
    @SerializedName("questionnaire_id")
    private Long idQuestionnaire;
    private Long created;       // Time when the survey was created by the administrator or system
    private Long updated;       // Time when the survey was updated by the administrator
    private Long starting;      // Time when the survey can be available to the user
    private Long ending;        // Time when the survey cannot be available to the user
    private Long invoking;        // Time when the survey can become available to the user
    private Long closing;       // Time when the survey would be open for the user to answer (closing = ([starting, ending] -> NOW() + validity * 60000)
    private Long notified;      // Time when the survey was notified to the user via notification GUI
    private Long silenced;      // Time when the survey was silenced by the user
    private Long opened;        // Time when the survey was opened by the user
    private Long completed;     // Time when the survey was completed by the user
    private List<QuestionJson> questions;
    private List<AnswerJson> answers;
    private List<NotificationJson> notifications;


    public QuestionnaireJson(
            Long id, Long idQuestionnaire,
            Long created, Long updated, Long starting, Long ending, Long invoking, Long closing, Long notified, Long silenced, Long opened, Long completed,
            List<QuestionJson> questions, List<AnswerJson> answers, List<NotificationJson> notifications) {
        this.id = id;
        this.idQuestionnaire = idQuestionnaire;
        this.created = created;
        this.updated = updated;
        this.starting = starting;
        this.ending = ending;
        this.invoking = invoking;
        this.closing = closing;
        this.notified = notified;
        this.silenced = silenced;
        this.opened = opened;
        this.completed = completed;
        this.questions = questions;
        this.answers = answers;
        this.notifications = notifications;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdQuestionnaire() {
        return idQuestionnaire;
    }

    public void setIdQuestionnaire(Long idQuestionnaire) {
        this.idQuestionnaire = idQuestionnaire;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public Long getStarting() {
        return starting;
    }

    public void setStarting(Long starting) {
        this.starting = starting;
    }

    public Long getEnding() {
        return ending;
    }

    public void setEnding(Long ending) {
        this.ending = ending;
    }

    public Long getInvoking() {
        return invoking;
    }

    public void setInvoking(Long invoking) {
        this.invoking = invoking;
    }

    public Long getClosing() {
        return closing;
    }

    public void setClosing(Long closing) {
        this.closing = closing;
    }

    public Long getNotified() {
        return notified;
    }

    public void setNotified(Long notified) {
        this.notified = notified;
    }

    public Long getSilenced() {
        return silenced;
    }

    public void setSilenced(Long silenced) {
        this.silenced = silenced;
    }

    public Long getOpened() {
        return opened;
    }

    public void setOpened(Long opened) {
        this.opened = opened;
    }

    public Long getCompleted() {
        return completed;
    }

    public void setCompleted(Long completed) {
        this.completed = completed;
    }

    public List<QuestionJson> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionJson> questions) {
        this.questions = questions;
    }

    public List<AnswerJson> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerJson> answers) {
        this.answers = answers;
    }

    public List<NotificationJson> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<NotificationJson> notifications) {
        this.notifications = notifications;
    }

    public String toString() {
        return new GsonBuilder().create().toJson(this, new TypeToken<QuestionnaireJson>(){}.getType());
    }
}
