package cz.muni.irtis.datacollector.lite.gui.bursts;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dinuscxj.progressbar.CircleProgressBar;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewAdapter;

public class BurstsViewHolder extends RecyclerListViewAdapter.ItemsListViewHolderBase {
    public TextView vTitle;
    public TextView vTime;
    public View vPace;
    public TextView vPaceCurrent;
    public TextView vPacePrevious;
    public ImageView vPacePreviousIcon;
    public TextView vCoinsBronze;
    public TextView vCoinsSilver;
    public TextView vCoinsGold;
    public CircleProgressBar vProgress;
    public View vHelp;

    public BurstsViewHolder(RecyclerListViewAdapter adapter, View view) {
        super(adapter, view);
        vTitle = view.findViewById(R.id.title);
        vTime = view.findViewById(R.id.time);
        vPace = view.findViewById(R.id.pace);
        vPaceCurrent = view.findViewById(R.id.paceCurrent);
        vPacePrevious = view.findViewById(R.id.pacePrevious);
        vPacePreviousIcon = view.findViewById(R.id.pacePreviousIcon);
        vCoinsBronze = view.findViewById(R.id.coinsBronze);
        vCoinsSilver = view.findViewById(R.id.coinsSilver);
        vCoinsGold = view.findViewById(R.id.coinsGold);
        vProgress = view.findViewById(R.id.progress);
        vHelp = view.findViewById(R.id.help);
    }
}
