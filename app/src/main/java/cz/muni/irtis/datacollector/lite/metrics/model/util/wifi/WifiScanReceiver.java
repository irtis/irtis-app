package cz.muni.irtis.datacollector.lite.metrics.model.util.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;

public class WifiScanReceiver {
    private Context context;

    static private WifiManager manager;
    static private ScanReceiver receiver = null;
    static private ArrayList<ScanObserver> observers;

    public interface ScanObserver {
        void onObserve(List<ScanResult> results, String connected);
    }


    public WifiScanReceiver(Context context) {
        this.context = context;
        getManager();
    }


    public void scan(ScanObserver observer) {
        if(observer!=null && !observers.isEmpty()) {
            if(observers.get(0)==observer) {
                getManager().startScan();
            }
        }
    }

    public boolean isAttached(ScanObserver observer) {
        if(observers!=null && !observers.isEmpty()) {
            return observers.contains(observer);
        } else {
            return false;
        }
    }

    public void attach(ScanObserver observer) {
        if(observers==null) {
            observers = new ArrayList<>();
        }

        if(receiver==null && observers.isEmpty()) {
            receiver = new ScanReceiver();
            context.registerReceiver(receiver,
                    new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        }

        observers.add(observer);
    }

    public void detach(ScanObserver observer) {
        observers.remove(observer);
        if(receiver!=null && observers.isEmpty()) {
            try {
                context.unregisterReceiver(receiver);
                receiver = null;
            } catch (Exception e) {
                Debug.getInstance().exception(e, this);
            }
        }
    }

    private class ScanReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(intent.getAction())) {
                try {
                    List<ScanResult> results = getManager().getScanResults();
                    String connected = getManager().getConnectionInfo().getSSID().replace("\"", "");

                    Iterator<ScanObserver> i = observers.iterator();
                    while(i.hasNext()) {
                        i.next().onObserve(results, connected);
                    }
                } catch (Exception e) {
                    Debug.getInstance().exception(e);
                }
            }
        }
    }

    private WifiManager getManager() {
        if(manager==null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                manager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            } else {
                manager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            }
        }

        return manager;
    }
}
