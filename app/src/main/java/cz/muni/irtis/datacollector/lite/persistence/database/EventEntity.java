package cz.muni.irtis.datacollector.lite.persistence.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import cz.muni.irtis.datacollector.lite.application.database.EntityBase;

@Entity(
    tableName = EventsRepository.TABLE_NAME
)
public class EventEntity extends EntityBase {
    @PrimaryKey
    @ColumnInfo(name = "id")
    private Long id;
    @ColumnInfo(name = "identifier")
    private Integer identifier;
    @ColumnInfo(name = "time")
    private Long time;
    @ColumnInfo(name = "uploaded")
    private Long uploaded;

    public EventEntity(Long id, Integer identifier, Long time, Long uploaded) {
        this.id = id;
        this.identifier = identifier;
        this.time = time;
        this.uploaded = uploaded;
    }

    @Ignore
    public EventEntity(Integer identifier, Long time) {
        this.identifier = identifier;
        this.time = time;
    }


    public Long getId() {
        return id;
    }

    public Integer getIdentifier() {
        return identifier;
    }

    public Long getTime() {
        return time;
    }

    public Long getUploaded() {
        return uploaded;
    }

    public void setUploaded(Long uploaded) {
        this.uploaded = uploaded;
    }


}
