package cz.muni.irtis.datacollector.lite.metrics.model.phone;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.phone.PhoneCallEntity;
import cz.muni.irtis.datacollector.lite.metrics.model.util.callhistory.CallRecord;
import cz.muni.irtis.datacollector.lite.metrics.Metric;

/**
 * Record call history
 *
 * @deprecated due GooglePlay's location requirements policy which we are not able to meet
 */
public class CallHistory extends Metric {
    private final String TAG = this.getClass().getSimpleName();
    public static final int IDENTIFIER = 249;

    public CallHistory(Context context, Integer delay) {
        super(context, delay);
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "Calls";
    }

    @Override
    public void run() {
        setRunning(true);
        long latestSaved = getMaxCallDate();
        Uri allCalls = Uri.parse("content://call_log/calls");
        Cursor c = getContext().getContentResolver().query(
                allCalls, null, "DATE > " + latestSaved, null, null);

        if (c != null) {
            if (c.getCount() > 0) {
                while (c.moveToNext()) {
                    CallRecord record = new CallRecord();
                    record.setPhoneNumber(c.getString(c.getColumnIndex(CallLog.Calls.NUMBER)));
                    record.setName(c.getString(c.getColumnIndex(CallLog.Calls.CACHED_NAME)));
                    record.setDuration(c.getLong(c.getColumnIndex(CallLog.Calls.DURATION)));
                    record.setType(c.getInt(c.getColumnIndex(CallLog.Calls.TYPE)));
                    record.setCallDate(c.getLong(c.getColumnIndex(CallLog.Calls.DATE)));
                    save(record);
                }
            }
            c.close();
        }
    }

    @Override
    public void stop() {
        setRunning(false);
    }

    @Override
    public long save(Object... params) {
        CallRecord record = (CallRecord) params[0];

        try {
            Debug.getInstance().log(TAG, record.getPhoneNumber());
            return getDatabase().getCallHistoryRepository().insert(new PhoneCallEntity(
                Time.getTime(),
                record.getName(),
                record.getPhoneNumber(),
                record.getType(),
                record.getDuration(),
                record.getCallDate()
            ));
        } catch (Exception e) {
            Debug.getInstance().exception(e);
            return -1;
        }
    }


    /**
     * Get latest call date
     * @return long (INTEGER) milliseconds since epoch
     */
    public long getMaxCallDate() {
        Long time = getDatabase().getCallHistoryRepository().getMaxCallDatetime();
        if (time!=null) {
            return time;
        } else {
            return Time.getTime();
        }
    }
}
