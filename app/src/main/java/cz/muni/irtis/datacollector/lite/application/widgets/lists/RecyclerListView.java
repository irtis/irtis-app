package cz.muni.irtis.datacollector.lite.application.widgets.lists;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import cz.muni.irtis.datacollector.lite.application.utils.Time;


public class RecyclerListView extends RecyclerView {

    public interface OnScrollListener {
        void onScroll(Integer count);
    }
    private OnScrollListener onScrollListener;
    private long scrollingThreshHold;


    public RecyclerListView(@NonNull Context context) {
        super(context);
        ini();
    }

    public RecyclerListView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        ini();
    }

    public RecyclerListView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        ini();
    }

    private void ini() {
        setLayoutManager(new LinearLayoutManager(getContext()));
        scrollingThreshHold = 0;
    }

    public LinearLayoutManager setLayoutManagerVertical() {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        setLayoutManager(manager);

        return manager;
    }

    public void setAdapter(RecyclerListViewAdapter adapter) {
        super.setAdapter(adapter);
        adapter.setListView(this);
    }

    public RecyclerListViewAdapter getAdapter() {
        return (RecyclerListViewAdapter) super.getAdapter();
    }

    public void setOnScrollListener(OnScrollListener listener) {
        this.onScrollListener = listener;
        final LinearLayoutManager manager = this.setLayoutManagerVertical();

        this.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int currentItems = manager.getChildCount();   // number of items that are currently visible on screen
                int scrolledItems = manager.findFirstCompletelyVisibleItemPosition(); // number of items that you have scrolled
                int totalItems = manager.getItemCount(); // total number of items
                boolean isElapsed = (Time.getTime()-scrollingThreshHold)>300;
                if(isElapsed && currentItems + scrolledItems == totalItems) {
                    if(onScrollListener!=null) {
                        scrollingThreshHold = Time.getTime();
                        onScrollListener.onScroll(totalItems);
                    }
                }
            }
        });
    }
}
