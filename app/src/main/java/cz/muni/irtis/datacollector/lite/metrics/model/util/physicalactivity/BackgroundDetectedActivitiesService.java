package cz.muni.irtis.datacollector.lite.metrics.model.util.physicalactivity;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;

/**
 * Register listeners to ActivityRecognitionClient with intent service.
 * https://www.androidhive.info/2017/12/android-user-activity-recognition-still-walking-running-driving-etc/
 */
public class BackgroundDetectedActivitiesService extends Service {
    private static final String TAG = BackgroundDetectedActivitiesService.class.getSimpleName();

    private Intent intentService;
    private PendingIntent pendingIntent;
    private ActivityRecognitionClient activityRecognitionClient;
    private int delay;

    public BackgroundDetectedActivitiesService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Debug.getInstance().task(TAG, "onCreate()");

        activityRecognitionClient = new ActivityRecognitionClient(this);
        intentService = new Intent(this, DetectedActivitiesIntentService.class);
        pendingIntent = PendingIntent
                .getService(this, 1, intentService, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Debug.getInstance().task(TAG, "onCommand()");

        if(intent!=null) {
            delay = intent.getIntExtra("delay", -1);
            if (delay < 0) {
                throw new IllegalStateException("Delay isActive less than 0!");
            }

            requestActivityUpdates();
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Debug.getInstance().task(TAG, "onDestroy()");
        removeActivityUpdates();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Debug.getInstance().task(TAG, "onBind()");
        throw new IllegalStateException("Non-bindable service");
    }

    /**
     * Start listening to activity changes
     */
    public void requestActivityUpdates() {
        Task<Void> task = activityRecognitionClient
            .requestActivityUpdates(delay, pendingIntent)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void result) {
                    Debug.getInstance().log(TAG, "Successfully requested activity updates");
                }})
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Debug.getInstance().log(TAG, "Requesting activity updates failed to start");
                }
            });
    }




    /**
     * Stop listening to activity changes
     */
    public void removeActivityUpdates() {
        Task<Void> task = activityRecognitionClient.removeActivityUpdates(
                pendingIntent);

        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void result) {
                Log.d(TAG, "Removed activity updates successfully!");
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "Failed to remove activity updates!");
            }
        });
    }


}
