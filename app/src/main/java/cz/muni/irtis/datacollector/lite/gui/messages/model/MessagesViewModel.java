package cz.muni.irtis.datacollector.lite.gui.messages.model;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import cz.muni.irtis.datacollector.lite.application.database.Repository;


public class MessagesViewModel extends AndroidViewModel {
    private MessagesModel model;
    MutableLiveData<List<Message>> data;

    public MessagesViewModel(Application application) {
        super(application);
        model = new MessagesModel(application);
        data = new MutableLiveData<List<Message>>();
    }

    public LiveData<List<Message>> getHandler() {
        return data;
    }

    public void getNewest() {
        model.getItemsNewest(new Repository.RepositoryListener<List<Message>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onDone(List<Message> items) {
                data.setValue(items);
            }
        });
    }
//    public void getItems(int count) {
//        getItems(0, count);
//    }
}
