package cz.muni.irtis.datacollector.lite.gui.updates;

import android.app.job.JobParameters;
import android.content.Context;

import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.components.JobServiceBase;

/**
 * UpdatesJob
 *
 * Updates are check exclusively on WIFI once an hour
 */
public class UpdatesJob extends JobServiceBase {
    public static final int JOB_ID = 1586;

    @Override
    public boolean onStartJob(JobParameters params) {
        UpdatesService.start(getApplicationContext());
        return true;
    }

    public static void schedule(Context context) {
        scheduleWithNetwork(context, UpdatesJob.class, JOB_ID, Config.UPDATES_REFRESH_PERIOD);
    }

    public static boolean isSet(Context context) {
        return JobServiceBase.isSet(context, JOB_ID);
    }
}

