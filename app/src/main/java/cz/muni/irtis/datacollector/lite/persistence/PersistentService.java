package cz.muni.irtis.datacollector.lite.persistence;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Build;

import java.util.TimeZone;

import cz.muni.irtis.datacollector.lite.application.components.ServiceBase;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.debug.DebugLogs;
import cz.muni.irtis.datacollector.lite.application.utils.Repeat;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.identity.components.IdentityConfigJob;
import cz.muni.irtis.datacollector.lite.gui.messages.MessagesService;
import cz.muni.irtis.datacollector.lite.gui.messages.components.MessagesJob;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesService;
import cz.muni.irtis.datacollector.lite.metrics.MetricsService;
import cz.muni.irtis.datacollector.lite.metrics.synchronization.components.SynchronizationJob;
import cz.muni.irtis.datacollector.lite.persistence.components.WifiReceiver;
import cz.muni.irtis.datacollector.lite.persistence.components.PersistentNotification;
import cz.muni.irtis.datacollector.lite.persistence.model.AliveWatcher;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.QuestionnairesJob;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.notifications.QuestionnairesNotification;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.updates.UpdateNotification;
import cz.muni.irtis.datacollector.lite.gui.updates.Updates;
import cz.muni.irtis.datacollector.lite.gui.updates.UpdatesJob;
import cz.muni.irtis.datacollector.lite.gui.updates.UpdatesService;
import cz.muni.irtis.datacollector.lite.persistence.runtime.RuntimeApplication;

import static cz.muni.irtis.datacollector.lite.application.Config.PERSISTENCE_REFRESH_PERIOD;

public class PersistentService extends ServiceBase {
    static private PersistentService instance;
    private Repeat hCheckForIssues;
    private PersistentNotification persistentNotification;

    static private WifiReceiver wifiTurnOnReceiver;

    public PersistentService() {
        super(PersistentService.class.getSimpleName());
        instance = this;
    }

    static public PersistentService getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Application was started
        RuntimeApplication.build(getContext()).start();

        // Device signature logging
        Debug.getInstance().task("Device",
                Build.MANUFACTURER + " (" + Build.MODEL + ")" + " | API version: " + Build.VERSION.SDK_INT + " | OS version: " + Build.VERSION.RELEASE);

        // Create service foreground notification
        persistentNotification = PersistentNotification.build(this)
                .start();

        // Send debug logs as soon as possible
        DebugLogs.build(getContext()).upload(); // Just WIFI

        /**
         * List of the most important jobs needed to be sent right now
         */
        // Create alive state watcher
        AliveWatcher.build(getContext())
            .start()
            .send();

        // Checkers
        if(!CheckersJob.isSet(getContext())) {
            Debug.getInstance().task(CheckersJob.class.getSimpleName(), "Job is not set");
            CheckersJob.schedule(getContext());
        } else {
            Debug.getInstance().task(CheckersJob.class.getSimpleName(), "Job is already set");
        }

        // Events
        if(!EventsJob.isSet(getContext())) {
            Debug.getInstance().task(EventsJob.class.getSimpleName(), "Job is not set");
            EventsJob.schedule(getContext());
        } else {
            Debug.getInstance().task(EventsJob.class.getSimpleName(), "Job is already set");
        }


        /**
         * Run services and jobs
         */
        // Synchronization
        if(!SynchronizationJob.isSet(getContext())) {
            Debug.getInstance().task(SynchronizationJob.class.getSimpleName(), "Job is not set");
            SynchronizationJob.schedule(getContext()); // Just WIFI
        } else {
            Debug.getInstance().task(SynchronizationJob.class.getSimpleName(), "Job is already set");
        }

        // Identity config
        if(!IdentityConfigJob.isSet(getContext())) {
            Debug.getInstance().task(IdentityConfigJob.class.getSimpleName(), "Job is not set");
            IdentityConfigJob.schedule(getContext());
        } else {
            Debug.getInstance().task(IdentityConfigJob.class.getSimpleName(), "Job is already set");
        }
        IdentityConfigJob.build().start(getContext());

        // Updates
        if(false && !UpdatesJob.isSet(getContext())) {  // @deprecated Resolved by GooglePlay
            UpdatesJob.schedule(getContext());
            UpdatesService.start(getContext());

            // Show notification if an update is ready
            Updates updates = Updates.build(getContext());
            if(updates.isUpdate()) {
                UpdateNotification.build(getContext()).show();
            }
        }

        // Run Metrics
        if(!MetricsService.isRunning(getContext())) {
            MetricsService.start(getContext());
        }

        // Messages
        if(!MessagesJob.isSet(getContext())) {
            Debug.getInstance().task(MessagesJob.class.getSimpleName(), "Job is not set");
            MessagesJob.schedule(getContext());
            MessagesService.start(getContext());
        } else {
            Debug.getInstance().task(MessagesJob.class.getSimpleName(), "Job is already set");
        }

        // Questionnaires
        if(!QuestionnairesJob.isSet(getContext())) {
            QuestionnairesModel questionnaires = QuestionnairesModel.build(getContext());
            Debug.getInstance().task(QuestionnairesJob.class.getSimpleName(), "Job is not set");

            QuestionnairesService.start(getContext());
//            QuestionnairesJob.schedule(getContext());

            // Show active questionnaires notification
            Questionnaire item = questionnaires.getItemAvailableNewest();
            if (QuestionnairesNotification.isShowable(item)) {
                QuestionnairesNotification.build(getContext(), item).show();
            }
        } else {
            Debug.getInstance().task(QuestionnairesJob.class.getSimpleName(), "Job is already set");
        }



        /**
         * Checks
         * Repeated check on problematic issues
         *
         */
        hCheckForIssues = Repeat.build(PERSISTENCE_REFRESH_PERIOD, new Runnable() {
            @Override
            public void run() {
                // Run metrics service (dealing with the case that it is scheduled for the future)
                boolean isMetricsRunning = MetricsService.isRunning(getContext());
                Debug.getInstance().warning(MetricsService.TAG,
                        "isRunning() =" + (isMetricsRunning ? "true" : "false"));
                if(!isMetricsRunning) {
                    MetricsService.start(getContext());
                } else {
                    MetricsService.should(getContext());
                }

                // This is needed in cases when job is no longer set
//                if(!QuestionnairesJob.isSet(getContext())) {
//                    Debug.getInstance().task(QuestionnairesJob.class.getSimpleName(), "Job is not set");
//                    QuestionnairesJob.schedule(getContext());
//                }
                // We now run questionnaire service from here
//                if(!QuestionnairesService.isRunning(getContext())) {
                    QuestionnairesService.start(getContext());
//                }

                // Check if the notification for the Persistent service still exists
                if (!persistentNotification.isShown()) {
                    persistentNotification.start();
                }

                // Runtime snapshot: save last running time point
                RuntimeApplication.build(PersistentService.this).setLastTime();

                // Monitoring the time zone
                TimeZone zone = TimeZone.getDefault();
                Debug.getInstance().log("TimeZone",
                    zone.getDisplayName(false, TimeZone.SHORT)+" "+
                        "(" +zone.getID()+")");
            }
        }, true);

        // Register on WIFI turn on (changed state) receiver (used for synchronization, and sending debugs)
        registerWifiTurnOnReceiver(getContext());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if(intent!=null && intent.getAction()!=null) {
            // @todo any needed actions
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(hCheckForIssues !=null) {
            hCheckForIssues.cancel();
        }

        unRegisterWifiTurnOnReceiver(getContext());

        if(MetricsService.isRunning(getContext())) {
            MetricsService.stop(getContext());
        }

        AliveWatcher.getInstance().stop();

        // Application was ended
        RuntimeApplication.build(getContext()).stop();
    }


    static public boolean isRunning(Context context) {
        return ServiceBase.isServiceRunning(context, PersistentService.class);
    }

    public static void start(Context context) {
        ServiceBase.startService(context, PersistentService.class, true);
    }

    public static void stop(Context context) {
        ServiceBase.stopService(context, new Intent(context, PersistentService.class), PersistentService.class);
    }

    public PersistentNotification getNotification() {
        return persistentNotification;
    }




    private void registerWifiTurnOnReceiver(Context context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        wifiTurnOnReceiver = new WifiReceiver();
        context.registerReceiver(wifiTurnOnReceiver, intentFilter);
    }

    private void unRegisterWifiTurnOnReceiver(Context context) {
        if(wifiTurnOnReceiver!=null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
            context.unregisterReceiver(wifiTurnOnReceiver);
        }
    }

    public Context getContext() {
        return PersistentService.getInstance();
    }
}
