package cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.EntityBase;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.WifiRepository;

@Entity(tableName = WifiRepository.TABLE_NAME)
public class WifiEntity extends EntityBase {
    @ColumnInfo(name = "ssid")
    private String ssid;

    public WifiEntity(Long datetime, String ssid) {
        super(datetime);
        this.ssid = ssid;
    }

    public String getSsid() {
        return ssid;
    }
}
