package cz.muni.irtis.datacollector.lite.application.permissions;

import android.content.Context;

import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_ADVANCED_BATTERY_OPTIMIZATION;
import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_AUTOSTART;


public class PermissionAdvancedBatteryOptimization extends Permission {

    public PermissionAdvancedBatteryOptimization(Context context, String title, String notes)
    {
        super(context, PERMISSION_ADVANCED_BATTERY_OPTIMIZATION, title, notes);
    }

    public boolean isGranted()
    {
        return provider.isGrantedAdvancedBatteryOptimizationPermission();
    }

    public void setGranted()
    {
        provider.setGrantedAdvancedBatteryOptimizationPermission();
    }
}
