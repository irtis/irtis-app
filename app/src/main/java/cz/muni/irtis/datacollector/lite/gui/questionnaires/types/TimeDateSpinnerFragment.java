package cz.muni.irtis.datacollector.lite.gui.questionnaires.types;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.NumberPicker;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;

public class TimeDateSpinnerFragment extends TypeFragmentBase {
    protected NumberPicker vDayPicker;
    protected NumberPicker vMonthPicker;
    protected NumberPicker vYearPicker;
    protected EditText vText;

    public TimeDateSpinnerFragment(Context context, Question question, boolean editable) {
        super(context, question, R.layout.activity_questionnaires_form_type_time_date, editable);
    }

    public TimeDateSpinnerFragment(Context context, Question question, int contentLayoutId, boolean editable) {
        super(context, question, contentLayoutId, editable);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        vDayPicker = iniDay((NumberPicker) view.findViewById(R.id.day));
        vMonthPicker = iniMonth((NumberPicker) view.findViewById(R.id.month));
        vYearPicker = iniYear((NumberPicker) view.findViewById(R.id.year));


        vDayPicker.setEnabled(isEditable());
        vDayPicker.setVisibility(isEditable() ? View.VISIBLE : View.GONE);
        vMonthPicker.setEnabled(isEditable());
        vMonthPicker.setVisibility(isEditable() ? View.VISIBLE : View.GONE);
        vYearPicker.setEnabled(isEditable());
        vYearPicker.setVisibility(isEditable() ? View.VISIBLE : View.GONE);

        if (getQuestion().getAnswer().isValue()) {
            String[] date = getQuestion().getAnswer().getValue().split(".");
            vDayPicker.setValue(Integer.valueOf(date[0]));
            vMonthPicker.setValue(Integer.valueOf(date[1]));
            vYearPicker.setValue(Integer.valueOf(date[2]));
        }

        vDayPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                Answer answer = getQuestion().getAnswer();
                answer.setValue(
                        String.format("%02d", vDayPicker.getValue()) + "." +
                                String.format("%02d", vMonthPicker.getValue()) + "." +
                                String.format("%02d", vYearPicker.getValue())
                );
                refresh();
            }
        });
        vMonthPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                Answer answer = getQuestion().getAnswer();
                answer.setValue(
                        String.format("%02d", vDayPicker.getValue()) + "." +
                                String.format("%02d", vMonthPicker.getValue()) + "." +
                                String.format("%02d", vYearPicker.getValue())
                );
                refresh();
            }
        });
        vYearPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                Answer answer = getQuestion().getAnswer();
                answer.setValue(
                        String.format("%02d", vDayPicker.getValue()) + "." +
                                String.format("%02d", vMonthPicker.getValue()) + "." +
                                String.format("%02d", vYearPicker.getValue())
                );
                refresh();
            }
        });


        vText = view.findViewById(R.id.text);
        vText.setVisibility(isEditable() ? View.GONE : View.VISIBLE);
        vText.setEnabled(false);
        if(getQuestion().getAnswer().isValue()) {
            vText.setText(getQuestion().getAnswer().getValue());
        }

        return view;
    }


    private NumberPicker iniDay(NumberPicker view) {
        view.setWrapSelectorWheel(true);
        view.setMinValue(1);
        view.setMaxValue(31);
        view.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
            }
        });
        return view;
    }

    private NumberPicker iniMonth(NumberPicker view) {
        view.setWrapSelectorWheel(true);
        view.setMinValue(1);
        view.setMaxValue(12);
        view.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
            }
        });
        return view;
    }

    private NumberPicker iniYear(NumberPicker view) {
        view.setWrapSelectorWheel(true);
        view.setMinValue(1940);
        view.setMaxValue(Integer.valueOf(Time.getTimeStamp("YYYY")) - 12);
        view.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
            }
        });
        return view;
    }
}
