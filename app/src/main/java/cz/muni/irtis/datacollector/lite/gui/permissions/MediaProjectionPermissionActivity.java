package cz.muni.irtis.datacollector.lite.gui.permissions;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;
import cz.muni.irtis.datacollector.lite.metrics.mediaprojection.MediaProjectionProvider;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.metrics.MetricsService;

/**
 * MediaProjectionPermissionActivity
 *
 * Invisible activity used for getting MediaProjection access token.
 *
 * This isActive shown in cases when the service has no media projection access token
 * and needs to ask the user to provide it.
 *
 * Wee need to use an invisible activity in order to receive onActivityResult() to process
 * the access token.
 *
 * NOTES:
 * Android Q privacy change: needs a special permission (https://developer.android.com/preview/privacy/background-activity-starts)
 */
public class MediaProjectionPermissionActivity extends ActivityBase {
    private MediaProjectionProvider provider;
    private AlertDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);

        provider = MediaProjectionProvider.getInstance();

        if(!Settings.getSettings().getBoolean(Settings.MEDIAPROJECTION_PERMISSION_ACTIVITY_DO_NOT_SHOW, false)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.permissions_mediaprojection))
                .setIcon(R.mipmap.ic_launcher_round)
                .setMessage(Html.fromHtml(getString(R.string.permissions_mediaprojection_dialog).replace("{appname}", getString(R.string.app_name))))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        provider.request(MediaProjectionPermissionActivity.this);
                    }
                }).setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            finish();
                        }
                        return true;
                    }
                })
                .setView(R.layout.activity_mediaprojection_permission_dialog)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            finish();
                        }
                    });
//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//                builder.setMessage(Html.fromHtml(getString(R.string.permissions_mediaprojection_dialog).replace("{appname}", getString(R.string.app_name)), Html.FROM_HTML_MODE_LEGACY));
//            } else {
//                builder.setMessage(Html.fromHtml(getString(R.string.permissions_mediaprojection_dialog).replace("{appname}", getString(R.string.app_name))))
//            }

            dialog = builder.create();
            dialog.show();

            ((CheckBox) dialog.findViewById(R.id.remember)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    SharedPreferences.Editor editor = Settings.getSettings().edit();
                    editor.putBoolean(Settings.MEDIAPROJECTION_PERMISSION_ACTIVITY_DO_NOT_SHOW, isChecked);
                    editor.commit();
                }
            });
        } else {
            provider.request(MediaProjectionPermissionActivity.this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    static public void start(Context context)  {
        if(!isActivityRunning(context, MediaProjectionPermissionActivity.class)) {
            Intent intent = new Intent(context, MediaProjectionPermissionActivity.class);
            intent.setFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS
            );
            context.startActivity(intent);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(provider.onActivityResult(requestCode, resultCode, data)) {
            MetricsService.startMetricScreenshots(this);
        }
        finish();
    }
}
