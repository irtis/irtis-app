package cz.muni.irtis.datacollector.lite.persistence.components;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.service.notification.StatusBarNotification;

import androidx.core.app.NotificationCompat;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.NotificationBase;
import cz.muni.irtis.datacollector.lite.gui.LauncherActivity;

public class StorageInsufficientNotification extends NotificationBase {
    private static final int NOTIFICATION_IDENTIFIER = 69173;

    private NotificationCompat.Builder builder;

    private StorageInsufficientNotification(Context context) {
        super(context);

    }

    static public StorageInsufficientNotification build(Context context) {
        return new StorageInsufficientNotification(context);
    }

    @Override
    public Notification create() {
        PendingIntent intent = PendingIntent.getActivity(context, NOTIFICATION_IDENTIFIER,
                new Intent(context, LauncherActivity.class), 0);

        builder = new NotificationCompat.Builder(context, CHANNEL_LOW)
            .setSmallIcon(R.drawable.ic_sd_storage_black_24dp)
            .setSubText(context.getString(R.string.persistence_notification_storage_type))
            .setContentTitle(context.getString(R.string.persistence_notification_storage_title))
            .setContentText(context.getString(R.string.persistence_notification_storage_text))
            .setCategory(NotificationCompat.CATEGORY_ERROR)
            .setContentIntent(intent)
            .setAutoCancel(true)
            .setOnlyAlertOnce(true);

        notification = builder.build();

        return notification;
    }

    public void show() {
        manager.notify(NOTIFICATION_IDENTIFIER, create());
    }

    public void hide() {
        if(notification!=null){
            manager.cancel(NOTIFICATION_IDENTIFIER);
        }
    }

    /**
     * isShown()
     *
     * @warning this properly works only for >= API23, otherwise it is rather difficult to get shown notifications
     * @param context
     * @return
     */
    static public boolean isShown(Context context) {
        boolean isShown = false;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            StatusBarNotification[] notifications = mNotificationManager.getActiveNotifications();
            for (StatusBarNotification notification : notifications) {
                if (notification.getId() == NOTIFICATION_IDENTIFIER) {
                    isShown = true;
                    break;
                }
            }
        }

        return isShown;
    }
}