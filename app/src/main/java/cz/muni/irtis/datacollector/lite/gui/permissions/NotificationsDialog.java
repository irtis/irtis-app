package cz.muni.irtis.datacollector.lite.gui.permissions;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;

import androidx.appcompat.app.AlertDialog;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.permissions.Permission;

public class NotificationsDialog extends PermissionDialogBase {

    public NotificationsDialog(Context context, Permission permission)
    {
        super(context, permission, NotificationsDialog.class.getSimpleName());
    }

    @Override
    protected AlertDialog onBuilderCreate(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getContext().getString(R.string.permissions_notifications))
                .setIcon(R.drawable.ic_notifications_black_24dp)
                .setPositiveButton("OK", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                        getContext().startActivity(intent);
                    }
                });
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            builder.setMessage(Html.fromHtml(getContext().getString(R.string.permissions_notifications_dialog).replace("{appname}", getContext().getString(R.string.app_name)), Html.FROM_HTML_MODE_LEGACY));
        } else {
            builder.setMessage(Html.fromHtml(getContext().getString(R.string.permissions_notifications_dialog).replace("{appname}", getContext().getString(R.string.app_name))));
        }
        return builder.create();
    }
}