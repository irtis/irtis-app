package cz.muni.irtis.datacollector.lite.application.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.io.File;

import cz.muni.irtis.datacollector.lite.gui.messages.database.MessageEntity;
import cz.muni.irtis.datacollector.lite.gui.messages.database.MessagesRepository;
import cz.muni.irtis.datacollector.lite.persistence.database.BurstEntity;
import cz.muni.irtis.datacollector.lite.persistence.database.BurstsRepository;
import cz.muni.irtis.datacollector.lite.persistence.database.EventEntity;
import cz.muni.irtis.datacollector.lite.persistence.database.EventsRepository;

/**
 * Room ORM DB access class
 *
 * Documentation:
 *  - https://developer.android.com/training/data-storage/room
 *  - https://developer.android.com/reference/android/arch/persistence/room/package-summary
 *
 */
@Database(
    version = 1,
    entities = {
        MessageEntity.class,
        BurstEntity.class,
        EventEntity.class,
    },
    exportSchema = false
)
public abstract class StorageDatabase extends RoomDatabase {
    private static volatile StorageDatabase INSTANCE;

    public abstract MessagesRepository getMessagesRepository();
    public abstract BurstsRepository getBurstsRepository();
    public abstract EventsRepository getEventsRepository();


    public static StorageDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (StorageDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    StorageDatabase.class, "storage.db")
                    .allowMainThreadQueries()
                    .build();
                }
            }
        }
        return INSTANCE;
    }

    public long getSize() {
        return getFile().length();
    }

    public File getFile() {
        return new File(getOpenHelper().getReadableDatabase().getPath());
    }
}
