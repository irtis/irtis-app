package cz.muni.irtis.datacollector.lite.persistence;

import android.app.job.JobParameters;
import android.content.Context;

import cz.muni.irtis.datacollector.lite.application.components.JobServiceBase;

import static cz.muni.irtis.datacollector.lite.application.Config.PERSISTENCE_ALERTS_REFRESH_PERIOD;

public class CheckersJob extends JobServiceBase {
    public static final int JOB_ID = 15678;

    @Override
    public boolean onStartJob(JobParameters params) {
        CheckersService.start(getApplicationContext());
        return true;
    }

    public static void schedule(Context context) {
        schedule(context, CheckersJob.class, JOB_ID, PERSISTENCE_ALERTS_REFRESH_PERIOD);
    }

    public static boolean isSet(Context context) {
        return JobServiceBase.isSet(context, JOB_ID);
    }

    public static void cancel(Context context) {
        JobServiceBase.cancel(context, JOB_ID);
    }
}

