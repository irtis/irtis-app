package cz.muni.irtis.datacollector.lite.persistence;

import android.app.job.JobParameters;
import android.content.Context;

import cz.muni.irtis.datacollector.lite.application.components.JobServiceBase;

import static cz.muni.irtis.datacollector.lite.application.Config.PERSISTENCE_ALIVE_REFRESH_PERIOD;

public class AliveWatcherJob extends JobServiceBase {
    public static final int JOB_ID = 59130;

    @Override
    public boolean onStartJob(JobParameters params) {
        AliveWatcherService.start(getApplicationContext());
        return true;
    }

    public static void schedule(Context context) {
        scheduleWithNetwork(context, AliveWatcherJob.class, JOB_ID, PERSISTENCE_ALIVE_REFRESH_PERIOD);
    }

    public static boolean isSet(Context context) {
        return JobServiceBase.isSet(context, JOB_ID);
    }

    public static void cancel(Context context) {
        JobServiceBase.cancel(context, JOB_ID);
    }
}

