package cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.json.Exclude;

public class Label {
    @Exclude
    private Question question;

    @SerializedName("multi_slider_label_id")
    private Long id;
    private String text;
    private List<Answer> answers;


    public Question getQuestion() {
        return question;
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public List<Answer> getAnswers() {
        return answers;
    }
}
