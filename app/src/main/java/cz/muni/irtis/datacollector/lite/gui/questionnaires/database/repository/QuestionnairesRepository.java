package cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.room.Update;
import androidx.sqlite.db.SupportSQLiteQuery;

import java.util.List;

import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionnaireEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;

@Dao
public interface QuestionnairesRepository {
    public static final String TABLE_NAME = "questionnaires";

    @RawQuery
    List<QuestionnaireEntity> getItemsByQuery(SupportSQLiteQuery query);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void store(QuestionnaireEntity item);

    @Insert
    long insert(QuestionnaireEntity item);

    @Update
    void update(QuestionnaireEntity item);

    @Query("SELECT MAX(id) FROM " + TABLE_NAME)
    Long getIdMax();

    // All items
    @Query("SELECT * FROM " + TABLE_NAME + " ORDER BY starting DESC")
    List<QuestionnaireEntity> getItems();

    @Query("SELECT * FROM " + TABLE_NAME + " ORDER BY starting DESC LIMIT :offset, :count")
    List<QuestionnaireEntity> getItems(int offset, int count);

    @Query("SELECT COUNT(*) FROM " + TABLE_NAME)
    Integer getItemsCount();

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE id = :id")
    QuestionnaireEntity getItem(Long id);

    @Query("SELECT * FROM " + TABLE_NAME + " ORDER BY closing DESC LIMIT 1")
    QuestionnaireEntity getItemLastByClosing();

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE people_questionnaire_id = :id")
    QuestionnaireEntity getItemByPersonQuestionnaireId(Long id);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE identifier = :identifier ORDER BY starting ASC")
    List<QuestionnaireEntity> getItemsByIdentifier(String identifier);


    // Get items to be invoked
    @Query("SELECT * FROM " + TABLE_NAME + " WHERE completed IS NULL AND ((starting <= :time AND ending >= :time) OR invoking <= :time)  ORDER BY starting DESC")
    List<QuestionnaireEntity> getItemsToInvoke(Long time);

    @Query("SELECT COUNT(*) FROM " + TABLE_NAME + " WHERE completed IS NULL AND (starting <= :time AND ending >= :time)")
    Integer getItemsToInvokeCount(Long time);


    // Available
    @Query("SELECT * FROM " + TABLE_NAME + " WHERE completed IS NULL AND ((starting <= :time AND ending >= :time) OR closing > :time) ORDER BY starting DESC")
    List<QuestionnaireEntity> getItemsAvailable(Long time);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE completed IS NULL AND ((starting <= :time AND ending >= :time) OR closing > :time) ORDER BY starting DESC LIMIT :offset, :count")
    List<QuestionnaireEntity> getItemsAvailable(Long time, int offset, int count);

    @Query("SELECT COUNT(*) FROM " + TABLE_NAME + " WHERE completed IS NULL AND ((starting <= :time AND ending >= :time) OR closing > :time)")
    Integer getItemsAvailableCount(Long time);


    // Newest
    @Query("SELECT * FROM " + TABLE_NAME + " WHERE completed IS NULL AND ((starting <= :time AND ending >= :time) OR closing > :time) ORDER BY closing DESC")
    List<QuestionnaireEntity> getItemsAvailableNewest(Long time);

    @Query("SELECT COUNT(*) FROM " + TABLE_NAME + " WHERE completed IS NULL AND ((starting <= :time AND ending >= :time) OR closing > :time)")
    Integer getItemsAvailableNewestCount(Long time);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE completed IS NULL AND ((starting <= :time AND ending >= :time) OR closing > :time) ORDER BY closing DESC LIMIT 1")
    QuestionnaireEntity getItemAvailableNewest(Long time);


    // Future
    @Query("SELECT * FROM " + TABLE_NAME + " WHERE configurable = 1 AND identifier = :identifier AND starting > :time ORDER BY starting ASC")
    List<QuestionnaireEntity> getItemsFutureByIdentifier(String identifier, Long time);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE configurable = 1 AND type = "+ QuestionnairesModel.TYPE_REGULAR + " AND starting > :time ORDER BY identifier ASC, starting ASC")
    List<QuestionnaireEntity> getItemsFuture(Long time);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE configurable = 1 AND type = "+ QuestionnairesModel.TYPE_REGULAR + " AND starting > :time ORDER BY identifier ASC, starting ASC LIMIT :offset, :count")
    List<QuestionnaireEntity> getItemsFuture(Long time, int offset, int count);

    /**
     * getItemsFutureByWeekDay()
     *
     * day of week 0-6 with Sunday==0
     *
     * @param time
     * @param day
     * @return
     */
    @Query("SELECT * FROM " + TABLE_NAME + " WHERE configurable = 1 AND type = "+ QuestionnairesModel.TYPE_REGULAR +" AND starting > :time AND cast(strftime('%w', datetime(starting/1000, 'unixepoch')) as decimal) = :day GROUP BY identifier ORDER BY starting ASC")
    List<QuestionnaireEntity> getItemsFutureByWeekDay(Long time, Integer day);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE configurable = 1 AND type = "+ QuestionnairesModel.TYPE_REGULAR +" AND starting > :time AND cast(strftime('%w', datetime(starting/1000, 'unixepoch')) as decimal) = :day GROUP BY identifier ORDER BY starting ASC")
    QuestionnaireEntity getItemFutureFirstByWeekDay(Long time, Integer day);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE configurable = 1 AND type = "+ QuestionnairesModel.TYPE_REGULAR +" AND starting > :time AND cast(strftime('%w', datetime(starting/1000, 'unixepoch')) as decimal) = :day GROUP BY identifier ORDER BY starting DESC")
    QuestionnaireEntity getItemFutureLastByWeekDay(Long time, Integer day);


    // Finished
    @Query("SELECT * FROM " + TABLE_NAME + " WHERE ending < :time ORDER BY starting DESC")
    List<QuestionnaireEntity> getItemsPassed(Long time);

    @Query("SELECT COUNT(*) FROM " + TABLE_NAME + " WHERE ending < :time")
    Integer getItemsPassedCount(Long time);




    /**
     *
     * Uploaded questionnaires are with one of met condition:
     *  - with ended time frame (ending)
     *  - those that are closed - NULL or expired (closing)
     *  - those that are completed
     *
     * @param time
     * @return
     */
    @Query("SELECT * FROM " + TABLE_NAME + " WHERE uploaded IS NULL AND (ending <= :time OR closing <= :time OR completed IS NOT NULL) ORDER BY created DESC")
    List<QuestionnaireEntity> getNotUploaded(Long time);


    @Query("DELETE FROM " + TABLE_NAME + " WHERE starting > :time")
    void deleteFuture(Long time);
}
