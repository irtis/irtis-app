package cz.muni.irtis.datacollector.lite.gui.settings;

import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

import cz.muni.irtis.datacollector.R;

public class QuestionnairesFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.quesionnaires_screen, rootKey);
    }
}