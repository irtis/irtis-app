package cz.muni.irtis.datacollector.lite.gui.questionnaires.components.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesFormActivity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesService;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.QuestionnairesPostponeActivity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.SilenceJob;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.persistence.model.Events;

/**
 * QuestionnairesNotificationBroadcastReceiver
 */
public class QuestionnairesNotificationBroadcastReceiver extends BroadcastReceiver {
    static public final String ACTION_POSTPONE = "postpone";
    static public final String ACTION_SILENCE = "silence";

    @Override
    public void onReceive(final Context context, Intent intent) {
        final QuestionnairesModel model = QuestionnairesModel.build(context);
        final Long id = intent.getLongExtra(QuestionnairesFormActivity.PARAM_ITEM_ID, -1);
        if(id!=null) {
            Repository.RepositoryListener<Questionnaire> listener = null;
            if(intent.getAction().equals(ACTION_POSTPONE)) {
                listener = new Repository.RepositoryListener<Questionnaire>() {
                    @Override
                    public void onStart() {
                    }
                    @Override
                    public void onDone(Questionnaire item) {
                        if(item!=null) {
                            QuestionnairesPostponeActivity.startActivity(context, id);
                            Events.build(context).log(Events.EVENT_QUESTIONNAIRES_POSTPONED);
                            Debug.getInstance().warning(QuestionnairesService.TAG,
                                    "Notification postponed | Value: "+(item.getIdentifier()!=null ? item.getIdentifier() : "null"));
                        }
                    }
                };
            } else if(intent.getAction().equals(ACTION_SILENCE)) {
                listener = new Repository.RepositoryListener<Questionnaire>() {
                    @Override
                    public void onStart() {
                    }
                    @Override
                    public void onDone(Questionnaire item) {
                        if(item!=null) {
                            QuestionnairesNotification.cancel(context, item);
                            SilenceJob.schedule(context, item);
                            if(item.getSilenced()==null) {
                                model.setSilenced(item, Time.getTime());
                            }
                            model.setSilencedHistory(item, Time.getTime());
                            Events.build(context).log(Events.EVENT_QUESTIONNAIRES_SILENCED);
                            Debug.getInstance().warning(QuestionnairesService.TAG,
                                    "Notification silenced | Value: "+(item.getIdentifier()!=null ? item.getIdentifier() : "null"));
                        }
                    }
                };
            }

            if(listener!=null) {
                model.getItem(id, listener);
            }
        }

    }
}
