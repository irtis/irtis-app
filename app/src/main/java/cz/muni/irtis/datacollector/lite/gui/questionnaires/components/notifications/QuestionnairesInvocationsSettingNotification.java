package cz.muni.irtis.datacollector.lite.gui.questionnaires.components.notifications;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.service.notification.StatusBarNotification;

import androidx.core.app.NotificationCompat;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.NotificationBase;
import cz.muni.irtis.datacollector.lite.gui.MainActivity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesFragment;

public class QuestionnairesInvocationsSettingNotification extends NotificationBase {
    private static final int NOTIFICATION_IDENTIFIER = 1448;
    private NotificationCompat.Builder builder;

    public QuestionnairesInvocationsSettingNotification(Context context) {
        super(context);
    }

    static public QuestionnairesInvocationsSettingNotification build(Context context) {
        return new QuestionnairesInvocationsSettingNotification(context);
    }


    public boolean isShown() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            StatusBarNotification[] items = manager.getActiveNotifications();
            if (items != null && items.length > 0) {
                for (StatusBarNotification item : items) {
                    if (item.getId() == NOTIFICATION_IDENTIFIER) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @Override
    public Notification create() {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(MainActivity.PARAM_SELECTED_FRAGMENT, QuestionnairesFragment.class.getSimpleName());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pending = PendingIntent.getActivity(
                context, NOTIFICATION_IDENTIFIER, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder = new NotificationCompat.Builder(context, CHANNEL_PERSISTENT)
            .setSmallIcon(R.drawable.ic_questionnaires_grey_24dp)
            .setSubText(context.getString(R.string.questionnaires))
            .setContentTitle(context.getString(R.string.messagesNew))
            .setContentTitle(context.getString(R.string.questionnaires_invocations_setting_notification_title))
            .setContentText(context.getString(R.string.questionnaires_invocations_setting_notification_notes))
            .setContentIntent(pending)
            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
            .setStyle(new NotificationCompat.BigTextStyle()
                    .bigText(context.getString(R.string.questionnaires_invocations_setting_notification_notes)))
            .setAutoCancel(true)
            .setOnlyAlertOnce(true);

        return notification = builder.build();
    }

    public void show() {
        manager.notify(NOTIFICATION_IDENTIFIER, create());
    }

    public void hide() {
        if(notification!=null){
            manager.cancel(NOTIFICATION_IDENTIFIER);
        }
    }

}
