package cz.muni.irtis.datacollector.lite.application.network;

import android.util.Pair;

import com.google.common.reflect.Parameter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Identity;

public class UrlComposer {
//    static public String BASE_URL = Debug.isDebug() ?
//            Config.SERVER_URL_BASE_DEBUG : Config.SERVER_URL_BASE_RELEASE;
    static public String BASE_URL = Config.SERVER_URL_BASE_RELEASE;
//    static public String BASE_URL = Config.SERVER_URL_BASE_DEBUG;


    static public String compose(String endpoint, String token) {
        endpoint = endpoint.replaceAll("^\\/|\\/$", "");
        return BASE_URL + "/" + endpoint + (token!=null ? "?token=" + token : "");
    }

    static public String compose(String endpoint) {
        return compose(endpoint, Identity.getInstance().isToken() ? Identity.getInstance().getToken() : null);
    }


    static public String compose(String endpoint, List<Pair<String, String>> params, String token) {
        endpoint = endpoint.replaceAll("^\\/|\\/$", "");
        StringBuilder sParams = new StringBuilder();


        params = params!=null ? new ArrayList<Pair<String, String>>(params) :
                new ArrayList<Pair<String, String>>();
        params.add(new Pair<String, String>("token", token));

        Iterator<Pair<String, String>> iParams = params.iterator();
        while(iParams.hasNext()) {
            Pair<String, String> param = iParams.next();
            sParams.append((sParams.length()>0 ? "&" : "")+param.first+"="+param.second);
        }

        return BASE_URL + "/" + endpoint + (!params.isEmpty() ? "?" : "") + sParams.toString();
    }

    static public String compose(String endpoint, List<Pair<String, String>> params) {
        return compose(endpoint, params, Identity.getInstance().isToken() ? Identity.getInstance().getToken() : null);
    }
}
