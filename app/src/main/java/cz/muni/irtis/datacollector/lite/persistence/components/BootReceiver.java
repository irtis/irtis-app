package cz.muni.irtis.datacollector.lite.persistence.components;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import cz.muni.irtis.datacollector.lite.application.Applications;
import cz.muni.irtis.datacollector.lite.persistence.PersistentService;

/**
 * BootReceiver
 *
 * Starts the PersistentService when the system boots up
 */
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (!PersistentService.isRunning(context)) {
            PersistentService.start(context);
            Toast.makeText(context, Applications.getApplicationInfo(context).name+": service started", Toast.LENGTH_LONG).show();
        }
    }
}
