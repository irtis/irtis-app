package cz.muni.irtis.datacollector.lite.metrics.database.entities.screen;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.EntityBase;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ScreenshotsMetadataRepository;

@Entity(tableName = ScreenshotsMetadataRepository.TABLE_NAME)
public class ScreenshotMetadataEntity extends EntityBase {
    @ColumnInfo(name = "filename")
    @SerializedName("filename")
    private String filename;
    @ColumnInfo(name = "metadata")
    @SerializedName("metadata")
    private String metadata;

    public ScreenshotMetadataEntity(Long datetime, String filename, String metadata) {
        super(datetime);
        this.filename = filename;
        this.metadata = metadata;
    }

    public String getFilename() {
        return filename;
    }

    public String getMetadata() {
        return metadata;
    }
}
