package cz.muni.irtis.datacollector.lite.application;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.Application;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.Metric;
import cz.muni.irtis.datacollector.lite.metrics.manager.MetricDeclaration;
import cz.muni.irtis.datacollector.lite.metrics.manager.MetricsManager;
import cz.muni.irtis.datacollector.lite.metrics.model.BatteryState;
import cz.muni.irtis.datacollector.lite.metrics.model.Headphones;
import cz.muni.irtis.datacollector.lite.metrics.model.Location;
import cz.muni.irtis.datacollector.lite.metrics.model.Notifications;
import cz.muni.irtis.datacollector.lite.metrics.model.PhysicalActivity;
import cz.muni.irtis.datacollector.lite.metrics.model.Playback;
import cz.muni.irtis.datacollector.lite.metrics.model.Steps;
import cz.muni.irtis.datacollector.lite.metrics.model.applications.BackgroundApplication;
import cz.muni.irtis.datacollector.lite.metrics.model.applications.ForegroundApplication;
import cz.muni.irtis.datacollector.lite.metrics.model.applications.ForegroundApplication2;
import cz.muni.irtis.datacollector.lite.metrics.model.screen.Screen;
import cz.muni.irtis.datacollector.lite.metrics.model.screen.ScreenTap;
import cz.muni.irtis.datacollector.lite.metrics.model.wifi.AvailableWifi;
import cz.muni.irtis.datacollector.lite.metrics.model.wifi.ConnectedWifi;

public class Config {
    /**
     * Server identification
     */
    static public final String SERVER_APPLICATION_IDENTIFIER = Application.getInstance().getApplicationPackage().packageName;
    static public final String SERVER_APPLICATION_KEY = Debug.isDebug() ? "K9RrLvnxdFpqB2YZYe7RnJZTt7dcQcDFKD2HM8a7rTz3TpRknh2hdt54THUm" :
            "YNs5A6a4gmGS3KFQDMmkxSKzYNTam8GZqX6hggj7HHZUmny5hyA74jAjerkd";
    /**
     * Server URL base
     */
    public static final String SERVER_URL_BASE_RELEASE = "https://irtis.fi.muni.cz:8080";       // irtis-server
//    public static final String SERVER_URL_BASE_DEBUG = "https://irtis.fi.muni.cz:8080";       	// irtis-server
    public static final String SERVER_URL_BASE_DEBUG = "https://10.0.2.2:8080";               // irtis-local
//    public static final String SERVER_URL_BASE_DEBUG = "https://192.168.137.135:8080";          // irtis-local-device
//    public static final String SERVER_URL_BASE_DEBUG = "http://10.0.2.2:666";                	// mockoon-local

    /**
     * Service use links
     */
    public static final String SERVICE_PRIVACY_POLICY = "https://irtis.muni.cz/mladi-na-mobilu#tab-3";
    public static final String SERVICE_ABOUT = "https://irtis.muni.cz/mladi-na-mobilu";

    /**
     * Application start requests
     */
    public static final boolean REQUEST_PERMISSIONS = true;
    public static final boolean REQUEST_INTEGRATIONS = false;


    /**
     * Metrics
     */
    // List of allowed metrics (comment to disable)
    private List<MetricDeclaration> metrics = Arrays.asList(new MetricDeclaration[] {
//        MetricDeclaration.build(Location.class, MetricsManager.SECOND*5), // Optimized
        MetricDeclaration.build(PhysicalActivity.class, MetricsManager.SECOND*5), // Optimized
        MetricDeclaration.build(BatteryState.class, MetricsManager.SECOND*2), // Optimized
        MetricDeclaration.build(AvailableWifi.class, MetricsManager.MINUTE), // Optimized
        MetricDeclaration.build(ConnectedWifi.class, MetricsManager.SECOND*30), // Optimized
//        MetricDeclaration.build(CallHistory.class, MetricsManager.MINUTE*10),
//        MetricDeclaration.build(SmsConversation.class, MetricsManager.MINUTE*10),
        MetricDeclaration.build(Screen.class, MetricsManager.SECOND),
        MetricDeclaration.build(ForegroundApplication.class, MetricsManager.SECOND),
//        MetricDeclaration.build(ForegroundApplication2.class, MetricsManager.SECOND),
        MetricDeclaration.build(BackgroundApplication.class, MetricsManager.SECOND*5),
        MetricDeclaration.build(ScreenTap.class, MetricsManager.SECOND, true),
        MetricDeclaration.build(Headphones.class, MetricsManager.SECOND*2),
        MetricDeclaration.build(Playback.class, MetricsManager.SECOND),
        MetricDeclaration.build(Steps.class, MetricsManager.SECOND, true),
        MetricDeclaration.build(Notifications.class, MetricsManager.SECOND, true)
    });
    public static Integer METRICS_SCREENSHOTS_PERIOD = (int) Time.SECOND*5;
    public static Integer METRICS_SCREENSHOTS_REMINDER_PERIOD = (int) Time.HOUR*5;
    // Minutes, deals with withdrawn/lost screenshots permissions
    public static Long METRICS_AUTORUN_PERIOD = Time.SECOND*60;
    // Bursts decide if capture is running (if false manual turn on is enabled)
    public static Boolean METRICS_BURSTS_ENABLED = Debug.isDebug() ? false : true;

    /**
     * Persisetence
     */
    public static Long PERSISTENCE_REFRESH_PERIOD = Time.MINUTE; // minute
    public static Integer PERSISTENCE_ALERTS_REFRESH_PERIOD = 5; // minutes
    public static Integer PERSISTENCE_ALIVE_REFRESH_PERIOD = 5; // minutes

    /**
     * Identity
     */
    public static Integer IDENTITY_CONFIG_REFRESH_PERIOD = 10; // minutes

    /**
     * Messages
     */
    public static Integer MESSAGES_REFRESH_PERIOD = 10; // minutes

    /**
     * Synchronization
     */
    public static Integer SYNCHRONIZATION_REFRESH_PERIOD = 15;    // Minutes, if failed reschedule takes the half amount of the value

    /**
     * Questionnaires
     */
    public static Integer QUESTIONNAIRES_REFRESH_PERIOD = 1; // minutes
    public static Integer QUESTIONNAIRES_REFRESH_SYNC_PERIOD = 20; // minutes

    public static Boolean QUESTIONNAIRES_CHECK_FINISHED_ITEMS = true;   // This will enable a checking mechanism for canceling due notifications

    // Week: 6:00-8:00 -- 20:30-22:00
    public static Long QUESTIONNAIRES_WEEK_MORNING_MIN = Time.HOUR * 6;
    public static Long QUESTIONNAIRES_WEEK_MORNING_MAX = Time.HOUR * 8;// + Time.MINUTE * 30;
    public static Long QUESTIONNAIRES_WEEK_MORNING_DEFAULT = QUESTIONNAIRES_WEEK_MORNING_MIN;

    public static Long QUESTIONNAIRES_WEEK_NIGHT_MIN = Time.HOUR * 21;
    public static Long QUESTIONNAIRES_WEEK_NIGHT_MAX = Time.HOUR * 23 + Time.MINUTE * 59;
    public static Long QUESTIONNAIRES_WEEK_NIGHT_DEFAULT = QUESTIONNAIRES_WEEK_NIGHT_MAX;

    // Weekends: 6:00-11:00 -- 20:30-22:00
    public static Long QUESTIONNAIRES_WEEKEND_MORNING_MIN = QUESTIONNAIRES_WEEK_MORNING_MIN;
    public static Long QUESTIONNAIRES_WEEKEND_MORNING_MAX = Time.HOUR * 11;
    public static Long QUESTIONNAIRES_WEEKEND_MORNING_DEFAULT = QUESTIONNAIRES_WEEK_MORNING_DEFAULT;

    public static Long QUESTIONNAIRES_WEEKEND_NIGHT_MIN = QUESTIONNAIRES_WEEK_NIGHT_MIN;
    public static Long QUESTIONNAIRES_WEEKEND_NIGHT_MAX = QUESTIONNAIRES_WEEK_NIGHT_MAX;
    public static Long QUESTIONNAIRES_WEEKEND_NIGHT_DEFAULT = QUESTIONNAIRES_WEEK_NIGHT_DEFAULT;

    /**
     * Updates
     */
    public static Integer UPDATES_REFRESH_PERIOD = 60; // minutes

    /**
     * Debug
     */
    public static Boolean DEBUG_ENABLED = true; // @todo
    public static Integer DEBUG_REFRESH_PERIOD = 30; // minutes


    private Config() {
    }

    static public Config build() {
        return new Config();
    }

    public List<MetricDeclaration> getMetrics() {
        return metrics;
    }

    public boolean isMetric(Class<? extends Metric> metric) {
        if(metrics!=null && !metrics.isEmpty()) {
            Iterator<MetricDeclaration> iEnabled = metrics.iterator();
            while (iEnabled.hasNext()) {
                MetricDeclaration declaration = iEnabled.next();
                if(declaration.getMetric().equals(metric)) {
                    return true;
                }
            }
        }
        return false;
    }
}
