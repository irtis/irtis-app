package cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items;

import com.google.gson.annotations.SerializedName;

public class QuestionFilter {
    public static final int TYPE_SELECTION = 1;

    public static final int OPERATION_OR = 1;
    public static final int OPERATION_AND = 2;

    @SerializedName("owner_question_id")
    private Long idOwner;
    @SerializedName("target_question_id")
    private Long idQuestion;

    @SerializedName("target_answer_id")
    private Long idAnswer;

    @SerializedName("link_type")
    private Integer type;
    @SerializedName("value_operation")
    private Integer operation;
    @SerializedName("value_limit")
    private Long limit;

    private QuestionFilter(Long idOwner, Long idQuestion, Long idAnswer) {
        this.idOwner = idOwner;
        this.idQuestion = idQuestion;
        this.idAnswer = idAnswer;
    }

    static public QuestionFilter build(Long idOwner, Long idQuestion, Long idAnswer) {
        return new QuestionFilter(idOwner, idQuestion, idAnswer);
    }


    public Long getIdOwner() {
        return idOwner;
    }

    public Long getIdQuestion() {
        return idQuestion;
    }

    public Long getIdAnswer() {
        return idAnswer;
    }

    public Integer getType() {
        if(type==null) {
            return TYPE_SELECTION;
        } else {
            return type;
        }
    }

    public Integer getOperation() {
        if(operation==null) {
            return OPERATION_OR;
        } else {
            return operation;
        }
    }
}