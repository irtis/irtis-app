package cz.muni.irtis.datacollector.lite.gui.identity.model.response;

public class GenerateResponseEntity extends TokenResponseEntity {
    private String login;
    private String password;

    static public GenerateResponseEntity build(String login, String password) {
        GenerateResponseEntity instance = new GenerateResponseEntity();
        instance.setLogin(login);
        instance.setPassword(password);
        return instance;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
