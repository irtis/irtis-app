package cz.muni.irtis.datacollector.lite.gui.questionnaires.types;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import java.util.Iterator;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewAdapter;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.RecyclerListViewBuilder;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;

public class ScaleFragment extends TypeFragmentBase {
    private RecyclerListViewAdapter list;

    public ScaleFragment(Context context, Question question, boolean editable) {
        super(context, question, R.layout.activity_questionnaires_form_type_list, editable);
    }

    public class ViewHolder extends RecyclerListViewAdapter.ItemsListViewHolderBase {
        RadioButton vRadio;
        public ViewHolder(RecyclerListViewAdapter adapter, View view) {
            super(adapter, view);
            vRadio = view.findViewById(R.id.radio);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        list = RecyclerListViewBuilder.build(getActivity())
            .setView(view.findViewById(R.id.list))
            .setAdapter(new RecyclerListViewAdapter<String, ViewHolder>(R.layout.activity_questionnaires_form_type_scale))
            .setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<Answer, ViewHolder>() {
                @Override
                public ViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
                    return new ViewHolder(adapter, view);
                }

                @Override
                public void onBind(ViewHolder holder, Answer item) {
                    holder.vRadio.setText(item.getText());
                    holder.vRadio.setChecked(item.isSelected());
                    holder.vRadio.setEnabled(isEditable());
                }
            })
            .setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<Answer>() {
                @Override
                public void onClick(View view, Answer item) {
                    unSelect();
                    select(item);
                }
            })
            .create().getAdapter();


        list.addItems(getQuestion().getAnswers());

        return view;
    }


    /**
     * unSelect()
     *
     * Uncheck all options
     */
    public void unSelect() {
        if(isEditable()) {
            for (int position = 0; position < list.getCount(); position++) {
                ViewHolder holder = (ViewHolder) list.getViewHolder(position);
                if (holder != null) {
                    holder.vRadio.setChecked(false);
                }
            }

            Iterator<Answer> iterator = getQuestion().getAnswers().iterator();
            while (iterator.hasNext()) {
                Answer answer = iterator.next();
                answer.setSelected(false);
                answer.setValue(null);
            }
        }
    }

    /**
     * select()
     *
     * Check a particular option
     * @param item
     */
    public void select(Answer item) {
        if(isEditable()) {
            item.setSelected(true);
            item.setValue(String.valueOf(item.getId()));
            ViewHolder holder = (ViewHolder) list.getViewHolder(item);
            holder.vRadio.setChecked(true);
            refresh();
        }
    }
}