package cz.muni.irtis.datacollector.lite.metrics.model;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.Metric;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.NotificationEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.NotificationsRepository;
import cz.muni.irtis.datacollector.lite.metrics.hooking.NotificationBundle;
import cz.muni.irtis.datacollector.lite.metrics.hooking.NotificationsService;
import cz.muni.irtis.datacollector.lite.metrics.model.condition.IsNotificationsObservation;

/**
 * Capture battery state
 */
public class Notifications extends Metric {
    public static final String TAG = Notifications.class.getSimpleName();
    public static final int IDENTIFIER = 255;

    private NotificationServiceReceiver receiver;

    public Notifications(Context context, Integer delay) {
        super(context, delay);

        receiver = null;
        addPrerequisity(new IsNotificationsObservation());
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "Notification";
    }

    /**
     * Retrieve the battery state.
     */
    @Override
    public void run() {
        if (!isPrerequisitiesSatisfied()) {
            return;
        }

        if (receiver == null) {
            receiver = new NotificationServiceReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction(TAG);
            getContext().registerReceiver(receiver, filter);
        }

        setRunning(true);
    }

    @Override
    public void stop() {
        setRunning(false);

        getContext().unregisterReceiver(receiver);
        receiver = null;
    }

    @Override
    public long save(Object... params) {
        String identifier = (String) params[0];
        Long posted  = (Long) params[1];
        Long removed = (Long) params[2];
        String title = (String) params[3];
        String text = (String) params[4];

        try {
            Debug.getInstance().log(TAG, identifier + ": " + title);
            return getDatabase().getNotificationsRepository().insert(
                    new NotificationEntity(Time.getTime(), identifier, posted, removed, title, text));
        } catch (Exception e) {
            Debug.getInstance().exception(e);
            return -1;
        }
    }


    class NotificationServiceReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String source = intent.getStringExtra("source");
            String action = intent.getStringExtra("action");
            String content = intent.getStringExtra("content");

            if (source.equals(NotificationsService.TAG) && content!=null) {
                NotificationBundle bundle = new GsonBuilder().create()
                    .fromJson(content, new TypeToken<NotificationBundle>() {
                    }.getType());
                if (bundle != null ) { // && !bundle.getIdentifier().contains("cz.muni.irtis")
                    if(action.equals("posted")) {
                        save(bundle.getIdentifier(), bundle.getPosted(), bundle.getRemoved(), bundle.getTitle(), bundle.getText());
                    } else if(action.equals("removed")) {
                        NotificationsRepository repository = getDatabase().getNotificationsRepository();
                        NotificationEntity entity = repository.getOpenedPosted(bundle.getIdentifier());
                        if(entity!=null) {
                            entity.setRemoved(bundle.getRemoved());
                            repository.update(entity);
                        }
                    }
                }
            }
        }
    }
}
