package cz.muni.irtis.datacollector.lite.metrics.model.util.physicalactivity;

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.List;

import cz.muni.irtis.datacollector.lite.gui.identity.model.Credentials;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.QuestionnaireJson;

/**
 * Used as pending intent for ActivityRecognitionClient listener.
 * Results are sent back with a local broadcast.
 */
public class DetectedActivitiesIntentService extends IntentService {
    private static final String TAG = DetectedActivitiesIntentService.class.getSimpleName();

    public DetectedActivitiesIntentService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    /**
     * Get most probable activity & broadcast it
     * @param intent
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void onHandleIntent(Intent intent) {
        ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
        DetectedActivity activity = result.getMostProbableActivity();
        List<DetectedActivity> probables = result.getProbableActivities();
        broadcastActivity(activity, probables);
    }

    /**
     * Send local broadcast with result.
     * @param activity
     */
    private void broadcastActivity(DetectedActivity activity, List<DetectedActivity> probables) {
        Intent intent = new Intent("activity");
        intent.putExtra("type", activity.getType());
        intent.putExtra("confidence", activity.getConfidence());
        intent.putExtra("probables", new GsonBuilder().create().toJson(probables, new TypeToken<List<DetectedActivity>>(){}.getType()));

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
