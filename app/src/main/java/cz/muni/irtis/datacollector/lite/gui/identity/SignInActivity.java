package cz.muni.irtis.datacollector.lite.gui.identity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.gui.LauncherActivity;
import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.application.widgets.Box;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Identity;

public class SignInActivity extends ActivityBase {
    private View vLoading;
    private View vForm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        vLoading = findViewById(R.id.loading);
        vLoading.setVisibility(View.GONE);

        vForm = findViewById(R.id.container);

        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if(Network.isConnection()) {
                    TextInputEditText eLogin = findViewById(R.id.login);
                    TextInputEditText ePassword = findViewById(R.id.pass);

                    Identity.getInstance().signIn(eLogin.getText().toString(), ePassword.getText().toString(),
                        new Identity.OnIdentitySignInListener() {
                            @Override
                            public void onStart() {
                                vLoading.setVisibility(View.VISIBLE);
                                vForm.setVisibility(View.GONE);
                            }

                            @Override
                            public void onDone(boolean isLogged) {
                                if (isLogged) {
                                    LauncherActivity.start(SignInActivity.this);
                                } else {
                                    vLoading.setVisibility(View.GONE);
                                    vForm.setVisibility(View.VISIBLE);
                                    Snackbar.make(view, getString(R.string.signin_failed), Snackbar.LENGTH_LONG).show();
                                }
                            }
                        }
                    );
                } else {
                    Box.ok(SignInActivity.this, getString(R.string.network_no_connection), getString(R.string.network_no_connection_suggestion));
                }
            }
        });

        findViewById(R.id.generate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(GenerateActivity.class);
            }
        });

        // Privacy Policy
        findViewById(R.id.privacy).setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(
                        Uri.parse(Config.SERVICE_PRIVACY_POLICY)));
            }
        });
        // Terms of Use
        findViewById(R.id.about).setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(
                        Uri.parse(Config.SERVICE_ABOUT)));
            }
        });
    }


}
