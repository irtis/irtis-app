package cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class UserEntity {
    private Long id;
    @SerializedName("username")
    private String userName;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("profile_picture")
    private String picture;
    private String bio;
    private String website;
    @SerializedName("is_business")
    private boolean isBusiness;
    private UserCountsEntity counts;


    static public UserEntity buildFromJson(String json) {
        return new GsonBuilder().create().fromJson(json, UserEntity.class);
    }


    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPicture() {
        return picture;
    }

    public String getBio() {
        return bio;
    }

    public String getWebsite() {
        return website;
    }

    public boolean isBusiness() {
        return isBusiness;
    }

    public UserCountsEntity getCounts() {
        return counts;
    }
}
