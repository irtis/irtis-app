package cz.muni.irtis.datacollector.lite.metrics.database.entities.phone;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;

import cz.muni.irtis.datacollector.lite.metrics.database.repository.PhoneCallsRepository;

@Entity(tableName = PhoneCallsRepository.TABLE_NAME)
public class PhoneCallEntity extends PhoneEntity {
    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "phone_number")
    @SerializedName("phone_number")
    private String phoneNumber;

    @ColumnInfo(name = "type")
    private String type;

    @ColumnInfo(name = "duration")
    private Long duration;

    @ColumnInfo(name = "call_date")
    @SerializedName("call_date")
    private Long callDate;

    public PhoneCallEntity(Long datetime, String name, String phoneNumber, String type, Long duration, Long callDate) {
        super(datetime);
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.type = type;
        this.duration = duration;
        this.callDate = callDate;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getType() {
        return type;
    }

    public Long getDuration() {
        return duration;
    }

    public Long getCallDate() {
        return callDate;
    }
}
