package cz.muni.irtis.datacollector.lite.metrics.database.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

import cz.muni.irtis.datacollector.lite.metrics.database.repository.NotificationsRepository;

@Entity(tableName = NotificationsRepository.TABLE_NAME)
public class NotificationEntity extends EntityBase {
    @NonNull
    @ColumnInfo(name = "package")
    private String identifier;
    @ColumnInfo(name = "posted")
    private Long posted;
    @ColumnInfo(name = "removed")
    private Long removed;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "text")
    private String text;

    public NotificationEntity(Long datetime, @NonNull String identifier, Long posted, Long removed, String title, String text) {
        super(datetime);
        this.identifier = identifier;
        this.posted = posted;
        this.removed = removed;
        this.title = title;
        this.text = text;
    }

    @NonNull
    public String getIdentifier() {
        return identifier;
    }

    public Long getPosted() {
        return posted;
    }

    public Long getRemoved() {
        return removed;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public void setIdentifier(@NonNull String identifier) {
        this.identifier = identifier;
    }

    public void setPosted(Long posted) {
        this.posted = posted;
    }

    public void setRemoved(Long removed) {
        this.removed = removed;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }
}
