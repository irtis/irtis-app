package cz.muni.irtis.datacollector.lite.application.components;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.QuestionnairesJob;

abstract public class JobServiceBase extends JobService {
    @Override
    public boolean onStartJob(JobParameters params) {
        return false;
    }

    public boolean onStartJob(Context context, JobParameters params) {
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

    /**
     * isSet()
     *
     * Check if the job isActive scheduled
     * @param context
     * @param jobId
     * @return
     */
    public static boolean isSet(Context context, int jobId) {
        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE) ;
        for (JobInfo jobInfo : scheduler.getAllPendingJobs()) {
            if (jobInfo.getId() == jobId) {
                return true;
            }
        }
        return false;
    }

    /**
     * schedule()
     *
     * Basic schedule without any pre-condition limitations except timing
     *
     * @param context
     * @param jobType   Job type class
     * @param jobId     Job ID
     * @param minutes   Minutes in which the job will be executed
     */
    public static void schedule(Context context, Class jobType, int jobId, int minutes) {
        ComponentName serviceComponent = new ComponentName(context, jobType);

        JobInfo.Builder builder = new JobInfo.Builder(jobId, serviceComponent);
        builder.setOverrideDeadline(minutes * Time.MINUTE);
        builder.setMinimumLatency((minutes-1) * Time.MINUTE);

        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(builder.build());
        Debug.getInstance().task(jobType.getSimpleName(), "Job has been scheduled");
    }

    /**
     * scheduleWithNetwork()
     *
     * This schedule will require network connection
     *
     * @param context
     * @param jobType
     * @param jobId
     * @param minutes
     */
    public static void scheduleWithNetwork(Context context, Class jobType, int jobId, int minutes) {
        ComponentName serviceComponent = new ComponentName(context, jobType);

        JobInfo.Builder builder = new JobInfo.Builder(jobId, serviceComponent);
        builder.setOverrideDeadline(minutes * Time.MINUTE);
        builder.setMinimumLatency((minutes-1) * Time.MINUTE);
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);

        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(builder.build());
        Debug.getInstance().task(jobType.getSimpleName(), "Job has been scheduled");
    }


    public void start() {
        onStartJob(null);
    }

    public void start(Context context) {
        onStartJob(context, null);
    }

    public static void cancel(Context context, int jobId) {
        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        scheduler.cancel(jobId);
    }
}
