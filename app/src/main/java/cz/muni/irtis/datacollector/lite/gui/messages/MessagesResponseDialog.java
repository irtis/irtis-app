package cz.muni.irtis.datacollector.lite.gui.messages;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;


import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.messages.model.Message;
import cz.muni.irtis.datacollector.lite.gui.messages.model.MessagesModel;

/**
 * DictionaryReportDialog
 * <p>
 * Author: Michal Schejbal<br>
 * Created: 27.02.2020
 */
public class MessagesResponseDialog extends Dialog {
    private AlertDialog dialog;
    private Message item;
    private MessagesModel model;
    private View content;

    public interface ReportResultListener {
        public void onDone(Boolean result);
    }
    private ReportResultListener listener;

    public MessagesResponseDialog(Context context, Message item, ReportResultListener listener) {
        super(context);
        this.item = item;
        this.listener = listener;
        model = MessagesModel.build(getContext());
        this.dialog = onBuilderCreate(null);
    }

    protected AlertDialog onBuilderCreate(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        content = getLayoutInflater().inflate(R.layout.component_messages_response, null);

        builder
            .setView(content)
            .setTitle(getContext().getString(R.string.messagesResponse))
            .setPositiveButton(getContext().getString(R.string.messagesResponseSend), new OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    send();
                }
            })
            .setNegativeButton("Cancel", new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    hide();
                }
            })
            .setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        hide();
                    }
                    return true;
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    hide();
                }
            });

        return builder.create();
    }

    public void show() { this.dialog.show(); }

    public void hide() { this.dialog.hide(); }

    public void close() { this.dialog.hide(); }

    public AlertDialog getDialog() { return this.dialog; }

    private void send() {
        model.setResponse(
            item, ((EditText) content.findViewById(R.id.text)).getText().toString(),
            new Repository.RepositoryListener<Boolean>() {
                @Override
                public void onStart() {
                }

                @Override
                public void onDone(Boolean result) {
                    listener.onDone(result);
                }
            }
        );
    }
}
