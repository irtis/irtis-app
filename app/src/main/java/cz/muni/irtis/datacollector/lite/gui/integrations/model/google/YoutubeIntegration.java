package cz.muni.irtis.datacollector.lite.gui.integrations.model.google;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeScopes;
import com.google.api.services.youtube.model.ActivityListResponse;
import com.google.api.services.youtube.model.Caption;
import com.google.api.services.youtube.model.CaptionListResponse;
import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.ChannelListResponse;
import com.google.api.services.youtube.model.Comment;
import com.google.api.services.youtube.model.CommentListResponse;
import com.google.api.services.youtube.model.CommentThread;
import com.google.api.services.youtube.model.CommentThreadListResponse;
import com.google.api.services.youtube.model.Playlist;
import com.google.api.services.youtube.model.PlaylistItem;
import com.google.api.services.youtube.model.PlaylistItemListResponse;
import com.google.api.services.youtube.model.PlaylistListResponse;
import com.google.api.services.youtube.model.Subscription;
import com.google.api.services.youtube.model.SubscriptionListResponse;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoCategory;
import com.google.api.services.youtube.model.VideoCategoryListResponse;
import com.google.api.services.youtube.model.VideoGetRatingResponse;
import com.google.api.services.youtube.model.VideoListResponse;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.Applications;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.IntegrationBase;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;

/**
 * YoutubeIntegration
 *
 * Youtube calls are limited by quota that isActive affected by required scopes and by the
 * kind of method (lists are usually very cheap)
 *
 * Default quota allocation isActive 10 000 units per day
 *
 * https://developers.google.com/youtube/v3/quickstart/android
 * https://developers.google.com/youtube/v3/docs/
 *
 * https://developers.google.com/api-client-library/java/apis/youtube/v3
 *
 */
public class YoutubeIntegration extends IntegrationBase {
    private GoogleService google;
    private YouTube service;

    public YoutubeIntegration(Context context, final ServiceBase.OnServiceListener onSignInListener, final ServiceBase.OnServiceListener onSignOutListener) {
        super(context);
        google = GoogleServiceBuilder.buildYoutube(context, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
                onSignInListener.onStart();
            }

            @Override
            public void onError(int code) {
                onSignInListener.onError(code);
            }

            @Override
            public void onFinished(boolean isLogged) {
                GoogleAccountCredential credential = GoogleAccountCredential.usingOAuth2(getContext(),
                        Collections.singleton(YouTubeScopes.YOUTUBE_READONLY));
                credential.setSelectedAccount(google.getAccount().getAccount());

                HttpTransport transport = new NetHttpTransport();
                JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
                service = new YouTube.Builder(transport, jsonFactory, credential)
                    .setApplicationName(getContext().getString(R.string.app_name))
                    .build();

                onSignInListener.onFinished(isLogged);
            }
        }, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
                onSignOutListener.onStart();
            }

            @Override
            public void onError(int code) {
                onSignOutListener.onError(code);
            }

            @Override
            public void onFinished(boolean isLogged) {
                onSignOutListener.onFinished(isLogged);
            }
        });
    }

    public YoutubeIntegration(Context context) {
        super(context);
        google = GoogleServiceBuilder.buildYoutube(context);
    }

    static public YoutubeIntegration build(Context context) {
        return new YoutubeIntegration(context);
    }


    /**
     * isConnected()
     *
     * Check if the connection if established with Google API
     *
     * @return
     */
    @Override
    public boolean isConnected() {
        return google.isConnected();
    }

    /**
     * isApplication()
     *
     * Check if youtube app isActive installed on the device
     *
     * @return
     */
    @Override
    public boolean isApplication() {
        return Applications.isApplicationInstalled(getContext(), "com.google.android.youtube");
    }

    @Override
    public String getName() {
        return getContext().getString(R.string.youtube_integration_title);
    }

    @Override
    public Drawable getImage() {
        return getContext().getDrawable(R.drawable.integration_youtube_icon);
    }

    public void connect(Activity activity) {
        google.signIn(activity);
    }

    public void disconnect() {
        google.signOut();
    }


    public GoogleService getGoogle() {
        return google;
    }

    public YouTube getService() {
        return service;
    }


    /**
     * getChannels()
     *
     * Get self channels
     *
     * See https://developers.google.com/youtube/v3/docs/channels/list
     *
     * @param userName
     * @return
     * @throws IOException
     */
    public List<Channel> getChannels(String userName) throws IOException {
        YouTube.Channels.List list = getService().channels().list("snippet,contentDetails,statistics");
        if(userName.equals("mine")) {
            list.setMine(true);
        } else {
            list.setForUsername(userName);
        }
        ChannelListResponse result = list.execute();

        List<Channel> channels = result.getItems();
        if (channels != null && !channels.isEmpty()) {
            return channels;
        } else {
            return null;
        }
    }

    /**
     * getChannel()
     *
     * Get a particular channel by ID
     *
     * See https://developers.google.com/youtube/v3/docs/channels/list
     *
     * @param id
     * @return
     * @throws IOException
     */
    public Channel getChannel(String id) throws IOException {
        ChannelListResponse result = getService().channels().list("snippet,contentDetails,statistics")
            .setId(id)
            .execute();

        List<Channel> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            Channel item = items.get(0);
            return item;
        } else {
            return null;
        }
    }

    /**
     * getChannelBySelf()
     *
     * Get user's channel
     *
     * See https://developers.google.com/youtube/v3/docs/channels/list
     *
     * @return
     * @throws IOException
     */
    public Channel getChannelBySelf() throws IOException {
        List<Channel> items = getChannels("mine");
        if (items != null && !items.isEmpty()) {
            Channel item = items.get(0);
            return item;
        } else {
            return null;
        }
    }


    /**
     * getSubscriptionsBySelf()
     *
     * Get list of subscriptions of the user
     *
     * See https://developers.google.com/youtube/v3/docs/subscriptions/list
     *
     * @return
     * @throws IOException
     */
    public List<Subscription> getSubscriptionsBySelf() throws IOException {
        SubscriptionListResponse result = getService().subscriptions().list("snippet,contentDetails")
            .setMine(true)
            .execute();

        List<Subscription> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            return items;
        } else {
            return null;
        }
    }

    /**
     * getSubscription()
     *
     * Get subscription by ID
     *
     * See https://developers.google.com/youtube/v3/docs/subscriptions/list
     *
     * @param id
     * @return
     * @throws IOException
     */
    public Subscription getSubscription(String id) throws IOException {
        SubscriptionListResponse result = getService().subscriptions().list("snippet,contentDetails")
            .setId(id)
            .execute();

        List<Subscription> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            Subscription item = items.get(0);
            return item;
        } else {
            return null;
        }
    }

    /**
     * isSubscription()
     *
     * Is the user subscribed to the channel?
     *
     * See https://developers.google.com/youtube/v3/docs/subscriptions/list
     *
     * @param channelId
     * @return
     * @throws IOException
     */
    public boolean isSubscription(String channelId) throws IOException {
        SubscriptionListResponse result = getService().subscriptions().list("snippet,contentDetails")
            .setChannelId(channelId)
            .setMine(true)
            .execute();

        List<Subscription> items = result.getItems();
        if (items != null) {
            return !items.isEmpty();
        } else {
            return false;
        }
    }


    /**
     * getPlaylists()
     *
     * Get playlist by channel ID
     *
     * See https://developers.google.com/youtube/v3/docs/playlists/list
     *
     * @param channelId
     * @return
     * @throws IOException
     */
    public List<Playlist> getPlaylists(String channelId) throws IOException {
        YouTube.Playlists.List list = getService().playlists().list("snippet,contentDetails");
        if(channelId.equals("mine")) {
            list.setMine(true);
        } else {
            list.setChannelId(channelId);
        }
        PlaylistListResponse result = list.execute();

        List<Playlist> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            return items;
        } else {
            return null;
        }
    }

    public List<Playlist> getPlaylistsBySelf() throws IOException {
        return getPlaylists("mine");
    }

    /**
     * getPlaylist()
     *
     * Get playlist by ID
     *
     * See https://developers.google.com/youtube/v3/docs/playlists/list
     *
     * @param id
     * @return
     * @throws IOException
     */
    public Playlist getPlaylist(String id) throws IOException {
        PlaylistListResponse result = getService().playlists().list("snippet,contentDetails")
            .setId(id)
            .execute();

        List<Playlist> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            Playlist item = items.get(0);
            return item;
        } else {
            return null;
        }
    }

    /**
     * getPlaylistItems()
     *
     * Get the list of items contained in a particular playlist
     *
     * See https://developers.google.com/youtube/v3/docs/playlistItems/list
     *
     * @param playlistId
     * @return
     * @throws IOException
     */
    public List<PlaylistItem> getPlaylistItems(String playlistId) throws IOException {
        PlaylistItemListResponse result = getService().playlistItems().list("snippet,contentDetails")
            .setPlaylistId(playlistId)
            .execute();

        List<PlaylistItem> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            return items;
        } else {
            return null;
        }
    }

    /**
     * getPlaylistItem()
     *
     * Get playlist item by ID
     *
     * @param id
     * @return
     * @throws IOException
     */
    public PlaylistItem getPlaylistItem(String id) throws IOException {
        PlaylistItemListResponse result = getService().playlistItems().list("snippet,contentDetails")
            .setId(id)
            .execute();

        List<PlaylistItem> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            PlaylistItem item = items.get(0);
            return item;
        } else {
            return null;
        }
    }

    /**
     * getVideo()
     *
     * Get video by ID
     *
     * See https://developers.google.com/youtube/v3/docs/videos/list
     *
     * @param id
     * @return
     * @throws IOException
     */
    public Video getVideo(String id) throws IOException {
        VideoListResponse result = getService().videos().list("snippet,contentDetails,statistics")
            .setId(id)
            .execute();

        List<Video> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            Video item = items.get(0);
            return item;
        } else {
            return null;
        }
    }

    /**
     * getVideosLikedBySelf()
     *
     * Get liked videos by the user
     *
     * See https://developers.google.com/youtube/v3/docs/videos/list
     *
     * @return
     * @throws IOException
     */
    public List<Video> getVideosLikedBySelf() throws IOException {
        VideoListResponse result = getService().videos().list("snippet,contentDetails,statistics")
            .setMyRating("like")
            .execute();

        List<Video> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            return items;
        } else {
            return null;
        }
    }

    /**
     * getVideosDislikedBySelf()
     *
     * Get disliked videos by the user
     *
     * See https://developers.google.com/youtube/v3/docs/videos/list
     *
     * @return
     * @throws IOException
     */
    public List<Video> getVideosDislikedBySelf() throws IOException {
        VideoListResponse result = getService().videos().list("snippet,contentDetails,statistics")
            .setMyRating("like")
            .execute();

        List<Video> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            return items;
        } else {
            return null;
        }
    }

    /**
     * isVideoLikeBySelf()
     *
     * Is the video like by the user?
     *
     * See https://developers.google.com/youtube/v3/docs/videos/getRating
     *
     * @param id
     * @return
     * @throws IOException
     */
    public boolean isVideoLikeBySelf(String id) throws IOException {
        VideoGetRatingResponse result = getService().videos()
                .getRating(id)
                .setKey(getContext().getString(R.string.google_client_id))
                .execute();

        if(result.getItems()!=null && !result.getItems().isEmpty()) {
            return result.getItems().get(0).getRating().equals("like");
        } else {
            return false;
        }
    }

    /**
     * getVideoComments()
     *
     * Get comments of the video by ID
     *
     * See https://developers.google.com/youtube/v3/docs/commentThreads/list
     *
     * @param id
     * @throws IOException
     */
    public List<CommentThread> getVideoComments(String id) throws IOException {
        CommentThreadListResponse result = getService().commentThreads().list("snippet,replies")
            .setKey(getContext().getString(R.string.google_client_id))
            .setVideoId(id)
            .execute();

        List<CommentThread> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            return items;
        } else {
            return null;
        }
    }

    /**
     * getVideoCommentsByParent()
     *
     * Get video comments by parent comment ID
     *
     * See https://developers.google.com/youtube/v3/docs/comments/list
     *
     * @param id
     * @return
     * @throws IOException
     */
    public List<Comment> getVideoCommentsByParent(String id) throws IOException {
        CommentListResponse result = getService().comments().list("snippet,replies")
                .setKey(getContext().getString(R.string.google_client_id))
                .setParentId(id)
                .execute();

        List<Comment> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            return items;
        } else {
            return null;
        }
    }

    /**
     * getVideoComment()
     *
     * Get video comment by ID
     *
     * See https://developers.google.com/youtube/v3/docs/comments/list
     *
     * @param id
     * @return
     * @throws IOException
     */
    public Comment getVideoComment(String id) throws IOException {
        CommentListResponse result = getService().comments().list("snippet")
                .setKey(getContext().getString(R.string.google_client_id))
                .setId(id)
                .execute();

        List<Comment> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            Comment item = items.get(0);
            return item;
        } else {
            return null;
        }
    }



    /**
     * getVideoCategory()
     *
     * Get category of a video by ID
     *
     * See https://developers.google.com/youtube/v3/docs/videoCategories/list
     *
     * @param id
     * @return
     * @throws IOException
     */
    public VideoCategory getVideoCategory(String id) throws IOException {
        VideoCategoryListResponse result = getService().videoCategories().list("snippet")
            .setId(id)
            .execute();

        List<VideoCategory> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            VideoCategory item = items.get(0);
            return item;
        } else {
            return null;
        }
    }

    /**
     * getVideoSubtitles()
     *
     * Get video subtitles by video ID
     *
     * See https://developers.google.com/youtube/v3/docs/captions/list
     *
     * @param id
     */
    public List<Caption> getVideoSubtitles(String id) throws IOException {
        CaptionListResponse result = getService().captions().list("snippet", id)
            .execute();

        List<Caption> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            return items;
        } else {
            return null;
        }
    }


    /**
     * getActivities()
     *
     * Get user activities
     *
     * See https://developers.google.com/youtube/v3/docs/activities
     *
     * @return
     * @throws IOException
     */
    public List<com.google.api.services.youtube.model.Activity> getActivities() throws IOException {
        ActivityListResponse result = getService().activities().list("snippet,contentDetails")
                .setMine(true)
                .execute();

        List<com.google.api.services.youtube.model.Activity> items = result.getItems();
        if (items != null && !items.isEmpty()) {
            return items;
        } else {
            return null;
        }
    }

    /**
     * getActivitiesHistory()
     *
     * Get history of user's activity
     *
     * This isActive no longer available
     *
     * @deprecated https://stackoverflow.com/questions/46987690/tracking-youtube-watch-history
     * @return
     */
    public List<String> getActivitiesHistory() {
        return null;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        google.onActivityResult(requestCode, resultCode, data);
    }
}
