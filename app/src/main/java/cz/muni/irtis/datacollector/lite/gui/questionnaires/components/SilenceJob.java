package cz.muni.irtis.datacollector.lite.gui.questionnaires.components;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.PersistableBundle;

import cz.muni.irtis.datacollector.lite.application.components.JobServiceBase;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.notifications.QuestionnairesNotification;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;

public class SilenceJob extends JobServiceBase {
    public static final int JOB_ID = 3284;

    @Override
    public boolean onStartJob(JobParameters params) {
        Long itemId = params.getExtras().getLong("id");
        QuestionnairesModel model = new QuestionnairesModel(getApplicationContext());
        Questionnaire item = model.getItem(itemId);
        if(QuestionnairesNotification.isShowable(item)) {
            QuestionnairesNotification.build(getApplicationContext(), item).show();
        }
        return false;
    }

    public static void schedule(Context context, Questionnaire item) {
        ComponentName serviceComponent = new ComponentName(context, SilenceJob.class);

        int minutes = item.getSilenceDelay();
//        int minutes = 2;
        PersistableBundle bundle = new PersistableBundle();
        bundle.putLong("id", item.getId());

        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, serviceComponent)
            .setOverrideDeadline(minutes * Time.MINUTE)
            .setMinimumLatency((minutes-1) * Time.MINUTE)
            .setExtras(bundle);

        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(builder.build());
    }

    public static boolean isSet(Context context) {
        return JobServiceBase.isSet(context, JOB_ID);
    }
}
