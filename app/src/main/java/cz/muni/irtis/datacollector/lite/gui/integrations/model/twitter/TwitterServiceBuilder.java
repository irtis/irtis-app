package cz.muni.irtis.datacollector.lite.gui.integrations.model.twitter;

import android.content.Context;
import android.util.Log;

import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;

public class TwitterServiceBuilder {
    static public TwitterService buildTwitter(Context context, ServiceBase.OnServiceListener onSignInListener, ServiceBase.OnServiceListener onSignOutListener) {
        TwitterConfig options = new TwitterConfig.Builder(context)
                .logger(new DefaultLogger(Log.DEBUG))//enable logging when app isActive in debug mode
                .twitterAuthConfig(new TwitterAuthConfig(
                        context.getString(R.string.twitter_client_id),
                        context.getString(R.string.twitter_client_secret)))
                .debug(Debug.getInstance().isDebug())//enable debug mode
                .build();

        return new TwitterService(context, options, onSignInListener, onSignOutListener);
    }

    static public TwitterService buildTwitter(Context context) {
        return buildTwitter(context, null, null);
    }
}
