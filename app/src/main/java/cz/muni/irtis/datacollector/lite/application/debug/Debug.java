package cz.muni.irtis.datacollector.lite.application.debug;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.util.Log;

import cz.muni.irtis.datacollector.lite.Application;
import cz.muni.irtis.datacollector.BuildConfig;


/**
 * Debug
 *
 * @author Michal Schejbal
 * @year 2015
 */
public class Debug {
    public static final int TYPE_DEBUG = 3;
    public static final int TYPE_ERROR = 6;
    public static final int TYPE_WARNING = 5;
    public static final int TYPE_TASK = 7;

    static private Debug instance;

    /**
     * getInstance()
     * <p>
     * Get instance of Debug
     *
     * @return Debug
     */
    static public Debug getInstance() {
        if (Debug.instance == null) {
            Debug.instance = new Debug();
        }
        return Debug.instance;
    }

    public void Debug() {

    }

    /**
     * isDebug()
     * <p>
     * Is the current build debug (true) or release (false)
     *
     * @return
     */
    static public boolean isDebug() {
        return BuildConfig.DEBUG;
//		return true;
    }


    /**
     * log()
     * <p>
     * Output the log to the console (logcat)
     *
     * @param title
     * @param text
     * @param type
     */
    public void log(String title, String text, int type) {
//		if(isDebug()) {
        if (text == null) {
            text = "-";
        }

        String file = null;
        switch (type) {
            default:
            case TYPE_DEBUG:
                Log.d(title, text);
                file = "debug";
                break;
            case TYPE_ERROR:
                Log.e(title, text);
                file = "errors";
                break;
            case TYPE_WARNING:
                Log.w(title, text);
                file = "warnings";
                break;
            case TYPE_TASK:
                Log.w(title, text);
                file = "tasks";
                break;
        }

        if (file != null) {
            DebugLogs.build(Application.getInstance().getApplicationContext())
                    .append(title + ":\t" + text, file);
        }
//		}
    }

    /**
     * log()
     *
     * @param title
     * @param text
     */
    public void log(String title, String text) {
        this.log(title, text, TYPE_DEBUG);
    }

    /**
     * log()
     *
     * @param title
     */
    public void log(String title) {
        this.log(title, null, TYPE_DEBUG);
    }

    public void error(String title, String text) {
        this.log(title, text, TYPE_ERROR);
    }

    public void error(String title) {
        this.log(title, null, TYPE_ERROR);
    }

    public void warning(String title, String text) {
        this.log(title, text, TYPE_WARNING);
    }

    public void warning(String title) {
        this.log(title, null, TYPE_WARNING);
    }

    public void task(String title, String text) {
        this.log(title, text, TYPE_TASK);
    }

    public void task(String title) {
        this.log(title, null, TYPE_TASK);
    }

    /**
     * exception()
     *
     * @param exception
     * @param object
     * @param text
     */
    public void exception(Exception exception, Object object, String text) {
        String output = "";
        if (exception.getMessage() != null) {
            Throwable cause = exception.getCause();
            output += exception.getMessage();
        }

        if (text != null) {
            output += (!output.isEmpty() ? " | " : "") + text;
        }

        StackTraceElement[] trace = exception.getStackTrace();
        if (trace.length > 0) {
            if (object != null) {
                for (int i = 0; i < trace.length; i++) {
                    if (object.getClass().getName().contains(trace[i].getClassName())) {
                        output += (!output.isEmpty() ? " @ " : "") + trace[i].getClassName() + ":" + trace[i].getLineNumber();
                        break;
                    }
                }
            } else {
                output += (!output.isEmpty() ? " | " : "") + trace[0];
            }
        } else if (object != null) {
            output += (!output.isEmpty() ? " | " : "") + object.getClass().getSimpleName();
        }

        this.log(exception.getClass().getSimpleName(), output, TYPE_ERROR);
    }

    /**
     * exception()
     *
     * @param exception
     * @param object
     */
    public void exception(Exception exception, Object object) {
        this.exception(exception, object, null);
    }

    /**
     * @param exception
     */
    public void exception(Exception exception) {
        this.exception(exception, null, null);
    }


    /**
     * box()
     * <p>
     * Show info box for presenting the content to the user
     *
     * @param context
     */
    public void box(Context context, String title, String text) {
        if (isDebug()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            if ((Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)) {
                builder.setInverseBackgroundForced(true);
            }

            builder.setTitle(title);
            if (text != null) builder.setMessage(text);

            AlertDialog info = builder.create();

            try {
                if (!((Activity) context).isFinishing()) {
                    info.show();
                }
            } catch (Exception e) {
            }

            // Console output
            this.log(title, text);
        }
    }
}
