package cz.muni.irtis.datacollector.lite.gui.questionnaires.types;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.NumberPicker;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;

public class TimeBirthDateSpinnerFragment extends TimeDateSpinnerFragment {


    public TimeBirthDateSpinnerFragment(Context context, Question question, boolean editable) {
        super(context, question, R.layout.activity_questionnaires_form_type_time_date, editable);
    }

    public TimeBirthDateSpinnerFragment(Context context, Question question, int contentLayoutId, boolean editable) {
        super(context, question, contentLayoutId, editable);
    }


}
