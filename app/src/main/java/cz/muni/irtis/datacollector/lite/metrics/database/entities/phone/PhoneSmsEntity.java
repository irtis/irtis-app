package cz.muni.irtis.datacollector.lite.metrics.database.entities.phone;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;

import cz.muni.irtis.datacollector.lite.metrics.database.repository.PhoneSmsesRepository;

@Entity(tableName = PhoneSmsesRepository.TABLE_NAME)
public class PhoneSmsEntity extends PhoneEntity {
    @ColumnInfo(name = "phone_number")
    @SerializedName("phone_number")
    private String phoneNumber;
    @ColumnInfo(name = "type")
    private String type;
    @ColumnInfo(name = "content")
    private String content;
    @ColumnInfo(name = "message_date")
    @SerializedName("message_date")
    private Long messageDate;

    public PhoneSmsEntity(Long datetime, String phoneNumber, String type, String content, Long messageDate) {
        super(datetime);
        this.phoneNumber = phoneNumber;
        this.type = type;
        this.content = content;
        this.messageDate = messageDate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getType() {
        return type;
    }

    public String getContent() {
        return content;
    }

    public Long getMessageDate() {
        return messageDate;
    }
}
