package cz.muni.irtis.datacollector.lite.persistence.runtime;

import android.content.Context;

/**
 * RuntimeCapture
 */
public class RuntimeCapture extends RuntimeBase {
    private RuntimeCapture(Context context) {
        super(context);
    }

    static public RuntimeCapture build(Context context) {
        return new RuntimeCapture(context);
    }


    @Override
    public void start() {
        started(TYPE_CAPTURE);
    }

    @Override
    public void stop() {
        stopped(TYPE_CAPTURE);
    }

    public void getRuntimeElapsed(GetRuntimeElapsedListener listener) {
        getRuntimeElapsed(TYPE_CAPTURE, listener);
    }
}
