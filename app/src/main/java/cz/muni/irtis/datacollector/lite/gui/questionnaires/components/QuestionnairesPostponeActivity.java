package cz.muni.irtis.datacollector.lite.gui.questionnaires.components;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesFormActivity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesViewModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;


public class QuestionnairesPostponeActivity extends ActivityBase {
    private QuestionnairesModel model;
    private Questionnaire questionnaire;
    private QuestionnairesPostponeDialog dialog;

    private boolean isFirstTime;

    @Override
    protected void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);

        Long id = getIntent().getLongExtra(QuestionnairesFormActivity.PARAM_ITEM_ID, -1);
        if(id!=null && id!=-1) {
            get(id);
        }

        isFirstTime = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void get(Long id) {
        model.getItem(id, new Repository.RepositoryListener<Questionnaire>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onDone(Questionnaire item) {
                questionnaire = item;
                dialog = new QuestionnairesPostponeDialog(QuestionnairesPostponeActivity.this,
                        new QuestionnairesPostponeDialog.OnSubmitListener() {
                            @Override
                            public void onSubmit(int number) {
                                model.setPostponed(questionnaire, number);
                                finish();
                            }

                            @Override
                            public void onCancel() {
                                finish();
                            }
                        }
                );
                dialog.show();
            }
        });
    }

    static public void startActivity(Context context, Long itemId)  {
        Intent intent = new Intent(context, QuestionnairesPostponeActivity.class);
        intent.putExtra(QuestionnairesFormActivity.PARAM_ITEM_ID, itemId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(intent);

    }
}
