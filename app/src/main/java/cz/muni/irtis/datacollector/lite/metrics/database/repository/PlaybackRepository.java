package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.PlaybackEntity;

@Dao
public interface PlaybackRepository extends IRepository<PlaybackEntity> {
    public static final String TABLE_NAME = "playback";

    /**
     * Add new entity to database.
     * @param item entity
     */
    @Insert
    long insert(PlaybackEntity item);

    @Query("SELECT * " +
            "FROM "+TABLE_NAME+" " +
            "WHERE "+TABLE_NAME+".datetime = :time")
    PlaybackEntity getByTime(Long time);

    @Query("SELECT * " +
            "FROM "+TABLE_NAME+" " +
            "WHERE "+TABLE_NAME+".datetime < :time")
    List<PlaybackEntity> getPrevious(Long time);

    @Query("SELECT * " +
            "FROM "+TABLE_NAME+" " +
            "WHERE "+TABLE_NAME+".datetime < :time" + " " +
            "LIMIT :offset, :count")
    List<PlaybackEntity> getPrevious(Long time, int offset, int count);

    /**
     * {@link IRepository#deleteById(Long)}
     */
    @Query("DELETE FROM " + TABLE_NAME + " WHERE datetime = :id")
    int deleteById(Long id);



}
