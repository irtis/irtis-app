package cz.muni.irtis.datacollector.lite.persistence.database;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.sqlite.db.SupportSQLiteQuery;

import java.util.List;

import cz.muni.irtis.datacollector.lite.application.database.EntityRepositoryBase;

@Dao
public interface BurstsRepository extends EntityRepositoryBase<BurstEntity> {
    public static final String TABLE_NAME = "bursts";

    @RawQuery
    List<BurstEntity> getItemsByQuery(SupportSQLiteQuery query);

    @Query("SELECT * FROM "+TABLE_NAME+" ORDER BY starting DESC")
    List<BurstEntity> getItems();

    @Query("SELECT * FROM "+TABLE_NAME+" WHERE starting < :time AND ending > :time ORDER BY starting DESC")
    List<BurstEntity> getItemsActive(Long time);

    @Query("SELECT * FROM "+TABLE_NAME+" WHERE starting > :time ORDER BY starting DESC")
    List<BurstEntity> getItemsFuture(Long time);

    @Query("SELECT * FROM "+TABLE_NAME+" WHERE ending < :time ORDER BY starting DESC")
    List<BurstEntity> getItemsPast(Long time);

    @Query("SELECT * FROM "+TABLE_NAME+" WHERE starting > :time OR ending < :time ORDER BY starting DESC")
    List<BurstEntity> getItemsNonActive(Long time);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE id = :id")
    BurstEntity getItem(Long id);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE starting > :time OR (starting <= :time AND ending >= :time) ORDER BY starting ASC")
    BurstEntity getItemClosest(Long time);
}
