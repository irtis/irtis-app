package cz.muni.irtis.datacollector.lite.metrics.mediaprojection;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;

import com.google.gson.GsonBuilder;

import cz.muni.irtis.datacollector.lite.application.settings.Settings;

/**
 * MediaProjectionToken
 *
 * Is an abstraction class above data provided by MediaProjectionManager.createScreenCaptureIntent
 * storing handle. The idea si to provide an enclosed interface to work with the handle as token.
 *
 * The token represents Binder object of the MediaProjection service providing access to
 * its interface (token = a numeric value that uniquely identifies a Binder).
 *
 * Read/Write methods are yet experimental
 */
public class MediaProjectionToken {
    private Intent handle;

    public MediaProjectionToken() {
        this.handle = null;
    }

    public void iniFromBinder(IBinder binder) {
        Parcel parcel = Parcel.obtain();
        parcel.writeValue(binder);
        Bundle bundle = new Bundle();
//        bundle.readFromParcel(parcel);
        bundle.putBinder("android.media.projection.extra.EXTRA_MEDIA_PROJECTION", binder);

        Intent handle = new Intent();
        handle.putExtras(bundle);

        this.handle = handle;

    }

    public boolean isValid() {
        return this.handle!=null;
    }

    public void invalidate() {
        this.handle = null;
    }

    public void setHandle(Intent handle) {
        this.handle = (Intent) handle.clone();
//        this.write();
    }

    public Intent getHandle() {
//        if(this.handle==null) {
//            this.read();
//        }
        if(this.handle!=null) {
            return (Intent) this.handle.clone();
        } else {
            return null;
        }
    }

    /**
     * write()
     *
     * Store token handler into a persistent storage
     *
     * @experimental
     */
    public void write() {
//        try {
//            IBinder projection = handle.getIBinderExtra("android.media.projection.extra.EXTRA_MEDIA_PROJECTION");
//        } catch (RemoteException e) {
//            Debug.getInstance().exception(e, this);
//        }

        Intent h = (Intent) this.handle.clone();
        Bundle bundle = this.handle.getExtras();
//        Parcel parcel = bundle.get();
        Parcel parcel = Parcel.obtain();
        bundle.writeToParcel(parcel, 0);

        String content = new GsonBuilder().create().toJson(parcel);
//
//        byte[] bytes = parcel.marshall();
//        String content = Base64.encodeToString(bytes, 0, bytes.length, 0);

        SharedPreferences.Editor editor = Settings.getSettings().edit();
        editor.putString("mediaprojection_token", content);
        editor.commit();
    }

    /**
     * write()
     *
     * Read token handler from a persistent storage
     *
     * @experimental
     */
    public void read() {
        String content = Settings.getSettings().getString("mediaprojection_token", null);
        Parcel parcel = new GsonBuilder().create().fromJson(content, Parcel.class);
        Bundle bundle = new Bundle();
        bundle.readFromParcel(parcel);

        Intent handle = new Intent();
        handle.putExtras(bundle);

        this.handle = handle;
    }
}
