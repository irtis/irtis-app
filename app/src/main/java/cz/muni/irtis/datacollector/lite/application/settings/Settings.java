package cz.muni.irtis.datacollector.lite.application.settings;

import android.content.Context;
import android.content.SharedPreferences;

import cz.muni.irtis.datacollector.lite.Application;


public class Settings {
	public static final String MEDIAPROJECTION_PERMISSION_ACTIVITY_DO_NOT_SHOW = "mediaprojection_permission_activity_do_not_show";
	public static final String MEDIAPROJECTION_PERMISSION_DISABLED_TIME = "mediaprojection_permission_disabled_time";
	public static final String MEDIAPROJECTION_PERMISSION_FRAME_TIME = "mediaprojection_frame_time";

	public static final String SYNCHRONIZATION_LAST = "sync_last";
	public static final String SYNCHRONIZATION_NEXT = "sync_next";

	public static final String QUESTIONNAIRES_MORNING = "questionnaires_day_morning";
	public static final String QUESTIONNAIRES_NIGHT = "questionnaires_day_night";
	public static final String QUESTIONNAIRES_LASTTIME = "questionnaires_last_time";
	public static final String QUESTIONNAIRES_INVOCATIONS_SETTING_REMINDER = "questionnaires_invocations_setting_reminder";

	static public SharedPreferences getSettings() {
		Context context = Application.getInstance().getApplicationContext();
		SharedPreferences settings = context.getSharedPreferences(context.getPackageName() + ".preferences", Context.MODE_PRIVATE);

		return settings;
	}
}
