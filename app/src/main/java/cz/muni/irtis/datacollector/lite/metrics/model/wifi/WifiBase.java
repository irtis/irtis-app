package cz.muni.irtis.datacollector.lite.metrics.model.wifi;

import android.content.Context;
import android.net.wifi.ScanResult;

import java.util.ArrayList;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi.WifiEntity;
import cz.muni.irtis.datacollector.lite.metrics.model.condition.IsWifiOn;
import cz.muni.irtis.datacollector.lite.metrics.optimizer.Optimizer;
import cz.muni.irtis.datacollector.lite.metrics.model.util.wifi.WifiScanReceiver;
import cz.muni.irtis.datacollector.lite.metrics.Metric;

/**
 * Capture available WiFi SSIDs
 *
 * Requirements by Android versions:
 *  - https://developer.android.com/guide/topics/connectivity/wifi-scan
 */
abstract public class WifiBase extends Metric {
    private final String TAG = this.getClass().getSimpleName();

    private WifiScanReceiver.ScanObserver observer;
    private WifiScanReceiver wifiScanReceiver;

    public WifiBase(Context context, int delay) {
        super(context, delay, Optimizer.buildExponential(context, delay, Time.MINUTE*30));

        wifiScanReceiver = new WifiScanReceiver(context);

        observer = new WifiScanReceiver.ScanObserver() {
            @Override
            public void onObserve(List<ScanResult> results, String connected) {
                List<String> available = null;
                if(results!=null && !results.isEmpty()) {
                    available = new ArrayList<>(results.size());
                    for (int i = 0; i < results.size(); i++) {
                        if (!available.contains(results.get(i).SSID)) {
                            if (isValidSsid(results.get(i).SSID)) {
                                // Add to list
                                available.add(results.get(i).SSID);
                                // Save to db
                                long ssidId = getWifiSsid(results.get(i).SSID);
                                if (ssidId <= 0) {
                                    saveNewWifiSsid(results.get(i).SSID);
                                }
                            }
                        }
                    }
                } else if(connected!=null && !connected.isEmpty()) {
                    long ssidId = getWifiSsid(connected);
                    if (ssidId <= 0) {
                        saveNewWifiSsid(connected);
                    }
                }

                if (isValidSsid(connected)) {
                    save(available, connected);
                } else {
                    save(available, null);
                }
            }
        };


        addPrerequisity(new IsWifiOn());
//        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
//            addPrerequisity(new IsLocationOn());
//        }
    }


    @Override
    public void run() {
        if (isPrerequisitiesSatisfied()) {
            if (getOptimizer().isActive()) {
                if (isRunning()) {
                    stop();
                }
            } else {
                start();
            }
        } else {
            stop();
        }
    }

    public void start() {
        if (!wifiScanReceiver.isAttached(observer)) {
            wifiScanReceiver.attach(observer);
        }

        wifiScanReceiver.scan(observer);
        setRunning(true);
    }

    @Override
    public void stop() {
        if(wifiScanReceiver.isAttached(observer)) {
            wifiScanReceiver.detach(observer);
        }
        setRunning(false);
    }

    protected boolean isValidSsid(String ssid) {
        boolean result =
                ssid != null &&
                !"".equals(ssid) &&
                !"<unknown ssid>".equals(ssid);
        return result;
    }

    protected long getWifiSsid(String ssid) {
        Long id = getDatabase().getWifiRepository().getIdBySSID(ssid);
        if(id!=null && id!=0) {
            return id;
        } else {
            return -1;
        }
    }

    protected List<String> getSavedWifis() {
        List<String> names = getDatabase().getWifiRepository().getAllNames();
        return names;
    }


    protected long saveNewWifiSsid(String ssid) {
        WifiEntity entity = new WifiEntity(Time.getTime(), ssid);
        long result = getDatabase().getWifiRepository().insert(entity);
        return result;
    }

    @Override
    public long save(Object... params) {
        return 0;
    }

    abstract public long save(List<String> available, String connected);
}
