package cz.muni.irtis.datacollector.lite.gui.questionnaires.types;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.NumberPicker;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;

public class TimeHoursMinutesSpinnerFragment extends TypeFragmentBase {
    protected NumberPicker vMinutesPicker;
    protected NumberPicker vHoursPicker;
    protected EditText vText;

    public TimeHoursMinutesSpinnerFragment(Context context, Question question, boolean editable) {
        super(context, question, R.layout.activity_questionnaires_form_type_time_hoursminutes, editable);
    }

    public TimeHoursMinutesSpinnerFragment(Context context, Question question, int contentLayoutId, boolean editable) {
        super(context, question, contentLayoutId, editable);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        vMinutesPicker = iniMinutes((NumberPicker) view.findViewById(R.id.minutes));
        vHoursPicker = iniHours((NumberPicker) view.findViewById(R.id.hours));

        vMinutesPicker.setEnabled(isEditable());
        vMinutesPicker.setVisibility(isEditable() ? View.VISIBLE : View.GONE);
        vHoursPicker.setEnabled(isEditable());
        vHoursPicker.setVisibility(isEditable() ? View.VISIBLE : View.GONE);


        if (getQuestion().getAnswer().isValue()) {
            String[] date = getQuestion().getAnswer().getValue().split(":");
            vMinutesPicker.setValue(Integer.valueOf(date[0]));
            vHoursPicker.setValue(Integer.valueOf(date[1]));
        }

        vText = view.findViewById(R.id.text);
        vText.setVisibility(isEditable() ? View.GONE : View.VISIBLE);
        vText.setEnabled(false);
        if (getQuestion().getAnswer().isValue()) {
            vText.setText(getQuestion().getAnswer().getValue());
        }

        vMinutesPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                Answer answer = getQuestion().getAnswer();
                answer.setValue(
                        String.format("%02d", vMinutesPicker.getValue()) + ":" +
                                String.format("%02d", vHoursPicker.getValue())
                );
                refresh();
            }
        });
        vHoursPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            Answer answer = getQuestion().getAnswer();
            answer.setValue(
                String.format("%02d", vMinutesPicker.getValue()) + ":" +
                String.format("%02d", vHoursPicker.getValue())
            );
            refresh();
            }
        });

        return view;
    }



    private NumberPicker iniHours(NumberPicker view) {
        view.setWrapSelectorWheel(true);
        view.setMinValue(0);
        view.setMaxValue(23);
        view.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
            }
        });
        return view;
    }

    private NumberPicker iniMinutes(NumberPicker view) {
        view.setWrapSelectorWheel(true);
        view.setMinValue(0);
        view.setMaxValue(59);
        view.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
            }
        });
        return view;
    }
}
