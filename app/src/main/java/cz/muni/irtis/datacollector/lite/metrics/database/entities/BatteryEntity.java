package cz.muni.irtis.datacollector.lite.metrics.database.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;

import cz.muni.irtis.datacollector.lite.metrics.database.repository.BatteryRepository;

@Entity(tableName = BatteryRepository.TABLE_NAME)
public class BatteryEntity extends EntityBase {
    @NonNull
    @ColumnInfo(name = "state_percent")
    @SerializedName("state_percent")
    private int statePercent;

    public BatteryEntity(Long datetime, int statePercent) {
        super(datetime);
        this.statePercent = statePercent;
    }

    public int getStatePercent() {
        return statePercent;
    }
}
