package cz.muni.irtis.datacollector.lite.application.widgets.lists;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerListViewAdapter<T, H extends RecyclerListViewAdapter.ItemsListViewHolderBase> extends RecyclerView.Adapter<H> {
    private RecyclerListView listView;
    private int idItemLayout;
    private List<T> items; // Items cache

    /**
     * ViewHolder
     */
    static abstract public class ItemsListViewHolderBase extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RecyclerListViewAdapter adapter;

        public ItemsListViewHolderBase(RecyclerListViewAdapter adapter, View view) {
            super(view);
            this.adapter = adapter;
            view.setOnClickListener(this);
            view.setClickable(true);
        }

        @Override
        public void onClick(View view) {
            if(adapter.getOnItemClickListener()!=null) {
                adapter.getOnItemClickListener().onClick(view, adapter.getItem(getLayoutPosition()));
            }
        }
    }

    public interface OnLayoutListener {
        /**
         * Create layout view from resource or any other specific way
         * @param parent
         * @return
         */
        public View onCreate(ViewGroup parent);
    }
    private OnLayoutListener onLayoutListener;

    public interface OnHolderListener<T, H> {
        /**
         * Create holder for row views
         * @param adapter
         * @param view
         * @return
         */
        public H onCreate(RecyclerListViewAdapter adapter, View view);
        /**
         * Fill holder with item data
         * @param holder
         * @param item
         */
        public void onBind(H holder, T item);
    }
    private OnHolderListener onHolderListener;

    public interface OnItemClickListener<T> {
        void onClick(View view, T item);
    }
    private OnItemClickListener onItemClickListener;



    /**
     * RecyclerListViewAdapter()
     *
     * @param layout Row layout
     */
    public RecyclerListViewAdapter(int layout) {
        this.idItemLayout = layout;
        this.items = new ArrayList<>();
    }
    /**
     * RecyclerListViewAdapter()
     */
    public RecyclerListViewAdapter() {
        this(-1);
    }

    public RecyclerListView getListView() {
        return listView;
    }


    @Override
    public H onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create row layout
        View view = null;
        if(onLayoutListener !=null) {
            view = onLayoutListener.onCreate(parent);
        } else if(idItemLayout >0) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(idItemLayout, parent, false);
        }

        if(view==null) {
            throw new NullPointerException("Layout view cannot be null. You need to implement onLayoutListener.onCreate() listener or setItem setItemLayout()");
        }

        // Create row view holder
        H holder = null;
        if(onHolderListener!=null) {
            holder = (H) onHolderListener.onCreate(this, view);
        }

        if(holder==null) {
            throw new NullPointerException("ViewHolder cannot be null. You need to implement setOnHolderListener.onCreate() listener");
        }

        return holder;
    }
    @Override
    public void onBindViewHolder(H holder, int position) {
        if(onHolderListener!=null) {
            onHolderListener.onBind(holder, items.get(position));
        }
    }


    public ItemsListViewHolderBase getViewHolder(int position) {
        return (ItemsListViewHolderBase) listView.findViewHolderForAdapterPosition (position);
    }

    public ItemsListViewHolderBase getViewHolder(T item) {
        int position = this.items.indexOf(item);
        return getViewHolder(position);
    }


    @Override
    @Deprecated
    public int getItemCount() {
        return getCount();
    }

    public int getCount() {
        return items.size();
    }

    public T getItem(int position) {
        return this.items.get(position);
    }

    public List<T> getItems() {
        return this.items;
    }

    public void addItem(T item, boolean refresh) {
        this.items.add(item);
        if(refresh) {
            this.refresh();
        }
    }
    public void addItem(T item) {
        this.addItem(item, false);
    }

    public void addItems(List<T> items, boolean refresh) {
        this.items.addAll(items);
        if(refresh) {
            this.refresh();
        }
    }
    public void addItems(List<T> items) {
        this.addItems(items, false);
    }

    public void setItems(List<T> items, boolean refresh) {
        if(items!=null) {
            this.items = items;
        } else {
            this.items = new ArrayList<>();
        }
        if(refresh) {
            this.refresh();
        }
    }
    public void setItems(List<T> items) {
        this.setItems(items, false);
    }

    public void remove(int position) { this.items.remove(position); }
    public void remove(T item) { this.items.remove(item); }

    public boolean isEmpty() {
        return this.items!=null && this.items.isEmpty();
    }

    public void clear() {
        this.items.clear();
    }

    public void refresh() {
        notifyDataSetChanged();
    }


    public void setListView(RecyclerListView listView) {
        this.listView = listView;
    }

    /**
     * setItemLayout()
     *
     * @param layout Row layout
     */
    public void setItemLayout(int layout) {
        this.idItemLayout = layout;
    }

    public void setOnLayoutListener(OnLayoutListener listener) {
        this.onLayoutListener = listener;
    }

    public void setOnHolderListener(OnHolderListener listener) {
        this.onHolderListener = listener;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }
    public OnItemClickListener getOnItemClickListener() {
        return this.onItemClickListener;
    }
}
