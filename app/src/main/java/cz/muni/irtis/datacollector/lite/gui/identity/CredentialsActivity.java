package cz.muni.irtis.datacollector.lite.gui.identity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.gui.LauncherActivity;
import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Credentials;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Identity;
import cz.muni.irtis.datacollector.lite.gui.identity.model.response.ConfigResponseEntity;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.BurstsModel;

public class CredentialsActivity extends ActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credentials);

        Credentials credentials = Credentials.build(getIntent().getStringExtra("data"));

        EditText eLogin = findViewById(R.id.login);
        eLogin.setText(credentials.getLogin());
        EditText ePassword = findViewById(R.id.password);
        ePassword.setText(credentials.getPassword());

        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LauncherActivity.start(CredentialsActivity.this);
            }
        });

        Identity.getInstance()
            .downloadConfig(new Repository.RepositoryListener<ConfigResponseEntity>() {
                @Override
                public void onStart() {
                }
                @Override
                public void onDone(ConfigResponseEntity item) {
                    if(item!=null) {
                        BurstsModel bursts = BurstsModel.build(getApplicationContext());
                        bursts.storeFromIdentityConfig(item);
                    }
                }
            });
    }
}
