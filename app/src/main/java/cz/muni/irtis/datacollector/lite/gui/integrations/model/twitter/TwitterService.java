package cz.muni.irtis.datacollector.lite.gui.integrations.model.twitter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;

public class TwitterService extends ServiceBase {
    private static final int RC_SIGN_IN = 4165;

    private TwitterApiClient api;
    private TwitterAuthClient client;
    private TwitterConfig options;

    private TwitterLoginButton loginButton;


    public TwitterService(Context context, TwitterConfig options, OnServiceListener onSignInListener, OnServiceListener onSignOutListener) {
        super(context, onSignInListener, onSignOutListener);

        this.options = options;
        if(this.options==null) {
            this.options = new TwitterConfig.Builder(context)
                .twitterAuthConfig(new TwitterAuthConfig(
                    context.getString(R.string.twitter_client_id),
                    context.getString(R.string.twitter_client_secret)))
                .build();
        }

        Twitter.initialize(options);
        api = TwitterCore.getInstance().getApiClient();
        client = new TwitterAuthClient();

    }
    public TwitterService(Context context, TwitterConfig options, OnServiceListener onSignInListener) {
        this(context, null, onSignInListener, null);
    }
    public TwitterService(Context context, OnServiceListener onSignInListener, OnServiceListener onSignOutListener) {
        this(context, null, onSignInListener, onSignOutListener);
    }
    public TwitterService(Context context, OnServiceListener onSignInListener) {
        this(context, null, onSignInListener, null);
    }
    public TwitterService(Context context) {
        this(context, null, null, null);
    }


    @Override
    public boolean isConnected() {
        return TwitterCore.getInstance().getSessionManager().getActiveSession()!=null;
    }

    public void signIn(Activity activity) {
        onSignInListener.onStart();
        client.authorize(activity, new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                onSignInListener.onFinished(isConnected());
            }

            @Override
            public void failure(TwitterException e) {
                onSignInListener.onError(ServiceBase.ERROR_CANCELED);
            }
        });
    }

    public void signOut() {
        if(onSignOutListener!=null) {
            onSignOutListener.onStart();
        }
        TwitterCore.getInstance().getSessionManager().clearActiveSession();
        if(onSignOutListener!=null) {
            onSignOutListener.onFinished(isConnected());
        }
    }


    public void getAccessToken() {
        TwitterCore.getInstance().getSessionManager().getActiveSession();
    }


    public void setSignInButton(final View button) {
        loginButton = (TwitterLoginButton) button;
        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                onSignInListener.onFinished(isConnected());
            }

            @Override
            public void failure(TwitterException exception) {
                Debug.getInstance().exception(exception);
                onSignInListener.onError(ServiceBase.ERROR_CANCELED);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(loginButton != null) {
            loginButton.onActivityResult(requestCode, resultCode, data);
        } else if (client != null) {
            client.onActivityResult(requestCode, resultCode, data);
        }
    }
}
