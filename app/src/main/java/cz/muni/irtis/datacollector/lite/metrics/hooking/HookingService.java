package cz.muni.irtis.datacollector.lite.metrics.hooking;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.ComponentName;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import java.util.Arrays;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;


public class HookingService extends AccessibilityService {
    private boolean isTrackable = false;

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
//        info.eventTypes = AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED;
//        info.eventTypes = AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED |
//            AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED;
        info.eventTypes = AccessibilityEvent.TYPES_ALL_MASK;
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_ALL_MASK;
        info.notificationTimeout = 5000;
        info.packageNames = getPackages();
//        info.packageNames = null;
        setServiceInfo(info);
    }


    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if(event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
            ComponentName component = new ComponentName(
                event.getPackageName().toString(),
                event.getClassName().toString()
            );

            if (Arrays.asList(getPackages()).contains(component.getPackageName())) {
                ActivityInfo activity = getActivity(component);
                if (activity != null) {

                }
                isTrackable = true;
            } else {
//                isTrackable = false;
            }
        }

        if(isTrackable) {
            try {
                AccessibilityNodeInfo source = event.getSource();
                if (source != null) {
                    getTexts(source);
                }
            } catch (Exception e) {
                Debug.getInstance().exception(e);
            }
        }
    }

    @Override
    public void onInterrupt() {

    }

    private ActivityInfo getActivity(ComponentName componentName) {
        try {
            return getPackageManager().getActivityInfo(componentName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    private void getTexts(AccessibilityNodeInfo source) {
        if (source != null && source.isVisibleToUser()) {
            if (Arrays.asList(new String[]{"android.widget.TextView"}).contains(source.getClassName()) && source.getText()!=null) {
                BusProvider.build().post(TextEvent.build(source));
            }
            for (int i = 0; i < source.getChildCount(); i++) {
                try {
                    AccessibilityNodeInfo child = source.getChild(i);
                    if (child != null) {
                        getTexts(child);
                        child.recycle();
                    }
                } catch (Exception e) {
                    Debug.getInstance().exception(e);
                }
            }
        }
    }


    private String[] getPackages() {
        return new String[] {
            "com.google.android.youtube",                   // Youtube - ok
            "com.netflix.mediaclient",                      // Netflix
            "com.amazon.avod.thirdpartyclient",             // Amazon
            "eu.hbogo.android",                             // HBO

//            "com.facebook.katana", "com.facebook.lite",     // Facebook - ignored
            "com.linkedin.android",                         // Linkedin
            "com.pinterest",                                // Pinterest
            "com.reddit.frontpage",                         // Reddit

//            "com.facebook.orca", "com.facebook.mlite",      // Messenger - ignored
            "com.instagram.android",                        // Instagram - ok
            "com.whatsapp",                                 // Whatsapp - ok
            "com.twitter.android",                        // Twitter - ok
            "com.viber.voip",                             // Viber
            "com.tinder",                                 // Tinder - ok
            "com.zhiliaoapp.musically",                   // Tiktok - ok
            "com.snapchat.android",                       // Snapchat
            "tv.twitch.android.app",                        // Twitch - ok
            "org.telegram.messenger",                       // Telegram
            "org.thoughtcrime.securesms",                   // Signal

            "com.microsoft.teams",                          // Teams
            "us.zoom.videomeetings",                        // Zoom

            "com.shazam.android",                            // Shazam
            "com.spotify.music",                            // Spotify
        };
    }
}
