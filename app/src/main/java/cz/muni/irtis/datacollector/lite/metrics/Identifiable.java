package cz.muni.irtis.datacollector.lite.metrics;

/**
 * Identifiable
 *
 * Allows the metric to have a specific identity
 */
public interface Identifiable {
    int getIdentifier();
    String getName();
}
