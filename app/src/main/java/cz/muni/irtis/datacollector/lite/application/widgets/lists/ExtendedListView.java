package cz.muni.irtis.datacollector.lite.application.widgets.lists;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * ViewList
 *
 */
public class ExtendedListView extends ListView {
	private Integer adapterLastCount;

	public interface OnItemClickListener<T> {
		public void onClick(ExtendedListViewAdapter adapter, T item);
	}
	private OnItemClickListener onItemClickListener;
	private int onItemClickPosition = -1;

	public interface OnScrollListener {
		public void onScroll(Integer count);
	}
	private OnScrollListener onScrollListener;



	public ExtendedListView(Context context)
	{
		super(context);
		this.ini();
	}

	public ExtendedListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.ini();
	}

	public ExtendedListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.ini();
	}



	protected void ini()
	{
		this.adapterLastCount = 0;
		this.setOnScrollListener(new AbsListView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				int lastInScreen = firstVisibleItem + visibleItemCount;
				if(lastInScreen == totalItemCount) {
//					Debug.getInstance().log("ViewListScroll", "totalItemCount: "+totalItemCount+", firstVisibleItem: "+firstVisibleItem+", visibleItemCount:"+visibleItemCount);
					if(onScrollListener != null) {
						ExtendedListViewAdapter adapter = (ExtendedListViewAdapter) getAdapter();
						if(adapter!=null && adapterLastCount<adapter.getCount()) {
							adapterLastCount = adapter.getCount();
							onScrollListener.onScroll(adapter.getCount());
						}
					}
				}
			}
		});
	}



	public void setOnItemClickListener(OnItemClickListener listener)
	{
		this.onItemClickListener = listener;
		super.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
				onItemClickPosition = position;
				onItemClickListener.onClick((ExtendedListViewAdapter) getAdapter(), getAdapter().getItem(position));
			}
		});
	}

	public int getOnItemClickPosition()
	{
		return this.onItemClickPosition;
	}

	public void setOnScrollListener(OnScrollListener listener)
	{
		this.onScrollListener = listener;
	}
}
