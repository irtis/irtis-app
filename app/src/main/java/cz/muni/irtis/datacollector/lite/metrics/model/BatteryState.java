package cz.muni.irtis.datacollector.lite.metrics.model;

import android.content.Context;
import android.os.BatteryManager;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.BatteryEntity;
import cz.muni.irtis.datacollector.lite.metrics.Metric;
import cz.muni.irtis.datacollector.lite.metrics.optimizer.Optimizer;

import static android.content.Context.BATTERY_SERVICE;

/**
 * Capture battery state
 */
public class BatteryState extends Metric {
    private final String TAG = this.getClass().getSimpleName();
    public static final int IDENTIFIER = 255;

    public BatteryState(Context context, Integer delay) {
        super(context, delay,
            Optimizer.buildConstant(context, Time.MINUTE*10));
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "Battery";
    }

    /**
     * Retrieve the battery state.
     */
    @Override
    public void run() {
        setRunning(true);
        if(!getOptimizer().isActive()) {
            BatteryManager bm = (BatteryManager) getContext().getSystemService(BATTERY_SERVICE);
            save(bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY));
        }
    }

    @Override
    public void stop() {
        setRunning(false);
    }

    @Override
    public long save(Object... params) {
        int level = (int) params[0];

        try {
            Debug.getInstance().log(TAG, String.valueOf(level));
            return getDatabase().getBatteryStateRepository().insert(
                    new BatteryEntity(Time.getTime(), level));
        } catch (Exception e) {
            Debug.getInstance().exception(e);
            return -1;
        }
    }
}
