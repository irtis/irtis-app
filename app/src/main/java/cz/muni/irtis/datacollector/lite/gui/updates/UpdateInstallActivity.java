package cz.muni.irtis.datacollector.lite.gui.updates;

import android.os.Bundle;

import androidx.annotation.Nullable;

import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;


public class UpdateInstallActivity extends ActivityBase {
    private static final int REQUEST_CODE = 625;

     @Override
    protected void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);

        Updates updates = Updates.build(this);
        if(updates.isUpdate()) {
            startActivityForResult(updates.createInstallDialogIntent(), REQUEST_CODE);
//            PersistentService.stop(this);
//            finishAffinity();
        } else {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
