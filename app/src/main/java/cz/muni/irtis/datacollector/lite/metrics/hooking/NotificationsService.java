package cz.muni.irtis.datacollector.lite.metrics.hooking;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.model.Notifications;

public class NotificationsService extends NotificationListenerService {
    public static final String TAG = NotificationsService.class.getSimpleName();

    private NotificationReceiver receiver;


    @Override
    public void onCreate(){
        super.onCreate();
        Debug.getInstance().log(TAG, "created");

        receiver = new NotificationReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(TAG);
        registerReceiver(receiver ,filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public void onListenerConnected() {
        super.onListenerConnected();
        Debug.getInstance().log(TAG, "connected");
    }

    @Override
    public void onNotificationPosted(StatusBarNotification notification) {
        NotificationBundle bundle = getBundle(notification);

        Intent broadcast = new Intent(Notifications.TAG);
        broadcast.putExtra("source", TAG);
        broadcast.putExtra("action", "posted");
        broadcast.putExtra("content", bundle.toString());
        sendBroadcast(broadcast);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification notification) {
        NotificationBundle bundle = getBundle(notification);
        bundle.setRemoved(Time.getTime());

        Intent broadcast = new Intent(Notifications.TAG);
        broadcast.putExtra("source", TAG);
        broadcast.putExtra("action", "removed");
        broadcast.putExtra("content", bundle.toString());
        sendBroadcast(broadcast);
    }


    public class NotificationReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String source = intent.getStringExtra("source");
            String action = intent.getStringExtra("action");
            Intent broadcast = new Intent(source);
            broadcast.putExtra("source", TAG);
            broadcast.putExtra("action", action);

            List<StatusBarNotification> items = Arrays.asList(getActiveNotifications());
            if(items!=null && !items.isEmpty()) {
                List<NotificationBundle> bundle = new ArrayList<>();
                Iterator<StatusBarNotification> iItems = items.iterator();
                while (iItems.hasNext()) {
                    StatusBarNotification item = iItems.next();

                    // Get all ongoings
                    if (action.equals("ongoing") && item.isOngoing() && !item.getPackageName().equals("android")) {
                        bundle.add(getBundle(item));
                    }
                }

                broadcast.putExtra("content", bundle.toString());
                sendBroadcast(broadcast);
            }
        }
    }


    private NotificationBundle getBundle(StatusBarNotification notification) {
        String packagage = notification.getPackageName();
        String title = notification.getNotification().extras.getCharSequence(Notification.EXTRA_TITLE_BIG) !=null ? notification.getNotification().extras.getCharSequence(Notification.EXTRA_TITLE_BIG).toString() :
                (notification.getNotification().extras.getCharSequence(Notification.EXTRA_TITLE) != null ?
                    notification.getNotification().extras.getCharSequence(Notification.EXTRA_TITLE).toString() : null);
        String text = notification.getNotification().extras.getCharSequence(Notification.EXTRA_BIG_TEXT) !=null ? notification.getNotification().extras.getCharSequence(Notification.EXTRA_BIG_TEXT).toString() :
                (notification.getNotification().extras.getCharSequence(Notification.EXTRA_TEXT) != null ?
                    notification.getNotification().extras.getCharSequence(Notification.EXTRA_TEXT).toString() : null);
        Long posted = notification.getPostTime();

        return new NotificationBundle(packagage, title, text, posted);
    }
}
