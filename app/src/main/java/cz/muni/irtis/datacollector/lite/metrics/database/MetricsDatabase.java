package cz.muni.irtis.datacollector.lite.metrics.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.io.File;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.NotificationEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.PlaybackEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.StepEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationBackgroundEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationForegroundEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.BatteryEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.HeadphonesEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationForegroundEntity2;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenTapEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenshotMetadataEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi.WifiAvailableEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi.WifiConnectedEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.LocationEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.phone.PhoneCallEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.phone.PhoneSmsEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.ActivityEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.RuntimeEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.screen.ScreenshotEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi.WifiEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ApplicationsBackgroundRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ApplicationsForegroundRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ApplicationsForegroundRepository2;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ApplicationsRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.BatteryRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.HeadphonesRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.LocationsRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.NotificationsRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.PlaybackRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ScreenRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ScreenTapsRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ScreenshotsMetadataRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ScreenshotsRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.StepsRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.WifiAvailableRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.PhoneCallsRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.WifiConnectedRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ActivitiesRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.RuntimeRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.PhoneSmsesRepository;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.WifiRepository;

/**
 * Room ORM DB access class
 *
 * Documentation:
 *  - https://developer.android.com/training/data-storage/room
 *  - https://developer.android.com/reference/android/arch/persistence/room/package-summary
 *
 */
@Database(
    version = 2,
    entities = {
        ScreenshotEntity.class,
        ScreenshotMetadataEntity.class,

        BatteryEntity.class,
        LocationEntity.class,
        ActivityEntity.class,
        HeadphonesEntity.class,
        PlaybackEntity.class,

        PhoneCallEntity.class,
        PhoneSmsEntity.class,

        ScreenEntity.class,
        ScreenTapEntity.class,

        ApplicationEntity.class,
        ApplicationForegroundEntity.class,
        ApplicationForegroundEntity2.class,
        ApplicationBackgroundEntity.class,

        WifiEntity.class,
        WifiAvailableEntity.class,
        WifiConnectedEntity.class,

        StepEntity.class,
        NotificationEntity.class,

        RuntimeEntity.class
    },
    exportSchema = false
)
public abstract class MetricsDatabase extends RoomDatabase {
    private static volatile MetricsDatabase INSTANCE;

    public abstract ScreenshotsRepository getScreenshotRepository();
    public abstract ScreenshotsMetadataRepository getScreenshotsMetadataRepository();

    public abstract BatteryRepository getBatteryStateRepository();
    public abstract LocationsRepository getLocationRepository();
    public abstract ActivitiesRepository getPhysicalActivityRepository();
    public abstract HeadphonesRepository getHeadphonesRepository();
    public abstract PlaybackRepository getPlaybackRepository();

    public abstract PhoneCallsRepository getCallHistoryRepository();
    public abstract PhoneSmsesRepository getSmsConversationRepository();

    public abstract ScreenRepository getScreenRepository();
    public abstract ScreenTapsRepository getScreenTapsRepository();

    public abstract ApplicationsRepository getApplicationRepository();
    public abstract ApplicationsForegroundRepository getForegroundApplicationRepository();
    public abstract ApplicationsForegroundRepository2 getForegroundApplicationRepository2();
    public abstract ApplicationsBackgroundRepository getBackgroundApplicationRepository();

    public abstract WifiRepository getWifiRepository();
    public abstract WifiAvailableRepository getAvailableWifiRepository();
    public abstract WifiConnectedRepository getConnectedWifiRepository();

    public abstract StepsRepository getStepsRepository();
    public abstract NotificationsRepository getNotificationsRepository();

    public abstract RuntimeRepository getRuntimeRepository();


    /**
     * Get database instance. If none exists, append new & return it.
     * @param context app context
     * @return database instance
     */
    public static MetricsDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MetricsDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    MetricsDatabase.class, "metrics_database.db")
                    .allowMainThreadQueries()   // @todo optimize UI thread execution
                    .addMigrations(MIGRATION_1_2)
                    .build();
                }
            }
        }
        return INSTANCE;
    }

    public long getSize() {
        return getFile().length();
    }

    public File getFile() {
        return new File(getOpenHelper().getReadableDatabase().getPath());
    }


    /**
     * @date 14.05.2020
     * @changes a new table for metric
     */
    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE `steps` (`steps` INTEGER, `datetime` INTEGER NOT NULL, PRIMARY KEY(`datetime`))");
        }
    };
}
