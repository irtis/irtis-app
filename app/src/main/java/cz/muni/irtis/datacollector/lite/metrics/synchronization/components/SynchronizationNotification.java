package cz.muni.irtis.datacollector.lite.metrics.synchronization.components;


import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.gui.LauncherActivity;
import cz.muni.irtis.datacollector.lite.application.components.NotificationBase;

public class SynchronizationNotification extends NotificationBase {
    private static final int IDENTIFIER = 3547;

    public SynchronizationNotification(Context context) {
        super(context);
    }

    static public SynchronizationNotification build(Context context) {
        return new SynchronizationNotification(context);
    }

    public Notification create() {
        Intent intent = new Intent(context, LauncherActivity.class);
        PendingIntent pending = PendingIntent.getActivity(
                context, IDENTIFIER, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_LOW)
            .setSmallIcon(R.drawable.ic_sync_grey_24dp)
            .setSubText(context.getString(R.string.synchronization_service_type))
            .setContentTitle(context.getString(R.string.synchronization_service_running))
            .setContentIntent(pending)
            .setOngoing(true);

        return notification = builder.build();
    }

    public void show() {
        manager.notify(getIdentifierId(), create());
    }

    public void hide() {
        if(notification!=null){
            manager.cancel(getIdentifierId());
        }
    }

    static public int getIdentifierId() {
        return IDENTIFIER;
    }
}
