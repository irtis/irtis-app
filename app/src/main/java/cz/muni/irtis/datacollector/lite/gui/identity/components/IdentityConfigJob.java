package cz.muni.irtis.datacollector.lite.gui.identity.components;

import android.app.job.JobParameters;
import android.content.Context;

import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.components.JobServiceBase;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.gui.identity.model.Identity;
import cz.muni.irtis.datacollector.lite.gui.identity.model.response.ConfigResponseEntity;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.BurstsModel;

public class IdentityConfigJob extends JobServiceBase {
    public static final int JOB_ID = 9513;

    public static IdentityConfigJob build() {
        return new IdentityConfigJob();
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        start(getApplicationContext());
        schedule(getApplicationContext());
        return true;
    }

    public static void schedule(Context context) {
        scheduleWithNetwork(context, IdentityConfigJob.class, JOB_ID, Config.IDENTITY_CONFIG_REFRESH_PERIOD);
    }

    public static boolean isSet(Context context) {
        return JobServiceBase.isSet(context, JOB_ID);
    }

    public void start(final Context context) {
        Identity.getInstance()
            .downloadConfig(new Repository.RepositoryListener<ConfigResponseEntity>() {
                @Override
                public void onStart() {
                }
                @Override
                public void onDone(ConfigResponseEntity item) {
                    if(item!=null) {
                        BurstsModel bursts = BurstsModel.build(context);
                        bursts.storeFromIdentityConfig(item);
                    }
                }
            });
    }
}
