package cz.muni.irtis.datacollector.lite.gui.questionnaires.types;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.viewpager.widget.ViewPager;

import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.FragmentBase;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.application.widgets.ExtendedViewPager;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.QuestionnairesSkipDialog;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;

abstract public class TypeFragmentBase extends FragmentBase {
    private ExtendedViewPager pager;
    private ImageView vPrev;
    private ImageView vNext;

    private QuestionnairesModel model;
    private Question question;
    MutableLiveData<Boolean> liveSelected;

    private Long start;

    private int contentLayoutId;
    private boolean editable;


    public TypeFragmentBase() {
    }

    public TypeFragmentBase(Context context, Question question, int contentLayoutId, boolean editable) {
        super(R.layout.activity_questionnaires_form_question);
        setContext(context);
        this.model = new QuestionnairesModel(getContext());
        this.question = question;
        this.contentLayoutId = contentLayoutId;
        this.editable = editable;

        if(isEditable() && question.isShuffle()) {
            if(question.getType().equals(Question.TYPE_CHECK) || question.getType().equals(Question.TYPE_SCALE)) {
                question.shuffle();
            }
        }
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        liveSelected = new MutableLiveData<Boolean>();
        liveSelected.observe(this.getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean selected) {
                if(vNext!=null) {
//                    vNext.setVisibility(selected ? View.VISIBLE : View.GONE);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        // Question
        if(question!=null) {
            TextView vQuestion = view.findViewById(R.id.question);
            vQuestion.setText(question.getText());
        }

        // Answer item
        LayoutInflater.from(getContext()).inflate(contentLayoutId, (ViewGroup) view.findViewById(R.id.answers));

        // Next
        vNext = view.findViewById(R.id.next);
        if(vNext!=null) {
            vNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(getQuestion().isCompleted()) {
                        next();
                    } else {
                        new QuestionnairesSkipDialog(getContext(), new QuestionnairesSkipDialog.OnSubmitListener() {
                            @Override
                            public void onSubmit() {
                                next();
                            }
                        }).show();
                    }

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    View view = getActivity().getCurrentFocus();
                    if (view == null) {
                        view = new View(getActivity());
                    }
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            });

            if(isEditable()) {
                boolean isFilled = false;
                if (question != null && question.getAnswers() != null) {
                    Iterator<Answer> iAnswers = question.getAnswers().iterator();
                    while (iAnswers.hasNext()) {
                        if (iAnswers.next().isValue()) {
                            isFilled = true;
                            break;
                        }
                    }
                }

                isFilled = true;
                vNext.setVisibility(isFilled ? View.VISIBLE : View.GONE);
            } else {
                vNext.setVisibility(View.GONE);
            }
        }

        vPrev = view.findViewById(R.id.prev);
        if(vPrev!=null) {
            vPrev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    prev();

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    View view = getActivity().getCurrentFocus();
                    if (view == null) {
                        view = new View(getActivity());
                    }
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            });

            if(isEditable()) {
                vPrev.setVisibility(!pager.isFirstItem(this) ? View.VISIBLE : View.GONE);
            } else {
                vPrev.setVisibility(View.GONE);
            }
        }

        return view;
    }

    @Override
    public void onVisibleView(boolean visible) {
        super.onVisibleView(visible);
        if(visible) {
            start = Time.getTime();
        }
    }

    public void setViewPager(ViewPager pager) {
        this.pager = (ExtendedViewPager) pager;
    }

    public QuestionnairesModel getModel() {
        return model;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Question getQuestion() {
        return question;
    }

    public boolean isEditable() {
        return editable;
    }

    public void next() {
        if (question!=null) {
            save();

            Question qNext = null;
            Integer position = 0;

            if(question.getLink()!=null) {
                // Determine the next item based on link destination
                qNext = question.getQuestionnaire().getQuestionById(question.getLink());
                while (qNext!=null && qNext.isFiltered()) {
                    qNext = question.getQuestionnaire().getQuestionById(qNext.getLink());

                }
            } else {
                // Determine the next item based on the position
                qNext = question.getQuestionnaire().getQuestionByPosition(question.getPosition() + 1);
                while (qNext!=null && qNext.isFiltered()) {
                    qNext = question.getQuestionnaire().getQuestionByPosition(qNext.getPosition() + 1);
                }
            }

            if(qNext!=null) {
                // Get the position of the next item
                position = question.getQuestionnaire().getQuestionIndex(qNext);
            } else {
                // If there is not next item, end the list
                position = pager.getItemsCount()-1;
            }

            if((position-pager.getCurrentItem())>1) {
                pager.setSmoothScroll(false);
            }
            pager.setCurrentItem(position);
            pager.setSmoothScroll(true);
        } else {
            pager.setCurrentItem(pager.getCurrentItem() + 1);
        }
    }

    public void prev() {
        if(pager.getPosition()-pager.getPositionPrevious() > 1) {
            pager.setSmoothScroll(false);
        }
        pager.setPreviousItem();
        pager.setSmoothScroll(true);
    }


    public void save() {
        if(editable && question!=null) {
            Long time = Time.getTime() - start;
            Integer elapsed = question.getElapsed();
            question.setElapsed(time.intValue() + question.getElapsed());
            question.setSkipped(!question.isCompleted());

            Iterator<Answer> iAnswers = question.getAnswers().iterator();
            while (iAnswers.hasNext()) {
                Answer answer = iAnswers.next();
                if(answer.isValue()) {

                }
            }

            model.store(question.getQuestionnaire(), new Repository.RepositoryListener<Boolean>() {
                @Override
                public void onStart() {
                }
                @Override
                public void onDone(Boolean result) {
                }
            });
        }
    }

    public void refresh() {
        if(isEditable()) {
            switch (question.getType()) {
                default:
                    liveSelected.setValue(question.getAnswer().getValue() != null);
                    break;
                case Question.TYPE_CHECK:
                case Question.TYPE_SCALE:
                    liveSelected.setValue(question.getAnswersSelected() != null && !question.getAnswersSelected().isEmpty());
                    break;
            }
        }
    }

}
