package cz.muni.irtis.datacollector.lite.gui.questionnaires.components;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;

import cz.muni.irtis.datacollector.R;

public class QuestionnairesSkipDialog extends Dialog {
    private AlertDialog dialog;
    private OnSubmitListener listener;

    public interface OnSubmitListener {
        void onSubmit();
    }

    public QuestionnairesSkipDialog(Context context, OnSubmitListener listener) {
        super(context);
        this.listener = listener;
        this.dialog = onBuilderCreate(null);
    }


    protected AlertDialog onBuilderCreate(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder
            .setTitle(R.string.questionnaires_from_skip_title)
            .setMessage(R.string.questionnaires_from_skip_message)
            .setPositiveButton(getContext().getString(R.string.questionnaires_from_skip_yes), new OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    listener.onSubmit();
                    close();
                }
            })
            .setNegativeButton(getContext().getString(R.string.questionnaires_from_skip_no), new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    close();
                }
            });

        return builder.create();
    }

    public void show() { this.dialog.show(); }

    public void hide() { this.dialog.hide(); }

    public void close() { this.dialog.hide(); }

    public AlertDialog getDialog() { return this.dialog; }
}