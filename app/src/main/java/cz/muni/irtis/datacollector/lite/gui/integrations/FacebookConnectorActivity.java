package cz.muni.irtis.datacollector.lite.gui.integrations;

import android.os.Bundle;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.facebook.FacebookIntegration;

public class FacebookConnectorActivity extends ConnectorActivityBase {
    public FacebookConnectorActivity() {
        super(R.layout.activity_integrations_facebook_connector);
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setIntegration(new FacebookIntegration(this, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onError(int code) {
                refresh();
            }

            @Override
            public void onFinished(boolean isLogged) {
                Debug.getInstance().task(getIntegration().getName(), "Connected = " + (isLogged ? "true" : "false"));
                refresh();
            }
        }, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onError(int code) {
            }

            @Override
            public void onFinished(boolean isLogged) {
                Debug.getInstance().task(getIntegration().getName(), "Disonnected");
                refresh();
            }
        }));
    }
}
