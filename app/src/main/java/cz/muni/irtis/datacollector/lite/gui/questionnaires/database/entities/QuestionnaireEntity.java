package cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.QuestionnairesRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;

@Entity(tableName = QuestionnairesRepository.TABLE_NAME)
public class QuestionnaireEntity {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private Long id;
    @ColumnInfo(name = "type")
    private Integer type;
    @ColumnInfo(name = "identifier")
    private String identifier;
    @ColumnInfo(name = "configurable")
    private Boolean configurable;
    @ColumnInfo(name = "questionnaire_id")
    private Long idQuestionnaire;
    @ColumnInfo(name = "people_questionnaire_id")
    private Long idPeopleQuestionnaire;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "group")
    private String group;
    @ColumnInfo(name = "source")
    private String source;


    @ColumnInfo(name = "created")
    private Long created;       // Time when the survey was created by the administrator or system
    @ColumnInfo(name = "updated")
    private Long updated;       // Time when the survey was updated by the administrator
    @ColumnInfo(name = "starting")
    private Long starting;      // Time when the survey can be available to the user
    @ColumnInfo(name = "ending")
    private Long ending;        // Time when the survey cannot be available to the user
    @ColumnInfo(name = "invoking")
    private Long invoking;        // Time when the survey can become available to the user
    @ColumnInfo(name = "closing")
    private Long closing;       // Time when the survey would be open for the user to answer (closing = ([starting, ending] -> NOW() + validity * 60000)
    @ColumnInfo(name = "notified")
    private Long notified;      // Time when the survey was notified to the user via notification GUI
    @ColumnInfo(name = "silenced")
    private Long silenced;      // Time when the survey was silenced by the user
    @ColumnInfo(name = "opened")
    private Long opened;        // Time when the survey was opened by the user
    @ColumnInfo(name = "completed")
    private Long completed;     // Time when the survey was completed by the user



    @ColumnInfo(name = "uploaded")
    private Long uploaded;      // Time when the survey was received from the user on the server




    static public QuestionnaireEntity build(Questionnaire item) {
        QuestionnaireEntity instance = new QuestionnaireEntity();
        instance
            .setId(item.getId())
            .setType(item.getType())
            .setIdentifier(item.getIdentifier())
            .setConfigurable(item.getConfigurable())
            .setIdQuestionnaire(item.getIdQuestionnaire())
            .setIdPeopleQuestionnaire(item.getIdPeopleQuestionnaire())
            .setTitle(item.getTitle())
            .setGroup(item.getGroup())
            .setSource(item.getSource()!=null ? item.getSource().getAbsolutePath() : null)

            .setCreated(item.getCreated())
            .setUpdated(item.getUpdated())
            .setStarting(item.getStarting())
            .setEnding(item.getEnding())
            .setInvoking(item.getInvoking())
            .setClosing(item.getClosing())

            .setNotified(item.getNotified())
            .setSilenced(item.getSilenced())
            .setOpened(item.getOpened())
            .setCompleted(item.getCompleted())

            .setUploaded(item.getUploaded());

        return instance;
    }

    @Ignore
    public QuestionnaireEntity() { }

    public QuestionnaireEntity(@NonNull Long id, Integer type, String identifier, Boolean configurable, Long idQuestionnaire, Long idPeopleQuestionnaire, String title, String group, String source, Long created, Long updated, Long starting, Long ending, Long invoking, Long closing, Long notified, Long silenced, Long opened, Long completed, Long uploaded) {
        this.id = id;
        this.type = type;
        this.identifier = identifier;
        this.configurable = configurable;
        this.idQuestionnaire = idQuestionnaire;
        this.idPeopleQuestionnaire = idPeopleQuestionnaire;
        this.title = title;
        this.group = group;
        this.source = source;
        this.created = created;
        this.updated = updated;
        this.starting = starting;
        this.ending = ending;
        this.invoking = invoking;
        this.closing = closing;
        this.notified = notified;
        this.silenced = silenced;
        this.opened = opened;
        this.completed = completed;
        this.uploaded = uploaded;
    }

    public QuestionnaireEntity setId(@NonNull Long id) {
        this.id = id;
        return this;
    }

    public Long getIdQuestionnaire() {
        return idQuestionnaire;
    }

    public QuestionnaireEntity setIdQuestionnaire(Long idQuestionnaire) {
        this.idQuestionnaire = idQuestionnaire;
        return this;
    }

    public QuestionnaireEntity setIdPeopleQuestionnaire(Long idPeopleQuestionnaire) {
        this.idPeopleQuestionnaire = idPeopleQuestionnaire;
        return this;
    }

    public QuestionnaireEntity setType(Integer type) {
        this.type = type;
        return this;
    }

    public QuestionnaireEntity setIdentifier(String identifier) {
        this.identifier = identifier;
        return this;
    }

    public QuestionnaireEntity setTitle(String title) {
        this.title = title;
        return this;
    }

    public QuestionnaireEntity setGroup(String group) {
        this.group = group;
        return this;
    }

    public QuestionnaireEntity setCreated(Long created) {
        this.created = created;
        return this;
    }

    public QuestionnaireEntity setUpdated(Long updated) {
        this.updated = updated;
        return this;
    }

    public QuestionnaireEntity setStarting(Long starting) {
        this.starting = starting;
        return this;
    }

    public QuestionnaireEntity setEnding(Long ending) {
        this.ending = ending;
        return this;
    }

    public QuestionnaireEntity setInvoking(Long invoking) {
        this.invoking = invoking;
        return this;
    }

    public QuestionnaireEntity setClosing(Long closing) {
        this.closing = closing;
        return this;
    }

    public QuestionnaireEntity setNotified(Long notified) {
        this.notified = notified;
        return this;
    }

    public QuestionnaireEntity setSilenced(Long silenced) {
        this.silenced = silenced;
        return this;
    }

    public QuestionnaireEntity setOpened(Long opened) {
        this.opened = opened;
        return this;
    }

    public QuestionnaireEntity setCompleted(Long completed) {
        this.completed = completed;
        return this;
    }

    public QuestionnaireEntity setUploaded(Long uploaded) {
        this.uploaded = uploaded;
        return this;
    }

    public QuestionnaireEntity setSource(String source) {
        this.source = source;
        return this;
    }




    public Long getId() {
        return id;
    }

    public Long getIdPeopleQuestionnaire() {
        return idPeopleQuestionnaire;
    }

    public Integer getType() {
        return type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getTitle() {
        return title;
    }

    public String getGroup() {
        return group;
    }

    public String getSource() {
        return source;
    }

    public Long getCreated() {
        return created;
    }
    public String getCreatedFormatted() {
        return Time.getTimeStamp(created, "HH:mm dd-MM-yyyy");
    }

    public Long getUpdated() {
        return updated;
    }
    public String getUpdatedFormatted() {
        return Time.getTimeStamp(updated, "HH:mm dd-MM-yyyy");
    }

    public Long getStarting() {
        return starting;
    }

    public Long getEnding() {
        return ending;
    }

    public String getIntervalFormatted() {
        return Time.getTimeStamp(starting, "HH:mm") + " - " + Time.getTimeStamp(ending, "HH:mm dd.MM.yyyy");
    }

    public Long getInvoking() {
        return invoking;
    }
    public String getInvokingFormatted() {
        return Time.getTimeStamp(invoking, "HH:mm dd-MM-yyyy");
    }

    public Long getClosing() {
        return closing;
    }

    public Long getNotified() {
        return notified;
    }

    public boolean isNotified() {
        return notified!=null && !notified.equals(0);
    }

    public Long getSilenced() {
        return silenced;
    }

    public Long getOpened() {
        return opened;
    }

    public boolean isCompleted() {
        return completed!=null && !completed.equals(0);
    }
    public Long getCompleted() {
        return completed;
    }
    public String getCompletedFormatted() {
        return Time.getTimeStamp(getCompleted());
    }


    public Boolean getConfigurable() {
        return configurable;
    }

    public QuestionnaireEntity setConfigurable(Boolean configurable) {
        this.configurable = configurable;
        return this;
    }

    public boolean isUploaded() {
        return uploaded!=null && !uploaded.equals(0);
    }
    public Long getUploaded() {
        return uploaded;
    }
    public String getUploadedFormatted() {
        return Time.getTimeStamp(getUploaded());
    }

}
