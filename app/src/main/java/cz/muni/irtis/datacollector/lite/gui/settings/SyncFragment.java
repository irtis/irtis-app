package cz.muni.irtis.datacollector.lite.gui.settings;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.synchronization.MetricsSynchronizationService;

public class SyncFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.sync_screen, rootKey);

        findPreference("sync_now").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                MetricsSynchronizationService.start(getContext());
                return false;
            }
        });

        SharedPreferences settings = Settings.getSettings();
        String last = settings.getLong(Settings.SYNCHRONIZATION_LAST, 0)!=0 ?
                Time.getTimeStamp(settings.getLong(Settings.SYNCHRONIZATION_LAST, 0)) : getString(R.string.main_never);
        String next = settings.getLong(Settings.SYNCHRONIZATION_NEXT, 0)!=0 ?
                Time.getTimeStamp(settings.getLong(Settings.SYNCHRONIZATION_NEXT, 0)) : getString(R.string.main_never);
        findPreference("last_sync_info").setSummary(last);
        findPreference("next_sync_scheduled").setSummary(next);
    }
}
