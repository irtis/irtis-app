package cz.muni.irtis.datacollector.lite.metrics.model.wifi;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;

import java.util.List;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi.WifiAvailableEntity;

/**
 * Capture available WiFi SSIDs
 */
public class AvailableWifi extends WifiBase {
    private final String TAG = this.getClass().getSimpleName();
    public static final int IDENTIFIER = 253;

    public AvailableWifi(Context context, Integer delay) {
        super(context, delay);
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "Wifi available";
    }

    @Override
    public long save(List<String> available, String connected) {
        String availableList = "";
        if(available!=null) {
            for (int i = 0; i < available.size(); i++) {
                availableList += (!availableList.isEmpty() ? ", " : "") + available.get(i);

                long ssidId = getWifiSsid(available.get(i));
                if (ssidId > 0) {
                    WifiAvailableEntity entity = new WifiAvailableEntity(Time.getTime(), ssidId);
                    try {
                        getDatabase().getAvailableWifiRepository().insert(entity);
                    } catch (SQLiteConstraintException exception) {
                        Debug.getInstance().exception(exception, null, "Time: "+Time.getTime());
                    }
                }
            }
        }

        if(!availableList.isEmpty()) {
            Debug.getInstance().log(TAG, availableList);
        }

        return 0;
    }
}
