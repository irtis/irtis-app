package cz.muni.irtis.datacollector.lite.gui.questionnaires.components;

import android.app.job.JobParameters;
import android.content.Context;

import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.components.JobServiceBase;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesService;

public class QuestionnairesJob extends JobServiceBase {
    public static final int JOB_ID = 8545;

    public static QuestionnairesJob build() {
        return new QuestionnairesJob();
    }

    @Override
    public boolean onStartJob(Context context, JobParameters params) {
        if(isExecutable()) {
            QuestionnairesService.start(context);
            Settings.getSettings().edit()
                    .putLong(Settings.QUESTIONNAIRES_LASTTIME, Time.getTime())
                    .commit();
        }
        return true;
    }



    public static void schedule(Context context) {
        scheduleWithNetwork(context, QuestionnairesJob.class, JOB_ID, Config.QUESTIONNAIRES_REFRESH_PERIOD);
    }

    public static boolean isSet(Context context) {
        return JobServiceBase.isSet(context, JOB_ID);
    }

    public boolean isExecutable() {
        Long time = Settings.getSettings().getLong(Settings.QUESTIONNAIRES_LASTTIME, 0);
        return (time + (Config.QUESTIONNAIRES_REFRESH_PERIOD * Time.MINUTE)) < Time.getTime();
    }
}
