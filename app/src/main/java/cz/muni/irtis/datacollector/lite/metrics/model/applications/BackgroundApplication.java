package cz.muni.irtis.datacollector.lite.metrics.model.applications;

import android.app.ActivityManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.Metric;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationBackgroundEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.application.ApplicationForegroundEntity;
import cz.muni.irtis.datacollector.lite.metrics.hooking.NotificationBundle;
import cz.muni.irtis.datacollector.lite.metrics.hooking.NotificationsService;
import cz.muni.irtis.datacollector.lite.metrics.model.condition.IsNotificationsObservation;

public class BackgroundApplication extends Metric {
    public static final String TAG = BackgroundApplication.class.getSimpleName();
    public static final int IDENTIFIER = 261;

    private NotificationServiceReceiver receiver;

    public BackgroundApplication(Context context, Integer delay) {
        super(context, delay);

        receiver = null;
        addPrerequisity(new IsNotificationsObservation());
    }

    @Override
    public int getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public String getName() {
        return "Service";
    }

    @Override
    public void run() {
        if (!isPrerequisitiesSatisfied()) {
            return;
        }

        if(receiver==null) {
            receiver = new NotificationServiceReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction(BackgroundApplication.TAG);
            getContext().registerReceiver(receiver, filter);
        }

        Intent broadcast = new Intent(NotificationsService.TAG);
        broadcast.putExtra("source", BackgroundApplication.TAG);
        broadcast.putExtra("action", "ongoing");
        getContext().sendBroadcast(broadcast);

        setRunning(true);
    }

    @Override
    public void stop() {
        setRunning(false);
        getContext().unregisterReceiver(receiver);
        receiver = null;
    }

    @Override
    public long save(Object... params) {
        String current = (String) params[0];

        // save new apps
        long appId = getAppId(current);
        if (appId <= 0) {
            appId = saveNewAppName(current);
        }

        try {
            Debug.getInstance().log(TAG, current);
            return getDatabase().getBackgroundApplicationRepository().insert(
                    new ApplicationBackgroundEntity(Time.getTime(), appId));
        } catch (Exception e) {
            Debug.getInstance().exception(e);
            return -1;
        }
    }

    private long getAppId(String name) {
        ApplicationEntity entity = getDatabase().getApplicationRepository().getIdByName(name);
        if(entity!=null) {
            return entity.getId();
        } else {
            return -1;
        }
    }

    private long saveNewAppName(String name) {
        return getDatabase().getApplicationRepository().insert(new ApplicationEntity(Time.getTime(), name));
    }


    class NotificationServiceReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String source = intent.getStringExtra("source");
            String action = intent.getStringExtra("action");
            String content = intent.getStringExtra("content");

            if(source.equals(NotificationsService.TAG) && content!=null) {
                if (action.equals("ongoing")) {
                    List<NotificationBundle> bundle = new GsonBuilder().create()
                            .fromJson(content, new TypeToken<List<NotificationBundle>>() {
                            }.getType());

                    if (bundle != null && !bundle.isEmpty()) {
                        Iterator<NotificationBundle> iBundle = bundle.iterator();
                        while (iBundle.hasNext()) {
                            NotificationBundle notification = iBundle.next();
                            save(notification.getIdentifier());
                        }
                    }
                }
            }
        }
    }
}
