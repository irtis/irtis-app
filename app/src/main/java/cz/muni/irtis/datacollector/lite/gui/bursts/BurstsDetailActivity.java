package cz.muni.irtis.datacollector.lite.gui.bursts;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dinuscxj.progressbar.CircleProgressBar;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.Burst;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.BurstsModel;


public class BurstsDetailActivity extends ActivityBase {
    public static final String PARAM_ITEM_ID = "identifier";

    private Burst burst;
    private BurstsModel model;


    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(null);
        setContentView(R.layout.activity_bursts_detail);


        getRefreshView().setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                get();
            }
        });

        getLoadingView().setBackgroundColor(
                ContextCompat.getColor(this, android.R.color.transparent));

        burst = Burst.build(Long.valueOf(getIntent().getStringExtra("data")));
        model = BurstsModel.build(this);

        get();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


    private void ini() {
        TextView vTitle = findViewById(R.id.title);
        vTitle.setText(burst.getTitle());

        TextView vTime = findViewById(R.id.time);
        vTime.setText(burst.getTimeFormatted());

        findViewById(R.id.offline).setVisibility(
                !Network.isConnection() ? View.VISIBLE : View.GONE);

        CircleProgressBar vProgress = findViewById(R.id.progress);
        vProgress.setProgress(burst.getProgress());

        TextView vRemaining = findViewById(R.id.remaining_value);
        if(burst.getTimeRemaining()>0) {
            vRemaining.setText(getString(R.string.bursts_detail_remaining).replace("%s", burst.getTimeRemaining().toString()));
        } else {
            vRemaining.setText(getString(R.string.bursts_detail_remaining_completed));
        }

        TextView vPace = findViewById(R.id.paceCurrent);
        vPace.setText(burst.getPaceCurrentFormatted());

        findViewById(R.id.pace).setVisibility(burst.getStarting() < Time.getTime() ? View.VISIBLE : View.GONE);
        TextView vPacePrevious = findViewById(R.id.pacePrevious);
        ImageView vPacePreviousIcon = findViewById(R.id.pacePreviousIcon);
        if (burst.getPacePrevious() != null) {
//            findViewById(R.id.pacePreviousContainer).setVisibility(View.VISIBLE);
            vPacePrevious.setText(burst.getPacePreviousFormatted());
            if (burst.getPacePrevious() <= burst.getPaceCurrent()) {
                vPacePrevious.setText((burst.getPaceCurrent()-burst.getPacePrevious())+"%");
                vPacePreviousIcon.setImageDrawable(getDrawable(R.drawable.ic_trending_up_black_24dp));
                vPacePreviousIcon.setColorFilter(ContextCompat.getColor(this, R.color.colorGreen));
            } else if (burst.getPacePrevious() > burst.getPaceCurrent()) {
                vPacePrevious.setText((burst.getPacePrevious()-burst.getPaceCurrent())+"%");
                vPacePreviousIcon.setImageDrawable(getDrawable(R.drawable.ic_trending_down_black_24dp));
                vPacePreviousIcon.setColorFilter(ContextCompat.getColor(this, R.color.colorRed));
            }
        } else {
//            findViewById(R.id.pacePreviousContainer).setVisibility(View.GONE);
        }

        if(burst.getPacePrevious()!=null) {
            TextView vPaceNotes = findViewById(R.id.pace_notes);
            if (burst.getPacePrevious() == burst.getPaceCurrent()) {
                vPaceNotes.setText(getString(R.string.bursts_detail_pace_motivation_excelent));
            } else if (burst.getPacePrevious() < burst.getPaceCurrent()) {
                vPaceNotes.setText(getString(R.string.bursts_detail_pace_motivation_good).replace("%s", String.valueOf(burst.getPaceCurrent() - burst.getPacePrevious())));
            } else if (burst.getPacePrevious() > burst.getPaceCurrent()) {
                vPaceNotes.setText(getString(R.string.bursts_detail_pace_motivation_bad).replace("%s", String.valueOf(burst.getPacePrevious() - burst.getPaceCurrent())));
            }
        }

        if(burst.getPurse()!=null) {
            TextView vCoinsBronze = findViewById(R.id.coins_bronze);
            vCoinsBronze.setText(burst.getPurse().getCountBronzeFormatted());
            TextView vCoinsSilver = findViewById(R.id.coins_silver);
            vCoinsSilver.setText(burst.getPurse().getCountSilverFormatted());
            TextView vCoinsGold = findViewById(R.id.coins_gold);
            vCoinsGold.setText(burst.getPurse().getCountGoldFormatted());
        }

        TextView vTickets = findViewById(R.id.tickets_value);
        vTickets.setText(burst.getPurse().getAmount().toString());

        findViewById(R.id.help).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(BurstsHelpActivity.class);
            }
        });
    }

    private void get() {
        if(Network.isConnection()) {
            model.getItemFromServer(burst, new Repository.RepositoryListener<Burst>() {
                @Override
                public void onStart() {
                    getLoadingView().show();
                }

                @Override
                public void onDone(Burst item) {
                    burst = item;
                    ini();
                    getLoadingView().hide();
                    getRefreshView().setRefreshing(false);
                }
            });
        } else {
            model.getItem(burst, new Repository.RepositoryListener<Burst>() {
                @Override
                public void onStart() {
                    getLoadingView().show();
                }

                @Override
                public void onDone(Burst item) {
                    burst = item;
                    ini();
                    getLoadingView().hide();
                    getRefreshView().setRefreshing(false);
                }
            });
        }
    }


}
