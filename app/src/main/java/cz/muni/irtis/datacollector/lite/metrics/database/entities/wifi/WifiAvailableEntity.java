package cz.muni.irtis.datacollector.lite.metrics.database.entities.wifi;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import com.google.gson.annotations.SerializedName;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.EntityBase;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.WifiAvailableRepository;

@Entity(tableName = WifiAvailableRepository.TABLE_NAME)
public class WifiAvailableEntity extends EntityBase {
    @ColumnInfo(name = "wifis_ssid_id")
    @ForeignKey(entity = WifiEntity.class, childColumns = "wifiSSIDId", parentColumns = "datetime")
    @SerializedName("wifis_ssid_id")
    private Long wifisSSIDId;

    public WifiAvailableEntity(Long datetime, Long wifisSSIDId) {
        super(datetime);
        this.wifisSSIDId = wifisSSIDId;
    }

    public Long getWifisSSIDId() {
        return wifisSSIDId;
    }
}
