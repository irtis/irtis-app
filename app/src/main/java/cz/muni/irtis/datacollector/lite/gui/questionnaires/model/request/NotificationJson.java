package cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request;

import com.google.gson.annotations.SerializedName;

import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.AnswerEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionnaireNotificationEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Notification;

public class NotificationJson {
    @SerializedName("questionnaire_id")
    private Long idQuestionnaire;
    @SerializedName("people_questionnaire_id")
    private Long idPeopleQuestionnaire;
    private Integer action;
    private Long time;


    public NotificationJson(QuestionnaireNotificationEntity entity) {
        this.idQuestionnaire = entity.getIdQuestionnaire();
        this.idPeopleQuestionnaire = entity.getIdPeopleQuestionnaire();
        this.action = entity.getAction();
        this.time = entity.getTime();
    }

    public NotificationJson(Notification item) {
        this.idQuestionnaire = item.getQuestionnaire().getIdQuestionnaire();
        this.idPeopleQuestionnaire = item.getQuestionnaire().getIdPeopleQuestionnaire();
        this.action = item.getAction();
        this.time = item.getTime();
    }


    public Long getIdQuestionnaire() {
        return idQuestionnaire;
    }

    public void setIdQuestionnaire(Long idQuestionnaire) {
        this.idQuestionnaire = idQuestionnaire;
    }

    public Long getIdPeopleQuestionnaire() {
        return idPeopleQuestionnaire;
    }

    public void setIdPeopleQuestionnaire(Long idPeopleQuestionnaire) {
        this.idPeopleQuestionnaire = idPeopleQuestionnaire;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
