package cz.muni.irtis.datacollector.lite.gui.questionnaires.components.notifications;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;

import androidx.core.app.NotificationCompat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.NotificationBase;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Repeat;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesFormActivity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesService;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.persistence.model.Events;

import static cz.muni.irtis.datacollector.lite.gui.questionnaires.components.notifications.QuestionnairesNotificationBroadcastReceiver.ACTION_SILENCE;

public class QuestionnairesNotification extends NotificationBase {
    public static final String TAG = QuestionnairesNotification.class.getSimpleName();

    private QuestionnairesModel model;
    private Questionnaire item;
    private Repeat repeat;
    private Long created;

    static private List<QuestionnairesNotification> notifications;

    private NotificationCompat.Builder builder;

    private QuestionnairesNotification(Context context, Questionnaire item) {
        super(context);

        if(notifications==null) {
            notifications = new ArrayList<>();
        }

        // All other notifications should be cancelled
        if(notifications!=null && !notifications.isEmpty()) {
            Iterator<QuestionnairesNotification> iNotifications = notifications.iterator();
            while (iNotifications.hasNext()) {
                QuestionnairesNotification notification = iNotifications.next();
                notification.cancel(false);
            }
            notifications.clear();
        }

        this.item = item;
        model = new QuestionnairesModel(context);
        notifications.add(this);
    }



    static public QuestionnairesNotification build(Context context, Questionnaire item) {
        return new QuestionnairesNotification(context, item);
    }

    static public boolean isShowable(Questionnaire item) {
        if(item==null) {
            Debug.getInstance().warning(QuestionnairesNotification.TAG,
                    "isNotEqualNull | Value: null");
            return false;
        } else if(!item.isOpenForStarting()) {
            Debug.getInstance().warning(QuestionnairesNotification.TAG,
                    "isOpenForStarting | Value: false | Instance: "+item.getId().toString());
            return false;
        } else if(item.isClosed()) {
            Debug.getInstance().warning(QuestionnairesNotification.TAG,
                    "isClosed | Value: true | Instance: "+item.getId().toString());
            return false;
        } else if(QuestionnairesNotification.isBuilt(item)) {
            Debug.getInstance().warning(QuestionnairesNotification.TAG,
                    "isBuilt | Value: true | Instance: "+item.getId().toString());
            return false;
        } else {
            return true;
        }
    }

    static public boolean isBuilt(Questionnaire item) {
        if(notifications!=null) {
            Iterator<QuestionnairesNotification> iNotifications = notifications.iterator();
            while (iNotifications.hasNext()) {
                QuestionnairesNotification notification = iNotifications.next();
                if (notification.getItem().getId().equals(item.getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    static public void cancel(Context context, Questionnaire item) {
        if(item!=null && item.getId()!=null) {
            if (notifications != null) {
                Iterator<QuestionnairesNotification> iNotifications = notifications.iterator();
                while (iNotifications.hasNext()) {
                    QuestionnairesNotification notification = iNotifications.next();
                    if (notification.getItem().getId().equals(item.getId())) {
                        notification.cancel();
                        break;
                    }
                }
            } else {
                NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                Integer notificationId = (int) (item.getId() / 1000);
                manager.cancel(notificationId);
            }
        }
    }

    static public void update(Questionnaire item) {
        if(notifications!=null) {
            Iterator<QuestionnairesNotification> iNotifications = notifications.iterator();
            while (iNotifications.hasNext()) {
                QuestionnairesNotification notification = iNotifications.next();
                if (notification.getItem().getId().equals(item.getId())) {
                    notification.setItem(item);
                    break;
                }
            }
        }
    }

    @Override
    public Notification create() {
        Intent formIntent = new Intent(context, QuestionnairesFormActivity.class);
        formIntent.putExtra(QuestionnairesFormActivity.PARAM_ITEM_ID, item.getId());
        PendingIntent formPendingIntent = PendingIntent.getActivity(
            context, getIdentifierId(), formIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent postponeIntent = new Intent(context, QuestionnairesNotificationBroadcastReceiver.class);
        postponeIntent.setAction(ACTION_SILENCE);
        postponeIntent.putExtra(QuestionnairesFormActivity.PARAM_ITEM_ID, item.getId());
//        PendingIntent postponePendingIntent =
//            PendingIntent.getBroadcast(context, 1, postponeIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent silencePendingIntent =
                PendingIntent.getBroadcast(context, 1, postponeIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri sound = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        Uri.parse("android.resource://"
//                + context.getPackageName() + "/" + R.raw.siren)

        builder = new NotificationCompat.Builder(context, CHANNEL_TOP)
            .setSmallIcon(R.drawable.ic_questionnaires_grey_24dp)
            .setSubText(context.getString(R.string.questionnaires_notification_type))
            .setContentTitle(context.getString(R.string.questionnaires_notification_title))
            .setContentText(item.getClosedRemainingFormatted())
            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
            .setSound(sound)
            .setVibrate(new long[]{300, 600, 300, 600, 300, 600})
            .setContentIntent(formPendingIntent)
            .setOngoing(true)
            .setOnlyAlertOnce(true)
            .addAction(R.drawable.ic_questionnaires_grey_24dp,
                context.getString(R.string.questionnaires_silence_silence).replaceAll("%s", item.getSilenceDelay().toString()), silencePendingIntent)
            .setProgress(100, 100-item.getClosedRemainingPercents(), false);

        Debug.getInstance().warning(QuestionnairesService.TAG,
                "Notification created | Value: "+(item.getIdentifier()!=null ? item.getIdentifier() : "null"));

        return notification = builder.build();
    }

    public void show() {
        created = Time.getTime();
        manager.notify(getIdentifierId(), create());
        if(!item.isNotified()) {
            model.setNotified(item, Time.getTime());
        }
        model.setNotifiedHistory(item, Time.getTime());
        Events.build(context).log(Events.EVENT_QUESTIONNAIRES_NOTIFIED);

        if(repeat!=null) {
            repeat.cancel();
        }
        repeat = Repeat.build(Time.SECOND, new Runnable() {
            @Override
            public void run() {
                if(created+Time.MINUTE*10 < Time.getTime()) {
                    cancel(false);
                    show();
                } else {
                    refresh();
                }
            }
        });
    }

    public void cancel(boolean remove) {
        if(notification!=null){
            manager.cancel(getIdentifierId());
            if (repeat != null) {
                repeat.cancel();
                repeat = null;
            };
            if(remove) {
                notifications.remove(this);
            }
        }
    }

    public void cancel() {
        cancel(true);
    }


    private Notification update() {
        builder
            .setContentText(item.getClosedRemainingFormatted())
            .setProgress(100, 100-item.getClosedRemainingPercents(), false);

        return notification = builder.build();
    }

    private void refresh() {
        if(!item.isClosed()) {
            manager.notify(getIdentifierId(), update());
        } else {
            cancel();
        }
    }

    public void setItem(Questionnaire item) {
        this.item = item;
    }

    public Questionnaire getItem() {
        return item;
    }

    public int getIdentifierId() {
        return (int) (item.getId()/1000);
    }
}