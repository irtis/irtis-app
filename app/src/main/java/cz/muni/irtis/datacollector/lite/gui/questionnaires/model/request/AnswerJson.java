package cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.AnswerEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Label;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;

public class AnswerJson {
    @SerializedName("questionnaire_id")
    private Long idQuestionnaire;
    @SerializedName("question_id")
    private Long idQuestion;
    @SerializedName("answer_id")
    private Long idAnswer;
    private String value;

    @SerializedName("multi_slider_label_id")
    private Long idLabel;

    public AnswerJson(AnswerEntity entity) {
        this.idQuestionnaire = entity.getIdQuestionnaire();
        this.idQuestion = entity.getIdQuestion();
        this.idAnswer = entity.getIdAnswer();
        this.value = entity.getValue();
    }

    public AnswerJson(Answer item) {
        this.idQuestionnaire = item.getQuestionnaire().getIdQuestionnaire();
        this.idQuestion = item.getQuestion().getId();
        this.idAnswer = item.getId();
        this.value = item.getValue();

        switch (item.getQuestion().getType()) {
            case Question.TYPE_MULTIPLE_SLIDER:
                Label label = new GsonBuilder().create()
                        .fromJson(item.getText(), new TypeToken<Label>() {}.getType());
                this.idLabel = label.getId();
                break;
        }

    }

    public void setIdQuestionnaire(Long idQuestionnaire) {
        this.idQuestionnaire = idQuestionnaire;
    }

    public void setIdQuestion(Long idQuestion) {
        this.idQuestion = idQuestion;
    }

    public void setIdAnswer(Long idAnswer) {
        this.idAnswer = idAnswer;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getIdQuestionnaire() {
        return idQuestionnaire;
    }

    public Long getIdQuestion() {
        return idQuestion;
    }

    public Long getIdAnswer() {
        return idAnswer;
    }

    public String getValue() {
        return value;
    }
}
