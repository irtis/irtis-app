package cz.muni.irtis.datacollector.lite.metrics.database.entities.screen;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.EntityBase;
import cz.muni.irtis.datacollector.lite.metrics.database.repository.ScreenRepository;

@Entity(tableName = ScreenRepository.TABLE_NAME)
public class ScreenEntity extends EntityBase {
    @ColumnInfo(name = "state")
    private Boolean state;

    public ScreenEntity(Long datetime, Boolean state) {
        super(datetime);
        this.state = state;
    }

    public Boolean getState() {
        return state;
    }
}
