package cz.muni.irtis.datacollector.lite.gui.questionnaires.types;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.bursts.game.BurstPurse;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.Burst;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.BurstsModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesFormActivity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesService;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.notifications.QuestionnairesNotification;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.persistence.model.Events;

public class FinishFragment extends TypeFragmentBase {
    private Questionnaire questionnaire;
    private QuestionnairesModel model;
    private BurstsModel bursts;
    private Burst burst;
    private BurstPurse pursePrevious;
    private BurstPurse purseCurrent;

    public FinishFragment(Context context, Questionnaire questionnaire) {
        super(context, null, R.layout.activity_questionnaires_form_finish, false);
        this.questionnaire = questionnaire;
        this.model = QuestionnairesModel.build(getContext());
        this.bursts = BurstsModel.build(getContext());

        Long starting = Time.getTime(Time.getYear(), Time.getMonth(), Time.getDay(), 0, 0, 0);
        Long ending = starting + Time.DAY;
        List<Burst> items = bursts.getItemsActive();
        if(items!=null && !items.isEmpty()) {
            Burst burst = items.get(0);
            pursePrevious = bursts.getBurstPurse(burst, starting, ending);
        } else {
            pursePrevious = bursts.getBurstPurse(null, starting, ending);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);
        if(view!=null) {
            view.findViewById(R.id.finish).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });
        }

        return view;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            get();
        }

    }

    private void get() {
        new StoreTask().execute();
    }

    /**
     * Store the questionnaire into DB
     */
    private void store() {
        if (questionnaire != null && !questionnaire.isCompleted()) {
            questionnaire.setCompleted();
            model.store(questionnaire);

            Events.build(getContext()).log(Events.EVENT_QUESTIONNAIRES_COMPLETED);
            Debug.getInstance().warning(QuestionnairesService.TAG,
                    "Instance finished | Value: " + (questionnaire.getIdentifier() != null ?
                            questionnaire.getIdentifier() : "null"));
        }
    }

    private void refreshPurse() {
        View vCoins = getView().findViewById(R.id.coins);
        View vCoinsEarned = getView().findViewById(R.id.coins_earned);

        vCoins.setVisibility(View.INVISIBLE);
        vCoinsEarned.setVisibility(View.INVISIBLE);

        boolean isRegular = questionnaire.isType(QuestionnairesModel.TYPE_REGULAR) && burst!=null;
        boolean isSpecial = questionnaire.isType(QuestionnairesModel.TYPE_SPECIAL);
        boolean isPurse = purseCurrent != null;
        if ((isRegular || isSpecial) && isPurse) {
            // Show the content
            vCoins.setVisibility(View.VISIBLE);

            // Coins
            if (purseCurrent.getCountBronze() - pursePrevious.getCountBronze() > 0) {
                getView().findViewById(R.id.coins_bronze).setVisibility(View.VISIBLE);
            } else {
                getView().findViewById(R.id.coins_bronze).setVisibility(View.GONE);
            }

            if (purseCurrent.getCountSilver() - pursePrevious.getCountSilver() > 0) {
                getView().findViewById(R.id.coins_silver).setVisibility(View.VISIBLE);
            } else {
                getView().findViewById(R.id.coins_silver).setVisibility(View.GONE);
            }

            if (purseCurrent.getCountGold() - pursePrevious.getCountGold() > 0) {
                getView().findViewById(R.id.coins_gold).setVisibility(View.VISIBLE);
            } else {
                getView().findViewById(R.id.coins_gold).setVisibility(View.GONE);
            }


            TextView vCoinsEarnedAmount = getView().findViewById(R.id.coins_earned_amount);
            if (purseCurrent.getCount() - pursePrevious.getCount() > 1) {
                int count = purseCurrent.getCount() - pursePrevious.getCount();
                int idStringCoinsEarnNotes = R.string.questionnaires_form_finish_coins_earn_notes_pl;
                if(count >= 2 && count <= 4) {
                    idStringCoinsEarnNotes = R.string.questionnaires_form_finish_coins_earn_notes_pl_two_to_four;
                }
                vCoinsEarnedAmount
                        .setText(getString(idStringCoinsEarnNotes).replace("%s",
                                Integer.valueOf(purseCurrent.getCount() - pursePrevious.getCount()).toString()));
                vCoinsEarned.setVisibility(View.VISIBLE);
            } else if(purseCurrent.getCount() - pursePrevious.getCount() == 1) {
                vCoinsEarnedAmount
                        .setText(getString(R.string.questionnaires_form_finish_coins_earn_notes));
                vCoinsEarned.setVisibility(View.VISIBLE);
            } else {
                vCoinsEarnedAmount.setVisibility(View.INVISIBLE);
                vCoinsEarned.setVisibility(View.INVISIBLE);
            }
        }
    }


    private class StoreTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... nul) {
            // Store the finished questionnaire
            store();

            // Current purse values - after storing new values
            Long starting = Time.getTime(Time.getYear(), Time.getMonth(), Time.getDay(), 0, 0, 0);
            Long ending = starting + Time.DAY;
            List<Burst> items = bursts.getItemsActive();
            if(items!=null && !items.isEmpty()) {
                burst = items.get(0);
                purseCurrent = bursts.getBurstPurse(burst, starting, ending);
            } else {
                purseCurrent = bursts.getBurstPurse(null, starting, ending);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            QuestionnairesNotification.cancel(getContext(), FinishFragment.this.questionnaire);
            refreshPurse();
        }
    }
}