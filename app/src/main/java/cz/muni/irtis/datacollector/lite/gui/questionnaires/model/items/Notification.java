package cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.json.Exclude;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionnaireNotificationEntity;

public class Notification {
    public static final Integer ACTION_NOTIFIED = 1;
    public static final Integer ACTION_SILENCED = 2;

    private Long id;
    private Questionnaire questionnaire;
    private Integer action;
    private Long time;

    public Notification(Questionnaire questionnaire, QuestionnaireNotificationEntity entity) {
        this.id = entity.getId();
        this.questionnaire = questionnaire;
        this.action = entity.getAction();
        this.time = entity.getTime();
    }

    public Long getId() {
        return id;
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public Integer getAction() {
        return action;
    }

    public Long getTime() {
        return time;
    }
}
