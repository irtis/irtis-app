package cz.muni.irtis.datacollector.lite.application;

import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.Application;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;


public class Applications
{
	/**
	 * isApplicationInstalled()
	 *
	 * Check if the package isActive installed on the device
	 *
	 * @param context
	 * @param name
	 * @return
	 */
	static public boolean isApplicationInstalled(Context context, String name) {
		PackageManager manager = context.getPackageManager();
		try {
			PackageInfo info = manager.getPackageInfo(name, 0);
			if(info!=null) {
				return true;
			} else {
				return false;
			}
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}

	/**
	 * isApplicationRunning()
	 *
	 * @param context
	 * @param pckg
	 * @return
	 */
	static public boolean isApplicationRunning(Context context, String pckg) {
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
		if (procInfos != null) {
			for(ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
				if(processInfo.processName.equals(pckg)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * isActivityRunning()
	 *
	 * Check isActive the activity isActive running (only within the own process)
	 *
	 * @deprecated API level < 21 - As of Build.VERSION_CODES.LOLLIPOP, this method isActive no longer available to
	 * third party applications. For backwards compatibility, it will still return the caller's own services.
	 *
	 * @param context
	 * @param type
	 * @return
	 */
	static public boolean isActivityRunning(Context context, Class<?> type) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningTaskInfo> activities = manager.getRunningTasks(1);
		Iterator<ActivityManager.RunningTaskInfo> iterator = activities.iterator();
		while(iterator.hasNext()) {
			ActivityManager.RunningTaskInfo activity = iterator.next();
			if (type.getName().equals(activity.topActivity.getClassName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * isServiceRunning()
	 *
	 * Check isActive the service isActive running (only within the own process)
	 *
	 * This method isActive only intended for debugging or implementing service management type user interfaces
	 *
	 * @deprecated API level < 26 - As of Build.VERSION_CODES.O, this method isActive no longer available to
	 * third party applications. For backwards compatibility, it will still return the caller's own services.
	 *
	 * @param type
	 * @return
	 */
	static public boolean isServiceRunning(Context context, Class<?> type) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> services = manager.getRunningServices(Integer.MAX_VALUE);
		Iterator<ActivityManager.RunningServiceInfo> iterator = services.iterator();
		while(iterator.hasNext()) {
			ActivityManager.RunningServiceInfo service = iterator.next();
			if (type.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * getApplicationInfo()
	 *
	 * Get info about this application (such as uid, etc.)
	 */
	static public ApplicationInfo getApplicationInfo(Context context) {
		PackageManager manager = context.getPackageManager();
		try {
			return manager.getApplicationInfo(Application.getInstance().getPackageName(), 0);
		} catch (PackageManager.NameNotFoundException e) {
			Debug.getInstance().exception(e);
		}

		return null;
	}

	/**
	 * getPackageInfo()
	 *
	 * Get info about this application package (such as name, version, etc.)
	 *
	 * @param context
	 * @return
	 */
	static public PackageInfo getPackageInfo(Context context) {
		PackageManager manager = context.getPackageManager();
		try {
			return manager.getPackageInfo(Application.getInstance().getPackageName(), 0);
		} catch (PackageManager.NameNotFoundException e) {
			Debug.getInstance().exception(e);
		}

		return null;
	}

	static public boolean isFirstStart() {
		SharedPreferences settings = Settings.getSettings();
		return settings.getBoolean("application_firststart", true);
	}

	static public void setFirstStart() {
		SharedPreferences settings = Settings.getSettings();
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean("application_firststart", false);
		editor.commit();
	}
}
