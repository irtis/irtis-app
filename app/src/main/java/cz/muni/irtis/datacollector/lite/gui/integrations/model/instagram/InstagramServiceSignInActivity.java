package cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram;

import androidx.collection.SimpleArrayMap;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.connection.AccessToken;

public class InstagramServiceSignInActivity extends ActivityBase {
    private String redirect_url;
    private String request_url;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_integrations_instagram_service_sign_in);

        redirect_url = getString(R.string.instagram_callback_url);
        request_url = getString(R.string.instagram_base_url) +
                "oauth/authorize/?client_id=" +
                getResources().getString(R.string.instagram_client_id) +
                "&redirect_uri=" + redirect_url +
                "&response_type=token";

        WebViewClient webViewClient = new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith(redirect_url)) {

                    return true;
                }
                return false;
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                parseResult(url);
            }
        };

        WebView webView = findViewById(R.id.content);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(request_url);
        webView.setWebViewClient(webViewClient);
    }

    private void parseResult(String url) {
        if(url.contains("instagram.com/accounts/login")) {

        } else if(url.contains("facebook.com/login")) {

        } else if(url.contains("instagram.com/accounts/signup")) {

        } else if (url.contains("access_token")) {
            Uri uri = Uri.EMPTY.parse(url);
            String access_token = uri.getEncodedFragment();
            access_token = access_token.substring(access_token.lastIndexOf("=") + 1);

            AccessToken.getInstance().save(access_token);
            setResultForStatingActivity(RESULT_OK, -1,null);
        } else if (url.contains("error")) {
            Uri uri = Uri.EMPTY.parse(url);
            String query = uri.getEncodedQuery();
            String[] errors = query.split("&");

            SimpleArrayMap<String, String> values = new SimpleArrayMap<String, String>();
            for (int i = 0; i < errors.length; i++) {
                String[] param = errors[i].split("=");
                values.put(param[0], param[1]);
            }

            // Errors
            String errorMessage = values.get("error_description");
            Integer errorCode = -1;
            if(values.get("error").equals("access_denied")) {
                errorCode = ServiceBase.ERROR_ACCESS_DENIED;
            }
            // Exception
            Exception exception = new AccessToken.AccessTokenErrorException(errorCode, errorMessage);
            Debug.getInstance().exception(exception, "AccessTokenErrorException: reason=" + values.get("error_reason") + ", error=" + values.get("error"));

            AccessToken.getInstance().delete();
            setResultForStatingActivity(RESULT_CANCELED, errorCode, errorMessage);
        }
    }


    private void setResultForStatingActivity(int result, int errorCode, String errorMessage) {
        Intent intent = new Intent();
        intent.putExtra("error_code", errorCode);
        intent.putExtra("error_message", errorMessage);
        setResult(result, intent);
        finish();
    }
}
