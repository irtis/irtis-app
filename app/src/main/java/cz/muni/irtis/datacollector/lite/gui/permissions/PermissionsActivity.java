package cz.muni.irtis.datacollector.lite.gui.permissions;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;


import androidx.core.app.ActivityCompat;

import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.components.ActivityBase;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.debug.DebugLogs;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionAccessibility;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionActivityRecognition;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionAdvancedBatteryOptimization;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionNotifications;
import cz.muni.irtis.datacollector.lite.application.widgets.Box;
import cz.muni.irtis.datacollector.lite.gui.MainActivity;
import cz.muni.irtis.datacollector.lite.application.permissions.Permission;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionAutostart;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionDangerous;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionBatteryOptimization;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionOverlay;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionUsage;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.ExtendedListView;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.ExtendedListViewAdapter;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.ExtendedListViewAdapter.OnHolderListener;
import cz.muni.irtis.datacollector.lite.application.widgets.lists.StaticListViewBuilder;
import cz.muni.irtis.datacollector.lite.metrics.model.Location;
import cz.muni.irtis.datacollector.lite.metrics.model.PhysicalActivity;
import cz.muni.irtis.datacollector.lite.metrics.model.phone.CallHistory;
import cz.muni.irtis.datacollector.lite.metrics.model.phone.SmsConversation;

import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_ACTIVITY_RECOGNITION;
import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_BOOT;
import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_CALLS;
import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_CONTACTS;
import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_LOCATION;
import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_SMS;

public class PermissionsActivity extends ActivityBase {
    public static final String DISPLAY          = "display";
    public static final int DISPLAY_REQUIRE     = 735;
    public static final int DISPLAY_OVERVIEW    = 672;


    static final public int REQUEST_PERMISSIONS_CODE = 61125;
    private PermissionsProvider permissions;

    private ExtendedListViewAdapter list;

    static private class ViewHolder extends ExtendedListViewAdapter.ViewHolder {
        public TextView vTitle;
        public Switch vButton;
        public TextView vNotes;

        public ViewHolder(View view) {
            super(view);
            this.vTitle = view.findViewById(R.id.title);
            this.vButton = view.findViewById(R.id.button);
            this.vNotes = view.findViewById(R.id.notes);
        }
    }


    /**
     * onCreate()
     *
     * On the start wee need to acquire the requested permissions
     *
     * @param bundle
     */
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_permissions);

        switch (this.getIntent().getIntExtra(DISPLAY, DISPLAY_REQUIRE)) {
            default:
            case DISPLAY_REQUIRE:
                break;
            case DISPLAY_OVERVIEW:
                findViewById(R.id.submit).setVisibility(View.GONE);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                break;
        }

        // Get all desired permissions before we start
        this.permissions = PermissionsProvider.build(this, true);

//        Box.ok(this, "Device",
//                Build.MANUFACTURER + " ("+Build.MODEL+")"+ " | API version: " + Build.VERSION.SDK_INT+ " | OS version: " + Build.VERSION.RELEASE);

        iniLayout();
    }

    /**
     * onResume()
     *
     * When user returns back to the activity, we need to ensure that
     * we still have the required permissions
     */
    @Override
    protected void onResume() {
        super.onResume();

        // Check all desired permissions before we resume
//        this.permissions.request(REQUIRED_PERMISSIONS, REQUEST_PERMISSIONS_CODE);

        list.refresh();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);

            case android.R.id.home:
               this.finish();
               return true;
        }
    }

    /**
     * iniLayout()
     *
     * Initialize the whole GUI here
     */
    private void iniLayout() {
        // Initialize ListView and its Adapter
        list = StaticListViewBuilder.build(this)
            .setView(R.id.list)
            .setAdapter(new ExtendedListViewAdapter<Permission>(R.layout.activity_permissions_list_row))
            .setOnItemClickListener(new ExtendedListView.OnItemClickListener<Permission>() {
                @Override
                public void onClick(ExtendedListViewAdapter adapter, Permission permission) {
                    if(permission instanceof PermissionDangerous) {
                        if(!permissions.isGranted(permission.getId())) {
                            permissions.request(permission.getId());
                        } else {
                            Intent intent = new Intent();
                            intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", PermissionsActivity.this.getPackageName(), null);
                            intent.setData(uri);
                            ActivityCompat.startActivity(PermissionsActivity.this, intent, null);
                        }
                    } else if(permission instanceof PermissionUsage) {
                        new UsageStatsDialog(PermissionsActivity.this, permission).show();
                    } else if(permission instanceof PermissionAutostart) {
                        new AutoStartDialog(PermissionsActivity.this, permission, new PermissionDialogBase.PermissionDialogResult() {
                            @Override
                            public void onDone(boolean isSuccess) {
                                if(!isSuccess) {
                                    Box.ok(PermissionsActivity.this,
                                        getString(R.string.permissions_autostart_dialog_manual_title),
                                        getString(R.string.permissions_autostart_dialog_manual_message));
                                }
                            }
                        }).show();
                    } else if(permission instanceof PermissionBatteryOptimization) {
                        new BatteryOptimizationDialog(PermissionsActivity.this, permission).show();
                    } else if(permission instanceof PermissionAdvancedBatteryOptimization) {
                        new AdvancedBatteryOptimizationDialog(PermissionsActivity.this, permission).show();
                    } else if(permission instanceof PermissionOverlay) {
                        new OverlayDialog(PermissionsActivity.this, permission).show();
                    } else if(permission instanceof PermissionNotifications) {
                        new NotificationsDialog(PermissionsActivity.this, permission).show();
                    } else if(permission instanceof PermissionAccessibility) {
                        new AccessibilityDialog(PermissionsActivity.this, permission).show();
                    }

                    list.refresh();
                }
            })
            .setOnHolderListener(new OnHolderListener<Permission, ViewHolder>() {
                @Override
                public ExtendedListViewAdapter.ViewHolder onCreate(ExtendedListViewAdapter adapter, View view) {
                    return new ViewHolder(view);
                }

                @Override
                public void onBind(ViewHolder holder, Permission permission) {
                    holder.vTitle.setText(permission.getTitle());
                    holder.vNotes.setText(permission.getDescription());
                    holder.vButton.setChecked(permission.isGranted());
                }
            })
            .create()
            .getAdapter();




        // Dangerous permissions
        if(Config.build().isMetric(CallHistory.class)) {
            list.addItem(permissions.add(new PermissionDangerous(this, PERMISSION_CONTACTS, getString(R.string.permissions_dangerous_contacts), getString(R.string.permissions_dangerous_contacts_description))));
            list.addItem(permissions.add(new PermissionDangerous(this, PERMISSION_CALLS, getString(R.string.permissions_dangerous_calls), getString(R.string.permissions_dangerous_calls_description))));
        }
        if(Config.build().isMetric(SmsConversation.class)) {
            list.addItem(permissions.add(new PermissionDangerous(this, PERMISSION_SMS, getString(R.string.permissions_dangerous_sms), getString(R.string.permissions_dangerous_sms_description))));
        }
        if(Config.build().isMetric(Location.class)) {
            list.addItem(permissions.add(new PermissionDangerous(this, PERMISSION_LOCATION, getString(R.string.permissions_dangerous_location), getString(R.string.permissions_dangerous_location_description))));
        }

        // Ordinary permissions
        if(!permissions.isGranted(PERMISSION_BOOT)) {    // On lower APIs can be turned off, therefore, we better select for it
            list.addItem(permissions.add(new PermissionDangerous(this, PERMISSION_BOOT, getString(R.string.permissions_boot), getString(R.string.permissions_boot_description))));
        }
        if(Config.build().isMetric(PhysicalActivity.class) && permissions.isRequiredActivityRecognitionPermission()) {
            list.addItem(permissions.add(new PermissionDangerous(this, PERMISSION_ACTIVITY_RECOGNITION, getString(R.string.permissions_activity_recognition), getString(R.string.permissions_activity_recognition_description))));
        }

        // Special permissions
        list.addItem(permissions.add(new PermissionUsage(this, getString(R.string.permissions_usage), getString(R.string.permissions_usage_description))));
        if(permissions.isRequiredAutostartPermission()) {
            list.addItem(permissions.add(new PermissionAutostart(this, getString(R.string.permissions_autostart), getString(R.string.permissions_autostart_description))));
        }
        if(permissions.isRequiredBatteryOptimizationPermission()) {
            list.addItem(permissions.add(new PermissionBatteryOptimization(this, getString(R.string.permissions_batteryoptimization), getString(R.string.permissions_batteryoptimization_description))));
        }
        if(permissions.isRequiredAdvancedBatteryOptimizationPermission()) {
            list.addItem(permissions.add(new PermissionAdvancedBatteryOptimization(this, getString(R.string.permissions_advanced_battery_optimization), getString(R.string.permissions_advanced_battery_optimization_description))));
        }
        if(permissions.isRequiredOverlayPermission()) {
            list.addItem(permissions.add(new PermissionOverlay(this, getString(R.string.permissions_overlay), getString(R.string.permissions_overlay_description))));
        }
        if(permissions.isRequiredNotificationsPermission()) {
            list.addItem(permissions.add(new PermissionNotifications(this, getString(R.string.permissions_notifications), getString(R.string.permissions_notifications_description))));
        }

        list.addItem(new PermissionAccessibility(this, getString(R.string.permissions_accessibility), getString(R.string.permissions_accessibility_description)));

        list.refresh();


        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Debug.getInstance().log(PermissionsActivity.class.getSimpleName(), "onSubmit()");
                boolean isGranted = permissions.isGranted();
                DebugLogs.build(PermissionsActivity.this).upload(); // Just WIFI

                if(isGranted) {
                    onPermissionsGranted();
                } else {
                    onPermissionsDenied();
                }
            }
        });
    }


    /**
     * onPermissionsGranted()
     *
     * This method isActive called when the permissions were granted
     */
    protected void onPermissionsGranted() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    /**
     * onPermissionsDenied()
     *
     * This method isActive called when the permissions were not granted
     */
    protected void onPermissionsDenied() {
        // Communicate to the user what to do...
        new AlertDialog.Builder(this)
            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
//                    finish();
                }
            })
            .setTitle(getString(R.string.permissions_denied))
            .setMessage(getString(R.string.permissions_denied_description))
            .create()
            .show();
    }


//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        if (requestCode == REQUEST_PERMISSIONS_CODE) {
//            this.permissions.result(permissions);
//        }
//    }
}
