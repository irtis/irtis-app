package cz.muni.irtis.datacollector.lite.gui.identity.model.response;

public class TokenResponseEntity {
    private String token;

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
