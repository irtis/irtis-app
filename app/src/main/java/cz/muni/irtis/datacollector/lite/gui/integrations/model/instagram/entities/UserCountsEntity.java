package cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities;

import com.google.gson.annotations.SerializedName;

public class UserCountsEntity {
    private Integer media;
    private Integer follows;
    @SerializedName("followed_by")
    private Integer followed;

    public Integer getMedia() {
        return media;
    }

    public Integer getFollows() {
        return follows;
    }

    public Integer getFollowed() {
        return followed;
    }
}
