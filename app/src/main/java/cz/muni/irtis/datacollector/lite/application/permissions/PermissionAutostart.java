package cz.muni.irtis.datacollector.lite.application.permissions;

import android.content.Context;

import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_AUTOSTART;


public class PermissionAutostart extends Permission {

    public PermissionAutostart(Context context, String title, String notes)
    {
        super(context, PERMISSION_AUTOSTART, title, notes);
    }

    public boolean isGranted()
    {
        return provider.isGrantedAutostartPermission();
    }

    public void setGranted()
    {
        provider.setGrantedAutostartPermission();
    }
}
