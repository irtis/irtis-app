package cz.muni.irtis.datacollector.lite.persistence.components;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.service.notification.StatusBarNotification;

import androidx.core.app.NotificationCompat;

import cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider;
import cz.muni.irtis.datacollector.lite.gui.LauncherActivity;
import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.components.NotificationBase;
import cz.muni.irtis.datacollector.lite.metrics.MetricsService;

public class PersistentNotification extends NotificationBase {
    private static final int NOTIFICATION_IDENTIFIER = 1337;

    private Service context;


    private PersistentNotification(Service context) {
        super(context);
        this.context = context;
    }

    static public PersistentNotification build(Service context) {
        return new PersistentNotification(context);
    }

    public boolean isShown() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            StatusBarNotification[] items = manager.getActiveNotifications();
            if(items!=null && items.length>0) {
                for(int i=0; i<items.length; i++) {
                    if(items[i].getId() == NOTIFICATION_IDENTIFIER) {
                        return true;
                    }
                }
            }
            return false;
        } else {
            return true;
        }
    }

    public Notification create() {
        Intent intent = new Intent(context, LauncherActivity.class);
        PendingIntent pending = PendingIntent.getActivity(
                context, NOTIFICATION_IDENTIFIER, intent, 0);

        String title = context.getString(R.string.persistence_notification_service_running_metrics);
        boolean isRunning = MetricsService.isRunning(context);
        title = title.replace("%s1", MetricsService.isRunning(context) ?
            context.getString(R.string.persistence_notification_capture_enabled) :
            context.getString(R.string.persistence_notification_capture_disabled));
        title = title.replace("%s2", MetricsService.isMetricRunningScreenshots(context) ?
                context.getString(R.string.persistence_notification_capture_enabled) :
                context.getString(R.string.persistence_notification_capture_disabled));

        String description = context.getString(PermissionsProvider.build(context).isGranted() ?
                R.string.main_permissions_value_granted : R.string.main_permissions_value_denied);
//        if(Debug.isDebug()) {
//               // Permissions
//            description = context.getString(R.string.persistence_notification_capture_permissions).replace("%s", description)+"\n\n";
//              // Metrics
//            List<Metric> metricsRunning = MetricsManagerService.getMetricsRunning();
//            if(metricsRunning!=null) {
//                Iterator<Metric> iMetricsRunning = metricsRunning.iterator();
//                StringBuilder metricsRunningBuilder = new StringBuilder();
//                while (iMetricsRunning.hasNext()) {
//                    Metric running = iMetricsRunning.next();
//                    metricsRunningBuilder.append((metricsRunningBuilder.length() > 0 ? ", " : "") + running.getName());
//                }
//                description += context.getString(R.string.persistence_notification_capture_metrics).replace("%s", metricsRunningBuilder.toString());
//            } else {
//                description += context.getString(R.string.persistence_notification_capture_metrics).replace("%s", context.getString(R.string.main_none));
//            }
//        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_PERSISTENT)
            .setSmallIcon(R.drawable.ic_launcher_notification)
            .setSubText(context.getString(R.string.persistence_notification_service_running))
            .setContentTitle(title)
            .setStyle(new NotificationCompat.BigTextStyle().bigText(description))
            .setContentIntent(pending)
            .setOngoing(true);

        return notification = builder.build();
    }

    public PersistentNotification start() {
        context.startForeground(getIdentifierId(), create());
        return this;
    }

    public PersistentNotification restart() {
        manager.cancel(getIdentifierId());
        return start();
    }

    static public int getIdentifierId() {
        return NOTIFICATION_IDENTIFIER;
    }
}
