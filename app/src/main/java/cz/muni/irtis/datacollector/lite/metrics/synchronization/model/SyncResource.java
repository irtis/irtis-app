package cz.muni.irtis.datacollector.lite.metrics.synchronization.model;

import android.content.Context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

import cz.muni.irtis.datacollector.lite.application.debug.Debug;

public class SyncResource {
    private BufferedWriter writer;
    private File json;
    private ArrayList<File> files;

    public SyncResource(Context context, String file) {
        files = new ArrayList<File>();

        File directory = new File(context.getCacheDir().getAbsolutePath());
        if(!directory.exists()) {
            directory.mkdir();
        }

        json = new File(directory.getAbsolutePath(), file);
    }

    public SyncResource(Context context) {
        files = new ArrayList<File>();
    }

    public void put(String string) {
        try {
            if(writer==null) {
                writer = new BufferedWriter(new FileWriter(this.json.getAbsolutePath()));
                writer.append("[");
                writer.append(string);
            } else {
                writer.append(",");
                writer.append(string);
            }
        } catch (IOException e) {
            Debug.getInstance().exception(e,this);
        }
    }

    public void put(File file) {
        if(file!=null) {
            files.add(file);
        }
    }

    public void close() {
        try {
            if(writer!=null) {
                writer.append("]");
                writer.close();

                put(json);
            }

        } catch (IOException e) {}
    }

    public File getJsonFile() {
        return json;
    }

    public String getJsonString() {
        try {
            FileInputStream input = new FileInputStream(json);

            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            StringBuilder builder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                builder.append(line).append("\n");
            }
            reader.close();
            input.close();
            return builder.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public ArrayList<File> getFiles() {
        return this.files;
    }

    public void delete() {
        Iterator<File> i = this.files.iterator();
        while(i.hasNext()) {
            i.next().delete();
        }
    }
}
