package cz.muni.irtis.datacollector.lite.application.widgets.lists;

import android.app.Activity;
import android.view.View;

import java.util.ArrayList;


/**
 * RecyclerListViewBuilder
 *
 * Builder for RecyclerListView
 */
public class RecyclerListViewBuilder {
    private Activity activity;
    private int resourceId;
    private RecyclerListView list;
    private boolean hasFixedSize;
    private RecyclerListViewAdapter adapter;

    private RecyclerListViewAdapter.OnLayoutListener onItemLayoutListener;
    private RecyclerListViewAdapter.OnHolderListener onHolderListener;
    private RecyclerListViewAdapter.OnItemClickListener onItemClickListener;
    private RecyclerListView.OnScrollListener onScrollListener;


    public RecyclerListViewBuilder(Activity activity) {
        this.activity = activity;
        this.hasFixedSize = true;
    }

    static public RecyclerListViewBuilder build(Activity activity) {
        return new RecyclerListViewBuilder(activity);
    }

    /**
     * setView()
     *
     * @param id resource id of the view
     * @return
     */
    public RecyclerListViewBuilder setView(int id) {
        this.resourceId = id;
        return this;
    }

    public RecyclerListViewBuilder setView(View view) {
        this.list = (RecyclerListView) view;
        return this;
    }


    public RecyclerListViewBuilder setAdapter(RecyclerListViewAdapter adapter) {
        this.adapter = adapter;
        return this;
    }

    public RecyclerListViewBuilder setHasFixedSize(boolean hasFixedSize) {
        this.hasFixedSize = hasFixedSize;
        return this;
    }

    /**
     * setOnLayoutListener()
     * <br><br>
     * Setting the layout of item row of the recycler list view
     * <br><br>
     * Initialization of the callback should be done like this:<br>
     *      setOnLayoutListener(new RecyclerListViewAdapter.OnLayoutListener() {
     *          @Override
     *          public View onCreate(ViewGroup parent) {
     *              return LayoutInflater.from(parent.getContext())
     *                  .inflate(R.layout.list_row, parent, false);
     *          }
     *      })
     *
     * @param listener
     * @return
     */
    public RecyclerListViewBuilder setOnLayoutListener(RecyclerListViewAdapter.OnLayoutListener listener) {
        this.onItemLayoutListener = listener;
        return this;
    }

    /**
     * setOnHolderListener()
     * <br><br>
     * Setting the ViewHolder of the recycler list view
     * <br><br>
     * You need to declare ViewHolder class somewhere in your code like this:<br>
     *      public class ViewHolder extends RecyclerListViewAdapter.ItemsListViewHolderBase {
     *          TextView vTitle;
     *          public ViewHolder(RecyclerListViewAdapter adapter, View view) {
     *              super(adapter, view);
     *              vTitle = view.findViewById(R.id.title);
     *          }
     *      }
     * <br><br>
     * Initialization of the callback should be done like this:<br>
     *      setOnHolderListener(new RecyclerListViewAdapter.OnHolderListener<String, ViewHolder>() {
     *          @Override
     *          public ViewHolder onCreate(RecyclerListViewAdapter adapter, View view) {
     *              return new ViewHolder(adapter, view);
     *          }
     *
     *          @Override
     *          public void onBind(ViewHolder holder, String item) {
     *              holder.vTitle.setText(item);
     *          }
     *      })
     *
     * @param listener RecyclerListViewAdapter.OnHolderListener
     * @return RecyclerListViewBuilder
     */
    public RecyclerListViewBuilder setOnHolderListener(RecyclerListViewAdapter.OnHolderListener listener) {
        this.onHolderListener = listener;
        return this;
    }

    /**
     * setOnItemClickListener()
     * <br><br>
     * Setting the item onClick listener of the recycler list view
     * <br><br>
     * Initialization of the callback should be done like this:<br>
     *      setOnItemClickListener(new RecyclerListViewAdapter.OnItemClickListener<String>() {
     *          @Override
     *          public void onClick(View view, String item) {
     *              Debug.getInstance().box(Activity.this, "Content of the string", item);
     *          }
     *      })
     *
     * @param listener
     * @return
     */
    public RecyclerListViewBuilder setOnItemClickListener(RecyclerListViewAdapter.OnItemClickListener listener) {
        this.onItemClickListener = listener;
        return this;
    }

    /**
     * setOnScrollListener()
     * <br><br>
     * Setting the onScroll listener of the recycler list view
     * <br><br>
     * Initialization of the callback should be done like this:<br>
     *      setOnScrollListener(new RecyclerListView.OnScrollListener() {
     *          @Override
     *          public void onScroll(Integer count) {
     *              List<String> items = ...;
     *              vList.getAdapter().addItems((ArrayList<String>) items, true);
     *          }
     *      })
     * @param listener
     * @return
     */
    public RecyclerListViewBuilder setOnScrollListener(RecyclerListView.OnScrollListener listener) {
        this.onScrollListener = listener;
        return this;
    }


    /**
     * append()
     *
     * Build instance of RecyclerListView
     *
     * @return
     */
    public RecyclerListView create(ArrayList<Object> items) {
        if(list==null) {
            list = activity.findViewById(this.resourceId);
        }
        list.setHasFixedSize(this.hasFixedSize);
        if(this.adapter!=null) {
            list.setAdapter(this.adapter);
            adapter.setListView(list);
        } else {
            throw new NullPointerException("Adapter isActive null. You need to setAdapter() with an instance.");
        }
        if(this.onItemLayoutListener!=null) {
            this.adapter.setOnLayoutListener(this.onItemLayoutListener);
        }
        if(this.onHolderListener!=null) {
            this.adapter.setOnHolderListener(this.onHolderListener);
        } else {
            throw new NullPointerException("Holder listener isActive null. You need to setOnHolderListener() with the creation and binding of holder view.");
        }
        if(this.onItemClickListener!=null) {
            this.adapter.setOnItemClickListener(this.onItemClickListener);
        }
        if(this.onScrollListener!=null) {
            list.setOnScrollListener(this.onScrollListener);
        }
        if(items!=null) {
            this.adapter.setItems(items);
        }
        return list;
    }

    public RecyclerListView create() {
        return this.create(null);
    }
}
