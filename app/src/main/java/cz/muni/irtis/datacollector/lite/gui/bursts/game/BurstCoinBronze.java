package cz.muni.irtis.datacollector.lite.gui.bursts.game;

public class BurstCoinBronze implements BurstCoin {

    static public BurstCoinBronze build() {
        return new BurstCoinBronze();
    }

    @Override
    public Integer getValue() {
        return 1;
    }
}
