package cz.muni.irtis.datacollector.lite.application.permissions;

import android.content.Context;

import static cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider.PERMISSION_OVERLAY;

/**
 * PermissionOverlay
 *
 * See:
 *  https://developer.android.com/reference/android/Manifest.permission#SYSTEM_ALERT_WINDOW
 *  https://commonsware.com/blog/2016/03/24/system-alert-window-now-more-hidden-than-ever.html
 */
public class PermissionOverlay extends Permission {

    public PermissionOverlay(Context context, String title, String notes)
    {
        super(context, PERMISSION_OVERLAY, title, notes);
    }

    public boolean isGranted()
    {
        return provider.isGrantedOverlayPermission();
    }

}
