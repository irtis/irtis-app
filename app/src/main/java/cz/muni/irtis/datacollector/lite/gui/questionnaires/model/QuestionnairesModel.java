package cz.muni.irtis.datacollector.lite.gui.questionnaires.model;

import android.content.Context;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.Application;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.ConnectionResult;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.QuestionnairesService;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.QuestionnairesDatabase;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.AnswerEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionnaireEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities.QuestionnaireNotificationEntity;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.AnswersRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.QuestionnairesNotificationsRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.QuestionnairesRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.QuestionsRepository;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Answer;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Notification;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.QuestionnaireJson;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.request.ResultResponse;

public class QuestionnairesModel extends Repository {
    public static final int TYPE_REGULAR = 1;    // Regular that is constantly repeated every same time throughout the burst
    public static final int TYPE_INVOKED = 2;    // Created by the user on the client side
    public static final int TYPE_TRIGGERED = 3;  // Triggered by the robot on the account of some incident
    public static final int TYPE_SPECIAL = 4;    // Out of ordinary, once in a time, questionnairy that can be place outside burst, does not earn any coins

    public static final int VALIDFRAME_HOUR_OFFSET = 0;


    private QuestionnairesDatabase database;
    private QuestionnairesRepository repository;
    private QuestionsRepository repositoryQuestions;
    private AnswersRepository repositoryAnswers;
    private QuestionnairesNotificationsRepository repositoryNotifications;


    static public QuestionnairesModel build(Context context) {
        return new QuestionnairesModel(context);
    }

    public QuestionnairesModel(Context context) {
        super(context);
        database = QuestionnairesDatabase.getDatabase(context);
        this.repository = database.getQuestionnairesRepository();
        this.repositoryQuestions = database.getQuestionsRepository();
        this.repositoryAnswers = database.getAnswersRepository();
        this.repositoryNotifications = database.getQuestionnairesNotificationsRepository();
    }


    public void store(final Questionnaire item, final RepositoryListener<Boolean> listener) {
        run(new TaskListener<Boolean>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Boolean onTask() {
                store(item);
                return true;
            }

            @Override
            public void onDone(Boolean result) {
                listener.onDone(result);
            }
        });
    }

    public void store(Questionnaire item) {
        // Save source (those built from templates must be excluded)
        if (!item.isFromTemplate(getContext())) {
            File file = storeToFile(item);
            item.setSource(file);
        }

        if (item.getId() == null) {
            if (Application.getInstance() != null && Application.getInstance().getBaseContext() != null) {
                QuestionnairesRepository repository = QuestionnairesDatabase.getDatabase(Application.getInstance().getBaseContext())
                        .getQuestionnairesRepository();
                Long id = null;
                if (item.getIdPeopleQuestionnaire() != null) {
                    QuestionnaireEntity entity = repository
                            .getItemByPersonQuestionnaireId(item.getIdPeopleQuestionnaire());
                    if (entity != null) {
                        id = entity.getId();
                    }
                }

                if (id == null) {
                    id = repository.getIdMax();
                    if (id == null) {
                        id = 0L;
                    }
                    id++;
                }

                item.setId(id);
            } else {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                item.setId(Time.getTime());
            }
        }

        // Set completion time
        if (!item.isCompleted()) {
            boolean isCompleted = !item.getQuestions().isEmpty();
            Iterator<Question> iQuestions = item.getQuestions().iterator();
            while (iQuestions.hasNext()) {
                Question question = iQuestions.next();
                if (!question.isCompleted()) {
                    isCompleted = false;
                    break;
                }
            }

            if (isCompleted) {
                Long time = Time.getTime();
                item.setCompleted(time);
                Debug.getInstance().warning(QuestionnairesService.TAG,
                        "setCompleted | Value: " + time + " | Instance: " + item.getId().toString());
            }
        }

        repository.store(QuestionnaireEntity.build(item));

        // Clear stored answers - new will be added
        repositoryAnswers.delete(item.getId());

        Iterator<Question> iQuestions = item.getQuestions().iterator();
        while (iQuestions.hasNext()) {
            Question question = iQuestions.next();

            // Store question
            QuestionEntity questionEntity = new QuestionEntity(
                    item.getId(), question.getId(), question.getElapsed(), question.getSkipped());
            try {
                repositoryQuestions.store(questionEntity);
            } catch (Exception e) {
                Debug.getInstance().exception(e, questionEntity,
                        new GsonBuilder().create().toJson(questionEntity, new TypeToken<QuestionEntity>() {
                        }.getType()));
            }

            // Store answers
            if (question.getAnswers() != null && !question.getAnswers().isEmpty()) {
                Iterator<Answer> iAnswers = question.getAnswers().iterator();
                while (iAnswers.hasNext()) {
                    Answer answer = iAnswers.next();
                    if (answer.isValue()) {
                        AnswerEntity answerEntity = new AnswerEntity(
                                item.getId(), question.getId(), answer.getId(), answer.getValue());
                        try {
                            Boolean isExisting = repositoryAnswers.getItem(item.getId(), question.getId(), answer.getId()) != null;
                            if (isExisting) {
                                repositoryAnswers.update(answerEntity);
                            } else {
                                repositoryAnswers.insert(answerEntity);
                            }
                        } catch (Exception e) {
                            Debug.getInstance().exception(e, answerEntity,
                                    new GsonBuilder().create().toJson(answerEntity, new TypeToken<AnswerEntity>() {
                                    }.getType()));
                        }
                    }
                }
            }
        }
    }

    public File storeToFile(Questionnaire item) {
        return item.toFile();
    }


    public void getItems(final RepositoryListener<List<Questionnaire>> listener) {
        run(new TaskListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public List<Questionnaire> onTask() {
                return getItems();
            }

            @Override
            public void onDone(List<Questionnaire> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Questionnaire> getItems() {
        List<QuestionnaireEntity> entities = repository.getItems();
        if (entities != null) {
            List<Questionnaire> items = new ArrayList<>();
            Iterator<QuestionnaireEntity> i = entities.iterator();
            while (i.hasNext()) {
                QuestionnaireEntity entity = i.next();
                Questionnaire item = fromEntity(entity);
                if (item != null) {
                    items.add(item);
                }
            }
            return items;
        } else {
            return null;
        }
    }


    public void getItems(final int offset, final int count, final RepositoryListener<List<Questionnaire>> listener) {
        run(new TaskListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public List<Questionnaire> onTask() {
                return getItems(offset, count);
            }

            @Override
            public void onDone(List<Questionnaire> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Questionnaire> getItems(int offset, int count) {
        List<QuestionnaireEntity> entities = repository.getItems(offset, count);
        if (entities != null) {
            List<Questionnaire> items = new ArrayList<>();
            Iterator<QuestionnaireEntity> i = entities.iterator();
            while (i.hasNext()) {
                QuestionnaireEntity entity = i.next();
                Questionnaire item = fromEntity(entity);
                if (item != null) {
                    items.add(item);
                }
            }
            return items;
        } else {
            return null;
        }
    }

    public void getItemsToInvoke(final RepositoryListener<List<Questionnaire>> listener) {
        run(new TaskListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public List<Questionnaire> onTask() {
                return getItemsToInvoke();
            }

            @Override
            public void onDone(List<Questionnaire> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Questionnaire> getItemsToInvoke() {
        Debug.getInstance().log(QuestionnairesService.TAG,
                "SELECT * " +
                        "FROM " + QuestionnairesRepository.TABLE_NAME + " " +
                        "WHERE completed IS NULL AND ((starting <= " + Time.getTime() + " AND ending >= " + Time.getTime() + ") OR invoking <= " + Time.getTime() + ") " +
                        "ORDER BY starting DESC");
        List<QuestionnaireEntity> entities = repository.getItemsToInvoke(Time.getTime());
        if (entities != null) {
            List<Questionnaire> items = new ArrayList<>();
            Iterator<QuestionnaireEntity> i = entities.iterator();
            while (i.hasNext()) {
                QuestionnaireEntity entity = i.next();
                Questionnaire item = fromEntity(entity);
                if (item != null) {
                    items.add(item);
                }
            }
            return items;
        } else {
            return null;
        }
    }

    public List<Questionnaire> getItemsAvailable() {
        List<QuestionnaireEntity> entities = repository.getItemsAvailable(Time.getTime());
        if (entities != null) {
            List<Questionnaire> items = new ArrayList<>();
            Iterator<QuestionnaireEntity> i = entities.iterator();
            while (i.hasNext()) {
                QuestionnaireEntity entity = i.next();
                Questionnaire item = fromEntity(entity);
                if (item != null) {
                    items.add(item);
                }
            }
            return items;
        } else {
            return null;
        }
    }

    public List<Questionnaire> getItemsAvailable(int offset, int count) {
        List<QuestionnaireEntity> entities = repository.getItemsAvailable(Time.getTime(), offset, count);
        if (entities != null) {
            List<Questionnaire> items = new ArrayList<>();
            Iterator<QuestionnaireEntity> i = entities.iterator();
            while (i.hasNext()) {
                QuestionnaireEntity entity = i.next();
                Questionnaire item = fromEntity(entity);
                if (item != null) {
                    items.add(item);
                }
            }
            return items;
        } else {
            return null;
        }
    }

    public void getItemsAvailableCount(final RepositoryListener<Integer> listener) {
        run(new TaskListener<Integer>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Integer onTask() {
                return repository.getItemsAvailableCount(Time.getTime());
            }

            @Override
            public void onDone(Integer count) {
                listener.onDone(count);
            }
        });
    }

    public void getItemsFuture(final RepositoryListener<List<Questionnaire>> listener) {
        run(new TaskListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public List<Questionnaire> onTask() {
                return getItemsFuture();
            }

            @Override
            public void onDone(List<Questionnaire> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Questionnaire> getItemsFuture() {
        List<QuestionnaireEntity> entities = repository.getItemsFuture(Time.getTime());
        if (entities != null) {
            List<Questionnaire> items = new ArrayList<>();
            Iterator<QuestionnaireEntity> i = entities.iterator();
            while (i.hasNext()) {
                QuestionnaireEntity entity = i.next();
                Questionnaire item = fromEntity(entity);
                if (item != null) {
                    items.add(item);
                }
            }
            return items;
        } else {
            return null;
        }
    }

    public void getItemsFuture(final int offset, final int count, final RepositoryListener<List<Questionnaire>> listener) {
        run(new TaskListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public List<Questionnaire> onTask() {
                return getItemsFuture(offset, count);
            }

            @Override
            public void onDone(List<Questionnaire> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Questionnaire> getItemsFuture(int offset, int count) {
        List<QuestionnaireEntity> entities = repository.getItemsFuture(Time.getTime(), offset, count);
        if (entities != null) {
            List<Questionnaire> items = new ArrayList<>();
            Iterator<QuestionnaireEntity> i = entities.iterator();
            while (i.hasNext()) {
                QuestionnaireEntity entity = i.next();
                Questionnaire item = fromEntity(entity);
                if (item != null) {
                    items.add(item);
                }
            }
            return items;
        } else {
            return null;
        }
    }


    public void getItemAvailableNewest(final RepositoryListener<Questionnaire> listener) {
        run(new TaskListener<Questionnaire>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Questionnaire onTask() {
                return getItemAvailableNewest();
            }

            @Override
            public void onDone(Questionnaire item) {
                listener.onDone(item);
            }
        });
    }

    public Questionnaire getItemAvailableNewest() {
        QuestionnaireEntity entity = repository.getItemAvailableNewest(Time.getTime());
        if (entity != null) {
            Questionnaire item = fromEntity(entity);
            if (item != null && !item.isClosed()) {
                return item;
            }
        }

        return null;
    }

    public List<Questionnaire> getItemsNotUploaded() {
        List<QuestionnaireEntity> entities = repository.getNotUploaded(Time.getTime());
        if (entities != null) {
            List<Questionnaire> items = new ArrayList<>();
            Iterator<QuestionnaireEntity> i = entities.iterator();
            while (i.hasNext()) {
                QuestionnaireEntity entity = i.next();
                Questionnaire item = fromEntity(entity);
                if (item != null) {
                    items.add(item);
                }
            }
            return items;
        } else {
            return null;
        }
    }


    public void getItemsPassed(final RepositoryListener<List<Questionnaire>> listener) {
        run(new TaskListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public List<Questionnaire> onTask() {
                return getItemsPassed();
            }

            @Override
            public void onDone(List<Questionnaire> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Questionnaire> getItemsPassed() {
        List<QuestionnaireEntity> entities = repository.getItemsPassed(Time.getTime());
        if (entities != null) {
            List<Questionnaire> items = new ArrayList<>();
            Iterator<QuestionnaireEntity> i = entities.iterator();
            while (i.hasNext()) {
                QuestionnaireEntity entity = i.next();
                Questionnaire item = fromEntity(entity);
                if (item != null) {
                    items.add(item);
                }
            }
            return items;
        } else {
            return null;
        }
    }


    public void getItemsFutureByDay(final Integer day, final RepositoryListener<List<Questionnaire>> listener) {
        run(new TaskListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public List<Questionnaire> onTask() {
                return getItemsFutureByDay(day);
            }

            @Override
            public void onDone(List<Questionnaire> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Questionnaire> getItemsFutureByDay(Integer day) {
        List<QuestionnaireEntity> entities = repository.getItemsFutureByWeekDay(Time.getTime(), day);
        if (entities != null) {
            List<Questionnaire> items = new ArrayList<>();
            Iterator<QuestionnaireEntity> i = entities.iterator();
            while (i.hasNext()) {
                QuestionnaireEntity entity = i.next();
                Questionnaire item = fromEntity(entity);
                if (item != null) {
                    items.add(item);
                }
            }
            return items;
        } else {
            return null;
        }
    }


    public Questionnaire getItemFutureFirstByDay(Integer day) {
        QuestionnaireEntity entity = repository.getItemFutureFirstByWeekDay(Time.getTime(), day);
        if (entity != null) {
            Questionnaire item = fromEntity(entity);
            return item;
        } else {
            return null;
        }
    }

    public Questionnaire getItemFutureLastByDay(Integer day) {
        QuestionnaireEntity entity = repository.getItemFutureLastByWeekDay(Time.getTime(), day);
        if (entity != null) {
            Questionnaire item = fromEntity(entity);
            return item;
        } else {
            return null;
        }
    }


    public void getItemsFutureWeeklyByMorning(final RepositoryListener<List<Questionnaire>> listener) {
        run(new TaskListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public List<Questionnaire> onTask() {
                return getItemsFutureWeeklyByMorning();
            }

            @Override
            public void onDone(List<Questionnaire> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Questionnaire> getItemsFutureWeeklyByMorning() {
        // day of week 0-6 with Sunday==0
        List<Integer> days = Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 0});
        List<Questionnaire> items = new ArrayList<>();

        Iterator<Integer> iDays = days.iterator();
        while (iDays.hasNext()) {
            Integer day = iDays.next();
            Questionnaire item = getItemFutureFirstByDay(day);   // Get the earliest questionnaire for the day

            if (item != null) {
                // We need to filter only the earliest in the frame of 00:00-12:00
                Boolean isEarliest = true;
                if (Time.getHour(item.getEnding()) < 12) {
                    if (!items.isEmpty()) {
                        Iterator<Questionnaire> iItems = items.iterator();
                        while (iItems.hasNext()) {
                            Questionnaire itemCheck = iItems.next();
                            if (itemCheck.getStarting() != null) {
                                if (Time.getHour(item.getStarting()) < Time.getHour(itemCheck.getStarting())) {
                                    isEarliest = false;
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    isEarliest = false;
                }
                if (isEarliest) {
                    items.add(item);
                } else {
                    items.add(new Questionnaire());
                }
            } else {
                items.add(new Questionnaire());
            }
        }

        return items;
    }


    public void getItemsFutureWeeklyByNight(final RepositoryListener<List<Questionnaire>> listener) {
        run(new TaskListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public List<Questionnaire> onTask() {
                return getItemsFutureWeeklyByNight();
            }

            @Override
            public void onDone(List<Questionnaire> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Questionnaire> getItemsFutureWeeklyByNight() {
        List<Integer> days = Arrays.asList(new Integer[]{1, 2, 3, 4, 5, 6, 0});
        List<Questionnaire> items = new ArrayList<>();

        Iterator<Integer> iDays = days.iterator();
        while (iDays.hasNext()) {
            Integer day = iDays.next();
            Questionnaire item = getItemFutureLastByDay(day);   // Get the latest questionnaire for the day

            if (item != null) {
                // We need to filter only the latest in the frame of 12:00-23:59
                Boolean isLatest = true;
                if (Time.getHour(item.getEnding()) > 12) {
                    if (!items.isEmpty()) {
                        Iterator<Questionnaire> iItems = items.iterator();
                        while (iItems.hasNext()) {
                            Questionnaire itemCheck = iItems.next();
                            if (itemCheck.getStarting() != null) {
                                if (Time.getHour(item.getEnding()) > Time.getHour(itemCheck.getEnding())) {
                                    isLatest = false;
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    isLatest = false;
                }
                if (isLatest) {
                    items.add(item);
                }
            } else {
                items.add(new Questionnaire());
            }
        }

        return items;
    }


    public void getItemsTemplates(final RepositoryListener<List<Questionnaire>> listener) {
        run(new TaskListener<List<Questionnaire>>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public List<Questionnaire> onTask() {
                return getItemsTemplates();
            }

            @Override
            public void onDone(List<Questionnaire> items) {
                listener.onDone(items);
            }
        });
    }

    public List<Questionnaire> getItemsTemplates() {
        List<Questionnaire> items = null;
        File directory = new File(Application.getInstance().getApplicationContext()
                .getCacheDir(), "questionnaires");
        if (!directory.exists()) {
            directory.mkdir();
        }

        List<File> files = Arrays.asList(directory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File f, String name) {
                return name.endsWith("-template.json");
            }
        }));

        if (files != null && !files.isEmpty()) {
            items = new ArrayList<>();
            Iterator<File> iFiles = files.iterator();
            while (iFiles.hasNext()) {
                File file = iFiles.next();
                items.add(Questionnaire.buildFromFile(file));
            }
        }

        return items;
    }


    public void getItem(final Long id, final RepositoryListener<Questionnaire> listener) {
        run(new TaskListener<Questionnaire>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Questionnaire onTask() {
                return getItem(id);
            }

            @Override
            public void onDone(Questionnaire item) {
                listener.onDone(item);
            }
        });
    }

    public Questionnaire getItem(Long id) {
        QuestionnaireEntity entity = repository.getItem(id);
        if (entity != null) {
            return fromEntity(entity);
        } else {
            return null;
        }
    }


    public void getItemLastByClosing(final RepositoryListener<Questionnaire> listener) {
        run(new TaskListener<Questionnaire>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Questionnaire onTask() {
                return getItemLastByClosing();
            }

            @Override
            public void onDone(Questionnaire item) {
                listener.onDone(item);
            }
        });
    }

    public Questionnaire getItemLastByClosing() {
        QuestionnaireEntity entity = repository.getItemLastByClosing();
        if (entity != null) {
            return fromEntity(entity);
        } else {
            return null;
        }
    }


    public void getItemByPersonQuestionnaireId(final Long id, final RepositoryListener<Questionnaire> listener) {
        run(new TaskListener<Questionnaire>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Questionnaire onTask() {
                return getItemByPersonQuestionnaireId(id);
            }

            @Override
            public void onDone(Questionnaire item) {
                listener.onDone(item);
            }
        });
    }

    public Questionnaire getItemByPersonQuestionnaireId(Long id) {
        QuestionnaireEntity entity = repository.getItemByPersonQuestionnaireId(id);
        if (entity != null) {
            return fromEntity(entity);
        } else {
            return null;
        }
    }


    public void setNotified(Questionnaire item, long time) {
        item.setNotified(time);
        repository.store(QuestionnaireEntity.build(item));
    }

    public void setNotifiedHistory(Questionnaire item, long time) {
        repositoryNotifications.insert(new QuestionnaireNotificationEntity(
                item, Notification.ACTION_NOTIFIED, Time.getTime()));
    }

    public void setSilenced(Questionnaire item, long time) {
        item.setSilenced(time);
        repository.store(QuestionnaireEntity.build(item));
    }

    public void setSilencedHistory(Questionnaire item, long time) {
        repositoryNotifications.insert(new QuestionnaireNotificationEntity(
                item, Notification.ACTION_SILENCED, Time.getTime()));
    }

    public void setOpened(Questionnaire item, long time) {
        item.setOpened(time);
        repository.store(QuestionnaireEntity.build(item));
    }

    public void setPostponed(Questionnaire item, int validity) {
        item.setUpdated(Time.getTime());
        item.setClosing(Time.getTime() + (validity * Time.MINUTE));
        repository.store(QuestionnaireEntity.build(item));
    }

    public void setUploaded(Questionnaire item, long time) {
        item.setUploaded(time);
        repository.store(QuestionnaireEntity.build(item));

        List<QuestionnaireNotificationEntity> notificationsEntities = repositoryNotifications.getNotUploaded(item.getId());
        if (notificationsEntities != null && !notificationsEntities.isEmpty()) {
            Iterator<QuestionnaireNotificationEntity> iNotificationsEntities = notificationsEntities.iterator();
            while (iNotificationsEntities.hasNext()) {
                QuestionnaireNotificationEntity entity = iNotificationsEntities.next();
                entity.setUploaded(Time.getTime());
                repositoryNotifications.update(entity);
            }
        }

    }

    /**
     * setInvoking()
     * <p>
     * Invoking time will be set to random values from the <starting, ending> interval
     * NotificationsDay setting is also taken into consideration when determining the time
     *
     * @param item
     */
    public void setInvoking(final Questionnaire item, final RepositoryListener<Void> listener) {
        run(new TaskListener<Void>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Void onTask() {
                setInvoking(item);
                return null;
            }

            @Override
            public void onDone(Void item) {
                listener.onDone(item);
            }
        });
    }

    /**
     * setInvoking()
     * <p>
     * Invoking time will be set to random values from the <starting, ending> interval
     * NotificationsDay setting is also taken into consideration when determining the time
     *
     * @param item
     */
    public void setInvoking(Questionnaire item) {
        Long tStarting = item.getStarting();
        Long tEnding = item.getEnding();

        NotificationsDay notifications = NotificationsDays.build().getItem(Time.getDayOfWeek(tEnding));
        Long tNotificationsStarting = Time.getTime(Time.getYear(tStarting), Time.getMonth(tStarting), Time.getDay(tStarting),
                notifications.getStartingHours(), notifications.getStartingMinutes(), 0);
        Long tNotificationsEnding = Time.getTime(Time.getYear(tEnding), Time.getMonth(tEnding), Time.getDay(tEnding),
                notifications.getEndingHours(), notifications.getEndingMinutes(), 0);

        if (tNotificationsStarting > tStarting) {
            tStarting = tNotificationsStarting;
        }
        if (tNotificationsEnding < tEnding) {
            tEnding = tNotificationsEnding;
        }

        item.setInvoking(tStarting, tEnding);
        repository.store(QuestionnaireEntity.build(item));
    }

    /***
     * setInvoking()
     *
     * Set to a precise invoking time
     *
     * @param item
     * @param time
     */
    public void setInvoking(final Questionnaire item, final Long time, final RepositoryListener<Void> listener) {
        run(new TaskListener<Void>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Void onTask() {
                setInvoking(item, time);
                return null;
            }

            @Override
            public void onDone(Void item) {
                listener.onDone(item);
            }
        });
    }

    /**
     * setInvoking()
     * <p>
     * Set to a precise invoking time
     *
     * @param item
     * @param time
     */
    public void setInvoking(Questionnaire item, Long time) {
        item.setInvoking(time);
        repository.store(QuestionnaireEntity.build(item));
    }

    /**
     * setInvocations()
     * <p>
     * Set invocation times for a particular set of questionnaires based on item's group identifier
     *
     * @param item
     * @param hours
     * @param minutes
     * @param listener
     */
    public void setInvocations(final Questionnaire item, final Integer hours, final Integer minutes, final RepositoryListener<Void> listener) {
        run(new TaskListener<Void>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Void onTask() {
                setInvocations(item, hours, minutes);
                return null;
            }

            @Override
            public void onDone(Void item) {
                listener.onDone(item);
            }
        });
    }

    /**
     * setInvocations()
     * <p>
     * Set invocation times for a particular set of questionnaires based on item's group identifier
     *
     * @param item
     * @param hours
     * @param minutes
     */
    public void setInvocations(Questionnaire item, Integer hours, Integer minutes) {
        Settings.getSettings()
                .edit()
                .putString("questionnaires_invocation_" + item.getIdentifier(), hours + ":" + minutes)
                .commit();

        List<QuestionnaireEntity> entities = repository.getItemsFutureByIdentifier(item.getIdentifier(), Time.getTime());
        if (entities != null && !entities.isEmpty()) {
            Iterator<QuestionnaireEntity> iEntities = entities.iterator();
            while (iEntities.hasNext()) {
                Questionnaire itemUpdate = fromEntity(iEntities.next());
                Integer year = Time.getYear(itemUpdate.getStarting());
                Integer month = Time.getMonth(itemUpdate.getStarting());
                Integer day = Time.getDay(itemUpdate.getStarting());

                itemUpdate.setInvoking(Time.getTime(year, month, day, hours, minutes, 0));

                store(itemUpdate);
            }
        }
    }

    /**
     * setInvocations()
     * <p>
     * Set invocation times for a particular set of questionnaires based on item's group identifier
     *
     * @param item
     * @param time
     */
    public void setInvocations(Questionnaire item, String time) {
        String[] parts = time.split(":");
        setInvocations(item, Integer.valueOf(parts[0]), Integer.valueOf(parts[1]));
    }

    /**
     * setInvocation()
     * <p>
     * Set invocation time based on user's settings for a particular group of which an item is a member
     *
     * @param item
     */
    public void setInvocation(Questionnaire item) {
        String invocation = getInvocations(item);
        if (invocation != null) {
            String[] parts = invocation.split(":");

            Integer year = Time.getYear(item.getStarting());
            Integer month = Time.getMonth(item.getStarting());
            Integer day = Time.getDay(item.getStarting());

            Integer hours = Integer.valueOf(parts[0]);
            Integer minutes = Integer.valueOf(parts[1]);

            item.setInvoking(Time.getTime(year, month, day, hours, minutes, 0));

            store(item);
        }
    }

    /**
     * getInvocations()
     * <p>
     * Get user's invocation times setting  for a particular set of questionnaires based on item's group identifier
     *
     * @param item
     * @return
     */
    public String getInvocations(Questionnaire item) {
        String invocation = Settings.getSettings().getString("questionnaires_invocation_" + item.getIdentifier(), null);
        return invocation;
    }

    /**
     * setInvocationsByDay()
     * <p>
     * Set invocation times for a particular day
     * <p>
     * This includes user's settings as well as DB
     *
     * @param item
     * @param hours
     * @param minutes
     * @param listener
     */
    public void setInvocationsByDay(final Questionnaire item, final Integer hours, final Integer minutes, final RepositoryListener<Void> listener) {
        run(new TaskListener<Void>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Void onTask() {
                setInvocationsByDay(item, hours, minutes);
                return null;
            }

            @Override
            public void onDone(Void item) {
                listener.onDone(item);
            }
        });
    }

    /**
     * setInvocationsByDay()
     * <p>
     * Set invocation times for a particular day
     * <p>
     * This includes user's settings as well as DB
     *
     * @param item
     * @param hours
     * @param minutes
     */
    public void setInvocationsByDay(Questionnaire item, Integer hours, Integer minutes) {
        Integer dayOfWeek = Time.getDayOfWeek(item.getStarting()) - 1;
        Settings.getSettings()
                .edit()
                .putString("questionnaires_invocation_" + dayOfWeek.toString() + "_" + item.getIdentifier(), String.format("%02d", hours) + ":" + String.format("%02d", minutes))
                .commit();

        List<QuestionnaireEntity> entities = repository.getItemsFutureByIdentifier(item.getIdentifier(), Time.getTime());
        if (entities != null && !entities.isEmpty()) {
            Iterator<QuestionnaireEntity> iEntities = entities.iterator();
            while (iEntities.hasNext()) {
                Questionnaire itemUpdate = fromEntity(iEntities.next());
                if (Time.getDayOfWeek(itemUpdate.getStarting()) == Time.getDayOfWeek(item.getStarting()) && !itemUpdate.isNotified()) {
                    Integer year = Time.getYear(itemUpdate.getStarting());
                    Integer month = Time.getMonth(itemUpdate.getStarting());
                    Integer day = Time.getDay(itemUpdate.getStarting());

                    itemUpdate.setInvoking(Time.getTime(year, month, day, hours, minutes, 0));

                    store(itemUpdate);
                }
            }
        }
    }

    /**
     * setInvocationByDay()
     * <p>
     * Set invocation time for a particular day based on user's settings for a particular group of which an item is a member
     *
     * @param item
     */
    public void setInvocationByDay(Questionnaire item) {
        String invocation = getInvocationsByDay(item);
        if (invocation != null) {
            String[] parts = invocation.split(":");

            Integer year = Time.getYear(item.getStarting());
            Integer month = Time.getMonth(item.getStarting());
            Integer day = Time.getDay(item.getStarting());

            Integer hours = Integer.valueOf(parts[0]);
            Integer minutes = Integer.valueOf(parts[1]);

            item.setInvoking(Time.getTime(year, month, day, hours, minutes, 0));

            store(item);
        }
    }

    /**
     * getInvocationsByDay()
     * <p>
     * Get invocations settings for a particular day
     *
     * @param item
     * @return
     */
    public String getInvocationsByDay(Questionnaire item) {
        Integer day = Time.getDayOfWeek(item.getStarting()) - 1;
        return Settings.getSettings()
                .getString("questionnaires_invocation_" + day.toString() + "_" + item.getIdentifier(), null);
    }

    /**
     * resetInvocations()
     * <p>
     * Clear user's invocations settings for a particular group of questionnaires based on item's identifier
     *
     * @param item
     * @param listener
     */
    public void resetInvocations(final Questionnaire item, final RepositoryListener<Void> listener) {
        run(new TaskListener<Void>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Void onTask() {
                resetInvocations(item);
                return null;
            }

            @Override
            public void onDone(Void item) {
                listener.onDone(item);
            }
        });
    }

    /**
     * resetInvocations()
     * <p>
     * Clear user's invocations settings for a particular group of questionnaires based on item's identifier
     *
     * @param item
     */
    public void resetInvocations(Questionnaire item) {
        Settings.getSettings()
                .edit()
                .remove("questionnaires_invocation_" + item.getIdentifier())
                .commit();

        List<QuestionnaireEntity> entities = repository.getItemsFutureByIdentifier(item.getIdentifier(), Time.getTime());
        if (entities != null && !entities.isEmpty()) {
            Iterator<QuestionnaireEntity> iEntities = entities.iterator();
            while (iEntities.hasNext()) {
                Questionnaire itemUpdate = fromEntity(iEntities.next());
                itemUpdate.setInvoking(itemUpdate.getStarting(), itemUpdate.getEnding());
                store(itemUpdate);
            }
        }
    }

    /**
     * resetInvocationsByDay()
     * <p>
     * Clear user's invocations settings for a particular day of particular group of questionnaires
     * based on item's identifier
     *
     * @param item
     * @param listener
     */
    public void resetInvocationsByDay(final Questionnaire item, final RepositoryListener<Void> listener) {
        run(new TaskListener<Void>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Void onTask() {
                resetInvocationsByDay(item);
                return null;
            }

            @Override
            public void onDone(Void item) {
                listener.onDone(item);
            }
        });
    }

    /**
     * resetInvocationsByDay()
     * <p>
     * Clear user's invocations settings for a particular day of particular group of questionnaires
     * based on item's identifier
     *
     * @param item
     */
    public void resetInvocationsByDay(Questionnaire item) {
        Integer dayOfWeek = Time.getDayOfWeek(item.getStarting()) - 1;
        Settings.getSettings()
                .edit()
                .remove("questionnaires_invocation_" + dayOfWeek.toString() + "_" + item.getIdentifier())
                .commit();

        List<QuestionnaireEntity> entities = repository.getItemsFutureByIdentifier(item.getIdentifier(), Time.getTime());
        Iterator<QuestionnaireEntity> iEntities = entities.iterator();
        while (iEntities.hasNext()) {
            Questionnaire itemUpdate = fromEntity(iEntities.next());
            if (Time.getDayOfWeek(itemUpdate.getStarting()) == Time.getDayOfWeek(item.getStarting())) {
                itemUpdate.setInvoking(itemUpdate.getStarting(), itemUpdate.getEnding());
                store(itemUpdate);
            }
        }
    }


    private Questionnaire fromEntity(QuestionnaireEntity entity) {
        Questionnaire item = null;
        if (entity.getSource()!=null && !entity.getSource().isEmpty()) {
            try {
                FileInputStream fin = new FileInputStream(entity.getSource());
                BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                reader.close();
                item = Questionnaire.buildFromString(sb.toString());
            } catch (FileNotFoundException e) {
                Debug.getInstance().exception(e, this,
                        "Missing JSON cache file for ID: " + entity.getIdentifier() + " (" + entity.getId() + ")");
                try {
                    Debug.getInstance().log(this.getClass().getSimpleName(),"Download missing JSON file: started");
                    item = download(entity.getIdPeopleQuestionnaire());
                    Debug.getInstance().log(this.getClass().getSimpleName(),"Download missing JSON file: finished");
                } catch (Exception exception) { // There might not be connection present or something else happened
                    Debug.getInstance().exception(exception, this, "Redownloading missing JSON cache file failed (" + entity.getIdentifier() + " (" + entity.getId() + "))");
                    Debug.getInstance().log(this.getClass().getSimpleName(),"Download missing JSON file: failed");
                }
            } catch (IOException e) {
                Debug.getInstance().exception(e, this, entity.getSource());
            }
        } else if (entity.getIdentifier() != null) {
            item = Questionnaire.buildFromAssets(getContext(), Questionnaire.getTemplatePathByIdentifier(entity.getIdentifier()));
        } else if (entity.getType() != null && entity.getType().equals(TYPE_INVOKED)) {
        }

        if (item != null) {
            item.setId(entity.getId());
            item.setType(entity.getType());
            item.setIdQuestionnaire(entity.getIdQuestionnaire());
            item.setIdPeopleQuestionnaire(entity.getIdPeopleQuestionnaire());
            item.setCreated(entity.getCreated());
            item.setUpdated(entity.getUpdated());
            item.setStarting(entity.getStarting());
            item.setEnding(entity.getEnding());
            item.setInvoking(entity.getInvoking());
            item.setClosing(entity.getClosing());
            item.setNotified(entity.getNotified());
            item.setSilenced(entity.getSilenced());
            item.setOpened(entity.getOpened());
            item.setCompleted(entity.getCompleted());
            item.setUploaded(entity.getUploaded());
            item.setSource(entity.getSource() != null ? new File(entity.getSource()) : null);

            Iterator<Question> iQuestions = item.getQuestions().iterator();
            while (iQuestions.hasNext()) {
                Question question = iQuestions.next();
                QuestionEntity questionEntity = repositoryQuestions.getItem(item.getId(), question.getId());
                if (questionEntity != null) {
                    question.setElapsed(questionEntity.getElapsed());
                    question.setSkipped(questionEntity.getSkipped());
                }

                Iterator<Answer> iAnswers = question.getAnswers().iterator();
                while (iAnswers.hasNext()) {
                    Answer answer = iAnswers.next();
                    AnswerEntity answerEntity = repositoryAnswers.getItem(item.getId(), question.getId(), answer.getId());
                    if (answerEntity != null) {
                        answer.setValue(answerEntity.getValue());
                        answer.setSelected();
                    }
                }
            }

            List<QuestionnaireNotificationEntity> notificationsEntities = repositoryNotifications.getNotUploaded(item.getId());
            if (notificationsEntities != null && !notificationsEntities.isEmpty()) {
                List<Notification> notifications = new ArrayList<>();
                Iterator<QuestionnaireNotificationEntity> iNotificationsEntities = notificationsEntities.iterator();
                while (iNotificationsEntities.hasNext()) {
                    notifications.add(new Notification(item, iNotificationsEntities.next()));
                }
                item.setNotifications(notifications);
            }
        }

        return item;
    }


    public void deleteByFuture(final RepositoryListener<Boolean> listener) {
        run(new TaskListener<Boolean>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Boolean onTask() {
                deleteByFuture();
                return true;
            }

            @Override
            public void onDone(Boolean item) {
                listener.onDone(item);
            }
        });
    }

    public void deleteByFuture() {
        repository.deleteFuture(Time.getTime());
    }


    public void download(final RepositoryListener<Boolean> listener) {
        run(new TaskListener<Boolean>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Boolean onTask() {
                return download() != null;
            }

            @Override
            public void onDone(Boolean result) {
                listener.onDone(result);
            }
        });
    }

    /**
     * download()
     * <p>
     * The method will all available questionnaires from the server
     *
     * @endpoint /questionnaires/person/download
     */
    public List<Questionnaire> download() {
        String content = Connection.build(getContext()).getJson(
                UrlComposer.compose("/questionnaires/person/download"));

        if (content != null && !content.isEmpty()) {
            List<Questionnaire> items = Questionnaire.buildListFromString(content);
            if (items != null && !items.isEmpty()) {
                deleteByFuture(); // Remove all that were not yet initiated

                Iterator<Questionnaire> iItems = items.iterator();
                while (iItems.hasNext()) {
                    Questionnaire item = iItems.next();
//                    Questionnaire stored = getItemByPersonQuestionnaireId(item.getIdPeopleQuestionnaire());
//                    if(stored!=null) {
//                        item.setClosing(stored.getClosing());
//                        item.setNotified(stored.getNotified());
//                        item.setSilenced(stored.getSilenced());
//                        item.setOpened(stored.getOpened());
//                        item.setCompleted(stored.getCompleted());
//                        item.setUploaded(stored.getUploaded());
//                    }

                    store(item);
                }
                return items;
            }
        }
        return null;
    }

    /**
     * download()
     *
     * Download one particular questionnaire
     *
     * @param idPeopleQuestionnaire
     */
    public void download(final Long idPeopleQuestionnaire, final RepositoryListener<Questionnaire> listener) {
        run(new TaskListener<Questionnaire>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Questionnaire onTask() {
                return download(idPeopleQuestionnaire);
            }

            @Override
            public void onDone(Questionnaire result) {
                listener.onDone(result);
            }
        });
    }

    public Questionnaire download(Long idPeopleQuestionnaire) {
        String content = Connection.build(getContext()).getJson(
                UrlComposer.compose("/questionnaires/person/get/"+idPeopleQuestionnaire.toString()));

        if (content != null && !content.isEmpty()) {
            Questionnaire item = Questionnaire.buildFromString(content);
            store(item);

            return item;
        } else {
            return null;
        }
    }


    public void upload(final Questionnaire item, final RepositoryListener<Boolean> listener) {
        run(new TaskListener<Boolean>() {
            @Override
            public void onStart() {
                listener.onStart();
            }

            @Override
            public Boolean onTask() {
                return upload(item);
            }

            @Override
            public void onDone(Boolean result) {
                listener.onDone(result);
            }
        });
    }

    /**
     * upload()
     * <p>
     * The method will try to upload a questionnaire to the server
     * <p>
     * If used for uploading completed or closed questionnaires, please,
     * use item.isClosed() check
     *
     * @param item
     * @endpoint /questionnaires/person/upload
     */
    public boolean upload(Questionnaire item) {
        QuestionnaireJson json = item.toRequest();
        String content = json.toString();

        ConnectionResult result = Connection.build(getContext()).postJson(
                UrlComposer.compose("/questionnaires/person/upload"),
                content);

        if (result != null && result.isResult() && result.isContent()) {
            ResultResponse response = result.getContent(new TypeToken<ResultResponse>() {
            });
            if (response != null && response.isSuccessful()) {
                setUploaded(item, Time.getTime());
                return true;
            }
        }

        return false;
    }
}
