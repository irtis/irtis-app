package cz.muni.irtis.datacollector.lite.application.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;

import androidx.annotation.RequiresApi;

import cz.muni.irtis.datacollector.lite.Application;

/**
 * Network
 *
 * Gets information about network
 *
 */
public class Network
{
	/**
	 * isConnection()
	 *
	 * Check if there isActive internet connection established
	 *
	 * @return boolean
	 */
	public static boolean isConnection()
	{
		NetworkInfo network = getNetworkInfo();
		return network != null && network.isConnected();
	}



	/**
	 * isWIFI()
	 *
	 * Is wifi enabled?
	 *
	 * @return
	 */
	public static boolean isWIFI() {
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
			return isWIFIAbove23();
		} else {
			return isWIFIPre23();
		}
	}

	/**
	 * isWIFI()
	 *
	 * Is wifi enabled?
	 *
	 * @deprecated Deprecated for version above API 23
	 * @return
	 */
	private static boolean isWIFIPre23() {
		NetworkInfo network = getNetworkInfo();
		if(network != null && network.isConnected()) {
			return network.getType() == ConnectivityManager.TYPE_WIFI;
		} else {
			return false;
		}
	}

	/**
	 * isWIFI()
	 *
	 * Is wifi enabled?
	 *
	 * For new APIs
	 *
	 * @return
	 */
	@RequiresApi(api = Build.VERSION_CODES.M)
	private static boolean isWIFIAbove23() {
		android.net.Network network = getConnectivityManager().getActiveNetwork();
		NetworkCapabilities capabilities = getConnectivityManager().getNetworkCapabilities(network);

		return capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
	}


	/**
	 * isMobileData()
	 *
	 * Is mobile data enabled?
	 *
	 * @return
	 */
	public static boolean isMobileData() {
		NetworkInfo network = getNetworkInfo();
		if(network != null && network.isConnected()) {
			return network.getType() == ConnectivityManager.TYPE_MOBILE;
		} else {
			return false;
		}
	}

	/**
	 * isConnectionSlow()
	 *
	 * Is the connected connection slow?
	 *
	 * @todo implement logic
	 * @return
	 */
	public static boolean isConnectionSlow() {
		return false;
	}



	private static NetworkInfo getNetworkInfo() {
		ConnectivityManager cm = getConnectivityManager();
		try {
			NetworkInfo ni = cm.getActiveNetworkInfo();
			return ni;
		} catch(Exception e) {
			return null;
		}
	}

	private static ConnectivityManager getConnectivityManager() {
		return (ConnectivityManager) Application.getInstance().getSystemService(Application.CONNECTIVITY_SERVICE);
	}
}
