package cz.muni.irtis.datacollector.lite.application.components;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.Applications;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionsProvider;
import cz.muni.irtis.datacollector.lite.application.widgets.LoadingView;

/**
 * ActivityBase
 *
 * This base class ensures that all activities within application has the required permissions.
 * If not, it will take necessary steps to fix this insufficiency.
 */
public abstract class ActivityBase extends AppCompatActivity {
    protected PermissionsProvider permissions;

    @Override
    protected void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);

        this.permissions = new PermissionsProvider(this, false);
    }

    public PermissionsProvider getPermissions() {
        return permissions;
    }

    public boolean isGrantedPermissions() {
        if (this.permissions.isGranted()) {
            return true;
        } else {
            return false;
        }
    }

    protected LayoutInflater getInflater() {
        return (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    static public boolean isActivityRunning(Context context, Class<?> c) {
        return Applications.isActivityRunning(context, c);
    }

    public void startActivity(Class<?> c) {
        Intent intent = new Intent(this, c);
        super.startActivity(intent);
    }

    /**
     * startActivity()
     *
     * For IDs
     *
     * @param c
     * @param id
     */
    public void startActivity(Class<?> c, Integer id) {
        Intent i = new Intent(this, c);
        i.putExtra("data", id);
        super.startActivity(i);
    }

    /**
     * startActivity()
     *
     * For IDs
     *
     * @param c
     * @param id
     */
    public void startActivity(Class<?> c, Long id) {
        Intent i = new Intent(this, c);
        i.putExtra("data", id);
        super.startActivity(i);
    }

    /**
     * startActivity()
     *
     * For serialized objects
     *
     * @param c
     * @param data
     */
    public void startActivity(Class<?> c, String data) {
        Intent i = new Intent(this, c);
        i.putExtra("data", data);
        super.startActivity(i);
    }

    public LoadingView getLoadingView(int resId) {
        return ((LoadingView) findViewById(resId));
    }
    public LoadingView getLoadingView() {
        return getLoadingView(R.id.loading);
    }

    public SwipeRefreshLayout getRefreshView(int resId) {
        return ((SwipeRefreshLayout) findViewById(resId));
    }
    public SwipeRefreshLayout getRefreshView() {
        return getRefreshView(R.id.refresh);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}

