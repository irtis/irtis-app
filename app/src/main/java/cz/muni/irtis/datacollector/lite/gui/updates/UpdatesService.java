package cz.muni.irtis.datacollector.lite.gui.updates;

import android.content.Context;

import java.io.File;

import cz.muni.irtis.datacollector.lite.application.components.TaskServiceBase;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.network.Connection;
import cz.muni.irtis.datacollector.lite.application.network.Network;
import cz.muni.irtis.datacollector.lite.application.network.UrlComposer;
import okhttp3.Response;

/**
 * @Deprecated now resolved by GooglePlay
 */
public class UpdatesService extends TaskServiceBase {
    private Updates updates;

    public UpdatesService(Context context) {
        super(context, UpdatesService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if(Network.isWIFI()) {
            updates = new Updates(getContext());
            if (!updates.isUpdate()) {
                Connection.build(getContext()).getFile(
                        UrlComposer.compose("/update/check"),
                        new Connection.OnConnectionFileListener() {
                            @Override
                            public void onStart() {
                            }

                            @Override
                            public void onError(Exception e) {
                                Debug.getInstance().exception(e, UpdatesService.this, "Unable to download an update");
                                stop(getContext());
                            }

                            @Override
                            public File onCreateFile(String name) {
                                return updates.create(name);
                            }

                            @Override
                            public void onDone(Response response, File file) {
                                if (file != null) {
                                    UpdateNotification.build(getContext()).show();
                                }
                                stop(getContext());
                            }
                        }, true
                );
            } else {
                UpdateNotification.build(getContext()).show();
            }
        }

        UpdatesJob.schedule(getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public static void start(Context context) {
        TaskServiceBase.startService(context, UpdatesService.class);
    }

    public static void stop(Context context) {
        TaskServiceBase.stopService(context, UpdatesService.class);
    }

}
