package cz.muni.irtis.datacollector.lite.persistence;

import android.app.usage.UsageStatsManager;
import android.content.Context;

import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.components.TaskServiceBase;
import cz.muni.irtis.datacollector.lite.application.database.Repository;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.Burst;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.BurstsModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.notifications.QuestionnairesInvocationsSettingNotification;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.components.notifications.QuestionnairesNotification;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.QuestionnairesModel;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Questionnaire;
import cz.muni.irtis.datacollector.lite.metrics.MetricsService;
import cz.muni.irtis.datacollector.lite.metrics.mediaprojection.MediaProjectionReminderNotification;
import cz.muni.irtis.datacollector.lite.metrics.model.util.Storage;
import cz.muni.irtis.datacollector.lite.metrics.synchronization.MetricsSynchronizationService;
import cz.muni.irtis.datacollector.lite.persistence.components.StorageInsufficientNotification;
import cz.muni.irtis.datacollector.lite.persistence.components.SynchronizationRequiredNotification;
import cz.muni.irtis.datacollector.lite.persistence.model.Events;

/**
 * CheckersService
 *
 * This service checks necessary, control, or problematic features to be in effect
 */
public class CheckersService extends TaskServiceBase {
    private QuestionnairesInvocationsSettingNotification questionnairesInvocationsSettingNotification;

    public CheckersService(Context context) {
        super(context, CheckersService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Check if the synchronization is out of date (if the last sync occurred 24 hour ago)
        checkMetricsSynchronization();

        // Check if the screenshots are to be enabled
        checkMetricsScreenshots();

        // Check if invocations setting notification for this burst was offered
        checkBurstInvocationsSettings();

        // Check on storage space available
        checkFreeStorage();

        // checkFreezedNotifications
        checkFreezedNotifications();

        // Check if our application is somehow bucketed
        checkBucketing();

        CheckersJob.schedule(getContext());
        stop(getContext());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * start()
     *
     * Start the service
     *
     * @param context
     */
    public static void start(Context context) {
        TaskServiceBase.startService(context, CheckersService.class);
    }

    /**
     * stop()
     *
     * Stop the service
     *
     * @param context
     */
    public static void stop(Context context) {
        TaskServiceBase.stopService(context, CheckersService.class);
    }


    public void checkMetricsSynchronization() {
        if((Time.getTime() - Settings.getSettings().getLong(Settings.SYNCHRONIZATION_LAST, Time.getTime())) >
                Time.DAY && !MetricsSynchronizationService.isSyncing(getContext())) {
            SynchronizationRequiredNotification.build(getContext()).show();
        }
    }

    public void checkMetricsScreenshots() {
        if(MetricsService.isRunning(getContext())) {
            // Screenshots are turned off
            if(!MetricsService.isMetricRunningScreenshots(getContext()) && !MetricsService.isMetricEnabledScreenshots(getContext())) {
                Integer frameNumber = MetricsService.getFrameNumber(Time.getTime());
                Long disabledTime = Settings.getSettings()
                        .getLong(Settings.MEDIAPROJECTION_PERMISSION_DISABLED_TIME, -1);
                if(disabledTime!=-1 && frameNumber!=null) {
                    String frameIdentifier = Settings.MEDIAPROJECTION_PERMISSION_FRAME_TIME+"_"+Time.getTimeStamp("yyyy-MM-dd")+"-"+frameNumber.toString();
                    boolean isDisableNotification = (Time.getTime() - disabledTime) > Config.METRICS_SCREENSHOTS_REMINDER_PERIOD;
                    boolean isFrameSatisfied = Settings.getSettings().getBoolean(frameIdentifier, false);
                    if (isDisableNotification && !isFrameSatisfied && Time.isDayTime()) {
                        MediaProjectionReminderNotification.build(getContext()).show();
                        Settings.getSettings().edit().putBoolean(frameIdentifier, true).commit();
                    }
                }
            }
        }
    }

    public void checkBurstInvocationsSettings() {
        BurstsModel bursts = BurstsModel.build(getContext());
        List<Burst> items = bursts.getItemsActive();
        if (items != null) {
            for (Burst item : items) {
                String settKey = Settings.QUESTIONNAIRES_INVOCATIONS_SETTING_REMINDER + "_" + item.getIdentifier();
                Boolean isProcessed = Settings.getSettings().getBoolean(settKey, false);
                if (!isProcessed && Time.isDayTime()) {
                    if(questionnairesInvocationsSettingNotification==null) {
                        questionnairesInvocationsSettingNotification = QuestionnairesInvocationsSettingNotification.build(getContext());
                    }
                    if(!questionnairesInvocationsSettingNotification.isShown()) {
                        questionnairesInvocationsSettingNotification.show();
                    }
                }
            }
        }
    }

    public void checkFreeStorage() {
        if(!Storage.isFreeSpace()) {
            if(Storage.isZeroSpace()) {
                MetricsService.stop(getContext());
            }

            // Turn off capturing if running
            if (MetricsService.isMetricRunningScreenshots(getContext())) {
                MetricsService.stopMetricScreenshots(getContext());
            }

            // Show notification
            if(!StorageInsufficientNotification.isShown(getContext())) {
                StorageInsufficientNotification.build(getContext()).show();
                Events.build(getContext()).log(Events.EVENT_STORAGE_INSUFFICIENT_SPACE);
            }

            // Log for debug
            String amount = Storage.getFreeSpace() / (1024 * 1024)+"MB";
            Debug.getInstance().warning(MetricsService.TAG,
                    "Cannot capture | Reason: not enough free space in the storage (value="+amount+")");
        }
    }

    public void checkFreezedNotifications() {
        if(Config.QUESTIONNAIRES_CHECK_FINISHED_ITEMS) {
            QuestionnairesModel questionnaires = QuestionnairesModel.build(getContext());
            questionnaires.getItemsPassed(new Repository.RepositoryListener<List<Questionnaire>>() {
                @Override
                public void onStart() {}
                @Override
                public void onDone(List<Questionnaire> items) {
                    if(items!=null && !items.isEmpty()) {
                        Iterator<Questionnaire> iItems = items.iterator();
                        while(iItems.hasNext()) {
                            Questionnaire item = iItems.next();
                            if(item.isFinished()) {
                                QuestionnairesNotification.cancel(getContext(), item);
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * See more on bucketing here:
     * https://developer.android.com/topic/performance/appstandby
     */
    public void checkBucketing() {
        UsageStatsManager usage = (UsageStatsManager) getContext().getSystemService(Context.USAGE_STATS_SERVICE);
        if(usage!=null) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
                int bucket = usage.getAppStandbyBucket();
                String sBucket = "Unknown";
                switch (bucket) {
                    case 5: // UsageStatsManager.STANDBY_BUCKET_EXEMPTED
                        sBucket = "STANDBY_BUCKET_EXEMPTED";
                        break;

                    case UsageStatsManager.STANDBY_BUCKET_ACTIVE:
                        sBucket = "STANDBY_BUCKET_ACTIVE";
                        break;
                    case UsageStatsManager.STANDBY_BUCKET_WORKING_SET:
                        sBucket = "STANDBY_BUCKET_WORKING_SET";
                        break;
                    case UsageStatsManager.STANDBY_BUCKET_FREQUENT:
                        sBucket = "STANDBY_BUCKET_FREQUENT";
                        break;
                    case UsageStatsManager.STANDBY_BUCKET_RARE:
                        sBucket = "STANDBY_BUCKET_RARE";
                        break;

                    default:
                        sBucket = "STANDBY_BUCKET_UNKNOWN_" + String.valueOf(bucket);
                        break;

                        // Upper versions we cannot meet now due compatibilities
//                    case UsageStatsManager.STANDBY_BUCKET_RESTRICTED:
//                        sBucket = "STANDBY_BUCKET_RESTRICTED";
//                        break;
//                    case UsageStatsManager.STANDBY_BUCKET_NEVER:
//                        sBucket = "STANDBY_BUCKET_NEVER";
//                        break;


                }
                Debug.getInstance().log("Bucketing", sBucket);
            } else {
                Debug.getInstance().log("Bucketing", "Not available");
            }
        }
    }
}
