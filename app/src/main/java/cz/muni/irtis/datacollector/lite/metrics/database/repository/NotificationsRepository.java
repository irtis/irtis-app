package cz.muni.irtis.datacollector.lite.metrics.database.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import cz.muni.irtis.datacollector.lite.metrics.database.entities.BatteryEntity;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.NotificationEntity;

@Dao
public interface NotificationsRepository extends IRepository<NotificationEntity> {
    public static final String TABLE_NAME = "notifications";

    /**
     * Add new entity to database.
     * @param item entity
     */
    @Insert
    long insert(NotificationEntity item);

    @Update
    void update(NotificationEntity item);

    @Query("SELECT * FROM " + TABLE_NAME + " WHERE package = :identifier AND removed IS NULL ORDER BY posted DESC LIMIT 1")
    NotificationEntity getOpenedPosted(String identifier);

    /**
     * Get entity by it's PK datetime
      * @param time java style timestamp (1561544094*1000)
     * @return entity
     */
    @Query("SELECT * FROM " + TABLE_NAME + " WHERE datetime = :time")
    NotificationEntity getByTime(Long time);

    /**
     * {@link IRepository#getPrevious(Long)}
     */
    @Query("SELECT * FROM " + TABLE_NAME + " " +
            "WHERE datetime < :time")
    List<NotificationEntity> getPrevious(Long time);

    /**
     * {@link IRepository#getPrevious(Long, int, int)}
     */
    @Query("SELECT * FROM " + TABLE_NAME + " " +
            "WHERE datetime < :time" + " " +
            "LIMIT :offset, :count")
    List<NotificationEntity> getPrevious(Long time, int offset, int count);

    /**
     * {@link IRepository#deleteById(Long)}
     */
    @Query("DELETE FROM " + TABLE_NAME + " WHERE datetime = :id")
    int deleteById(Long id);

}
