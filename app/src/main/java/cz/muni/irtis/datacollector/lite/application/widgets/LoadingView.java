package cz.muni.irtis.datacollector.lite.application.widgets;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import cz.muni.irtis.datacollector.R;

/**
 * Created by Michal Schejbal on 06.04.2017.
 */
public class LoadingView extends LinearLayout {

	private int bgcolor = Color.WHITE;
	private float opacity = 0.7f;


	public LoadingView(Context context) {
		super(context);
		this.ini();
	}

	public LoadingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.ini();
	}

	public LoadingView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.ini();
	}


	private void ini() {
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.widget_loading, this);
		this.setBackgroundColor(this.bgcolor);
		this.setVisibility(View.GONE);

		ProgressBar progress = (ProgressBar) this.findViewById(R.id.wwlpb);
		progress.getIndeterminateDrawable().setColorFilter(
				getResources().getColor(R.color.colorAccent),
				PorterDuff.Mode.SRC_IN);
	}

	public void show() {
//		if(this.getParent() instanceof RelativeLayout) {
//			RelativeLayout.LayoutParams parent = (RelativeLayout.LayoutParams) ((ActivityBase)this.getParent()).getLayoutParams();
//			this.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, parent.height));
//		} else if(this.getParent() instanceof LinearLayout) {
//			this.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
//		}


//		this.requestLayout();
//		this.invalidate();

		AlphaAnimation animation = new AlphaAnimation(0.0f, this.opacity);
		animation.setDuration(500);
//		animation.setStartOffset(5000);
		animation.setFillAfter(true);
		animation.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				setVisibility(View.VISIBLE);
			}

			@Override
			public void onAnimationEnd(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
		this.startAnimation(animation);

		this.setClickable(true);
	}

	public void hide() {
		AlphaAnimation animation = new AlphaAnimation(this.opacity, 0.0f);
		animation.setDuration(500);
//		animation.setStartOffset(5000);
		animation.setFillAfter(true);
		animation.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				setVisibility(View.GONE);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
		this.startAnimation(animation);

		this.setClickable(false);
	}

	public void toggle() {
		if(getVisibility()== View.VISIBLE) {
			this.hide();
		} else {
			this.show();
		}
	}

	public void setCenterInRelativeLayout() {
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);
		params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
		setLayoutParams(params);
	}

	public void setCenterInLinearLayout() {
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);
		params.gravity = Gravity.CENTER;
		setLayoutParams(params);
	}

	public void setOpacity(float opacity) {
		this.opacity = opacity;
	}

	public void setBackgroundColor(int color) {
		this.bgcolor = color;
		super.setBackgroundColor(this.bgcolor);
	}




}
