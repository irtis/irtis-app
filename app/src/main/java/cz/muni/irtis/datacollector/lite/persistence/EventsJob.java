package cz.muni.irtis.datacollector.lite.persistence;

import android.app.job.JobParameters;
import android.content.Context;

import java.util.List;

import cz.muni.irtis.datacollector.lite.application.Config;
import cz.muni.irtis.datacollector.lite.application.components.JobServiceBase;
import cz.muni.irtis.datacollector.lite.application.debug.DebugLogs;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.Burst;
import cz.muni.irtis.datacollector.lite.gui.bursts.model.BurstsModel;
import cz.muni.irtis.datacollector.lite.persistence.model.Events;

public class EventsJob extends JobServiceBase {
    public static final int JOB_ID = 171256;

    @Override
    public boolean onStartJob(JobParameters params) {
        DebugLogs.build(getApplicationContext()).upload();
        Events.build(getApplicationContext()).upload();
        schedule(getApplicationContext());
        return true;
    }

    public static void schedule(Context context) {
        scheduleWithNetwork(context, EventsJob.class, JOB_ID, Config.DEBUG_REFRESH_PERIOD);
    }

    public static boolean isSet(Context context) {
        return JobServiceBase.isSet(context, JOB_ID);
    }

    public static void cancel(Context context) {
        JobServiceBase.cancel(context, JOB_ID);
    }
}

