package cz.muni.irtis.datacollector.lite.metrics.database.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import cz.muni.irtis.datacollector.lite.metrics.database.repository.ActivitiesRepository;

@Entity(tableName = ActivitiesRepository.TABLE_NAME)
public class ActivityEntity extends EntityBase {
    @ColumnInfo(name = "activity")
    private String activity;
    @ColumnInfo(name = "confidence")
    private Integer confidence;

    public ActivityEntity(Long datetime, String activity, Integer confidence) {
        super(datetime);
        this.activity = activity;
        this.confidence = confidence;
    }

    public String getActivity() {
        return activity;
    }

    public Integer getConfidence() {
        return confidence;
    }
}
