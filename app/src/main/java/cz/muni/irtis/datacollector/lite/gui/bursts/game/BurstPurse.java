package cz.muni.irtis.datacollector.lite.gui.bursts.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BurstPurse {
    private List<BurstCoin> coins;

    private BurstPurse() {
        coins = new ArrayList<>();
    }

    static public BurstPurse build() {
        return new BurstPurse();
    }

    public void put(BurstCoin coin) {
        coins.add(coin);
    }

    public Boolean isCoins() {
        if(coins!=null) {
            return !coins.isEmpty();
        } else {
            return false;
        }
    }

    public Integer getAmount() {
        Integer amount = 0;
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            amount += iCoins.next().getValue();
        }
        return amount;
    }

    public Integer getAmountBronze() {
        Integer amount = 0;
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            BurstCoin coin = iCoins.next();
            if(coin instanceof BurstCoinBronze) {
                amount += coin.getValue();
            }
        }
        return amount;
    }

    public Integer getAmountSilver() {
        Integer amount = 0;
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            BurstCoin coin = iCoins.next();
            if(coin instanceof BurstCoinSilver) {
                amount += coin.getValue();
            }
        }
        return amount;
    }

    public Integer getAmountGold() {
        Integer amount = 0;
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            BurstCoin coin = iCoins.next();
            if(coin instanceof BurstCoinGold) {
                amount += coin.getValue();
            }
        }
        return amount;
    }

    public Integer getCount() {
        return coins.size();
    }

    public Integer getCountBronze() {
        Integer count = 0;
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            BurstCoin coin = iCoins.next();
            if(coin instanceof BurstCoinBronze) {
                count++;
            }
        }
        return count;
    }

    public String getCountBronzeFormatted() {
        return getCountBronze().toString()+"x";
    }

    public Integer getCountSilver() {
        Integer count = 0;
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            BurstCoin coin = iCoins.next();
            if(coin instanceof BurstCoinSilver) {
                count++;
            }
        }
        return count;
    }

    public String getCountSilverFormatted() {
        return getCountSilver().toString()+"x";
    }

    public Integer getCountGold() {
        Integer count = 0;
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            BurstCoin coin = iCoins.next();
            if(coin instanceof BurstCoinGold) {
                count++;
            }
        }
        return count;
    }

    public String getCountGoldFormatted() {
        return getCountGold().toString()+"x";
    }
}
