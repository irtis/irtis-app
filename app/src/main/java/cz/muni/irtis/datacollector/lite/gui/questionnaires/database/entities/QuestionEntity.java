package cz.muni.irtis.datacollector.lite.gui.questionnaires.database.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

import cz.muni.irtis.datacollector.lite.gui.questionnaires.database.repository.QuestionsRepository;

@Entity(
    tableName = QuestionsRepository.TABLE_NAME,
    primaryKeys = {
        "questionnaire_id", "question_id"
    }
)
public class QuestionEntity {
    @NonNull
    @ColumnInfo(name = "questionnaire_id")
    private Long idQuestionnaire;
    @NonNull
    @ColumnInfo(name = "question_id")
    private Long idQuestion;
    @NonNull
    @ColumnInfo(name = "elapsed")
    private Integer elapsed;
    @ColumnInfo(name = "skipped")
    private Boolean skipped;

    public QuestionEntity(@NonNull Long idQuestionnaire, @NonNull Long idQuestion, Integer elapsed, Boolean skipped) {
        this.idQuestionnaire = idQuestionnaire;
        this.idQuestion = idQuestion;
        this.elapsed = elapsed;
        this.skipped = skipped;
    }

    public Long getIdQuestionnaire() {
        return idQuestionnaire;
    }

    public Long getIdQuestion() {
        return idQuestion;
    }

    public Integer getElapsed() {
        return elapsed;
    }

    public Boolean getSkipped() {
        return skipped;
    }
}
