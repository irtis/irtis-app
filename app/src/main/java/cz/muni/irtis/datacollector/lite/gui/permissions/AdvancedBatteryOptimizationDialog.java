package cz.muni.irtis.datacollector.lite.gui.permissions;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;

import androidx.appcompat.app.AlertDialog;

import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.debug.Debug;
import cz.muni.irtis.datacollector.lite.application.permissions.Permission;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionAdvancedBatteryOptimization;
import cz.muni.irtis.datacollector.lite.application.permissions.PermissionAutostart;

/**
 * AdvancedBatteryOptimizationDialog()
 *
 * This is proxy dialog to set up very custom battery optimizations per manufacturer
 */
public class AdvancedBatteryOptimizationDialog extends PermissionDialogBase {

    public AdvancedBatteryOptimizationDialog(Context context, Permission permission)
    {
        super(context, permission, AdvancedBatteryOptimizationDialog.class.getSimpleName());
    }

    @Override
    protected AlertDialog onBuilderCreate(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getContext().getString(R.string.permissions_advanced_battery_optimization))
                .setIcon(R.drawable.ic_battery_60_grey_24dp)
                .setPositiveButton("OK", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent();
                        try {
                            ComponentName component = getSettingsComponentName();
                            if(component!=null) {
                                intent.setComponent(component);
                                List<ResolveInfo> list = getContext().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                                if (list.size() > 0) {
                                    getContext().startActivity(intent);
                                    ((PermissionAdvancedBatteryOptimization) getPermission()).setGranted();
                                    if(getListener()!=null) {
                                        getListener().onDone(true);
                                    }
                                }
                            } else {
                                ((PermissionAdvancedBatteryOptimization) getPermission()).setGranted();
                                if(getListener()!=null) {
                                    getListener().onDone(false);
                                }
                            }
                        } catch (Exception e) {
                            Debug.getInstance().exception(e);
                            ((PermissionAdvancedBatteryOptimization) getPermission()).setGranted();
                            if(getListener()!=null) {
                                getListener().onDone(false);
                            }
                        }
                    }
                });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            builder.setMessage(Html.fromHtml(getContext().getString(R.string.permissions_advanced_battery_optimization_dialog).replace("{appname}", getContext().getString(R.string.app_name)), Html.FROM_HTML_MODE_LEGACY));
        } else {
            builder.setMessage(Html.fromHtml(getContext().getString(R.string.permissions_advanced_battery_optimization_dialog).replace("{appname}", getContext().getString(R.string.app_name))));
        }
        return builder.create();
    }


    private ComponentName getSettingsComponentName() {
        try {
            String manufacturer = Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                return new ComponentName("com.miui.powerkeeper", "com.miui.powerkeeper.ui.HiddenAppsContainerManagementActivity");
            }
        } catch (Exception e) {
            Debug.getInstance().exception(e);
        }

        return null;
    }
}