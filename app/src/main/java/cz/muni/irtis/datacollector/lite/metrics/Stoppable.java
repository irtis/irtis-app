package cz.muni.irtis.datacollector.lite.metrics;

/**
 * Stoppable
 *
 * Allows the metric to be ceased by a standard execution process (MetricsManager)
 */
public interface Stoppable {
    void stop();
}
