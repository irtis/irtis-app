package cz.muni.irtis.datacollector.lite.gui.integrations.model.instagram.entities;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class CommentsEntity {
    private long id;
    @SerializedName("created_time")
    private Long created;
    private String text;
    @SerializedName("from")
    private UserEntity user;

    static public CommentsEntity buildFromJson(String json) {
        return new GsonBuilder().create().fromJson(json, CommentsEntity.class);
    }

    static public List<CommentsEntity> buildListFromJson(String json) {
        return new GsonBuilder().create().fromJson(json, new TypeToken<List<CommentsEntity>>() {}.getType());
    }


    public long getId() {
        return id;
    }

    public Long getCreated() {
        return created;
    }

    public String getText() {
        return text;
    }

    public UserEntity getUser() {
        return user;
    }
}
