package cz.muni.irtis.datacollector.lite.persistence.runtime;

import android.content.Context;
import android.os.AsyncTask;

import cz.muni.irtis.datacollector.lite.application.settings.Settings;
import cz.muni.irtis.datacollector.lite.application.utils.Time;
import cz.muni.irtis.datacollector.lite.metrics.Startable;
import cz.muni.irtis.datacollector.lite.metrics.Stoppable;
import cz.muni.irtis.datacollector.lite.metrics.database.MetricsDatabase;
import cz.muni.irtis.datacollector.lite.metrics.database.entities.RuntimeEntity;

abstract public class RuntimeBase implements Startable, Stoppable {
    public static final int TYPE_APPLICATION = 1;
    public static final int TYPE_CAPTURE = 2;
    public static final int TYPE_CAPTURE_SCREEN = 3;

    private static final String SETTING_RUNTIME_LAST  = "runtime_last";

    private static final int CAUSE_USER  = 100;
    private static final int CAUSE_FALL  = 200;

    GetRuntimeElapsedTask runtimeElapsedTask;


    private Context context;

    public interface GetRuntimeElapsedListener {
        void onDone(String elapsed);
    }


    public RuntimeBase(Context context) {
        this.context = context;
    }

    public void started(int type) {
        RuntimeEntity entity = last(type);
        if(entity!=null && entity.getStopped()==null) {
            save(entity.getId(), type, entity.getStarted(), getLastTime(), CAUSE_FALL);
        }

        save(Time.getTime(), type, Time.getTime());
    }

    public void stopped(int type) {
        RuntimeEntity entity = last(type);
        if(entity!=null) {
            save(entity.getId(), type, entity.getStarted(), Time.getTime(), CAUSE_USER);
        }
    }

    public RuntimeEntity last(int type) {
        RuntimeEntity entity = MetricsDatabase.getDatabase(context).getRuntimeRepository().getLast(type);
        if (entity != null) {
            return entity;
        } else {
            return null;
        }
    }


    protected long save(Long id, Integer type, Long started, Long stopped, Integer cause) {
        MetricsDatabase.getDatabase(context).getRuntimeRepository().store(new RuntimeEntity(
            id,
            started,
            stopped,
            cause,
            type
        ));
        return 0;
    }

    protected long save(Long id, Integer type, Long started) {
        MetricsDatabase.getDatabase(context).getRuntimeRepository().store(new RuntimeEntity(
            id,
            started,
            type
        ));
        return 0;
    }

    public Context getContext() {
        return context;
    }

    public Long getLastTime() {
        return Settings.getSettings().getLong(SETTING_RUNTIME_LAST, -1);
    }

    public void setLastTime() {
        Settings.getSettings().edit().putLong(SETTING_RUNTIME_LAST, Time.getTime()).commit();
    }

    public String getRuntimeElapsed(int type) {
        RuntimeEntity entity = last(type);
        if(entity!=null) {
            return calculateElapsedTime(entity.getStarted());
        } else {
            return calculateElapsedTime(Time.getTime());
        }
    }
    public void getRuntimeElapsed(int type, GetRuntimeElapsedListener listener) {
        if(runtimeElapsedTask!=null) {
            runtimeElapsedTask.cancel(true);
        }
        runtimeElapsedTask = (GetRuntimeElapsedTask) new GetRuntimeElapsedTask(type, listener)
                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class GetRuntimeElapsedTask extends AsyncTask<Void, Void, String> {
        private int type;
        GetRuntimeElapsedListener listener;

        public GetRuntimeElapsedTask(int type, GetRuntimeElapsedListener l) {
            this.type = type;
            listener = l;
        }

        @Override
        protected String doInBackground(Void... folder) {
            return getRuntimeElapsed(type);
        }
        @Override
        protected void onPostExecute(String elapsed) {
            listener.onDone(elapsed);
        }
    }


    static public String calculateElapsedTime(Long start) {
        long tEnd = System.currentTimeMillis();
        long elapsedMilis = tEnd - start;

        int seconds = (int) (elapsedMilis / 1000);
        int minutes = (int) (seconds / 60);
        int hours = (int) (minutes / 60);

        return longify(hours) + ":"
                + longify(minutes % 59) +":"
                + longify(seconds % 59);
    }

    static private String longify(int time) {
        String t = String.valueOf(time);
        return t.length() < 2 ? "0" + t : t;
    }
}
