package cz.muni.irtis.datacollector.lite.persistence.model;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import cz.muni.irtis.datacollector.lite.persistence.database.EventEntity;

public class EventJson {
    private Integer identifier;
    private Long time;

    private EventJson(Integer identifier, Long time) {
        this.identifier = identifier;
        this.time = time;
    }

    static public EventJson build(Integer identifier, Long time) {
        return new EventJson(identifier, time);
    }

    public String toString() {
        return new GsonBuilder().create().toJson(this, new TypeToken<EventJson>(){}.getType());
    }
}
