package cz.muni.irtis.datacollector.lite.application.components;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;


import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.widgets.HtmlViewer;

/**
 * TextActivityBase
 * <p>
 * Author: Michal Schejbal<br>
 * Created: 19.12.2019
 */
abstract public class TextActivityBase extends ActivityBase {
    private HtmlViewer html;

    public interface OnSetContentListener {
        void onStart();
        void onDone();
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.html_content);
    }

    public HtmlViewer getHtmlView() {
        if(html == null) {
            html = findViewById(R.id.html);
        }
        return html;
    }

    public void setContent(String content, OnSetContentListener listener) {
        new SetContentTask(content, listener).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class SetContentTask extends AsyncTask<Void, Void, String> {
        private OnSetContentListener listener;
        private String content;

        public SetContentTask(String content, OnSetContentListener listener) {
            this.content = content;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            listener.onStart();
        }

        @SuppressLint("WrongThread")
        @Override
        protected String doInBackground(Void... nul)  {
            return getHtmlView().injectContent(content, onGetContentTemplate());
        }
        @Override
        protected void onPostExecute(String content) {
            getHtmlView().setHtml(content);
            listener.onDone();
        }
    }

    protected String onGetContentTemplate() {
        return getString(R.string.html);
    }
}
