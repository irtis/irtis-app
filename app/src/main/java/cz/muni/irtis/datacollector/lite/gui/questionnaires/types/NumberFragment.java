package cz.muni.irtis.datacollector.lite.gui.questionnaires.types;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;

public class NumberFragment extends TextFragment {
    public NumberFragment(Context context, Question question, boolean editable) {
        super(context, question, R.layout.activity_questionnaires_form_type_number, editable);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        return view;
    }

}
