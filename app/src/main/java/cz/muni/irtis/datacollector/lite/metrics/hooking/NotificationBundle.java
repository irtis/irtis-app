package cz.muni.irtis.datacollector.lite.metrics.hooking;


import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class NotificationBundle {
    private String identifier;
    private Long posted;
    private Long removed;
    private String title;
    private String text;

    public NotificationBundle(String identifier, Long posted) {
        this.identifier = identifier;
        this.posted = posted;
    }

    public NotificationBundle(String identifier, String title, String text, Long posted) {
        this.identifier = identifier;
        this.title = title;
        this.text = text;
        this.posted = posted;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public Long getPosted() {
        return posted;
    }

    public Long getRemoved() {
        return removed;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setPosted(Long posted) {
        this.posted = posted;
    }

    public void setRemoved(Long removed) {
        this.removed = removed;
    }

    public String toString() {
        return new GsonBuilder().create().toJson(this, new TypeToken<NotificationBundle>() {}.getType());
    }
}
