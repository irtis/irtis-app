package cz.muni.irtis.datacollector.lite.gui.questionnaires.types;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Locale;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.widgets.Box;
import cz.muni.irtis.datacollector.lite.gui.questionnaires.model.items.Question;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class SpeechFragment extends TextFragment {
    private static final int REQ_CODE = 722;

    private ImageView vSpeaker;


    public SpeechFragment(Context context, Question question, boolean editable) {
        super(context, question, R.layout.activity_questionnaires_form_type_speech, editable);
    }

    public SpeechFragment(Context context, Question question, int contentLayoutId, boolean editable) {
        super(context, question, contentLayoutId, editable);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle bundle) {
        View view = super.onCreateView(inflater, container, bundle);

        if(getQuestion().getAnswer().isValue()) {
            vText.setVisibility(View.VISIBLE);
        }

        vSpeaker = view.findViewById(R.id.speaker);
        vSpeaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.questionnaires_form_type_speech_recording_now));
                try {
                    startActivityForResult(intent, REQ_CODE);
                } catch (ActivityNotFoundException a) {
                    Box.ok(getContext(), "", getString(R.string.questionnaires_form_type_speech_error_not_supported));
                    vText.setVisibility(View.VISIBLE);
                }
            }
        });

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE: {
                switch (resultCode) {
                    case RESULT_OK:
                        if(data!=null) {
                            ArrayList result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                            vText.setText(String.valueOf(result.get(0)));
                        } else {
                            Box.ok(getContext(), "", getString(R.string.questionnaires_form_type_speech_error_result_empty));
                        }
                        break;
                    case RESULT_CANCELED:
                        Box.ok(getContext(), "", getString(R.string.questionnaires_form_type_speech_error_result_empty));
                        break;
                }
                vText.setVisibility(View.VISIBLE);
                break;
            }
        }
    }
}
