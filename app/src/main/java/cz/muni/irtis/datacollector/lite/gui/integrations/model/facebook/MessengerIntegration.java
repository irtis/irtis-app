package cz.muni.irtis.datacollector.lite.gui.integrations.model.facebook;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import java.util.List;

import cz.muni.irtis.datacollector.R;
import cz.muni.irtis.datacollector.lite.application.Applications;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.IntegrationBase;
import cz.muni.irtis.datacollector.lite.gui.integrations.model.ServiceBase;

public class MessengerIntegration extends IntegrationBase {
    private FacebookService facebook;

    public MessengerIntegration(Context context, final ServiceBase.OnServiceListener onSignInListener, final ServiceBase.OnServiceListener onSignOutListener) {
        super(context);

        facebook = FacebookServiceBuilder.buildMessengerBusiness(context, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
                onSignInListener.onStart();
            }

            @Override
            public void onError(int code) {
                onSignInListener.onError(code);
            }

            @Override
            public void onFinished(boolean isLogged) {
                onSignInListener.onFinished(isLogged);
            }
        }, new ServiceBase.OnServiceListener() {
            @Override
            public void onStart() {
                onSignOutListener.onStart();
            }

            @Override
            public void onError(int code) {
                onSignOutListener.onError(code);
            }

            @Override
            public void onFinished(boolean isLogged) {
                onSignOutListener.onFinished(false);
            }
        });
    }
    public MessengerIntegration(Context context) {
        super(context);
        facebook = FacebookServiceBuilder.buildMessengerBusiness(context);
    }

    /**
     * isConnected()
     *
     * Check if the connection if established with facebook API
     *
     * @return
     */
    @Override
    public boolean isConnected() {
        return facebook.isConnected();
    }

    /**
     * isApplication()
     *
     * Check if messenger app isActive installed (both standard and lite)
     *
     * @return
     */
    @Override
    public boolean isApplication() {
        return Applications.isApplicationInstalled(getContext(), "com.facebook.orca") ||
                Applications.isApplicationInstalled(getContext(), "com.facebook.mlite");
    }

    @Override
    public String getName() {
        return getContext().getString(R.string.messenger_integration_title);
    }

    @Override
    public Drawable getImage() {
        return getContext().getDrawable(R.drawable.integration_messanger_icon);
    }

    public void connect(Activity activity) {
        facebook.signIn(activity);
    }

    public void disconnect() {
        facebook.signOut();
    }

    public FacebookService getService() {
        return facebook;
    }


    public List<String> getFriends() {
        GraphRequest request = new GraphRequest(
                facebook.getAccessToken(),
                "/me/friends",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        if(response!=null) {
                            // @todo…
                        }
                    }
                }
        );
        request.executeAsync();

        return null;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        facebook.onActivityResult(requestCode, resultCode, data);
    }
}
