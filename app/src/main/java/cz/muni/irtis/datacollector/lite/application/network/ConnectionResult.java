package cz.muni.irtis.datacollector.lite.application.network;

import androidx.annotation.NonNull;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import okhttp3.Response;

public class ConnectionResult {
    private Response response;
    private Object content;

    public ConnectionResult(Response response, Object content) {
        this.response = response;
        this.content = content;
    }

    public static ConnectionResult build(Response response, Object content) {
        return new ConnectionResult(response, content);
    }

    public Response getResponse() {
        return response;
    }

    public Object getContent() {
        return content;
    }

    public boolean isResult() {
        return response!=null && response.isSuccessful();
    }


    public <T> T getContent(TypeToken<T> type) {
        if(content!=null) {
            String c = (String) content;
            return (T) new GsonBuilder().create().fromJson(c, type.getType());
        } else {
            return null;
        }
    }

    public boolean isContent() {
        if(content!=null) {
            String c = (String) content;
            return !c.isEmpty() || !c.equals("null");
        } else {
            return false;
        }
    }
}
