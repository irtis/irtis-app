package cz.muni.irtis.datacollector.lite;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
abstract public class UnitTestBase {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }
}