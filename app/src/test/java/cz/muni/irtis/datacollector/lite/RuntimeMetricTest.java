package cz.muni.irtis.datacollector.lite;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import cz.muni.irtis.datacollector.lite.application.utils.Time;

/**
 */
public class RuntimeMetricTest {

    @Test
    public void compareTimes() {
        List<Long> starters = Arrays.asList(new Long[] {
            1585301125063L,
            1585299479789L,
            1585299159187L,
            1585298942895L,
            1585298492644L,
            1585297715453L,
            1585297514047L,
            1585297449471L,
            1585297168956L,
            1585297087045L,
            1585297060147L,
            1585296868191L,
            1585296761023L,
            1585296552723L,
            1585296282895L,
            1585296266981L,
            1585295455989L
        });
        List<Long> stoppers = Arrays.asList(new Long[] {
            1585301124439L,
            1585299479549L,
            1585299158310L,
            1585298942678L,
            1585298492376L,
            1585297715245L,
            1585297513792L,
            1585297449222L,
            1585297168775L,
            1585297086873L,
            1585297059986L,
            1585296867979L,
            1585296760718L,
            1585296552488L,
            1585296282664L,
            1585296266830L,
            1585295455821L,
        });


        for(int i = 0; i<starters.size(); i++) {
            System.out.println(Time.getTimeStamp(stoppers.get(i))+" - "+Time.getTimeStamp(starters.get(i)));
        }

        Assert.assertTrue(true);
    }
}